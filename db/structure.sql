-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 25 mai 2023 à 21:00
-- Version du serveur : 8.0.27
-- Version de PHP : 8.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `pwe`
--

-- --------------------------------------------------------

--
-- Structure de la table `pw_config`
--

DROP TABLE IF EXISTS `pw_config`;
CREATE TABLE IF NOT EXISTS `pw_config` (
  `context` varchar(100) NOT NULL,
  `cfg` json NOT NULL,
  PRIMARY KEY (`context`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `pw_config`
--

INSERT INTO `pw_config` (`context`, `cfg`) VALUES
('roles', '{\"1\": {\"rules\": false, \"export\": false, \"manage\": false}, \"2\": {\"rules\": false, \"export\": false, \"manage\": false}, \"3\": {\"rules\": false, \"export\": false, \"manage\": false}, \"4\": {\"rules\": false, \"export\": false, \"manage\": false}, \"5\": {\"rules\": true, \"export\": true, \"manage\": true}, \"6\": {\"rules\": false, \"export\": false, \"manage\": false}, \"37\": {\"rules\": false, \"export\": false, \"manage\": false}}'),
('users', '{\"1\": {\"rules\": false, \"export\": false, \"mailto\": false, \"manage\": false}, \"2\": {\"rules\": false, \"export\": true, \"mailto\": false, \"manage\": false}, \"3\": {\"rules\": false, \"export\": true, \"mailto\": false, \"manage\": false}, \"4\": {\"rules\": false, \"export\": true, \"mailto\": false, \"manage\": false}, \"5\": {\"rules\": true, \"export\": true, \"mailto\": true, \"manage\": true}, \"6\": {\"rules\": false, \"export\": true, \"mailto\": true, \"manage\": true}, \"37\": {\"rules\": false, \"export\": true, \"mailto\": true, \"manage\": false}}'),
('formations', '{\"1\": {\"rules\": false, \"access\": true, \"export\": false, \"mailto\": false, \"manage\": false}, \"2\": {\"rules\": false, \"access\": true, \"export\": true, \"mailto\": false, \"manage\": false}, \"3\": {\"rules\": false, \"access\": true, \"export\": true, \"mailto\": false, \"manage\": false}, \"4\": {\"rules\": false, \"access\": true, \"export\": true, \"mailto\": false, \"manage\": false}, \"5\": {\"rules\": false, \"access\": true, \"export\": true, \"mailto\": true, \"manage\": true}, \"6\": {\"rules\": true, \"access\": true, \"export\": true, \"mailto\": true, \"manage\": true}, \"37\": {\"rules\": false, \"access\": true, \"export\": true, \"mailto\": false, \"manage\": false}}'),
('courses', '{\"1\": {\"rules\": false, \"access\": true, \"export\": false, \"mailto\": false, \"manage\": false}, \"2\": {\"rules\": false, \"access\": true, \"export\": true, \"mailto\": false, \"manage\": false}, \"3\": {\"rules\": false, \"access\": true, \"export\": true, \"mailto\": false, \"manage\": false}, \"4\": {\"rules\": false, \"access\": true, \"export\": true, \"mailto\": false, \"manage\": false}, \"5\": {\"rules\": false, \"access\": true, \"export\": true, \"mailto\": true, \"manage\": true}, \"6\": {\"rules\": true, \"access\": true, \"export\": true, \"mailto\": true, \"manage\": true}, \"37\": {\"rules\": false, \"access\": true, \"export\": true, \"mailto\": true, \"manage\": false}}'),
('degrees', '{\"1\": {\"rules\": false, \"export\": false, \"manage\": false}, \"2\": {\"rules\": false, \"export\": false, \"manage\": false}, \"3\": {\"rules\": false, \"export\": false, \"manage\": false}, \"4\": {\"rules\": false, \"export\": false, \"manage\": false}, \"5\": {\"rules\": true, \"export\": true, \"manage\": true}, \"6\": {\"rules\": false, \"export\": true, \"manage\": true}, \"37\": {\"rules\": false, \"export\": true, \"manage\": false}}');

-- --------------------------------------------------------

--
-- Structure de la table `pw_course`
--

DROP TABLE IF EXISTS `pw_course`;
CREATE TABLE IF NOT EXISTS `pw_course` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` varchar(10) NOT NULL,
  `start_on` date NOT NULL,
  `end_on` date NOT NULL,
  `attribs` json NOT NULL,
  `published` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `pw_course`
--

INSERT INTO `pw_course` (`id`, `uid`, `start_on`, `end_on`, `attribs`, `published`) VALUES
(1, 'ELEC', '2023-01-23', '2023-12-31', '{\"image\": {\"imgUrl\": \"course-1.jpg\"}}', 1),
(2, 'ISFR', '2023-01-23', '2023-12-31', '{\"image\": {\"imgUrl\": \"course-2.jpg\"}}', 1),
(3, 'MFRI1', '2023-05-01', '2023-05-31', '{\"image\": {\"imgUrl\": \"course-3.jpg\"}}', 1),
(4, 'MFRI2', '2023-06-01', '2023-06-30', '{\"image\": {\"imgUrl\": \"course-4.jpg\"}}', 1),
(5, 'SOFRI', '2023-05-03', '2023-05-31', '{\"image\": {\"imgUrl\": \"course-5.jpg\"}}', 1),
(6, 'OEEM', '2023-05-01', '2023-05-31', '{\"image\": {\"imgUrl\": \"course-6.jpg\"}}', 1),
(7, 'TCP', '2023-05-01', '2023-05-31', '{\"image\": {\"imgUrl\": \"course-7.jpg\"}}', 1),
(8, 'PEBN', '2023-05-01', '2023-05-31', '{\"image\": {\"imgUrl\": \"course-8.jpg\"}}', 1),
(9, 'PMARK', '2023-05-01', '2023-05-31', '{\"image\": {\"imgUrl\": \"course-9.jpg\"}}', 1),
(10, 'COMB', '2023-05-01', '2023-06-11', '{\"image\": {\"imgUrl\": \"course-10.jpg\"}}', 1),
(11, 'COMA', '2023-05-01', '2023-06-11', '{\"image\": {\"imgUrl\": \"course-11.jpg\"}}', 1),
(12, 'EXFR', '2023-05-01', '2023-06-11', '{\"image\": {\"imgUrl\": \"course-12.png\"}}', 1),
(13, 'LAN1', '2023-05-01', '2023-05-25', '{\"image\": {\"imgUrl\": \"course-13.jpg\"}}', 1),
(14, 'STATS', '2023-05-02', '2023-06-10', '{\"image\": {\"imgUrl\": \"course-14.jpg\"}}', 1),
(15, 'ANA', '2023-05-01', '2023-06-09', '{\"image\": {\"imgUrl\": \"course-15.jpg\"}}', 1);

-- --------------------------------------------------------

--
-- Structure de la table `pw_degree`
--

DROP TABLE IF EXISTS `pw_degree`;
CREATE TABLE IF NOT EXISTS `pw_degree` (
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `pw_degree`
--

INSERT INTO `pw_degree` (`id`) VALUES
(1),
(2),
(3),
(4),
(9),
(10);

-- --------------------------------------------------------

--
-- Structure de la table `pw_formation`
--

DROP TABLE IF EXISTS `pw_formation`;
CREATE TABLE IF NOT EXISTS `pw_formation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` varchar(10) NOT NULL,
  `degree` int NOT NULL,
  `attribs` json NOT NULL,
  `published` tinyint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `pw_formation`
--

INSERT INTO `pw_formation` (`id`, `uid`, `degree`, `attribs`, `published`) VALUES
(1, 'BEBU', 1, '{\"image\": {\"imgUrl\": \"formation-1.jpg\"}}', 1),
(2, 'BACO', 1, '{\"image\": {\"imgUrl\": \"formation-2.jpg\"}}', 1),
(3, 'BCONS', 1, '{\"image\": {\"imgUrl\": \"formation-3.jpg\"}}', 1),
(4, 'BEME', 1, '{\"image\": {\"imgUrl\": \"formation-4.jpg\"}}', 1),
(5, 'BINF', 1, '{\"image\": {\"imgUrl\": \"formation-5.png\"}}', 1),
(6, 'CAP', 4, '{\"image\": {\"imgUrl\": \"formation-6.jpg\"}}', 1),
(7, 'MFRI', 3, '{\"image\": {\"imgUrl\": \"formation-7.jpg\"}}', 1);

-- --------------------------------------------------------

--
-- Structure de la table `pw_formation_course`
--

DROP TABLE IF EXISTS `pw_formation_course`;
CREATE TABLE IF NOT EXISTS `pw_formation_course` (
  `fid` int NOT NULL,
  `cid` int NOT NULL,
  UNIQUE KEY `fid` (`fid`,`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `pw_formation_course`
--

INSERT INTO `pw_formation_course` (`fid`, `cid`) VALUES
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(2, 10),
(2, 11),
(3, 1),
(3, 6),
(4, 1),
(4, 6),
(4, 7),
(5, 8),
(5, 13),
(5, 14),
(5, 15),
(6, 12),
(7, 1),
(7, 2),
(7, 3),
(7, 4),
(7, 5);

-- --------------------------------------------------------

--
-- Structure de la table `pw_formation_user`
--

DROP TABLE IF EXISTS `pw_formation_user`;
CREATE TABLE IF NOT EXISTS `pw_formation_user` (
  `fid` int NOT NULL,
  `uid` int NOT NULL,
  `published` tinyint NOT NULL DEFAULT '0',
  UNIQUE KEY `fid` (`fid`,`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `pw_formation_user`
--

INSERT INTO `pw_formation_user` (`fid`, `uid`, `published`) VALUES
(5, 101, 1),
(7, 101, 0);

-- --------------------------------------------------------

--
-- Structure de la table `pw_role`
--

DROP TABLE IF EXISTS `pw_role`;
CREATE TABLE IF NOT EXISTS `pw_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `context` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `enrolable` tinyint(1) NOT NULL DEFAULT '1',
  `teachers` tinyint(1) NOT NULL DEFAULT '0',
  `students` tinyint(1) NOT NULL DEFAULT '0',
  `isGuest` tinyint(1) NOT NULL DEFAULT '0',
  `isLoggedIn` tinyint(1) NOT NULL DEFAULT '1',
  `protected` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `pw_role`
--

INSERT INTO `pw_role` (`id`, `context`, `enrolable`, `teachers`, `students`, `isGuest`, `isLoggedIn`, `protected`) VALUES
(1, NULL, 0, 0, 0, 1, 0, 1),
(2, NULL, 0, 0, 0, 0, 1, 1),
(3, NULL, 1, 1, 0, 0, 1, 1),
(4, NULL, 0, 0, 1, 0, 1, 1),
(5, NULL, 1, 0, 0, 0, 1, 1),
(6, NULL, 1, 0, 0, 0, 1, 0),
(7, 'course.1.students', 1, 0, 0, 0, 1, 0),
(8, 'course.1.teachers', 1, 0, 0, 0, 1, 0),
(9, 'course.2.students', 1, 0, 0, 0, 1, 0),
(10, 'course.2.teachers', 1, 0, 0, 0, 1, 0),
(11, 'course.3.students', 1, 0, 0, 0, 1, 0),
(12, 'course.3.teachers', 1, 0, 0, 0, 1, 0),
(13, 'course.4.students', 1, 0, 0, 0, 1, 0),
(14, 'course.4.teachers', 1, 0, 0, 0, 1, 0),
(15, 'course.5.students', 1, 0, 0, 0, 1, 0),
(16, 'course.5.teachers', 1, 0, 0, 0, 1, 0),
(17, 'course.6.students', 1, 0, 0, 0, 1, 0),
(18, 'course.6.teachers', 1, 0, 0, 0, 1, 0),
(19, 'course.7.students', 1, 0, 0, 0, 1, 0),
(20, 'course.7.teachers', 1, 0, 0, 0, 1, 0),
(21, 'course.8.students', 1, 0, 0, 0, 1, 0),
(22, 'course.8.teachers', 1, 0, 0, 0, 1, 0),
(23, 'course.9.students', 1, 0, 0, 0, 1, 0),
(24, 'course.9.teachers', 1, 0, 0, 0, 1, 0),
(25, 'course.10.students', 1, 0, 0, 0, 1, 0),
(26, 'course.10.teachers', 1, 0, 0, 0, 1, 0),
(27, 'course.11.students', 1, 0, 0, 0, 1, 0),
(28, 'course.11.teachers', 1, 0, 0, 0, 1, 0),
(29, 'course.12.students', 1, 0, 0, 0, 1, 0),
(30, 'course.12.teachers', 1, 0, 0, 0, 1, 0),
(31, 'course.13.students', 1, 0, 0, 0, 1, 0),
(32, 'course.13.teachers', 1, 0, 0, 0, 1, 0),
(33, 'course.14.students', 1, 0, 0, 0, 1, 0),
(34, 'course.14.teachers', 1, 0, 0, 0, 1, 0),
(35, 'course.15.students', 1, 0, 0, 0, 1, 0),
(36, 'course.15.teachers', 1, 0, 0, 0, 1, 0),
(37, NULL, 1, 0, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `pw_role_user`
--

DROP TABLE IF EXISTS `pw_role_user`;
CREATE TABLE IF NOT EXISTS `pw_role_user` (
  `roleid` int NOT NULL,
  `userid` int NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  UNIQUE KEY `roleid` (`roleid`,`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `pw_role_user`
--

INSERT INTO `pw_role_user` (`roleid`, `userid`, `published`) VALUES
(16, 80, 1),
(14, 80, 1),
(12, 80, 1),
(10, 80, 1),
(26, 90, 1),
(28, 90, 1),
(29, 75, 1),
(18, 91, 1),
(20, 91, 1),
(22, 91, 1),
(24, 91, 1),
(36, 93, 1),
(34, 93, 1),
(32, 93, 1),
(37, 94, 1),
(35, 101, 1),
(33, 101, 1),
(31, 101, 1),
(21, 101, 1),
(17, 101, 0);

-- --------------------------------------------------------

--
-- Structure de la table `pw_session`
--

DROP TABLE IF EXISTS `pw_session`;
CREATE TABLE IF NOT EXISTS `pw_session` (
  `sessionid` varchar(255) NOT NULL,
  `userid` int NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `pw_session`
--

INSERT INTO `pw_session` (`sessionid`, `userid`, `time`) VALUES
('1986180160646fa63cf1e684.04084892', 74, '2023-05-25 20:28:48'),
('215314354646b698a70fbe2.90797334', 92, '2023-05-22 13:34:23'),
('1702362020646eceb20817e6.11679637', 101, '2023-05-25 09:27:13');

-- --------------------------------------------------------

--
-- Structure de la table `pw_translate`
--

DROP TABLE IF EXISTS `pw_translate`;
CREATE TABLE IF NOT EXISTS `pw_translate` (
  `id` int NOT NULL AUTO_INCREMENT,
  `context` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `content` mediumtext NOT NULL,
  `lang` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `pw_translate`
--

INSERT INTO `pw_translate` (`id`, `context`, `content`, `lang`) VALUES
(1, 'role.1.title', 'Invités', 'fr-FR'),
(2, 'role.1.description', 'Utilisateurs non identifiés.', 'fr-FR'),
(3, 'role.2.title', 'Membres', 'fr-FR'),
(4, 'role.2.description', 'Utilisateurs identifiés.', 'fr-FR'),
(5, 'role.3.title', 'Enseignants', 'fr-FR'),
(6, 'role.3.description', 'Enseignants (actifs ou non).', 'fr-FR'),
(7, 'role.4.title', 'Etudiants', 'fr-FR'),
(8, 'role.4.description', 'Etudiants actifs.', 'fr-FR'),
(9, 'role.5.title', 'Direction', 'fr-FR'),
(10, 'role.5.description', 'Direction de l\'établissement.', 'fr-FR'),
(11, 'role.6.title', 'Secrétariat', 'fr-FR'),
(12, 'role.6.description', 'Personnel du secrétariat de l\'établissement.', 'fr-FR'),
(13, 'role.1.title', 'Guests', 'en-GB'),
(14, 'role.1.description', 'Unidentified users.', 'en-GB'),
(15, 'role.2.title', 'Members', 'en-GB'),
(16, 'role.2.description', 'Identified users.', 'en-GB'),
(17, 'role.3.title', 'Teachers', 'en-GB'),
(18, 'role.3.description', 'Teachers (active or not).', 'en-GB'),
(19, 'role.4.title', 'Students', 'en-GB'),
(20, 'role.4.description', 'Active students.', 'en-GB'),
(21, 'role.5.title', 'Management ', 'en-GB'),
(22, 'role.5.description', 'Management of the establishment.', 'en-GB'),
(23, 'role.6.title', 'Office', 'en-GB'),
(24, 'role.6.description', 'Secretariat staff of the establishment.', 'en-GB'),
(25, 'degree.1.title', 'Bachelor', 'en-GB'),
(26, 'degree.1.title', 'Bachelier', 'fr-FR'),
(27, 'degree.1.description', 'Bachelor Degree', 'en-GB'),
(28, 'degree.1.description', 'Diplôme de niveau Bachelier.', 'fr-FR'),
(29, 'degree.2.title', 'DI', 'en-GB'),
(30, 'degree.2.title', 'DI', 'fr-FR'),
(31, 'degree.2.description', 'Lower Secondary School Certificate', 'en-GB'),
(32, 'degree.2.description', 'Certificat d\'enseignement secondaire inférieur.', 'fr-FR'),
(33, 'degree.3.title', 'DS', 'en-GB'),
(34, 'degree.3.title', 'DS', 'fr-FR'),
(35, 'degree.3.description', 'Higher Secondary Education Certificate.', 'en-GB'),
(36, 'degree.3.description', 'Certificat d\'enseignement secondaire supérieur.', 'fr-FR'),
(37, 'degree.4.title', 'Certificate ', 'en-GB'),
(38, 'degree.4.title', 'Certificat', 'fr-FR'),
(39, 'degree.4.description', 'Certificate of Achievement.', 'en-GB'),
(40, 'degree.4.description', 'Certificat de réussite.', 'fr-FR'),
(59, 'formation.1.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(58, 'formation.1.title', 'Bachelier en E-Business', 'fr-FR'),
(57, 'formation.1.title', 'Bachelor in E-Business', 'en-GB'),
(60, 'formation.1.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(61, 'formation.1.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'en-GB'),
(62, 'formation.1.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'fr-FR'),
(63, 'formation.2.title', 'Bachelor of Accounting', 'en-GB'),
(64, 'formation.2.title', 'Bachelier en comptabilité', 'fr-FR'),
(65, 'formation.2.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(66, 'formation.2.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(67, 'formation.2.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>', 'en-GB'),
(68, 'formation.2.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>', 'fr-FR'),
(69, 'formation.3.title', 'Bachelor in Construction', 'en-GB'),
(70, 'formation.3.title', 'Bachelier en construction', 'fr-FR'),
(71, 'formation.3.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(72, 'formation.3.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(73, 'formation.3.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>', 'en-GB'),
(74, 'formation.3.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>', 'fr-FR'),
(75, 'formation.4.title', 'Bachelor in electromechanics', 'en-GB'),
(76, 'formation.4.title', 'Bachelier en electromécanique', 'fr-FR'),
(77, 'formation.4.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(78, 'formation.4.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(79, 'formation.4.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>', 'en-GB'),
(80, 'formation.4.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>', 'fr-FR'),
(81, 'formation.5.title', 'Bachelor\'s degree in computer science.', 'en-GB'),
(82, 'formation.5.title', 'Bachelier en informatique de gestion.', 'fr-FR'),
(83, 'formation.5.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(84, 'formation.5.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(85, 'formation.5.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>', 'en-GB'),
(86, 'formation.5.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>', 'fr-FR'),
(87, 'formation.6.title', 'Certificate of Teaching Ability', 'en-GB'),
(88, 'formation.6.title', 'Certificat d\'aptitudes pédagogiques', 'fr-FR'),
(89, 'formation.6.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(90, 'formation.6.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(91, 'formation.6.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>', 'en-GB'),
(92, 'formation.6.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>', 'fr-FR'),
(93, 'formation.7.title', 'Refrigeration technician', 'en-GB'),
(94, 'formation.7.title', 'Monteur frigoriste', 'fr-FR'),
(95, 'formation.7.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(96, 'formation.7.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(97, 'formation.7.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>', 'en-GB'),
(98, 'formation.7.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>', 'fr-FR'),
(99, 'course.1.title', 'Electricity and Applied Electrical Engineering', 'en-GB'),
(100, 'course.1.title', 'Électricité et Électrotechnique appliquée', 'fr-FR'),
(101, 'course.1.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(102, 'course.1.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(103, 'course.1.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam</p>', 'en-GB'),
(104, 'course.1.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam</p>', 'fr-FR'),
(105, 'course.2.title', 'Refrigeration system: basic principles', 'en-GB'),
(106, 'course.2.title', 'Installation frigorifique : principes de base', 'fr-FR'),
(107, 'course.2.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(108, 'course.2.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(109, 'course.2.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'en-GB'),
(110, 'course.2.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'fr-FR'),
(111, 'course.3.title', 'Refrigeration fitter : level 1', 'en-GB'),
(112, 'course.3.title', 'Monteur-frigoriste : niveau 1', 'fr-FR'),
(113, 'course.3.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(114, 'course.3.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(115, 'course.3.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'en-GB'),
(116, 'course.3.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'fr-FR'),
(117, 'course.4.title', 'Refrigeration fitter : level 2', 'en-GB'),
(118, 'course.4.title', 'Monteur-frigoriste : niveau 2', 'fr-FR'),
(119, 'course.4.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(120, 'course.4.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(121, 'course.4.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'en-GB'),
(122, 'course.4.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'fr-FR'),
(123, 'course.5.title', 'Welding for refrigeration systems', 'en-GB'),
(124, 'course.5.title', 'Soudure pour installation frigorifique', 'fr-FR'),
(125, 'course.5.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(126, 'course.5.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(127, 'course.5.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'en-GB'),
(128, 'course.5.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'fr-FR'),
(129, 'course.6.title', 'Organization of companies and elements of of management', 'en-GB'),
(130, 'course.6.title', 'Organisation des entreprises et éléments de management', 'fr-FR'),
(131, 'course.6.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(132, 'course.6.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(133, 'course.6.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'en-GB'),
(134, 'course.6.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'fr-FR'),
(135, 'course.7.title', 'Techniques of communication professional', 'en-GB'),
(136, 'course.7.title', 'Techniques de communication professionnelle', 'fr-FR'),
(137, 'course.7.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(138, 'course.7.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(139, 'course.7.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'en-GB'),
(140, 'course.7.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'fr-FR'),
(141, 'course.8.title', 'Principles of Ebusiness', 'en-GB'),
(142, 'course.8.title', 'Principes de l’Ebusiness', 'fr-FR'),
(143, 'course.8.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(144, 'course.8.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(145, 'course.8.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'en-GB'),
(146, 'course.8.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'fr-FR'),
(147, 'course.9.title', 'Basic principles of marketing', 'en-GB'),
(148, 'course.9.title', 'Principes de base du marketing', 'fr-FR'),
(149, 'course.9.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(150, 'course.9.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(151, 'course.9.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'en-GB'),
(152, 'course.9.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'fr-FR'),
(153, 'course.10.title', 'Accounting, basic principles', 'en-GB'),
(154, 'course.10.title', 'Comptabilité, principes de base', 'fr-FR'),
(155, 'course.10.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(156, 'course.10.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(157, 'course.10.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'en-GB');
INSERT INTO `pw_translate` (`id`, `context`, `content`, `lang`) VALUES
(158, 'course.10.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'fr-FR'),
(159, 'course.11.title', 'Advanced accounting', 'en-GB'),
(160, 'course.11.title', 'Comptabilité avancée', 'fr-FR'),
(161, 'course.11.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(162, 'course.11.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(163, 'course.11.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'en-GB'),
(164, 'course.11.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'fr-FR'),
(165, 'course.12.title', 'Oral and written expression in French for teaching purposes', 'en-GB'),
(166, 'course.12.title', 'Expression orale et écrite en français orientée vers l’enseignement', 'fr-FR'),
(167, 'course.12.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(168, 'course.12.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(169, 'course.12.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam</p>', 'en-GB'),
(170, 'course.12.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam</p>', 'fr-FR'),
(171, 'course.13.title', 'Administration, management and security of networks', 'en-GB'),
(172, 'course.13.title', 'Administration, gestion et sécurisation des réseaux', 'fr-FR'),
(173, 'course.13.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(174, 'course.13.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(175, 'course.13.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'en-GB'),
(176, 'course.13.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'fr-FR'),
(177, 'course.14.title', 'Elements of statistics', 'en-GB'),
(178, 'course.14.title', 'Éléments de statistique', 'fr-FR'),
(179, 'course.14.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(180, 'course.14.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(181, 'course.14.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'en-GB'),
(182, 'course.14.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'fr-FR'),
(183, 'course.15.title', 'Principles of computer analysis', 'en-GB'),
(184, 'course.15.title', 'Principes d’analyse informatique', 'fr-FR'),
(185, 'course.15.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'en-GB'),
(186, 'course.15.description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac libero elit. Aliquam a orci eget nisi egestas scelerisque vitae id dolor. Sed luctus vel quam vel facilisis. Donec at odio sed erat pretium vulputate sit amet non neque. Duis aliquam velit at elit gravida, eu varius urna accumsan. Maecenas commodo bibendum lorem sed euismod. Phasellus ac lectus vel neque gravida tristique. Vestibulum in enim pellentesque, imperdiet orci vitae, rutrum turpis.', 'fr-FR'),
(187, 'course.15.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'en-GB'),
(188, 'course.15.content', '<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>\r\n<p class=\"alert alert-info\" role=\"alert\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Nunc eu mi nec nulla mattis tristique id a ipsum.</li>\r\n</ul>\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique, mauris ut dapibus luctus, nisl felis vulputate nisi, ut sodales diam mauris a metus. Nulla facilisi. Vestibulum urna leo, dapibus vel dolor ut, aliquet hendrerit urna. Donec sed diam lacus. Donec a orci id nisi euismod consectetur quis vel dolor.\r\n</p>', 'fr-FR'),
(189, 'role.37.title', 'Accounting department', 'en-GB'),
(190, 'role.37.title', 'Service comptabilité', 'fr-FR'),
(191, 'role.37.description', 'Service de la comptabilité.', 'en-GB'),
(192, 'role.37.description', 'Accounting department.', 'fr-FR'),
(193, 'degree.9.title', 'Master', 'en-GB'),
(194, 'degree.9.title', 'Master', 'fr-FR'),
(195, 'degree.9.description', 'Master degree.', 'en-GB'),
(196, 'degree.9.description', 'Niveau master.', 'fr-FR'),
(197, 'degree.10.title', 'BES', 'en-GB'),
(198, 'degree.10.title', 'BES', 'fr-FR'),
(199, 'degree.10.description', 'Higher education diploma.', 'en-GB'),
(200, 'degree.10.description', 'Brevet de l\'enseignement supérieur.', 'fr-FR');

-- --------------------------------------------------------

--
-- Structure de la table `pw_user`
--

DROP TABLE IF EXISTS `pw_user`;
CREATE TABLE IF NOT EXISTS `pw_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `lang` varchar(8) NOT NULL,
  `attribs` json NOT NULL,
  `access` int NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `resetpwd` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `pw_user`
--

INSERT INTO `pw_user` (`id`, `username`, `email`, `firstname`, `lastname`, `password`, `lang`, `attribs`, `access`, `created_on`, `resetpwd`, `published`) VALUES
(75, 'polikrol', 'test@test.be', 'Pol2', 'Kikrolll', '$2y$10$NprHzrOHD1thGG2.xf6YJO4QPPWItOZBUZoqnOKOCjMCcE1LYizUi', 'en-GB', '{\"social\": {\"avatar\": \"\"}, \"address\": {\"zip\": \"5000\", \"city\": \"Namur\", \"number\": \"44\", \"street\": \"Rue Godefroid\", \"country\": \"BE\"}, \"contact\": {\"phone\": \"0456144959\"}, \"identity\": {\"birthday\": \"1979-02-01\"}}', 3, '2022-12-04 10:58:07', 0, 1),
(74, 'admin', 'christian.savels@gmail.com', 'Christian', 'Savels', '$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', 'en-GB', '{\"social\": {\"avatar\": \"avatar-74.jpg\"}, \"address\": {\"zip\": \"5020\", \"city\": \"Vedrin\", \"number\": \"24\", \"street\": \"rue des broux\", \"country\": \"BE\"}, \"contact\": {\"phone\": \"+32456144959\"}, \"identity\": {\"birthday\": \"1979-02-01\"}}', 5, '2022-12-04 04:29:56', 1, 1),
(76, 'jef', 'test2@test.be', 'Jean-François', 'VAN ASS', '$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', 'fr-FR', '{\"social\": {\"avatar\": \"\"}, \"address\": {\"zip\": \"5000\", \"city\": \"Namur\", \"number\": \"44\", \"street\": \"Rue Godefroid\", \"country\": \"BE\"}, \"contact\": {\"phone\": \"0456144959\"}, \"identity\": {\"birthday\": \"1969-12-19\"}}', 5, '2022-12-04 10:58:07', 0, 1),
(80, 'karlita', 'dqzdqzdqz@dqzdqz.com', 'Karl', 'Duquenne', '$2y$10$NprHzrOHD1thGG2.xf6YJO4QPPWItOZBUZoqnOKOCjMCcE1LYizUi$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', 'fr-FR', '{\"social\": {\"avatar\": \"avatar-74.jpg\"}, \"address\": {\"zip\": \"5000\", \"city\": \"Namur\", \"number\": \"44\", \"street\": \"Rue Godefroid\", \"country\": \"BE\"}, \"contact\": {\"phone\": \"0456144959\"}, \"identity\": {\"birthday\": \"1979-02-01\"}}', 1, '2022-12-30 14:43:00', 0, 1),
(93, 'domio', 'test22@test.be', 'Dominique', 'Servais', '$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', '2', '{\"social\": {\"avatar\": \"\"}, \"address\": {\"zip\": \"5000\", \"city\": \"Namur\", \"number\": \"44\", \"street\": \"Rue Godefroid\", \"country\": \"BE\"}, \"contact\": {\"phone\": \"0456144959\"}, \"identity\": {\"birthday\": \"1872-06-09\"}}', 3, '2022-12-04 10:58:07', 0, 0),
(88, 'ab', 'gg@gg.com', 'Alphonse', 'Brown', '$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', '', '{\"social\": {\"avatar\": null}, \"address\": {\"zip\": null, \"city\": null, \"number\": null, \"street\": null, \"country\": null}, \"contact\": {\"phone\": null}, \"identity\": {\"birthday\": null}}', 1, '2022-12-30 15:59:57', 0, 1),
(90, 'carlito', 'aze@dazdaz.d', 'Carl', 'Cox', '$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', '', '{\"social\": {\"avatar\": null}, \"address\": {\"zip\": null, \"city\": null, \"number\": null, \"street\": null, \"country\": null}, \"contact\": {\"phone\": null}, \"identity\": {\"birthday\": null}}', 1, '2022-12-30 16:12:16', 0, 1),
(91, 'vlad', 'online@savels.infod', 'Vladimir', 'Costa', '$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', '', '{\"social\": {\"avatar\": null}, \"address\": {\"zip\": null, \"city\": null, \"number\": null, \"street\": null, \"country\": null}, \"contact\": {\"phone\": null}, \"identity\": {\"birthday\": null}}', 1, '2022-12-30 16:13:39', 0, -1),
(92, 'bita', 'pld@dz.com', 'Bela', 'Vita', '$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', '', '{\"social\": {\"avatar\": null}, \"address\": {\"zip\": null, \"city\": null, \"number\": null, \"street\": null, \"country\": null}, \"contact\": {\"phone\": null}, \"identity\": {\"birthday\": null}}', 1, '2022-12-30 16:16:38', 0, 1),
(94, 'accounting', 'accounting@gg.com', 'Sara', 'Kroch', '$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', '', '{\"social\": {\"avatar\": null}, \"address\": {\"zip\": null, \"city\": null, \"number\": null, \"street\": null, \"country\": null}, \"contact\": {\"phone\": null}, \"identity\": {\"birthday\": null}}', 1, '2022-12-30 15:59:57', 0, 1),
(96, 'student1', 'student1@gg.com', 'François', 'Guisset', '$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', '', '{\"social\": {\"avatar\": null}, \"address\": {\"zip\": null, \"city\": null, \"number\": null, \"street\": null, \"country\": null}, \"contact\": {\"phone\": null}, \"identity\": {\"birthday\": null}}', 1, '2022-12-30 15:59:57', 0, 1),
(97, 'student2', 'student2@gg.com', 'Adeline', 'Guisset', '$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', '', '{\"social\": {\"avatar\": null}, \"address\": {\"zip\": null, \"city\": null, \"number\": null, \"street\": null, \"country\": null}, \"contact\": {\"phone\": null}, \"identity\": {\"birthday\": null}}', 1, '2022-12-30 15:59:57', 0, 1),
(98, 'student3', 'student3@gg.com', 'Marko', 'Pasalic', '$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', '', '{\"social\": {\"avatar\": null}, \"address\": {\"zip\": null, \"city\": null, \"number\": null, \"street\": null, \"country\": null}, \"contact\": {\"phone\": null}, \"identity\": {\"birthday\": null}}', 1, '2022-12-30 15:59:57', 0, 1),
(99, 'student4', 'student4@gg.com', 'Denis', 'Delvaux', '$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', '', '{\"social\": {\"avatar\": null}, \"address\": {\"zip\": null, \"city\": null, \"number\": null, \"street\": null, \"country\": null}, \"contact\": {\"phone\": null}, \"identity\": {\"birthday\": null}}', 1, '2022-12-30 15:59:57', 0, 1),
(100, 'student5', 'student5@gg.com', 'Damien', 'Delvaux', '$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', '', '{\"social\": {\"avatar\": null}, \"address\": {\"zip\": null, \"city\": null, \"number\": null, \"street\": null, \"country\": null}, \"contact\": {\"phone\": null}, \"identity\": {\"birthday\": null}}', 1, '2022-12-30 15:59:57', 0, 1),
(101, 'student6', 'student6@gg.com', 'Lyam', 'Bernard', '$2y$10$XGI/iawP.BRPDfsTMRQNcO1I.KqCqWnlLv9XkOrMJoglaupqEtR.q', 'en-GB', '{\"social\": {\"avatar\": \"avatar-101.png\"}, \"address\": {\"zip\": \"\", \"city\": \"\", \"number\": \"\", \"street\": \"\", \"country\": \"\"}, \"contact\": {\"phone\": \"\"}, \"identity\": {\"birthday\": \"\"}}', 1, '2022-12-30 15:59:57', 0, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
