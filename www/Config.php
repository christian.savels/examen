<?php
defined('_PWE') or die("Limited acces");

class Config
{
    // Site
    //public const SITE_ACTIVE = true;
    // public const MAIL_FROM = "christian.savels@gmail.com";
    //public const SITE_REGISTER_ACTIVE = true;
    //public const SITE_NAME = "Web Project - Christian Savels";
    //public const PWD_LEN_MIN = 5; // (cisco partout)
    //public const DATES_FORMAT = "d-m-Y H:i";
    
    // Database 
    //public const DB_TYPE = "mysqli";
    //public const DB_HOST = "localhost";
    //public const DB_USER = "root";
    //public const DB_NAME = "pwe";
    //public const DB_PWD = "";
    
    // Sessions
    //public const SESSION_NAME = "PWE";
    //public const SESSION_LIMIT = 300; // minutes
    
    // Multilanguage
    //public const LANG_MULTI = true;
    //public const LANG_DEFAULT = "fr-FR";

    
    // Logs
    //public const LOG_SEPARATOR = ";";
    //public const LOG_DB_ERRORS = true;
   // public const LOG_DB_FILE = __DIR__ . DS . "logs" . DS . "db.errors.txt";
    
    // Lists & Pagination
    //public const LISTS_TITLE_LEN = 0;
   // public const LISTS_DESCRIPTION_LEN = 128;
    //public const LISTS_EMAIL_LEN = 10;
    //public const PAG_LIMIT_DEFAULT = 3;
    //public const PAG_LIMIT_MAX = 20;
    
    // Assets
    //public const BOOTSTRAP = true;
    
    // Redirect 
    //public const REDIRECT_DELAY = 5000; // ms
    
    // Notify
    //public const NOTIIFY_DELAY = 5000; // ms
    
    // Forms
    //public const FORM_PATH = __DIR__ . DS . "form" . DS;
    //public const FORM_RULES_PATH = __DIR__ . DS . "form" . DS . "rules" . DS;
    //public const FORM_IMG_MIME = ['image/gif', 'image/jpeg', 'image/png'];
    //public const FORM_IMG_MAX_SIZE = 1000000;

    // Courses
    //public const COURSES_DEFAULT_IMG = "assets/img/course.png";

    // Formations
    //public const FORMATIONS_DEFAULT_IMG = "assets/img/formation.jpg";
    
    // Common paths
    //public const PATH_AVATAR = __DIR__ . DS . "media" . DS . "users" . DS . "avatar" . DS;
    //public const PATH_IMG_FORMATIONS = __DIR__ . DS . "media" . DS . "formations" . DS;
    //public const PATH_IMG_COURSES = __DIR__ . DS . "media" . DS . "courses" . DS;
    //public const PATH_JSON = __DIR__ . DS . "assets" . DS . "json" . DS;
  
    // Modal 
    //public const MODAL_ID = "PWExamModal";
    
    // users 
    //public const DEFAULT_AVATAR = "assets/img/avatar.png";
}
?>