<?php
/*
 * @note Ce fichier doit rester à la racine pour initialiser le dossier root.
 */
defined('_PWE') or die("Limited acces");
define('DS', DIRECTORY_SEPARATOR);

require_once __DIR__ . DS . 'autoload.php';

// Init factory (scope)
$factory = \app\Helpers\Factory::getInstance(__DIR__);

// init session
require_once __DIR__ . DS . 'session.php';
?>