<?php
defined('_PWE') or die("Limited acces");

use \app\Helpers\SessionHelper as Sh;

if ( $_POST || \app\Helpers\Helper::getValue("format", "html")=='json' ) {
    Sh::makeJSONSession();
} 
else {
    Sh::makeHTMLSession();
}


if ( Sh::isConnected() )
{ 
    // Set current user on factory.
    if ( !$factory->getUser()->id && $_SESSION["userid"] ) 
    {
        $u = \app\Helpers\UserHelper::getUser($_SESSION["userid"]);
        if ( !is_null($u) ) {
            $factory->setUser(new \app\Objects\User($u));
        }
    } 
    else {
        Sh::extendSession();
    }
}