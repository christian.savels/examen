<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Helper related to editable params.
 * Do not confuse with the "configuration.ini" file 
 * that contains parameters of the site.
 * 
 * @author chris
 *
 */
class ConfigHelper
{
    private string $context;
    private object $params;
    
    
    public function __construct(string $context=null) {
        $this->init($context);
    }
    
   
   /**
    * Init object.
    * 
    * @param string $context
    */
        private function init(string $context=null) 
        {
            $this->initContext($context);
            $this->params = $this->getFromDb();
        }
        
    
    /**
     * Get configuration of the current context.
     * 
     * @param string $param
     * @param mixed $default
     * @return object
     */
        public function getParam( string $param, $default=null) : object
        {
            if ( isset($this->params->{$param}) ) {
                return $this->params->{$param};
            }
            
            return $default;
        }
        
        
    /**
     * Init the context.
     *
     * @param string $context
     */
        private function initContext(string $context=null) : void
        {
            if ( !is_null($context) ) {
                $this->context = $context;
            }
            else
            {
                $view = explode("/", Helper::getValue("view", "home") );
                if ( count($view)>1 ) {
                    $this->context = $view[0] . '.' . $view[1];
                } else {
                    $this->context = $view[0] . '.' . $view[0];
                }
            }
        }
    
        
        
    /**
     * Get loaded context.
     * @return string
     */
        public function getContext() : string {
            return $this->context;
        }
        
        
    /**
     * Get context params from db.
     * 
     * @return object|null
     */
        private function getFromDb() : object 
        {
            $m = new \app\Models\ConfigModel();
            $f = new \app\Databases\DBFilters();
             $f->filters->isIn['context'] = [$this->getContext()];
            $o = $m->getItem($f);
             
            return is_null($o) ? new \stdClass() : $o;
        }
}

