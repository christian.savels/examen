<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");


class Text
{

    private const EXT = ".ini";

    private const DEFAULT = "fr-FR";
    private array $content = [];
    private array $langs = [];
    private string $lang;
    
    
    public function __construct( string $activeLang, array $filesToLoad=[] ) {
        $this->init($activeLang, $filesToLoad);
    }
    
    
    /**
     * Init main.
     * @param string $activeLang
     * @param array $filesToLoad
     */
        private function init( string $activeLang, array $filesToLoad=[] ) 
        {
            $this->initActiveLang($activeLang);
            $this->loadFiles($filesToLoad);
        }


    /**
     * Check asked language to ensure that it is a valid language.
     * 
     * (To avoid bad entry in configuration.ini)
     * 
     * @return void
     */
        private function initActiveLang( string $activeLang ) : void {
            $this->lang = $this->isValid($activeLang) ? $activeLang : "fr-FR";
        }
        

    /**
     * Get valid default language.
     * 
     * @return string Language code.
     */
        public function getLangDefault() : string 
        {
            $d = Factory::getInstance()->cfg("lang.default");
            return $this->isValid($d) ? $d : self::DEFAULT;
        }
        

    /**
     * Load translated files related to active lang.
     * 
     * @param array $filesToLoad Complete finame without path.
     * @param string $lang Lang to load. 
     * 
     * @return void
     */
        public function loadFiles( array $filesToLoad=[], string $lang=null ) : void
        {
            $def = $this->getLangDefault();
            $default = [];
            foreach ( $filesToLoad as $file )
            {
                // Default language
                if ( $this->lang!=$def ) 
                {
                    $path = $this->getPath($file . self::EXT, $def);
                    if ( file_exists($path) ) {
                        $default = array_merge($default, parse_ini_file($path));
                    }  
                }                
                
                // Asked language
                $path = $this->getPath($file . self::EXT, $lang);
                if ( file_exists($path) ) {
                    $this->content = array_merge($this->content, parse_ini_file($path));
                }   
            }

            $this->content = array_merge($default, $this->content);
        }
    
        
    /**
     * Get translated string.
     * 
     * @param string $ident
     * @param string $vars Values to bind.
     * 
     * @return string
     */
        public function get( string $ident=null, ...$vars ) : string
        {
            if ( !is_null($ident) )
            {
                $ident = strtoupper($ident);
                if ( array_key_exists($ident, $this->content) ) {
                    return empty($vars) ? $this->content[$ident] : sprintf($this->content[$ident], ...$vars);
                }
            }
            
            return is_null($ident) ? "" : $ident;
        }
        
        
    /**
     * Get translated string to apply on title attribute.
     * 
     * @param string $ident
     * @param string $vars Values to bind.
     * 
     * @return string
     */
        public function getTitle( string $ident=null, ...$vars ) : string{
            return htmlentities($this->get($ident, ...$vars));
        }
    
        
    /**
     * Define if lang is valid.
     * 
     * @param string $ident
     * 
     * @return bool
     */
        public function isValid( string $lang=null ) : bool {
            return !is_null($lang) && in_array($lang, $this->getLangs());
        }
    
    
    /**
     * Get current lang.
     * @return string
     */
        public function getLang() : string {
            return $this->lang;
        }
    
    
    /**
     * Get current lang ISO code.
     * 
     * @return string
     */
        public function getLangISO() : string {
            return explode("-", $this->lang)[0];
        }

    
    /**
     * Get lang image from code.
     * 
     * @param string $code Like "en-GB". Default = current language.
     * 
     * @return string
     */
        public function getImgFromCode(string $code=null) : string {
            return $this->getImgFlagPath( $this->getCountryFromCode($code) );
        }

    
    /**
     * Get lang image from country code.
     * 
     * @param string $country Like "GB". Default = current language.
     * 
     * @return string
     */
        public function getImgFromCountry(string $country=null) : string {
            return $this->getImgFlagPath($country);
        }


    /**
     * Get country flag path.
     * 
     * @param string $country Country code like "GB".
     * 
     * @return string
     */
        public function getImgFlagPath(string $country) : string 
        {
            $f = Factory::getInstance();
            $dir = $f->path("img_flags", 0);
            $root = $f->getUrlFromPath($dir);
            $ext = "." . $f->cfg("lang.flags_ext", "gif");

            return $root . '/' . $country . $ext;
        }

    
    /**
     * Get lang country from code.
     * 
     * @param string $code Like "en-GB". Default = current language.
     * 
     * @return string
     */
        public function getCountryFromCode(string $code=null) : string 
        {
            if ( is_null($code) ) {
                $code = $this->lang;
            }

            return explode("-", $code)[1];
        }
    

    /**
     * Get country title from iso.
     * 
     * @param string $code Like "GB". 
     * 
     * @return string
     */
        public function getCountryTitle(string $iso) : string 
        {
            $country = CountriesHelper::getCountry($iso);
            
            return is_null($country) ? $iso : $country->name;
        }
        
    
    /**
     * Get available langs.
     * 
     * @return array
     */
        public function getLangs() : array
        {
            if ( !count($this->langs) )
            {
                $dir = $this->getPath();
                $this->langs = array_diff(scandir($dir), ['.', '..']);            
            }
            
            return $this->langs;
        }
    
    
    /**
     * Get available langs.
     * @return string
     */
        public function getPath( string $file=null, string $lang=null ) 
        {
            if ( !is_null($file) && !$this->isValid($lang) ) { 
                $lang = $this->lang; 
            }
            $suffix = is_null($file) ? '' : DS . $lang . DS . $file;
            
            return Factory::getInstance()->getRootPath() . DS . "languages" . $suffix;
        }


    /**
     * Get lang chooser.
     *      
     * @return string HTML of full form select.
     */
        public function getLangChooser() : string {
            return HtmlHelper::makeLangChooser($this->getLangs());
        }


    /**
     * Get lang chooser.
     *      
     * @return string HTML of full form select.
     */
        public function getLangSelect(string $name, string $value=null) : string 
        {
            return FieldHelper::getField(
                'lang', $name, $name, 
                is_null($value) ? Factory::getInstance()->getTxt()->getLang() : $value
            );
        }
        
        
}