<?php
namespace app\Helpers;
use stdClass;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Pagination helper.
 * 
 * @author chris
 *
 */
class PaginationHelper
{
    public int $count = 0;         // Total of items.

    /**
     * Items by page.
     * 0 = no limit, 
     * -1 = not initialized (return \Config::PAG_LIMIT_DEFAULT).
     * 
     * @var int
     */
    public int $limit = -1;         // Items by page.
    public int $offset = 0;        // Db offset
    public int $page = 1;         // Current page.
    public int $pages = 1;         // Total of pages.
    
    
    public function __construct(mixed $page=null) {
        $this->init($page);
    }
    
    
    /**
     * Init pagination.
     */
        private function init(mixed $page=null) : void
        {
            $p = Helper::getValue("pagination", null);
            
            if ( !is_null($p) ) 
            {
                $p = json_decode($p, false);
                $this->setLimit($p->limit);
                $this->setPage($p->page);
            }
            elseif ( !empty($page) ) {
                $this->setPage($page);
            }
        }


    /**
     * Set limit (by page).
     * 
     * @param int $limit
     * @return void
     */
        public function setLimit(int $limit ) : void {
            $this->limit = max(0, $limit);
        }


    /**
     * Set page to show.
     * 
     * @param int $page
     * 
     * @return void
     */
        public function setPage(int $page) : void 
        {
            $this->offset = $this->getLimit()*max(0, $page-1);
            $this->page = $page;
        }


    /**
     * Set count. (number of items max)
     * 
     * @param int $count
     * @return void
     */
        public function setCount( int $count ) : void 
        {
            $this->count = max(0, $count);

            if ( $this->getLimit() ) 
            {            
                $m = $this->count%$this->getLimit();
                $r = $this->count / $this->getLimit();
                
                $this->pages = $m===0 ? $r : $r + 1;
            }
        }
        
        
    /**
     * Get pagination limit.
     * 
     * @return int
     */
        public function getLimit() : int 
        {            
            if ( $this->limit == -1) {
                $this->limit = Factory::getInstance()->cfg("pagination.limit_default");
            }
            
            return min(Factory::getInstance()->cfg("pagination.limit_max"), $this->limit);
        }
    
        
    /**
     * Get pagination offset.
     * 
     * @return int
     */
        public function getOffset() : int {
            return $this->offset;
        }  
        

    /**
     * Get current page.
     * 
     * @return int
     */
        public function getPage() : int {
            return $this->page;
        }     
        
        
    /**
     * Get number of pages.
     * 
     * @return int
     */
        public function getPages() : int {
            return max(1, $this->pages);
        }  

        
    /**
     * Get items count.
     * 
     * @return int
     */
        public function getCount() : int {
            return $this->count;
        }


    /**
     * Render pagination html.
     * 
     * @return string Pagination html.
     */
        public function render(string $ariaTitle, string $listUrl=null) : string 
        {
            $txt = Factory::getInstance()->getTxt();
            $pages = $this->getPages();
            $html = [];

           // if ($pages>1)
           // {
                $page = 0;
                $ariaTitle = \app\Helpers\Factory::getInstance()->getTxt()->getTitle($ariaTitle);
                $prevLink = "#";
                $nextLink = "#";

                // Init previous/next links.
                if ( !is_null($listUrl) ) 
                {
                    if ( $this->getPage()<$this->getPages() ) {
                        $nextLink = $listUrl . '/' . $this->getPage()+1;
                    }
                    if ( ($this->getPage()-1)>0 ) {
                        $prevLink = $listUrl . '/' . $this->getPage()-1;
                    }
                } 
                
                $html[] = '<nav aria-label="' . $ariaTitle . '" data-pw-pagination="' . $pages . '" >';
                $html[] = '<ul class="pagination pagination-lg flex-wrap">';
                $html[] = '<li class="page-item">';
                $html[] = '<a class="page-link" data-pw-prev href="' . $prevLink . '">' . $txt->get("PAG_PREV_PAGE") . '</a>';
                $html[] = '</li>';
                
                while ( $page<$pages )
                {
                    $page++;
                    $active = $page == $this->page ? "active" : "";
                    $title = \app\Helpers\Factory::getInstance()->getTxt()->get("PAG_LINK_PAGE", $page);

                    // Init page link.
                    if ( !is_null($listUrl) ) {
                        $link = $listUrl . '/' . $page;
                    } else {
                        $link = "#";
                    }

                    // Make it.
                    $html[] = '<li class="page-item '. $active .'" data-pw-page="' . $page . '" >';
                    $html[] = '<a class="page-link" href="' . $link . '" title="' . $title . '">' . $page . '</a>';
                    $html[] = '</li>';
                }

                $html[] = '<li class="page-item">';
                $html[] = '<a class="page-link" data-pw-next href="' . $nextLink . '">' . $txt->get("PAG_NEXT_PAGE") . '</a>';
                $html[] = '</li>';
                $html[] = '</ul>';
                $html[] = '</nav>';
            //}

            return implode('', $html);
        }


    /**
     * Get JSON related to pagination to get items.
     * 
     * @param string $urlItems
     * @return string
     */
        public function getJSON() : string 
        {
            $o = new stdClass;
             $o->page = $this->getPage();
             $o->pages = $this->getPages();
             $o->limit = $this->getLimit();

            return json_encode($o);
        }

        
    /**
     * Get page from post.
     * 
     * @return int
     */
        public static function getPageFromPost() : int 
        {
            $page = 0;
            $p = \app\Helpers\Helper::getValue("pagination", null);
            if ( !is_null($p) ) 
            {
                $p = json_decode($p);
                $page = $p->page;
            }

            return !$page ? 1 : max(1, $page);
        }
    
}

