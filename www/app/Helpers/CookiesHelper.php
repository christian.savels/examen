<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Cookies helper.
 * 
 * @author chris
 *
 */
class CookiesHelper
{
    /**
     * Get value from cookie.
     * 
     * @param string $what Cookie name.
     * @param mixed $default Value if cookie is not set.
     * 
     * @return mixed
     */
        public static function getValue(string $what, mixed $default=null) : mixed {
            return isset($_COOKIE[$what]) ? $_COOKIE[$what] : $default;
        }


    /**
     * Set cookie value.
     * 
     * @param string $what Cookie name.
     * @param mixed $value Value. Value will be converted to json if it is not a string. 
     * @param string Cookie path.
     * 
     * @return void
     */
        public static function setValue(string $what, mixed $value, string $path="/") : void 
        {
            if ( !is_null($value) && !is_string($value) ) {
                $value = json_encode($value);
            }

            setcookie
            (
                $what, $value, 
                time() + (86400 * \app\Helpers\Factory::getInstance()->cfg("cookies.validity")), 
                $path
            );
        }
}

