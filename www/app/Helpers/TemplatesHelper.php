<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Templates helper.
 * 
 * @author chris
 *
 */
class TemplatesHelper
{
    /**
     * Load <template>.
     * 
     * @param string $what Filename without extension.
     * 
     * @return string Template html.
     */
        public static function load(string $what, object $item=null) : void
        {
            $f = Factory::getInstance();
            $path = $f->path("templates") . $what . '.php';
            if ( file_exists($path) && is_file($path) ) 
            {
                ob_start();
                include $path;
                $c = ob_get_contents();
                ob_end_clean();
                $f->addTemplateDeclaration($what, $c);
            } 
        } 
        
    
    /**
     * Get content of the template.
     * 
     * @param string $what Filename without extension.
     * 
     * @return string Template html.
     */
        public static function get(string $what, object $item) : string
        {
            $f = Factory::getInstance();
            $path = $f->path("templates") . $what . '.php';
            if ( file_exists($path) && is_file($path) ) 
            {
                ob_start();
                include $path;
                $c = ob_get_contents();
                ob_end_clean();
                $f->addTemplateDeclaration($what, $c);

                return $c;
            } 
            
            return '';
        } 


    /**
     * Get all <template>.
     * 
     * @return string Htlm.
     */
        public static function getTemplates() : string 
        {
            $r = '';
            $items = Factory::getInstance()->getTemplates();
            foreach ( $items as $k => $v) {
                $r .= '<template id="' . $k . '">' . $v . '</template>';
            }

            return $r;
        }
}