<?php 
namespace app\Helpers;
use app\Objects\Filter;
use stdClass;

defined('_PWE') or die("Limited acces");

/**
 * 
 * List filters helper.
 * 
 */
class FiltersHelper
{
    private object $filters;

    private string $context;
    private string $formName;

    public function __construct(string $context, string $formName) {
        $this->init($context, $formName);
    }


    /**
     * 
     * Init class.
     * 
     * @param string $filename
     * @param string $formName
     * 
     * @return void
     */
        private function init(string $context, string $formName) : void 
        {
            $this->context = $context;
            $this->filters = new stdClass;
            $this->formName = $formName;

            $this->bindFilters();
        }


    /**
     * 
     * Get filters.
     * 
     * @return object
     */
        public function getFilters() : object {
            return $this->filters;
        }


    /**
     * 
     * Get filters names.
     * @return array
     */
        public function getFiltersNames() : array {
            return array_keys((array)$this->filters);
        }

        
    /**
     * 
     * Init filters to show.
     * @param string $filename
     * @return void
     */
        private function bindFilters() : void
        {
            $path = Factory::getInstance()->path("form") . $this->context . ".xml";

            if ( file_exists($path) ) 
            {
                $xml = simplexml_load_file($path);
                foreach ($xml->fieldset as $fieldset) 
                {
                    foreach ($fieldset->field as $field)
                    {
                        if ( isset($field["filter"]) && !empty(($f=(string)$field["filter"])) ) {
                            $this->filters->{(string) $field["name"]} = new Filter($field);
                        }
                    }
                }
            }
        }


    /**
     * 
     * Make a input field.
     * @param string $field
     * @return string
     */
        public function makeInputName(string $field) : string {
            return "filters-" . $field;
        }


    /**
     * Make input placeholder.
     * 
     * @param string $field
     * 
     * @return string
     */
        private function makeHint(string $field) : string {
            //return Factory::getInstance()->getTxt("HINT_FILTER_" . strtoupper($field));
            return "HINT_FILTER_" . strtoupper($field);
        }

        
    /**
     * 
     * Render filters form.
     * 
     * @param string $formName
     * @param string|null $formAction
     * @param array $ignore
     * @param string addContent
     * 
     * @return string
     */
        public function render(string $formName, string $formAction=null, array $ignore=[], string $addContent="") : string
        {
            $r = [];
            $factory = Factory::getInstance();
            $idFilters = "filters-" . time();
            $cValues = $this->getFromCookie($formName);
            $show = false;
            
            if ( !empty(trim($addContent)) ) {
                $addContent = '<hr />' . $addContent;
            }

            // Make inputs.
            foreach ( $this->filters as $fname => $filter )
            {
                if ( !in_array($fname, $ignore) ) 
                {
                    $iname = $this->makeInputName($fname);
                    $hint = empty($filter->hint) ? $this->makeHint($fname) : $filter->hint;         
                    $v = isset($cValues->{$iname}) ? $cValues->{$iname} : null;
                    if ( is_null($v) || empty(trim($v)) ) {
                        $v = $filter->value;
                    } else {
                        $show = true; // Force showed when a filter is used.
                    }

                    $r[] = FieldHelper::getField
                    (
                        $filter->type,
                        $iname,
                        $iname,
                        $v,
                        $filter->style,
                        false,
                        "data-pw-filter",
                        null,
                        $hint
                    );
                }
            }

            if ( count($r) ) 
            {
                // Make final form.
                $form = FormHelper::encloseFields
                (
                    implode('', $r),
                    $formName,
                    $formName,
                    $formAction
                );

                $link = IconsHelper::get("filters", 0, "me-3") . $factory->getTxt()->get("FILTERS");
                
                $offcanvas = HtmlHelper::makeOffcanvas($idFilters,
                    $factory->getTxt()->get("FILTERS"),
                    $link,
                    $form . $addContent,
                    $show
                );                

                return $offcanvas;
            }

            return '';
        }
        public function renderOLDTODELETE(string $formName, string $formAction=null, array $ignore=[]) : string
        {
            $r = [];
            $factory = Factory::getInstance();
            $idFilters = "filters-" . time();
            $cValues = $this->getFromCookie($formName);
            $collapsed = false;

            // Make inputs.
            foreach ( $this->filters as $fname => $filter )
            {
                if ( !in_array($fname, $ignore) ) 
                {
                    $iname = $this->makeInputName($fname);
                    $hint = empty($filter->hint) ? $this->makeHint($fname) : $filter->hint;         
                    $v = isset($cValues->{$iname}) ? $cValues->{$iname} : null;
                    if ( is_null($v) || empty(trim($v)) ) {
                        $v = $filter->value;
                    } else {
                        $collapsed = true; // Force collapsed when a filter is used.
                    }

                    $r[] = FieldHelper::getField
                    (
                        $filter->type,
                        $iname,
                        $iname,
                        $v,
                        $filter->style,
                        false,
                        "data-pw-filter",
                        null,
                        $hint
                    );
                }
            }

            if ( count($r) ) 
            {
                $show = $collapsed ? 'show' : '';
                // Make final form.
                $form = '<div id="' . $idFilters . '" class="collapse ' . $show . '" data-pw-filters>' 
                        . FormHelper::encloseFields(
                            implode('', $r),
                            $formName,
                            $formName,
                            $formAction
                        )
                        . '</div>';
                        
                // Collapse link
                $collapse = IconsHelper::get("filters", 0, "me-3");
                $collapse .= $factory->getTxt()->get("FILTERS");
                $collapse = HtmlHelper::makeCollapseLink
                (
                    $idFilters, 
                    $collapse, 
                    "btn btn-outline-secondary w-100", 
                    $collapsed ? true : $factory->cfg("list.filters_collapsed")
                );

                return '<h5>' . $collapse . '</h5>' . $form;
            }

            return '';
        }


    /**
     * Render filters active alert.
     * 
     * @param bool $show Show alert.
     * 
     * @return string Html.
     */
        public function renderAlert(bool $show=true) : string
        {
            $content = IconsHelper::get("warning", 0, "me-2 text-warning");
            $content .= Factory::getInstance()->getTxt("ITEMS_FILTERS_ACTIVE");
            $att = $show ? 1 : 0;

            return HtmlHelper::makeAlert($content, "alert-secondary", true, 'data-pw-alert-filters="' . $att . '"');
        }
        

    /**
     * Get values related to the form from cookie.
     * 
     * @param string $formName Form name.
     * 
     * @return object
     */
        public function getFromCookie($formName) : object
        {
            $fromCook = \app\Helpers\CookiesHelper::getValue($formName, new stdClass);
            if ( is_string($fromCook) ) {
                $fromCook = json_decode($fromCook);
            }

            return is_string($fromCook) ? json_decode($fromCook) : $fromCook;
        }


    /**
     * Get form name.
     * 
     * @return string
     */
        public function getFormName() : string {
            return $this->formName;
        }
    
}