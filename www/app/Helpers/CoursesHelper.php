<?php
namespace app\Helpers;
use app\Models\CoursesModel;
use app\Models\FormationModel;
use app\Models\FormationUsersModel;

defined('_PWE') or die("Limited acces");


class CoursesHelper
{
    /**
     * Make a list item html.
     * 
     * @param \app\Objects\Course $item
     * 
     * @return string Html.
     */
        public static function makeListItem(\app\Objects\Course $item) : string 
        {
            // Init image as background.
            $item->idImg = "itemImg" . $item->id;
            Factory::getInstance()->addStyleDeclaration("#" . $item->idImg . " {
                background-image: url('" . $item->image . "');
            }");

            return TemplatesHelper::get("course.list", $item);
        }


    /**
     * Check course before user registration.
     * 
     * @param int $cid Course id.
     * @param bool $redirect If true, redirect on error.
     * 
     * @return \app\Objects\Course|null Return null in case of error. 
     */
        public static function checkCourseRegistration(int $cid, bool $redirect=true) : \app\Objects\Course|null
        {
            $factory = Factory::getInstance();
            $m = new CoursesModel();
            $item = new \app\Objects\Course($m->getById($cid)); 
            $redUrl = null;
                
            if ( !$item->id || !$item->isPublished() ) 
            {
                $factory->setError("BAD_COURSE", $cid);
                $redUrl = $factory->getUrl(\app\Enums\Page::COURSES);
            }
            elseif ( $factory->getUser()->isGuest() )
            {
                $factory->setError("MUST_BE_CONNECTED");
                $redUrl = $factory->getUrl(\app\Enums\Page::COURSE, $cid);
            } 
            elseif ( $item->isStudent )
            {
                $factory->setError("ALREADY_STUDENT");
                $redUrl = $factory->getUrl(\app\Enums\Page::COURSE, $cid);
            } 
            // Check if user is pending for a formation including this course.
            else 
            {
                $m = new \app\Models\FormationCoursesModel();
                $formations = $m->getPendingFormationsRelatedToCourse($cid,  $factory->getUser()->id); 
                if ( !empty($formations) ) 
                {
                    $redUrl = $factory->getUrl(\app\Enums\Page::COURSE, $cid);
                    $m = new FormationModel();
                    $f = new \app\Databases\DBFilters();
                     $f->filters->isIn['id'] = $formations;
                    $formations = $m->getItems($f);
                    foreach ( $formations as $formation ) {
                        $factory->setError("ALREADY_PENDING_FORMATION_COURSE", $formation->title);
                    }
                }
            }
            
            // Redirect
            if ($redirect && $factory->hasErrors() && !is_null($redUrl) ) {
                \app\Helpers\Helper::redirect($redUrl);
            }

            return $factory->hasErrors() ? null : $item;
        }


    /**
     * Make a list of selectable courses.
     * @param array $courses
     * @param array $groups
     */
         public static function makeSelectableCourses( array $courses ) : string
         {
             $txt = Factory::getInstance()->getTxt();
             $heads = [
                $txt->get("COURSE_TITLE"), 
                $txt->get("COURSE_DESCRIPTION"),
                $txt->get("COURSE_ID"),
                $txt->get("COURSE_SELECT")
            ];
             $cols = [];

             foreach ( $courses as $course )
             {
               $a = [
                   $course->title,
                   $course->description
               ];
               
               // Course id
               $a[] = '<span data-pw-courseid="' . $course->id . '">#' . $course->id . '</span>';
               
               // Select col
               $a[] = '<span class="d-block" data-pw-select>'. IconsHelper::get("select") . '</span>';
               
               $cols[] = $a;
            }
             
            return HtmlHelper::makeTable($heads, $cols, ['class="table-courses table"']);
         }
    
    
}

