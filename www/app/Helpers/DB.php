<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

use PDO;


/**
 * 
 *  DB helper
 *  
 * @author chris
 *
 */
class DB
{   
    private function __construct() {}

    
    /**
     * Get instance of db.
     */
        static public function get() : PDO|null
        {
            switch(\app\Helpers\Factory::getInstance()->cfg("db.type"))
            {
                case "mysqli" :
                default :
                    return \app\Databases\Mysqli::connect();
            }
        }
}
?>