<?php
namespace app\Helpers;
use stdClass;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Order by queries helper.
 * 
 * @author chris
 *
 */
class OrderByHelper
{
    private string $form = "";          // Form name.s
    private string $col = "";           // Sorted column.
    private string $dir = "asc";        // Sorted direction.
    
    
    public function __construct(string $formName) {
        $this->init($formName);
    }
    
    
    /**
     * Init.
     */
        private function init(string $formName) : void
        {
            $this->form = $formName;

            $ob = Helper::getValue("orderBy", null);

            // Get from cookie if not set.
            if ( is_null($ob) ) {
                $ob = $this->getFromCookie();
            } 
            
            if ( !is_null($ob) ) 
            {
                $ob = explode(".", $ob);
                if ( count($ob)==2 ) 
                {
                    $this->setCol($ob[0]);
                    $this->setDir($ob[1]);
                }
            }

            // Save into cookie.
            $this->saveToCookie();
        }


    /**
     * 
     * Set sorted column value.
     * 
     * @param string $v
     * 
     * @return void
     */
        private function setCol(string $v) : void {
            $this->col = $v;
        }
    

    /**
     * 
     * Set sorted direction value.
     * 
     * @param string $v
     * 
     * @return void
     */
        private function setDir(string $v) : void 
        {
            if ( in_array($v, ["asc", "desc"]) ) {
                $this->dir = $v;
            }
        }


    /**
     * 
     * Get full value (to use with select or cookie).
     * 
     * @return string
     */
        public function getVal() : string {
            return $this->col . '.' . $this->dir;
        }

        
    /**
     * 
     * Get sorted direction.
     * 
     * @return string
     */
        public function getDir() : string {
            return $this->dir;
        }


    /**
     * 
     * Get sorted column.
     * 
     * @return string
     */
        public function getCol() : string {
            return $this->col;
        }


    /**
     * 
     * Get from cookie.
     * 
     * @return string
     */
        public function getFromCookie() : string {
            return \app\Helpers\CookiesHelper::getValue($this->form, "");
        }

        
    /**
     * 
     * Set order by value from cookie.
     * 
     * @param string Form name.
     * 
     * @return void
     */
        public function setFromCookie(string $formName) : void 
        {
            $cv = \app\Helpers\CookiesHelper::getValue($formName, "");
            $seg = explode('.', $cv);

            $this->col = count($seg)==2 ? $cv[0] : '';
            $this->dir = count($seg)==2 ? $cv[1] : '';
        }


    /**
     * Save current value to cookie.
     * 
     * @return void
     */
        private function saveToCookie() : void {
            \app\Helpers\CookiesHelper::setValue($this->form, $this->getVal());
        }
    
}

