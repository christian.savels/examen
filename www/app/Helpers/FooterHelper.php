<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Footer helper.
 * 
 * @author chris
 *
 */
class FooterHelper
{
    /**
     * Get public sitemap (links)
     * 
     * @return array
     */
        public static function getSitemap() : array
        {
            $r = [];
            $t = Factory::getInstance()->getTxt();

            foreach ( \app\Enums\Page::cases() as $page )
            {
                if ( str_contains($page->value, '/list') ) {
                    $r[$t->get("MENU_".$page->name)] = Factory::getInstance()->getUrl($page);
                }
            }

            return $r;
        }


    /**
     * Get sections counts.
     * (formations, courses, ...)
     * 
     * @return array
     */
        public static function getCounts() : array 
        {
            $m = new \app\Models\FooterModel();
            $counts = $m->getItems();

            foreach ( $counts as &$c) {
                $c->title = Factory::getInstance()->getTxt("COUNTS_" . $c->role);
            }

            return $counts;
        }


    /**
     * Get social icons.
     * 
     * @return array
     */
        public static function getSocialIcons() : array 
        {
            $t = Factory::getInstance()->getTxt();
            $social = ['facebook', 'google-plus', 'twitter', 'pinterest', 'vimeo'];
            $r = [];

            foreach ( $social as $s ) {
                $r[$s] = (object)[
                    'icon' => IconsHelper::get($s), 
                    'title' => $t->getTitle($s),
                    'link' => '#'
                ];
            }
            
            return $r;
        }




}

