<?php
namespace app\Helpers;
use app\Objects\User;

defined('_PWE') or die("Limited acces");


class UserHelper
{
    /**
     * Check form validity 
     * 
     * @param object $relatedUser
     * 
     * @return object|null
     */
        public static function updateFromPost(object &$relatedUser) : void 
        {
            $factory = Factory::getInstance();
            $ztext = $factory->getTxt();

            // Collect old data before apply newest.
            $access = $relatedUser->access;
            $published = $relatedUser->published;
            $created_on = $relatedUser->created_on;
            $resetpwd = $relatedUser->resetpwd;
            $ignore = ["remove_avatar", "pwd_secure"];
            $xml = FormHelper::extractTree("user.account");

            foreach ( $xml as $node )
            {
                // Create attribs tree if needed.
                if ( $node->attribs ) 
                {
                    if ( !isset($relatedUser->attribs->{$node->attribs_tree}) ) {
                        $relatedUser->attribs->{$node->attribs_tree} = new \stdClass();
                    }
                    if ( !isset($relatedUser->attribs->{$node->attribs_tree}->{$node->name}) ) {
                        $relatedUser->attribs->{$node->attribs_tree}->{$node->name} = null;
                    }
                }
                
                /*
                * Manage avatar.
                * Upload new avatar and/or remove old.
                */ 
                if ( $node->name=="avatar" ) 
                {
                    $oldAvatar = $relatedUser->attribs->{$node->attribs_tree}->avatar;
                    $hasUpload = FileHelper::hasFileToUpload($node->name);
                    $removeAvatar = Helper::getValue("remove_avatar", 0);
                                        
                    // Remove old avatar
                    if ( !empty($oldAvatar) && ($removeAvatar || $hasUpload) ) 
                    {
                        FileHelper::removeFile($factory->path("avatar") . $oldAvatar);
                        $relatedUser->attribs->{$node->attribs_tree}->avatar = "";
                    }
                    
                    // Upload new
                    if ($hasUpload)
                    {
                        $avatar = FileHelper::uploadImage
                        (
                            $node->name, 
                            $factory->path("avatar"), 
                            "avatar-" . $relatedUser->id,
                            Factory::getInstance()->cfg("form.img_max_size"),
                            Factory::getInstance()->cfg("form.img_mime")
                        ); 

                        if ( !empty($avatar) ) {
                            $relatedUser->attribs->{$node->attribs_tree}->avatar = $avatar;
                        }
                    }
                }
                
                /*
                * Others fields.
                */
                elseif ( !in_array($node->name, $ignore) ) 
                {
                    $val = Helper::getValue($node->name, $node->default);
                    
                    // Check empty required fields.
                    if ( $node->required && !strlen(trim($val)) ) {
                        $factory->setError("REQUIRED_FIELD", $ztext->get("FIELD_" . $node->name));
                    } 
                    else 
                    {
                        // To attribs
                        if ( $node->attribs ) {                                
                            $relatedUser->attribs->{$node->attribs_tree}->{$node->name} = $val;
                        }
                        else 
                        {
                            if ( !in_array($node->name, ["username", "email", "pwd_secure", "pwd", "pwd2", "avatar"]) ) {
                                $relatedUser->{$node->name} = $val;
                            }
                        }
                    }                    
                }                    
            } 

            // Check protected values (only root is allowed to modiy thoses values).
            if ( !$factory->getUser()->isRoot() ) 
            {
                $relatedUser->access = $access;
                $relatedUser->published = $published;
                $relatedUser->created_on = $created_on;
                $relatedUser->resetpwd = $resetpwd;
            }
            
            // Check uniq fields if value changed.
            $mod = new \app\Models\UserModel();
            $toCheck = ["username", "email"];
            foreach ( $toCheck as $tc )
            {
                $v = trim(Helper::getValue($tc, ""));
                if ( empty($v) ) {
                    $factory->setError("REQUIRED_FIELD", $ztext->get("FIELD_" . $tc));
                }
                elseif ( $v!=$relatedUser->{$tc} ) 
                {
                    $filters = new \app\Databases\DBFilters();
                    $filters->filters->isIn[$tc] = [$v];
                    $u = $mod->getItem($filters);
                    if ( is_null($u) ) {
                        $relatedUser->{$tc} = $v;
                    } else {
                        $factory->setError("FIELD_" . $tc . "_UNAVAILABLE");
                    }
                } 
                else {
                    $relatedUser->{$tc} = $v;
                }
            }
            
            // Check new password
            $pwd = trim(Helper::getValue("pwd", ""));
            $pwd2 = trim(Helper::getValue("pwd2", ""));
            if ( !empty($pwd) ) 
            {
                if ( $pwd===$pwd2 ) 
                {
                    // Check easy password
                    if ( strtolower($pwd)==strtolower($relatedUser->username) ) {
                        $factory->setError("PWD_EQ_USERNAME");
                    } elseif ( strtolower($pwd)==strtolower($relatedUser->email) ) {
                        $factory->setError("PWD_EQ_EMAIL");
                    }
                    else 
                    {
                        $relatedUser->password = password_hash($pwd, PASSWORD_DEFAULT);
                        if ( $factory->getUser()->id!=$relatedUser->id )
                        {
                            $factory->setInfo("PWD_CHANGED_HAVE_TO_RESET");
                            $relatedUser->resetpwd = 1;
                        }
                    }
                } 
                else 
                {
                    $factory->setError("PASSWORD_BAD");    
                    $factory->setError("PASSWORD2_BAD");    
                }
            }
            
            // Prepare valid object that can be saved.
            $relatedUser->attribs = json_encode($relatedUser->attribs);
        }


    /**
     * 
     * Make an user card.
     * 
     * @param User $user
     * @param array $groups
     * @param array $formations
     * @param array $teacherCourses
     * @param array $studentCourses
     * @return string
     */
        public static function makeStudentCard( User $user, array $groups, array $formations, array $teacherCourses, array $studentCourses ) : string
        {
            $t = Factory::getInstance()->getTxt();
            $country = "BE";
            $lang = "fr-FR";
            $h = [];
            if ( empty($user->token_time) ) {
                $user->token_time = '<i class="not-active">' . $t->get("NEVER_LOGGED") . '</i>';
            } else {
                $user->token_time = $t->get("LAST_ACTION", Helper::getDate($user->token_time));
            }

            // Init country
            if ( isset($user->attribs->social->country) && !empty($user->attribs->social->country) ) {
                $country = $user->attribs->social->country;
            }

            if ( !empty($user->lang) ) {
                $lang = $user->lang;
            }
            // User info.
            $h[] = '<div class="card card-user card-about">';
            $h[] = '<div class="row g-0">';
           

            // Avatar
            $h[] = '<div class="col-md-4">';
            $h[] = '<img src="' . $user->attribs->social->avatar . '" class="img-fluid rounded-start img-about" alt="' . $user->lastname . '" />'; // avatar
            $h[] = '</div>';

            $h[] = '<div class="col-md-8">';
            $h[] = '<div class="card-body">';
            $h[] = '<ul class="list-group list-group-flush ">';
            $h[] = '<li class="list-group-item">';
            $h[] = '<span class="lastname">' . $user->lastname . '</span>';
            $h[] = '<span class="firstname">' . $user->firstname . '</span>';
            $h[] = '</li>';
            $h[] = '<li class="list-group-item">' 
                    . $t->get("LI_COUNTRY") 
                    . ' : <img src="' . $t->getImgFromCountry($country) . '" class="float-end" />'
                    . '</li>';
            $h[] = '<li class="list-group-item">' 
                    . $t->get("LI_LANG") 
                    . ' : <img src="' . $t->getImgFromCode($lang) . '" class="float-end" />'
                    . '</li>';
            $h[] = '<li class="list-group-item">' . $t->get("REGISTERED_ON", Helper::getDate($user->created_on)) . '</li>';
            $h[] = '<li class="list-group-item">' . $user->token_time . '</li>';



            // Address
            $h[] = '<li class="list-group-item">';
            $h[] = '<address>';
            $h[] = $user->getAddressFull();
            $h[] = "</address>";
            $h[] = "</li>";

            

            $h[] = '</ul>';


            // Groups (roles).
            $h[] = '<h5 class="about-title">';
            $h[] = $t->get("H_ROLES");
            $h[] = IconsHelper::get("roles", 0, "float-end");
            $h[] =  '</h5>';
            if ( !count($groups) ) {
                $h[] = '<i class="not-active">' . $t->get("NOT_ENROLED") . '</i>';
            }
        
            if ( count($groups) ) {
                $h[] = HtmlHelper::makeList($groups);
            } 

            // Is Teacher
            $h[] = '<h5 class="about-title">';
            $h[] = $t->get("H_TEACHER");
            $h[] = IconsHelper::get("teachers", 0, "float-end");
            $h[] =  '</h5>';
            if (!count($teacherCourses)) {
                $h[] = '<i class="not-active">' . $t->get("NOT_A_TEACHER") . '</i>';
            }
        
            if ( count($teacherCourses) ) {
                $h[] = HtmlHelper::makeList($teacherCourses);
            } 

            // Formations (student)
            $h[] = '<h5 class="about-title">';
            $h[] = $t->get("H_FORMATIONS");
            $h[] = IconsHelper::get("formations", 0, "float-end");
            $h[] = '</h5>';
            if ( !count($formations) ) {
                $h[] = '<i class="not-active">' . $t->get("NOT_IN_FORMATION") . '</i>';
            }

            if ( count($formations) ) {
                $h[] = HtmlHelper::makeList($formations);
            } 

            // Is Student (courses)
            $h[] = '<h5 class="about-title">';
            $h[] = $t->get("H_STUDENT");
            $h[] = IconsHelper::get("students", 0, "float-end");
            $h[] = '</h5>';
            if ( !count($studentCourses) ) {
                $h[] = '<i class="not-active">' . $t->get("NOT_A_STUDENT") . '</i>';
            }

            if ( count($studentCourses) ) {
                $h[] = HtmlHelper::makeList($studentCourses);
            } 

            $h[] = "</div>"; // card-body
            $h[] = "</div>"; // col
            $h[] = "</div>"; // row
            $h[] = "</div>"; // card

            return implode('', $h);
        }
    

    /**
     * 
     * Check password validity.
     * 
     * @param string $pwd
     * @return bool
     */
        public static function isValidPassword(string $pwd) : bool
        {
            $f = Factory::getInstance();
            $min = $f->cfg("password_len_min");
            $valid = strlen($pwd)>=$min;
            if ( !$valid ) {
                $f->setError("PWD_TOO_SHORT", $min);
            }

            return $valid;
        }


    /**
     * 
     * Init user avatar.
     * Assign default if not set.
     * 
     * @param \app\Objects\User $user
     * 
     * @return void
     */
        public static function initAvatar( Object &$user ) : void
        {
            $f = Factory::getInstance();
            $url = $f->getUrlFromPath($f->path("avatar"));
            $tree = isset($user->social) ? $user->social : $user->attribs->social;
            if( empty($tree->avatar) ) {
                $tree->avatar = $f->getRootUrl() . $f->cfg("img.default_avatar");
            } else {
                $tree->avatar = $url . $tree->avatar;
            }
        }


        /**
         * @deprecated
         */
        public static function makeSelectableUserCard( User $user, array $groups=[] ) : string
        {
            $f = Factory::getInstance();

            $html = '<div class="col-md-6" data-pw-user="74" data-pw-selected="0">
            <div class="card" title="' . $f->getTxt("SELECT_USER") . '">
                <div class="card-body p-0">
                    <div class="row ">
                        
                        <!-- Infos -->
                        <div class="col col-9 p-3">
                            <div class="ps-3">
                                <h6>
                                    <span data-pw-firstname="">' . $user->firstname . '</span>
                                    <span data-pw-lastname="">' . $user->lastname . '</span>
                                </h6>
                                <ul class="fs-6 mx-auto">
                                    <li>
                                        <span class="fa-solid fa-phone me-3"></span>
                                        <span data-pw-phone=""></span>
                                    </li>
                                    <li>
                                        <span class="fa-regular fa-address-card me-3"></span>
                                        
                                        <span data-pw-street="">rue</span>
                                        <span data-pw-number="">0</span><br>
                                    </li>
                                    <li>
                                        <span class="fa-solid fa-street-view me-3"></span>
                                        
                                        <span data-pw-zip="">0</span>
                                        <span data-pw-city="">-</span>
                                    
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                        <!-- Avatar -->
                        <div class="col col-3 p-0">
                            <div class="h-100 pw-flex-col-center ">
                                <div class="ratio ratio-1x1 bg-cover circle my-auto shadow" data-pw-social-avatar="" style="background-image: url(&quot;http://pwe/media/users/avatar/avatar-74.jpg&quot;);">
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- card -->
        </div>';
            return $html;
         }
    
        

    /**
     * Make a list of selectable users.
     * 
     * @param array $users
     * @param array $groups
     * 
     * @return string
     */
         public static function makeSelectableUsers( array $users, array $groups=[] ) : string
         {
             $txt = Factory::getInstance()->getTxt();
             $txt->loadFiles(["users.manage"], Factory::getInstance()->getUser()->getLang());

             // Table head.
             $heads = [
                $txt->get("USER_NAME"), 
                $txt->get("USER_BIRTHDAY"), 
                $txt->get("USER_ADDRESS")
             ];
             $cols = [];
             
             if ( count($groups) ) {
                 $heads[] = $txt->get("USER_GROUPS");
             }
             $heads[] = $txt->get("USER_ID");
             $heads[] = $txt->get("SELECT_USER");
             
             // Users list.
             foreach ( $users as $user )
             {
               $a = [
                   $user->firstname . ' ' . $user->lastname,
                   $user->identity->birthday,
                   $user->address->zip . ' ' . $user->address->city
               ];
               
               // Current user groups title.
               if ( count($groups) )
               {
                    $g = [];
                    foreach ( $user->groups as $gid ) 
                    {
                        if ( array_key_exists($gid, $groups) ) {
                            $g[] = $groups[$gid]->title;
                        }
                    } 
                   
                    asort($g);
                    $a[] = "<small>" . implode(', ', $g) . "</small>";
               }
              
               // User id
               $a[] = '<span data-pw-userid="' . $user->id . '">#' . $user->id . '</span>';
               
               // Selection col
               $a[] = '<span class="d-block" data-pw-select>' . IconsHelper::get("select") . '</span>';
               
               $cols[] = $a;
            }
             
            return HtmlHelper::makeTable($heads, $cols, ['class="table-users table"']);
         }
    
    
    
    /**
     * Get related url.
     * Return default if avatar is empty.
     * 
     * @param string $avatar
     * 
     * @return string
     */
        public static function getAvatarUrl( string $avatar=null ) : string
        {
            // Apply default
            if ( empty($avatar) ) {
                return Factory::getInstance()->getRootUrl() . "assets/img/avatar.png";
            } else { 
                return Factory::getInstance()->getRootUrl() . "media/users/avatar/" . $avatar;
            }
        }
    
    
    /**
     * Define if user is logged in.
     * (Shortcut)
     * 
     * @return bool
     */
        public static function isLoggedIn() : bool {
            return SessionHelper::isConnected();
        }
        
        
    /**
     * Get user with id.
     * 
     * @param object|null $uid
     */
        public static function getUser(int $uid) : object|null
        {
            $m = new \app\Models\UserModel();            
            return $m->getById($uid);
        }
                
        
    /**
     * Define if an user exists with this username OR email.
     * 
     * @param string $username
     * @param string $email
     * 
     * @return bool
     */
        public static function isExistingUser( string $username, string $email, bool $setMsg=false ) : bool
        {
            $exists = false;
            $f = new \app\Databases\DbFilters();
             $f->behavior->forceOR = true;
             $f->filters->isIn["username"] = [$username];
             $f->filters->isIn["email"] = [$email];
           
            $mod = new \app\Models\UserModel();
            $usr = $mod->getItem($f);
            if ( !is_null($usr) )
            {
                $exists = true;
                if ($setMsg)
                {
                    if ($usr->username==$username) {
                        Factory::getInstance()->setError("SIGNUP_BAD_USERNAME");
                    }
                    if ($usr->email==$email) {
                        Factory::getInstance()->setError("SIGNUP_BAD_EMAIL");
                    }
                }
            }
            
            return $exists;
        }


    /**
     * Update logged user.
     * 
     * @return bool
     */
        public static function updateCurrentUser() : bool
        {
            if ( !Factory::getInstance()->getUser()->isGuest() ) 
            {
                $u = Factory::getInstance()->getUser();
                if ( !is_string($u->attribs) ) {
                    $u->attribs = json_encode($u->attribs);
                }

                $m = new \app\Models\UserModel();
                $done = $m->updateObject($u);

                

                return $done;
            }
            
            return false;
        }
        
        
}

