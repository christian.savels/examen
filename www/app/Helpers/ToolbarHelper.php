<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

/**
 *
 * Options of the page.
 *
 * @author chris
 *
 */
class ToolbarHelper
{    
    public readonly string $id;
    
    private const DIV_MAIN = "btn-group btn-group-lg";
    private const DIV_OPT = "list-option";
    private const BTN_CLASS = ""; /*"list-option-inner btn"; */
    
    private array $options = [];
    
    
   
    public function __construct(string $uid) 
    {
        $this->id = $uid;
        Factory::getInstance()->addStyleFile("assets/css/toolbar.css");
    }
    
        
    /**
     * Add an option to the toolbar.
     * 
     * @param \app\Objects\ToolbarOption $option
     * @param int $part
     */
        public function addOption( \app\Objects\ToolbarOption $option, int $part=1 ) : void
        {
            // Init part
            $this->makePart($part);
            
            // Init attributes
            $icon = $option->icon;
            if ( !empty($icon) ) {
                $icon = IconsHelper::get($icon, false, "toolbar-opt-icon");
            }

            // Apply common class (style)
            $option->class = "btn btn-pw-toolbar " . $option->class;

            $option->boxAttributes = $this->makeBoxAttributes($option);
            $option->linkAttributes = $this->makeLinkAttributes($option);
            $option->url = $this->makeUrl($option->page);
            $option->content = $this->makeContent($option->text, $icon);

            $this->options[$part][] = $option;
        }
        
    
    /**
     * Render toolbar.
     * 
     * @param string $mainClass
     * 
     * @return string
     */
        public function render(string $mainClass="") : string
        {
            $html = '';
            
            if ( count($this->options) )
            {
                $html = '<div class="btn-toolbar pw-toolbar ' . $mainClass . '" id="' . $this->id . '">';
                
                foreach ( $this->options as $part => $options )
                {
                    $html .= '<div class="' . self::DIV_MAIN . ' part-' . $part . '">';
                    
                    foreach ( $options as $opt )
                    {
                        $html .= '<a ' . $opt->url . ' ' . $opt->linkAttributes . ' >';
                        $html .= $opt->content;
                        $html .= '</a>';
                    }
                    
                    $html .= '</div>';
                }
                
                $html .= '</div>';
            }
            
            return $html;
        }
        
        
    /**
     * Init url attribute.
     * 
     * @param \app\Enums\Page $page
     * 
     * @return string
     */
        private function makeUrl(\app\Enums\Page $page=null) : string 
        {
            return is_null($page) 
                ? '' 
                : 'href="' . Factory::getInstance()->getUrl($page) . '"';
        }
        
        
    /**
     * Transform to array.
     * 
     * @param mixed $attributes
     * 
     * @return array
     */
        private function makeArray(mixed $attributes) : array 
        {
            if ( !is_null($attributes) )
            {
                if ( is_object($attributes) ) {
                    $attributes = (array)$attributes;
                } elseif ( is_string($attributes) ) {
                    $attributes = [$attributes];
                }
            }
            else {
                $attributes = [];
            }
            
            return $attributes;
        }
        
        
    /**
     * Make attributes.
     * 
     * @param mixed $attributes
     * 
     * @return string
     */
       // private function makeBoxAttributes(mixed $attributes=null, $confirm=null, string $title=null, string $class=null) : string
        private function makeBoxAttributes(\app\Objects\ToolbarOption $options) : string
        {            
            $attributes = $this->makeArray($options->boxAttributes); 
            
            // Apply title.
            if ( !is_null($options->title) ) {
                $attributes[] = $this->makeTitle($options->title);
            }
            
            // Action ?
            if ( !is_null($options->action) ) {
                $attributes[] = 'data-pw-action="' . $options->action . '"';
            }
            
            // have to confirm action ?
            if ( !is_null($options->confirm) ) 
            {
                $confirm = htmlspecialchars(
                    Factory::getInstance()->getTxt()->get($options->confirm), ENT_QUOTES
                );
                $attributes[] = 'data-pw-confirm="' . $confirm . '"';
            }
                        
            // Apply style classes.            
            $attributes[] = $this->makeStyle($options->class, SELF::DIV_OPT);  
            
            return implode(' ', $attributes);
        }

        
    /**
     * Make attributes.
     * 
     * @param mixed $attributes
     * 
     * @return string
     */
        private function makeLinkAttributes(\app\Objects\ToolbarOption $options) : string
        {            
            $attributes = $this->makeArray($options->linkAttributes);
            

            // Apply title.
            if ( !is_null($options->title) ) {
                $attributes[] = $this->makeTitle($options->title);
            }
            
            // Action ?
            if ( !is_null($options->action) ) {
                $attributes[] = 'data-pw-action="' . $options->action . '"';
            }
            
            // have to confirm action ?
            if ( !is_null($options->confirm) ) 
            {
                $confirm = htmlspecialchars(
                    Factory::getInstance()->getTxt()->get($options->confirm), ENT_QUOTES
                );
                $attributes[] = 'data-pw-confirm="' . $confirm . '"';
            }
                        
            // Apply style classes.            
            $attributes[] = $this->makeStyle($options->class, SELF::BTN_CLASS);  
            
            // Apply forced attributes.
            $attributes[] = $options->boxAttributes;
            
            return implode(' ', $attributes);
        }

    
    /**
     * Init options part.
     * @param int part to initialize.
     */
        private function makePart(int $part) : void
        {
            if ( !array_key_exists($part, $this->options) ) {
                $this->options[$part] = [];
            }
        }
    
    
    /**
     * Make option content.
     *
     * @param string $txt
     * @param string $icon
     *
     * @return string
     */
        private function makeContent(string $txt=null, string $icon=null) : string
        {
            $icon = is_null($icon) ? '' : $icon;
            $txt = is_null($txt) ? '' : $txt;

            if ( !empty($icon) && !str_contains($icon, "class=") ) {
                $icon = '<i class="' . $icon . '"></i>';
            } 
            
            return $icon . $txt;
        }
    
    
    /**
     * Make style (class) attribute.
     *
     * @param string $link
     *
     * @return string
     */
        private function makeStyle(string $class=null, string $moreStyle=null) : string
        {
            if ( is_null($class) ) {
                $class = "";
            }
            if ( is_null($moreStyle) ) {
                $moreStyle = "";
            }
            
            return 'class="' . $moreStyle . ' ' . $class . '"';
        }
    
    
    /**
     * Make title attribute.
     *
     * @param string $title
     *
     * @return string
     */ 
        private function makeTitle(string $title) : string {
            return 'title="' . htmlspecialchars(Factory::getInstance()->getTxt()->get($title)) . '"';
        }

}