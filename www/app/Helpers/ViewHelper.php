<?php
namespace app\Helpers;

use Exception;

defined('_PWE') or die("Limited acces");


class ViewHelper
{        
    private ConfigHelper $configHelper;
    
    private $view;    // View from requested url or post.
    private $format;  // JSON or HTML
    private $section; // Controller name.
    private $page;    // View name.
    
    
    public function __construct() {
        $this->init();
    }
    
    
    /**
     * Init object.
     */
        private function init() : void
        {            
            $this->view = strtolower(Helper::getValue("view", "home"));
            $this->format = strtolower(Helper::getValue("format", "html"));
            $this->section = \app\Helpers\ControllerHelper::extractName($this->view);
            $this->page = \app\Helpers\ViewHelper::extractViewName($this->view, $this->section);
            $this->configHelper = new ConfigHelper($this->section . "." . $this->page);

            if ( Factory::getInstance()->cfg("site.bootstrap") ) {
                Factory::getInstance()->addAppendAfterDeclaration("assets/js/bootstrap.bundle.min.js");
            }
        }
    
    
    /**
     * Get page name.
     * Related to the view directory.
     * 
     * @return string
     */
        public function getPage() : string {
            return $this->page;
        }
    
    
    /**
     * Get section name.
     * Related to the controller name.
     * 
     * @return string
     */
        public function getSection() : string {
            return $this->section;
        }
    
    
    /**
     * Get view from requested url or post.
     * 
     * @return string
     */
        public function getView() : string {
            return $this->view;
        }
        
    
    /**
     * Get format that the page will return.
     * 
     * @return string
     */
        public function getFormat() : string {
            return $this->format;
        }
    
    
        
    /**
     * Extract view name (page) from url view value.
     * @param string $view
     * @return string|mixed
     */
        public static function extractViewName( string $view, string $section="" ) 
        {
            if ( empty($section) ) {
                $section = "default";
            }
            $segments = explode("/", $view);
            return count($segments)>=2 ? $segments[1] : $section;
        }
    
    
    /**
     * Get view content.
     * 
     * @param string $format
     * 
     * @return string|null
     */
        public function get(string $format, string $section, string $page)
        {
            $view = strtolower(\app\Helpers\Helper::getValue("view", "home"));
            $controller = null;
            $params = [];
            $data = null;
            $method = $page;
            
            if ( $method=="default" ) {
                $method = $section;
            }
            
            if ( strlen($view) )
            {
                /**
                 *    Segments :
                 *   0 => controller
                 *   1 => method of controller + related view
                 */ 
                    $segments = explode("/", $view);
                    
                $helper = new \app\Helpers\ControllerHelper();
                $nsController = $helper->getNSController($section, $format=="json");

                if ( !is_null($nsController) )
                {
                    $len = count($segments);
                    
                    // Get method params.
                    for ( $i=2; $i<$len; $i++ ) 
                    {
                        if ( strlen($segments[$i]) ) {
                            $params[] = $segments[$i];
                        }
                    }   
                    
                    if ( is_numeric($method) ) {
                        $method = $section;
                    }
                    
                    // Try to load controller.
                    try 
                    {                    
                        $controller = $helper->getController($nsController, $method, $params);
                        
                        if ( !is_null($controller) )
                        {
                            $data = $controller->{$method}(...$params);
                            switch($format) 
                            {
                                case "json" : 
                                    return json_encode($data);
                                case "html" : 
                                default : 
                                    $this->setHeadFiles($section, $page);
                                    return $this->getViewHTML
                                    (
                                        $section, 
                                        $page,
                                        //count($segments)==1 ? "default" : $method, 
                                        $data
                                    );
                            }                           
                        } 
                    } 
                    catch (Exception $e) {
                        var_dump($format, $section, $page, $params);
                        var_dump($e);
                        // TODO Rediriger vers 404 ou index.
                    }
                }        
            }
            //exit;
        }
        
        
    /**
     * Get JSOn.
     * @param string $view
     * @param string $layout
     */
        public function getJSON( string $view, string $layout=null  )
        {
            $o = new \stdClass();
            $o->test = true;
            echo json_encode( $o );
            exit;
        }
        
    
    /**
     * Get view html.
     *
     * @param string $controllerName
     *
     * @return string | null
     */
        public function getViewHTML( string $section, string $page, $data=null )
        {
            $output = "";
            
            if ( ctype_alnum($section) && ctype_alnum($page) )
            {
                $root = Factory::getInstance()->getRootPath();
                $file = implode(DIRECTORY_SEPARATOR, [$root, "view", $section, $page . ".php"]);
                
                if ( file_exists($file) ) 
                {
                    ob_start();
                    include $file;
                    $output = ob_get_contents();
                    ob_end_clean();
                } else {
                    // TODO 404
                    echo "<br /> file not exists... " . $file ;
                }             
            }
            
            return $output;
        }
        
        
    /**
     * Insert files related to the view in <head>.
     * 
     * @param string $section
     * @param string $page
     */
        public function setHeadFiles( string $section, string $page ) : void
        {            
            // CSS file
            $f = 'assets/css/pages/' . $section . '.' . $page . '.css';
            if ( file_exists($f) ) {
                Factory::getInstance()->addStyleFile($f);
            }

            // JS File related to an edited item.
            // Load before view files.
            if ( $page=="edit" ) {
                Factory::getInstance()->addScriptFile("assets/js/edititem.js");
            } 
            
            // JS files
            $f = 'assets/js/pages/' . $section . '.' . $page . '.js';
            if ( file_exists($f) ) {
                Factory::getInstance()->addScriptFile($f);
            }

              
        }
        
        
    /**
     * Get config related to the current view.
     * 
     * @return ConfigHelper
     */
        public function getConfig() : ConfigHelper {
            return $this->configHelper;
        }
        
        
    
    /**
     * Translate $ident (shortcut).
     *
     * @param string $ident
     * @param ...$vars
     *
     * @return string
     */
        public function txt( string $ident=null, ...$vars)
        {
            return Factory::getInstance()
                ->getTxt()
                ->get($ident, ...$vars);
        }
}

