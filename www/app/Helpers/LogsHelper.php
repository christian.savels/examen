<?php
namespace app\Helpers;

use Exception;

defined('_PWE') or die("Limited acces");


class LogsHelper
{
    public static function logDbErrors( string $from, string $errorCode, array $errorInfo) : void
    {
        if ( Factory::getInstance()->cfg("log.db_errors") )
        {
            $lines = [];
            foreach ( $errorInfo as $info ) 
            {
                if ( !is_null($info) ) {
                    $lines[$errorCode . Factory::getInstance()->cfg("log.separator") . $info] = 1;
                }
            }
            
            self::log(Factory::getInstance()->cfg("log.db_file"), $from, array_keys($lines));
        }
    }
    
    
    /**
     * Log lines into the file.
     * @param string $file
     * @param array $lines
     * @param bool $append
     */
        public static function log( string $file, $from, array $lines, bool $append=true )
        {
            $t = time();
            $f = null;
            $s = Factory::getInstance()->cfg("log.separator");
            $uid = Factory::getInstance()->getUser()->id;
            
            try 
            {
                $f = fopen($file, $append ? 'a' : 'w');
                foreach( $lines as $line ) 
                {
                    if ( !is_null($line) ) {
                        fwrite($f, "\n" . $t . $s . $uid . $s . $from . $s . $line);
                    }
                }
                fclose($f);
            } 
            catch (Exception $e) {}
        }
}

