<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

use stdClass;

/**
 * 
 * Lists export.
 * 
 * @author chris
 *
 */
class ExportHelper
{    
    /**
     * Render html.
     * 
     * @param string $urlJSON
     * @param string $urlCSV
     * @param string $title
     * 
     * @return string
     */
        public static function render(string $urlJSON, string $urlCSV=null, string $title=null) 
        {
            $t = Factory::getInstance()->getTxt();
            
            if ( empty($title) ) {
                $title = $t->get("EXPORT_TITLE");
            }

            $h = [];
            $h[] = '<div class="dropdown export-part" >';
            $h[] = '<button class="btn btn-lg btn-secondary dropdown-toggle"  id="btnExportList"
            type="button" data-bs-toggle="dropdown" aria-expanded="false">';
            $h[] = IconsHelper::get("export", 0, 'text-main-color') . $title;
            $h[] = '</button>';
            $h[] = '<ul class="dropdown-menu" aria-labelledby="btnExportList">';
            if ( !is_null($urlCSV) ) {
                $h[] = '<li><a class="dropdown-item" data-pw-export-list="csv" href="' . $urlCSV . '" title="' . $t->getTitle("EXPORT_CSV_TITLE") . '">' . $t->get("EXPORT_CSV_TITLE") . '</a></li>';
            }
            $h[] = '<li><a class="dropdown-item" data-pw-export-list="json" href="' . $urlJSON . '" title="' . $t->getTitle("EXPORT_JSON_TITLE") . '">' . $t->get("EXPORT_JSON_TITLE") . '</a></li>';
            $h[] = '</ul>';
            $h[] = '</div>';

            return implode('', $h);
        }


    /**
     * 
     * Export data & download as format file.
     * 
     * @param string $format Default or invalid will return JSON.
     * @param array $items
     * 
     * @return void
     */
        public static function export(string $format, array $items)
        {
            switch(strtolower($format)) 
            {
                case 'csv':
                    self::exportCSV($items);
                    break;
                case "json":
                default :
                    self::exportJSON($items);
                    break;
            }
        }


    /**
     * 
     * Export & download data as json file.
     * 
     * @param array $items
     * 
     * @return void
     */
        public static function exportJSON( array $items ) : void  
        {
            $json = json_encode($items); 
            self::downloadData($json, "data-" . time(), "json", "application/json");
        }


    /**
     * 
     * Export CSV as file.
     * 
     * @param array $items
     * @return void
     */
        public static function exportCSV( array $items ) : void 
        {
            $data = self::makeCSVData($items);
            self::downloadData($data, "data-" . time(), "csv", "text/csv");
        }    


    /**
     * 
     * Make header to/and download temporary file.
     *
     * Inspiré de @source
     * 
     * @source https://stackoverflow.com/questions/15390769/how-to-download-a-temporary-file
     * @source https://cloud.google.com/appengine/docs/standard/php-gen2/using-temp-files?hl=fr#:~:text=Si%20vous%20souhaitez%20%C3%A9crire%20un,que%20les%20buckets%20Cloud%20Storage.
     * 
     * 
     * @param mixed $data Array for CSV, string for JSON.
     * @param string $name
     * @param string $extension
     * @param string $contentType
     *
     * @return void
    */
        public static function downloadData( mixed $data, string $name, string $extension, string $contentType ) : void 
        {
            $tmpName = tempnam(sys_get_temp_dir(), $name);
            $f = fopen($tmpName, 'a');

            file_put_contents($tmpName, $data);
            fclose($f);            

            header('Content-Description: File Transfer');
            header('Content-Type: ' . $contentType);
            header('Content-Disposition: attachment; filename=' . $name . '.' . $extension);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($tmpName));

            ob_clean();
            flush();
            readfile($tmpName);
            unlink($tmpName);
        }  


    /**
     * Prepare a valid array to write into a CSV file
     * 
     * @param array $items
     * @return array
     */
        public static function makeCSVData(array $items) : array
        {
            $tree = new stdClass;
            $tree->separator = ";;";
            $tree->branch = [];
            $tree->data = [];

            foreach ( $items as $item )
            {
                $tmp = [];
                    
                foreach ( $item as $attribute => $value )
                {
                    if ( !array_key_exists($attribute, $tree->branch) ) {
                        $tree->branch[$attribute] = 1;
                    }

                    if ( !is_string($value) ) {
                        $value = json_encode($value);
                    }

                    $tmp[] = $value;

                    // Separator is stlll valid ?
                
                    while ( str_contains($value, $tree->separator) ) {
                        $tree->separator = $tree->separator . ';';
                    }
                }

                $tree->data[] = $tmp;
            }

            // Apply valid separator
            $data = [
                implode($tree->separator, array_keys($tree->branch)) . "\n"
            ];
            foreach ( $tree->data as $d ) {
                $data[] = implode($tree->separator, $d) . "\n";
            }

            return $data;
        }
}