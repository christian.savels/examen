<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Icons helper.
 * 
 * @author chris
 *
 */
class IconsHelper
{
    const ICONS = [
        "add" => "fa-solid fa-circle-plus",
        "add-course" => ["fa-solid fa-c", "fa-solid fa-square-plus text-success"],
        "add-student" => ["fa-solid fa-graduation-cap", "fa-solid fa-square-plus text-success"],
        "add-teacher" => ["fa-solid fa-user-secret", "fa-solid fa-square-plus text-success"],
        "admin" => "fa-solid fa-head-side-virus",
        "cancel-pending" => ["fa-solid fa-graduation-cap", "fa-sharp fa-solid fa-xmark text-danger"],
        "clean" => "fa-solid fa-recycle",
        "cleancourses" => ["fa-solid fa-copyright", "fa-sharp fa-solid fa-recycle text-danger"],
        "cleanstudents" => ["fa-solid fa-graduation-cap", "fa-sharp fa-solid fa-recycle text-danger"],
        "create" => "fa-solid fa-circle-plus",
        "courses" => "fa-solid fa-copyright",
        "date-end" => "fa-regular fa-calendar-xmark",
        "date-start" => "fa-regular fa-calendar-check",
        "default" => "fa-solid fa-people-pulling",
        "degrees" => "fa-solid fa-arrow-up-right-dots",
        "delete" => "fa-solid fa-trash",
        "edit" => "fa-solid fa-pen-to-square",
        "export" => "fa-solid fa-file-export",
        "enrole" => ["fa-solid fa-people-pulling", "fa-solid fa-square-plus text-success"],
        "facebook" => "fa-brands fa-facebook",
        "filters" => "fa-solid fa-filter",
        "formations" => "fa-solid fa-eye-low-vision",
        "google-plus" => "fa-brands fa-google-plus",
        "help" => "fa-solid fa-circle-question",
        "homeTopInfoModern" => "fa-solid fa-eye-low-vision",
        "homeTopInfoSocial" => "fa-solid fa-eye-low-vision",
        "homeTopInfoSkill" => "fa-solid fa-eye-low-vision",
        "homeTopInfoWin" => "fa-solid fa-eye-low-vision",
        "info" => "fa-regular fa-lightbulb",
        "lang" => "fa-solid fa-earth-europe",
        "login" => "fa-solid fa-arrow-right-to-bracket",
        "logout" => "fa-solid fa-right-from-bracket",
        "mail" => "fa-regular fa-envelope",
        "mailto" => "fa-regular fa-envelope",
        "mailtoteachers" => ["fa-solid fa-user-secret", "fa-sharp fa-solid fa-envelope text-light"],
        "mailtostudents" => ["fa-solid fa-graduation-cap", "fa-sharp fa-solid fa-envelope text-light"],
        "mailtorole" => "fa-sharp fa-solid fa-envelope",
        "more" => "fa-solid fa-eye",
        "no" => "fa-solid fa-check",
        "notenrolable" => "fa-solid fa-ban text-danger",
        "nothing" => "fa-regular fa-face-frown-open",
        "params" => "fa-solid fa-gear",  
        "pending" => "fa-solid fa-clock-rotate-left",  
        "pinterest" => "fa-brands fa-pinterest-p",  
        "published" => "fa-solid fa-check",  
        "registerto" => "fa-solid fa-hand-sparkles",
        "roles" => "fa-solid fa-hand-sparkles",
        "select" => "fa-regular fa-hand-pointer",
        "select-end" => "fa-solid fa-list-ul",
        "signup" => "fa-solid fa-user-plus",
        "student" => "fa-solid fa-graduation-cap",
        "students" => "fa-solid fa-graduation-cap",
        "teacher" => "fa-solid fa-user-secret",
        "teachers" => "fa-solid fa-user-secret",
        "twitter" => "fa-brands fa-twitter",
        "unenrole" => ["fa-solid fa-people-pulling", "fa-solid fa-eject text-danger"],
        "unenrolecourse" => ["fa-solid fa-copyright", "fa-solid fa-square-minus text-danger"],
        "unenrolestudent" => ["fa-solid fa-graduation-cap", "fa-solid fa-square-minus text-danger"],
        "unpublished" => "fa-solid fa-charging-station text-danger",
        "user-area" => "fa-solid fa-person-through-window",
        "user-status-1" => "fa-solid fa-check text-success",
        "user-status-0" => "fa-solid fa-person-circle-exclamation text-danger",
        "user-status--1" => "fa-solid fa-skull-crossbones",
        "users" => "fa-solid fa-person-praying",  
        "validate-pending" => ["fa-solid fa-graduation-cap", "fa-sharp fa-solid fa-check text-success"],
        "vimeo" => "fa-brands fa-vimeo",
        "warning" => "fa-solid fa-triangle-exclamation",
        "yes" => "fa-solid fa-check"
        
    ];

    
    /**
     * 
     * Get icon related to the section.
     * 
     * @param string $what Section requested.
     * @param string $style More style to add in class attribute.
     * 
     * @return string
     */
        public static function get(string $what, bool $classOnly=false, string $style="") : string 
        {
            $ic = "";
            if ( array_key_exists($what, self::ICONS) ) {
                $ic = self::ICONS[$what];
            } else {
                $ic = self::ICONS["default"];
            }

            if ( is_array($ic) ) {
                return self::stack($ic, $style);
            }

            return $classOnly ? $ic : '<i class="' . $ic . ' ' . $style . '"></i>';
        }


        private static function stack( array $icons, string $style="" ) : string
        {
            $s = [];
            $stack = 2;
            foreach( $icons as $ic ) 
            {
                $addStyle = $stack == 1 ? "additional-icon" : "";
                //$s[] = '<i class="' . $ic . ' fa-stack-' . $stack . 'x ' . $addStyle . '"></i>';
                $s[] = '<i class="' . $ic . ' ' . $addStyle . '"></i>';
                $stack--;
            }

            //return '<span class="fa-stack  ' . $style . '">' . implode('', $s) . '</span>';
            return implode('', $s);
        } 

    
}

