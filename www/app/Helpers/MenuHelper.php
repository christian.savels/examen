<?php
namespace app\Helpers;
use app\Objects\RulesObj;

defined('_PWE') or die("Limited acces");


/**
 * 
 * Main menu helper.
 * 
 * @author chris
 *
 */
class MenuHelper
{    
    /**
     * Get links that user can manage.
     *    
     * @return array
     */
        public static function getUserManageLinks() : array
        {
            $f = Factory::getInstance();
            $m = new \app\Models\ConfigModel();
            $rules = $m->getByContext();
            $ugroups = $f->getUser()->getGroups();
            $values = [];
            
            foreach ( $rules as $context => $r )
            {
                if (self::canManage($ugroups, $r) )
                {
                    $ident = strtoupper($context) . "_MANAGE";
                    $txt = $f->getTxt("MENU_" . $ident);
                    $page = \app\Enums\Page::fromName($ident);                
                    $values[$txt] = $f->getUrl($page);
                }
            }

            ksort($values);
            
            return $values;
        }


    /**
     * Define if current user can manage.
     * 
     * @param array $ugroups Ids of user groups.
     * @param RulesObj $r Rules objecT.
     * 
     * @return bool
     */
        public static function canManage(array $ugroups, RulesObj $r) : bool 
        {
            $can = Factory::getInstance()->getUser()->isRoot();
            if ( !$can )
            {
                foreach ( $r->getCfg() as $group => $cfg )
                {
                    if ( in_array($group, $ugroups) && isset($cfg->manage) && $cfg->manage)  
                    {
                        return true;
                    }
                }
            }

            return $can;
        }

}