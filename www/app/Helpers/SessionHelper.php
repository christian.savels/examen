<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

use DateTimeImmutable;

/**
 * 
 * Session manager.
 * 
 * @author chris
 *
 */
class SessionHelper
{
    public const TABLE = "pw_session";
    public const TOKEN_USER = "user-token";
    public const TOKEN_CSRF = "csrf-token";
    public const TOKEN_TIME = "csrf-token_time";
    
    /**
     * 
     * Extend session time validity.
     * 
     * @return void
     */
        public static function extendSession() 
        {
            $modelSession = new \app\Models\SessionModel();
            $usr = Factory::getInstance()->getUser();

            // Update previous db session
            if ( !is_null($usr->token_session) ) 
            {
                $time = new DateTimeImmutable("now");

                // Record token to db
                $t = new \stdClass();
                 $t->sessionid = $usr->token_session;
                 $t->userid = $usr->id;
                 $t->time = $time->format("Y-m-d H:i:s");
                $modelSession->updateObject($t, "userid");

                $usr->token_session = $t->sessionid;
                $usr->token_time = $t->time; 
                $usr->token_csrf = self::makeToken(); 
                $usr->notify = isset($_SESSION['notify']) ? $_SESSION['notify'] : []; 

                self::destroyFor($usr);
            }
        }

        
    /**
     * Define if user is connected.
     * 
     * @return bool
     */
        public static function isConnected() : bool
        {
            $factory = Factory::getInstance();
            
            if ( isset($_SESSION["userid"]) && isset($_SESSION[self::TOKEN_USER]) ) 
            {
                $time = time();
                $validity = $time-(Factory::getInstance()->cfg("session.limit")*60);            
                $usr = $factory->getUser();
                if ( is_null($usr) || !$usr->id ) 
                {
                    $m = new \app\Models\UserModel();
                    if ( !is_null(($usr = $m->getById($_SESSION["userid"]))) ) {
                        $factory->setUser( new \app\Objects\User($usr) );
                    }
                }
                
                return $usr->token_session===$_SESSION[self::TOKEN_USER] 
                            && $usr->token_time>=$validity;
            }
            
            return false;
        }
    
        
        
    /**
     * Destroy current section.
     */
        public static function logout()
        {
            session_unset();
            session_destroy();
            session_write_close();
        }
    
        
        
    /**
     * Destroy session & create a clean.
     * 
     * @param \app\Objects\User $user
     * 
     * @return void
     */
        public static function destroyFor( \app\Objects\User $user=null ) : void 
        {
            session_destroy();
            self::initSession(!is_null($user));
            
            if ( !is_null($user) ) 
            {
                $_SESSION["userid"] = $user->id;
                $_SESSION["lang"] = $user->lang; 
                $_SESSION[self::TOKEN_USER] = $user->token_session;
                $_SESSION[self::TOKEN_TIME] = $user->token_time;
            
                if ( isset($user->token_csrf) ) {
                    $_SESSION[self::TOKEN_CSRF] = $user->token_csrf;
                }     
                if ( isset($user->notify) && !empty($user->notify) ) {
                    $_SESSION["notify"] = $user->notify;
                }                
            }            
        }
            
    
    /*
     * Make an html session.
     */
        public static function makeHTMLSession() : void 
        {
            self::initSession(); 
            
            $time = time();
         
            // Prevent CSRF
            $token = self::makeToken();
            $_SESSION[self::TOKEN_CSRF] = $token;
            $_SESSION[self::TOKEN_TIME] = $time;
        }
        
    
    /**
     * 
     * Define if the session is valid.
     * 
     * @return bool
     */
        public static function isValidSession() : bool
        {
            $tokenFrom = Helper::getValue(self::TOKEN_CSRF, null);
            $time = time();
            $oldestValid = $time-(Factory::getInstance()->cfg("session.limit")*60);

            return !is_null($tokenFrom) 
                    && isset($_SESSION[self::TOKEN_CSRF])
                    && isset($_SESSION[self::TOKEN_TIME])
                    && $_SESSION[self::TOKEN_CSRF]===$tokenFrom
                    && $oldestValid<=$_SESSION[self::TOKEN_TIME]
                    && self::isValidRequest();            
        }
        
        
    /*
     * Make a JSON session.
     */
        public static function makeJSONSession() : void 
        {
            self::initSession();
            
            $time = time();           
            $tokenFrom = Helper::getValue(self::TOKEN_CSRF, null);
            $exit = false;
            
            if( !is_null($tokenFrom) && isset($_SESSION[self::TOKEN_CSRF]) 
                && isset($_SESSION[self::TOKEN_TIME]) 
            )
            {
                if ( $_SESSION[self::TOKEN_CSRF]==$_POST[self::TOKEN_CSRF] )
                {
                    $oldestValid = $time-(Factory::getInstance()->cfg("session.limit")*60);
                    if ( $oldestValid<=$_SESSION[self::TOKEN_TIME] )
                    {
                        if ( !self::isValidRequest(true) )
                        {
                            echo "Session error : " . \app\Enums\ErrorCode::REQUEST_ATTEMPT;
                            $exit = true;
                        }
                    } 
                    else 
                    {
                        echo "Expired session.";
                        $exit = true;
                    }
                }
            } else {
                $exit = true;
            }
            
            // Exit if invalid request/session.
            if ($exit) {
                exit;
            }
        }
        
        
    /**
     * Define if request is valid.
     * (from site).
     * 
     * @param $log Log if not valid and set on true.
     * 
     * @return bool
     */
        public static function isValidRequest($log=true) : bool 
        {
            $prot = empty($_SERVER["HTTPS"]) ? 'http://' : 'https://';
            $rootUrl = $prot . $_SERVER["SERVER_NAME"] . '/';
            $valid = str_starts_with
            (
                $_SERVER["HTTP_REFERER"], 
                $rootUrl
            );
            
            // Invalid request, log it if needed.
            if ( !$valid && $log ) 
            {
                LogsHelper::logDbErrors
                (
                    'makeJSONSession',
                    \app\Enums\ErrorCode::REQUEST_ATTEMPT->value,
                    [
                        "REQUEST_URI: " . $_SERVER["REQUEST_URI"]
                         . ";HTTP_REFERER: " . $_SERVER["HTTP_REFERER"]
                    ]
                );
            }
            
            return $valid;
        }
        
        
    /**
     * Make a token.
     * @return string
     */
        public static function makeToken() : string {
            return uniqid(rand(), true);
        }
        

    /**
     * Get input related to the CSRF Token.
     * 
     * @return string
     */
        public static function getInputTokenCSRF() : string
        {
            $name = self::TOKEN_CSRF;
            $value = array_key_exists($name, $_SESSION) ? $_SESSION[$name] : '';
           
            return '<input '
                    . 'type="hidden" '
                    . 'name="' . $name . '" '
                    . 'value="' . $value . '" '
                    . '/>';
        }
        
    
    /**
     * Initialize the session.
     */
        private static function initSession( bool $setId=false )
        {
            session_name(Factory::getInstance()->cfg("session.name"));
            if ($setId) {
                session_id(bin2hex(openssl_random_pseudo_bytes(32)));
            }
            session_start(['cookie_lifetime' => Factory::getInstance()->cfg("session.limit")*60]);
        }
}