<?php 
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

use PDO;
use \app\Objects\User;

/**
 * 
 * PWE Factory
 * 
 * @author chris
 *
 */
class Factory extends MsgHelper
{
    private static $instance = null;
    private SiteParamsHelper $siteParams;
    private string $rootPath = "";
    private string $rootUrl = "";
    private PDO|null $db = null;
    private ViewHelper|null $viewHelper = null;
    private Text|null $txt = null;
    private User|null $user = null;
    
    private array $headStyle = [];  // To include in <head><style></style></head>
    private array $bodyAttribs = [];  // To include in <body attributes...></body>
    private array $onReady= [];     // Scripts loaded to the end of the page.
    private array $templates= [];     // Templates loaded to the end of the page.
    private array $appendAfter= [];     // Something to append to the end of the page (bootstrap scripts for ex.).
    private array $filesJS = [];    // JS Files for <head> (included in view/header.php).
    private array $filesCSS = [];   // CSS Files for <head> (included in view/head


    /**
     * Singleton !
     */
        private function __construct() {}
    
    
    /**
     * Get unique instance of the Factory.
     */
        public static function getInstance( string $rootPath=null ) : Factory
        {
            if ( is_null(self::$instance) ) 
            {
                self::$instance = new Factory();
                self::$instance->setRootPath($rootPath);
                self::$instance->initConfig();
            }
            
            return self::$instance;
        }
        
        
    /**
     * Init txt tools.
     */
        private function initTxt() : void 
        {
            $langUpdated = false;
            $f = Factory::getInstance();
            $lang = $f->cfg("lang.default");
            $files = [
                'common',                                       // Common
                'rules',                                        // Rules
                'pagination',                                   // Pagination
                $this->getSection() . '.default',               // Controller
                $this->getSection() . '.' . $this->getPage(),   // View
            ];

            // Lang from cookie
            if ( $f->cfg("lang.multi") ) 
            {
                $cookieLang = \app\Helpers\CookiesHelper::getValue("lang");

                // Change current lang ?
                $changeLang = \app\Helpers\CookiesHelper::getValue("changeLang");
                if ( !empty($changeLang) )
                {
                    $lang = $changeLang;

                    // Reset changeLang & record lang for later.
                    \app\Helpers\CookiesHelper::setValue("lang", $lang);
                    \app\Helpers\CookiesHelper::setValue("changeLang", null);

                    // Update user language.
                    if ( !$f->getUser()->isGuest() )
                    {
                        $f->getUser()->lang = $lang;
                        $langUpdated = UserHelper::updateCurrentUser();                       
                    }
                } 
                elseif ( !$f->getUser()->isGuest() ) {
                    $lang = $f->getUser()->getLang();
                } 
                elseif ( !empty($cookieLang) && $f->getUser()->isGuest() ) {
                    $lang = $cookieLang;
                }
            }

            $this->txt = new \app\Helpers\Text($lang, $files);

            // Notify update of the language.
            if ($langUpdated) {
                $f->setSuccess("USER_LANG_UPDATED", $lang);
            }
        }


    /**
     * Init configuration from ini file.
     * 
     * @return void
     */
        private function initConfig() : void {
            $this->siteParams = new \app\Helpers\SiteParamsHelper();
        }


    /**
     * Get value of configuration parameter.
     * @param string $what Searched attribute. Use "section.attribute" to get attribute from specific section.
     * @param mixed $default
     * @return mixed
     */
        public function cfg(string $what, $default=null) : mixed {
            return $this->siteParams->cfg($what, $default);
        }


    /**
     * Get path related to $where.
     * 
     * @param string $where
     * @param bool $fromRoot True to prefix with root path.
     * @return string
     */
        public function path(string $where, bool $fromRoot=true) : string {
            return $this->siteParams->path($where, $fromRoot);
        }


    /**
     * Get site params helper.
     * @return SiteParamsHelper
     */
        public function getSiteParams() : SiteParamsHelper {
            return $this->siteParams;
        }
    
    
    /**
     * Set current user. 
     * 
     * @param User $user
     * 
     * @return void
     */
        public function setUser(User $user) : void {
            $this->user = $user;
        }
    
        
    /**
     * Set root path. 
     * (related to system.php in root site folder).
     */
        public function setRootPath(string $rootPath) : void {
            $this->rootPath = $rootPath;
        }
    
        
    /**
     * Get root path. 
     * @return string
     */
        public function getRootPath() : string {
            return $this->rootPath; 
        }
    
        
    /**
     * Get root path. 
     * @return string
     */
        public function getUrlFromPath( string $path ) : string 
        {
            if ( str_starts_with($path, $this->getRootPath()) ) {
                $path = str_replace($this->getRootPath(), '', $path);               
            }
            
            return $this->getRootUrl() . str_replace('\\', '/', $path); 
        }
    
    
    /**
     * Get current user.
     *
     * @return \app\Objects\User
     */
        public function getUser() : User
        {
            if ( is_null($this->user) ) {
                $this->user = new User();
            }

            return $this->user;
        }
        
        
    /**
     *  Get unique instance of db object.
     */
        public function getDBo() : PDO|null
        {
            if ( is_null($this->db) ) {
                $this->db = \app\Helpers\DB::get();
            }
            
            return $this->db;
        }
        
        
    /**
     * Return language tool.
     * 
     * @param string $what
     * @param $vars Values to bind.
     * 
     * @return Text|string;
     */
        public function getTxt(string $what=null, ...$vars) : Text|string 
        {
            if ( is_null($this->txt) ) {
                $this->initTxt();
            } 
            
            return is_null($what) ? $this->txt : $this->txt->get($what, ...$vars);
        }
        
        
    /**
     * Get root url.
     * @return string
     */
        public function getRootUrl() : string 
        {
            if ( empty($this->rootUrl) )
            {
                $prot = empty($_SERVER["HTTPS"]) ? 'http://' : 'https://';
                $this->rootUrl = $prot . $_SERVER["SERVER_NAME"] . '/';
            }
            
            return $this->rootUrl;
        }
        
        
    /**
     * Get full url related to enum.
     * 
     * @param \app\Enums\Page $page
     * 
     * @return string
     */
        public function getUrl( \app\Enums\Page $page, string $suffix=null, bool $jsonFormat=false ) : string 
        {
            $json = $jsonFormat ? '&format=json' : '';
            $suffix = is_null($suffix) ? "" : "/" . $suffix;
            
            return $this->getRootUrl() . "?view=" . $page->value . $suffix . $json;
        }
        
        
    /**
     * Add a script that will be called on page ready.
     * 
     * @param string $script
     */
        public function addTemplateDeclaration( string $what, string $content ) : void  {
            $this->templates[$what] = $content;
        }    
          
        
    /**
     * Add a script that will be called on page ready.
     * 
     * @param string $script
     */
        
     
     /**
     * Get templates to load @ the end of the <body>.
     * 
     * @param bool $clean If true, clean the array.
     */
        public function getTemplates(bool $clean=true) : array 
        {
            $r = new \ArrayObject($this->templates);
            if ($clean) {
                $this->templates = [];
            }
            
            return (array)$r;
        }

        
    /**
     * Add a script that will be called on page ready.
     * 
     * @param string $script
     */
        public function addScriptDeclaration( string $script ) : void 
        {
            $called = $this->getCountCall($script, $this->onReady) + 1;
            $this->onReady[$script] = $called;
        }    
        

    /**
     * Add something to append to the end of the page
     * like bootstrap files.
     * 
     * @param string $what
     */
        public function addAppendAfterDeclaration( string $what ) : void 
        {
            $called = $this->getCountCall($what, $this->appendAfter) + 1;
            $this->appendAfter[$what] = $called;
        }

        
    /**
     * Add a script that will be called on page ready.
     * 
     * @param string $script
     */
        public function addStyleDeclaration( string $style ) : void 
        {
            $called = $this->getCountCall($style, $this->headStyle) + 1;
            $this->headStyle[$style] = $called;
        }
        
    /**
     * Add a script that will be called on page ready.
     * 
     * @param string $script
     */
        public function addBodyAttributes( string $attribs ) : void 
        {
            // Compte nb fois où les attributs sont insérés, utilisé pour debug.
            $called = $this->getCountCall($attribs, $this->bodyAttribs) + 1;
            $this->bodyAttribs[$attribs] = $called;
        }
        
        
    /**
     * Add a file to insert in header.
     * 
     * @param $file Url of file to load.
     */
        public function addScriptFile( string $file ) : void 
        {
            $called = $this->getCountCall($file, $this->filesJS) + 1;
            $this->filesJS[$file] = $called;
        }
       
        
    /**
     * Get script files to load in <head>
     * 
     * @param bool $clean If true, clean the array.
     */
        public function getScriptFiles(bool $clean=true) : array 
        {
            $r = new \ArrayObject($this->filesJS);
            if ($clean) {
                $this->filesJS = [];
            }
            
            return (array)$r;
        }
        
        
    /**
     * Get Attributes to include in <body attributes...>
     * 
     * @param bool $clean If true, clean the array.
     */
        public function getBodyAttributes(bool $clean=true) : array 
        {
            $r = new \ArrayObject($this->bodyAttribs);
            if ($clean) {
                $this->bodyAttribs = [];
            }
            
            return (array)$r;
        }
       
        
    /**
     * Get style files to load in <head>
     * 
     * @param bool $clean If true, clean the array.
     */
        public function getScriptsDeclaration(bool $clean=true) : array 
        {
            $r = new \ArrayObject($this->onReady);
            if ($clean) {
                $this->onReady = [];
            }
            
            return (array)$r;
        }
        
    /**
     * Get to append to the end of the page like boostrap files.
     * 
     * @param bool $clean If true, clean the array.
     */
        public function getAppendAfterDeclaration(bool $clean=true) : array 
        {
            $r = new \ArrayObject($this->appendAfter);
            if ($clean) {
                $this->appendAfter = [];
            }
            
            return (array)$r;
        }
        
        
    /**
     * Get styles declarations to load in <head>
     * 
     * @param bool $clean If true, clean the array.
     */
        public function getStylesDeclaration(bool $clean=true) : array 
        {
            $r = new \ArrayObject($this->headStyle);
            if ($clean) {
                $this->headStyle = [];
            }
            
            return (array)$r;
        }
        
        
        
    /**
     * Get style files to load in <head>
     *
     * @param bool $clean If true, clean the array.
     */
        public function getStyleFiles(bool $clean=true) : array 
        {
            $r = new \ArrayObject($this->filesCSS);
            if ($clean) {
                $this->filesCSS = [];
            }
            
            return (array)$r;
        }
                   
        
    /**
     * Add a file to insert in header.
     * 
     * @param $file Url of file to load.
     */
        public function addStyleFile( string $file ) : void 
        {
            $called = $this->getCountCall($file, $this->filesCSS) + 1;
            $this->filesCSS[$file] = $called;
        }
        
        
    /**
     * Check how much times $what is called in $where.
     * Used to debug.
     * 
     * @return int
     */
        private function getCountCall($what, $where) : int 
        {
            if ( !array_key_exists($what, $where) ) {
                return 0;
            }
            
            return $where[$what];
        }
        
        
    /**
     * Get view helper related to the current view.
     * @return ViewHelper
     */
        public function getViewHelper() : ViewHelper 
        {
            if ( is_null($this->viewHelper) ) {
                $this->viewHelper = new ViewHelper();
            }
            
            return $this->viewHelper;
        }        
        
    
    /**
     * Get page name.
     * Related to the view directory.
     *
     * @return string
     */
        public function getPage() : string {
            return $this->getViewHelper()->getPage();
        }
        
    
    /**
     * Get section name.
     * Related to the controller name.
     *
     * @return string
     */
        public function getSection() : string {
            return $this->getViewHelper()->getSection();
        }

    
    /**
     * Get format that the page will return.
     *
     * @return string
     */
        public function getFormat() : string {
            return $this->getViewHelper()->getFormat();
        }
        
        
    /**
     * Get input related to the CSRF Token.
     * (shortcut).
     * 
     * @return string
     */
        public function getTokenCSRF() : string {
            return \app\Helpers\SessionHelper::getInputTokenCSRF();
        }
}
?>