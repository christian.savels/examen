<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");


class MsgHelper
{
    private array $errors = [];
    private array $infos = [];
    private array $success = [];
    private array $warnings = [];
    
    
    /**
     * Define if system encountered errors.
     * @return bool
     */
        public function hasErrors() : bool {
            return count($this->errors) ? true : false;
        }
        
        
    /**
     * Get errors.
     * 
     * @param bool $clean Clean array before return if set on true.
     * 
     * @return array
     */
        public function getErrors(bool $clean=true) : array {
            return $this->get("errors", $clean);
        }
        
        
    /**
     * Get success.
     * 
     * @param bool $clean Clean array before return if set on true.
     * 
     * @return array
     */
        public function getSuccess(bool $clean=true) : array {
            return $this->get("success", $clean);
        }
        
        
    /**
     * Get infos.
     * 
     * @param bool $clean Clean array before return if set on true.
     * 
     * @return array
     */
        public function getInfos(bool $clean=true) : array {
            return $this->get("infos", $clean);
        }
        
        
    /**
     * Get warnings.
     * 
     * @param bool $clean Clean array before return if set on true.
     * 
     * @return array
     */
        public function getWarnings(bool $clean=true) : array {
            return $this->get("warnings", $clean);
        }
        
        
    /**
     * Record a message.
     * 
     * @param string $what
     * @param string $msg
     * @param mixed ...$vars
     */
        private function set( string $what, string $msg, ...$vars ) : void 
        {
            if ( isset($this->{$what}) )
            {
                $this->{$what}[] = Factory::getInstance()
                                    ->getTxt()
                                    ->get($msg, ...$vars);
            }
        }
        
        
    /**
     * Record a message.
     * @param string $msg
     * @param mixed ...$vars
     */
        public function setSuccess( string $msg, ...$vars ) : void {
            $this->set("success", $msg, ...$vars);
        }
        
        
    /**
     * Record a message.
     * @param string $msg
     * @param mixed ...$vars
     */
        public function setInfo( string $msg, ...$vars ) : void {
            $this->set("infos", $msg, ...$vars);
        }
        
        
    /**
     * Record a message.
     * @param string $msg
     * @param mixed ...$vars
     */
        public function setError( string $msg, ...$vars ) : void {
            $this->set("errors", $msg, ...$vars);
        }
        
        
    /**
     * Record a message.
     * @param string $msg
     * @param mixed ...$vars
     */
        public function setWarning( string $msg, ...$vars ) : void {
            $this->set("warnings", $msg, ...$vars);
        }

        
    /**
     * Bind messages to object.
     * 
     * @param object $to
     */
        public function bindMsgTo( object &$to ) 
        {
            foreach ( $this as $k => $v ) {
                $to->{$k} = $v;
            }
        }
        
    /**
     * Bind messages from object.
     * 
     * @param object $from
     */
        public function bindMsgFrom( object $from ) 
        {
            foreach ( $from as $k => $v ) {
                $this->{$k} = $v;
            }
        }
        
        
    /**
     * Get content of "$what" array.
     * 
     * @param string $what
     * @param bool $clean Clean array before return if set on true.
     * 
     * @return array
     */
        private function get( string $what, bool $clean=true ) : array
        {
            $r = [];
            if ( isset($this->{$what}) ) 
            {
                $r = new \ArrayObject($this->{$what});
                if($clean) {
                    $this->{$what} = [];
                }
            }
            
            return (array)$r;
        }  
        
        
}

