<?php
namespace app\Helpers;
use app\Objects\AObj;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Form fields helper.
 * 
 * @author chris
 *
 */
class FieldHelper
{

    /**
     * 
     * Make select to order items.
     * Related to object attributes.
     * 
     * @param AObj $obj
     * @param string $value Current value.
     * @param array $ignored Ignored object attributes.
     * @param string $attributes Attributes to apply on select.
     * 
     * @return string
     */
        public static function selectOrder( AObj $obj, string $value="", array $ignored=[], string $attributes="") 
        {
            $t = Factory::getInstance()->getTxt();
            $dirs = ['asc', 'desc'];
            $options = [self::makeOption(
                "", "OB_SELECT_HINT"
                )
            ];

            // Force ignore some attributes.
            $ignored = array_merge($ignored, ['attribs', 'content', 'link', 'editLink']);
            
            // Make a field for each object attribute.
            foreach ( $obj as $k => $v )
            {
                if ( !in_array($k, $ignored) ) 
                {
                    foreach ( $dirs as $dir )
                    {
                        $ident = strtoupper("OB_" . $k . "_" . $dir);
                        $val = $k . "." . $dir;
                        $options[] = self::makeOption($val, $ident, $value);
                    }
                }
            }
            
            if ( !str_contains($attributes, 'class=') ) {
                $attributes .= ' class="form-select" ';
            }

            return '<select ' . $attributes . ' >' . implode('', $options) . '</select>';
        }

    
    /**
     * Make a degree select.
     * 
     * @param string $name
     * @param string $id
     * @param string $value
     * @param string $class
     * @param bool $required
     * @param string $attributes
     * 
     * @return string
     */
        public static function makeFormations(string $name, string $id, string $value="",
            string $class="", bool $required=false,  $attributes="") : string
            {
                $options = [];
                $model  = new \app\Models\FormationModel();
                $formations = $model->getItems();
                $options[""] = Factory::getInstance()->getTxt()->get("NO_SELECTION");
                foreach ( $formations as $degree )  {
                    $options[$degree->id] = "(" . $degree->uid . ") " . $degree->title;
                }
                
                return self::makeSelect(
                    $id, $name, $value, $options, $hint='', $class, $required, $attributes
                );
        }
    
    /**
     * Make a degree select.
     * 
     * @param string $name
     * @param string $id
     * @param string $value
     * @param string $class
     * @param bool $required
     * @param string $attributes
     * 
     * @return string
     */
        public static function makeDegrees(string $name, string $id, string $value="",
            string $class="", bool $required=false,  $attributes="") : string
            {
                $options = [];
                $model  = new \app\Models\DegreeModel();
                $degrees = $model->getItems();
                $options[""] = Factory::getInstance()->getTxt()->get("NO_SELECTION");
                foreach ( $degrees as $degree )  {
                    $options[$degree->id] = $degree->title;
                }
                
                return self::makeSelect(
                    $id, $name, $value, $options, $hint='', $class, $required, $attributes
                    );
        }
    
    
    /**
     * Make rules fields panel.
     * 
     * @param RulesHelper $helper
     * 
     * @return string
     */
        public static function makeRules( RulesHelper $helper ) : string
        {
            $html = $groups = $rules = [];
            $submit = '<input type="submit" class="btn btn-success" value="' . Factory::getInstance()->getTxt()->get("SUBMIT_RULES") . '" />';
            $first = true;

            // Make groups html.
            foreach ( $helper->getGroups() as $g ) 
            {
                $gclss = "";
                $rclss = "";
                
                if ($first) 
                {
                    $first = false;
                    $gclss = "group-active";
                    $rclss = "rules-active";
                }

                // Protected groups
                if ( $g->isGuest )
                {
                    $gclss .= " is-guest";
                }

                $groups[] = self::makeRulesGroup($g, $gclss);
                $rules[] = self::makeRulesValues($g->id, $helper->getRules($g->id), $rclss);
            }
            
            $html[] = '<div class="rules-groups">' . implode('', $groups) . '</div>';
            $html[] = '<div class="rules-values">' . implode('', $rules) . '</div>';
            
            return '<div class="card shadow"><div class="rules-tree card-body">' 
                    . implode('', $html) 
                    . '</div><div class="card-footer text-center">' . $submit . '</div></div>';
        }
    
    
    /**
     * Make sub panel rules with values. (by group).
     * 
     * @param int $gid
     * @param object $rules
     * 
     * @return string
     */
        private static function makeRulesValues(int $gid, object $rules=null, string $rclass="") : string 
        {
            if ( !is_null($rules) ) 
            {
                $time = time() + rand(1000000, 10000000);
                $html = [];
                if ( !empty($rclass) ) {
                    $rclass = ' class="' . $rclass . '" ';
                }
                
                foreach ( $rules as $rule => $canDo ) 
                {
                    $name = 'rules-' . $gid . '-' . $rule;
                    $id = $name . $time;
                    $label = self::getLabel("RULE_" . $rule, $id, false);
                    
                    $html[] = '<div data-pw-rule >' . $label . self::getYesNo($name, $id, $canDo ? 1 : 0) . '</div>';
                    
                    $time++;
                }
                
                return '<div data-pw-group-rules="' . $gid . '" ' . $rclass . ' >' . implode('', $html) . '</div>';
            }

            return "";
        }
        
    
    /**
     * Make group part of a rules field.
     * 
     * @param object $group
     * 
     * @return string
     */
        private static function makeRulesGroup(object $group, string $class="") : string 
        {
            if ( !empty($class) ) {
                $class = ' class="' . $class . '"';
            }
            return '<div data-pw-group="' . $group->id . '" ' . $class . ' >'
                      . $group->title
                    . '</div>';
        }
    
    
    /**
     * Return an input of $type.
     * 
     * @param string $name
     * @param string $id
     * @param string $value
     * @param string $type
     * 
     * @return string
     */
        public static function getInput( string $name, string $id=null, string $value="", string $class="", $type="text",
            bool $required=false, $attributes=null, string $hint=null)
        {            
            $type = 'type="' . $type . '"';
            $name = 'name="' . $name . '"';
            $class = 'class="' . $class . '"';
            $id = is_null($id) ? $name : 'id="' . $id . '"';
            $value = 'value="' . $value . '"';
            $required = $required ? 'required="true"' : '';             
            $attributes = self::makeAttributes($attributes);
            $hint = is_null($hint) ? '' : 'placeholder="' . Factory::getInstance()->getTxt($hint) . '"';
            
            return "<input $type $name $id $value $class $required $attributes $hint />";
        }
    
    /**
     * Return a textarea
     * 
     * @param string $name
     * @param string $id
     * @param string $value
     * @param string $type
     * 
     * @return string
     */
        public static function textarea( string $name, string $id=null, 
            string $value="", string $class="", 
            bool $required=false, $attributes=null, string $hint=null)
        {            
            $name = 'name="' . $name . '"';
            $class = 'class="' . $class . '"';
            $id = is_null($id) ? $name : 'id="' . $id . '"';
            $required = $required ? 'required="true"' : '';             
            $attributes = self::makeAttributes($attributes);
            $hint = is_null($hint) ? '' : 'placeholder="' . $hint . '"';

            return "<textarea $name $id $class $required $attributes $hint >" . $value . "</textarea>";
        }
        
        
    /**
     * Compile attributes.
     * 
     * @param $attributes
     * 
     * @return string
     */
        public static function makeAttributes($attributes=null, $addTo=null) : string
        {
            if ( is_null($attributes) ) {
                $attributes = [];
            } elseif ( is_string($attributes) ) {
                $attributes = [$attributes];
            } elseif ( is_object($attributes) ) {
                $attributes = (array)$attributes;
            }
            
            if ( !empty($addTo) ) 
            {
                if ( is_array($addTo) ) {
                    $addTo = implode(" ", $addTo);
                } elseif ( is_object($addTo) ) {
                    $addTo = implode(" ", (array)$addTo);
                }
                
                $attributes[] = $addTo;
            }

            $attributes[] = "pw-form-input";

            return implode(' ', $attributes);
        }
    
        
    /**
     * Make field label.
     * 
     * @param string $fieldname
     * @param string $fieldid
     * @param bool $required
     * 
     * @return string
     */
        public static function getLabel( string $fieldname, string $fieldid, bool $required=false) : string
        {
            $text = Factory::getInstance()->getTxt()->get("FIELD_" . strtoupper($fieldname));
            $required = $required ? ' *' : '';
            
            return '<label for="' . $fieldid . '">' . $text . $required . '</label>';
        }
        
        
    /**
     * Return select for countries, by continent.
     * 
     * @param string $name
     * @param string $id
     * @param string $selection
     * @param string $class
     * @param $attributes
     * 
     * @return string
     */
        public static function getCountries( string $name, string $id, string $selection, string $class="", $attributes=null) : string 
        {
            $html = [];
            $groups = [];
            $continents = CountriesHelper::getFullTree(true);
            $attributes = FieldHelper::makeAttributes($attributes);

            foreach ( $continents as $continent )
            {
                $options = [];
                
                foreach ( $continent->countries as $code => $country )
                {
                    $txt = $country->name . ' (' . $country->native . ')';
                    $options[] = self::makeSelectOption($code, $txt, $selection);
                }
                
                $groups[] = self::makeSelectGroup($continent->name, $options );
            }
            
            $html = '<select name="' . $name . '" id="' . $id . '" class="form-select ' . $class . '" ' . $attributes . ' >';
            $html .= self::makeSelectOption("", "", $selection);
            $html .= implode('', $groups);
            $html .= "</select>";
            
            return $html;
        }
        
        
    /**
     * Make a select option html.
     * 
     * @param string $value
     * @param string $txt
     * @param string $selection
     * 
     * @return string
     */
        public static function makeSelectOption( string $value, string $txt, string $selection ) : string 
        {
            $selected = $value==$selection ? 'selected' : '';
            
            return '<option value="' . $value . '" ' . $selected . '>' . $txt . '</option>';
        }
        
    /**
     * Make a group of select options.
     * 
     * @param string $name
     * @param array $options     * 
     * 
     * @return string
     */
        public static function makeSelectGroup( string $name, array $options ) : string {
            return '<optgroup label="' . $name . '">' . implode('', $options) . '</optgroup>';
        }
        
        
    
    /**
     * Make a select with available languages.
     *
     * @param string $selected
     *
     * @return string
     */
        public static function makeSelect( string $id, string $name, string $selectedValue,
         array $values, string $hint=null, 
            string $style=null, bool $required=false, $attributes=null) : string
        {
            if ( is_null($style) ) {
                $style = "";
            }
            
            if ( !str_contains($style, 'form-select') ) {
                $style .= ' form-select';
            }

            $required = $required ? 'required="true"' : '';            
            $attributes = self::makeAttributes($attributes);
            
            $html = [];
            $html[] = "<select name=\"$name\" id=\"$id\" class=\"$style\" $required $attributes >";
            $html[] = implode('', self::extractOptions($values, $hint, $selectedValue) );
            $html[] = "</select>";
            
            return implode('', $html);
        }
        
        
    /**
     * Make an <option> to use with a <select>.
     * 
     * @return string
     */
        public static function makeOption( string $value, string $txt, string $selection=null ) : string
        {
            if ( is_null($selection) ) {
                $selection = "";
            }
            
            $selected = $value==$selection ? "selected" : "";
            $txt = Factory::getInstance()->getTxt($txt);
            
            return "<option value=\"$value\" $selected >$txt</option>";
        }
        

    /**
     * Make options for a select element.
     * 
     * @return array
     */
        public static function extractOptions( array $values, string $hint=null, string $selection=null ) : array
        {
            $options = [];
            
            // Empty value (hint)
            if ( !is_null($hint) ) {
                $options[] = self::makeOption("", $hint, $selection);
            }
            
            foreach ( $values as $v => $txt ) {
                $options[] = self::makeOption($v, $txt, $selection);
            }
            
            return $options;
        }
        
        
    /**
     * Make a "Status" select.
     * (published, unpublished, ...)
     * 
     * @param string $name
     * @param string $id
     * @param int $value
     * @param string $class
     * @param bool $required
     * @param string $attributes
     * 
     * @return string
     */
        public static function makeStatusSelect(string $name, string $id, string $value, 
            string $class="", bool $required=false,  $attributes="", string $hint=null) : string 
        {
            $options = [];
            foreach ( \app\Enums\Status::cases() as $s ) {
                $options[$s->value] = $s->name;
            }
            
            return self::makeSelect( 
                $id, $name, $value, $options, $hint, $class, $required, $attributes
            );
        } 
        
     
    /**
     * Make a "UserStatus" select.
     * 
     * @param string $name
     * @param string $id
     * @param int $value
     * @param string $class
     * @param bool $required
     * @param string $attributes
     * 
     * @return string
     */
        public static function makeUserStatusSelect(
            string $name, string $id, string $value, 
            string $class="", bool $required=false,  $attributes="", string $hint=null
        ) : string 
        {
            $options = [];
            foreach ( \app\Enums\UserStatus::cases() as $s ) {
                $options[$s->value] = 'USER_ACCESS_' . $s->name;
            }
            
            return self::makeSelect(
                $id, $name, $value, $options, $hint, $class, $required, $attributes
            );
        } 
        

    /**
     * Make an "Access" select.
     * (published, root, ...)
     * 
     * @param string $name
     * @param string $id
     * @param int $value
     * @param string $class
     * @param bool $required
     * @param string $attributes
     * 
     * @return string
     */
        public static function makeAccessSelect(string $name, string $id, string $value, 
            string $class="", bool $required=false,  $attributes="", string $hint=null) : string 
        {
            $options = [];

            foreach ( \app\Enums\Access::cases() as $s ) 
            {
                // Ignore guest because it can't be assigned.
                if ( $s!= \app\Enums\Access::GUEST ) {
                    $options[$s->value] = 'ACCESS_' . $s->name;
                }
            }
            
            return self::makeSelect(
                $id, $name, $value, $options, $hint, $class, $required, $attributes
            );
        } 
        
        
    /**
     * Make a "Yes or No" select.
     * 
     * @param string $name
     * @param string $id
     * @param int $value
     * @param string $class
     * @param bool $required
     * @param string $attributes
     * 
     * @return string
     */
        public static function getYesNo(string $name, string $id, string $value, 
            string $class="", bool $required=false,  $attributes="", string $hint=null) : string 
        {
            $options = [
                0=>Factory::getInstance()->getTxt()->get("NON"),
                1=>Factory::getInstance()->getTxt()->get("OUI")
            ];
            
            return self::makeSelect(
                $id, $name, $value, $options, $hint, $class, $required, $attributes
            );
        }
        
        
    /**
     * Make appropriate field.
     * 
     * @param string $type
     * @param string $name
     * @param string $id
     * @param string $value
     * @param string $class
     * @param bool $required
     * 
     * @return string
     */
        public static function getField( string $type, string $name, string $id="", 
            string $value="", string $class="", bool $required=false,  $attributes=null, 
            $values=null, string $hint=null)
        {
            $factory = Factory::getInstance();
            
            // Add common form style.
            if ( $type != "checkbox") {
                $class = "form-control " . $class;
            }

            // Reset value for image field 
            if ( $type=="image" ) {
                $value = "";
            }             

            switch($type)
            {
                case 'select' :
                    return self::makeSelect($id, $name, $value, $values, $hint, $class, $required, $attributes);
                case 'lang' :
                    $langs = [];
                    foreach ( $factory->getTxt()->getLangs() as $lang ) {
                        $langs[$lang] = $lang;
                    }
                    return self::makeSelect($id, $name, $value, $langs, $hint, $class, $required, $attributes);
                case 'country' : 
                    return self::getCountries($name, $id, $value, $class, $attributes);
                case 'image' :
                    return self::getInput($name, $id, $value, $class, "file", $required, $attributes, $hint);
                case 'yesno' : 
                    return self::getYesNo($name, $id, $value, $class, $required, $attributes, $hint);
                case 'status' : 
                    return self::makeStatusSelect($name, $id, $value, $class, $required, $attributes, $hint);
                case 'userstatus' : 
                    return self::makeUserStatusSelect($name, $id, $value, $class, $required, $attributes, $hint);
                case 'access' : 
                    return self::makeAccessSelect($name, $id, $value, $class, $required, $attributes, $hint);
                case 'degrees' : 
                    return self::makeDegrees($name, $id, $value, $class, $required, $attributes);
                case 'formations' : 
                    return self::makeFormations($name, $id, $value, $class, $required, $attributes);
                case "textarea":
                case "html":
                    return self::textarea($name, $id, $value, $class, $required, $attributes, $hint);
                case "text":
                default : 
                    return self::getInput($name, $id, $value, $class, $type, $required, $attributes, $hint);
            }
        }




    
}

