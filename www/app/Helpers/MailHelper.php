<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

use PHPMailer\PHPMailer\PHPMailer;


/**
 * 
 * Mail helper.
 * 
 * @author chris
 *
 */
class MailHelper
{    
    /**
     * Send mail to each user (separately) using PHPMailer.
     * Will check if mailing has been activated in the configuration.
     * 
     * @param array $who
     * @param string $subject
     * @param string $content
     * 
     * @return bool
     */
        public static function mailTo(array $who, string $subject, string $content) : bool
        {
            $f = Factory::getInstance();

            if ( $f->cfg("mail.active") ) 
            {
                require_once $f->getRootPath() . DS . 'assets' . DS . 'php' . DS . 'PHPMailer' . DS . 'src' . DS . 'Exception.php';
                require_once $f->getRootPath() . DS . 'assets' . DS . 'php' . DS . 'PHPMailer' . DS . 'src' . DS . 'PHPMailer.php';
                require_once $f->getRootPath() . DS . 'assets' . DS . 'php' . DS . 'PHPMailer' . DS . 'src' . DS . 'SMTP.php';

                $mail = new PHPMailer;
                $mail->isSMTP();
                $mail->SMTPDebug = 0; // Set to "2" for test.
                $mail->Host = $f->cfg("mail.host");
                $mail->Port = $f->cfg("mail.port");
                $mail->SMTPAuth = true;
                $mail->Username = $f->cfg("mail.username");
                $mail->Password = $f->cfg("mail.password");
                $mail->setFrom($f->cfg("mail.from"), $f->cfg("site.name"));
                $mail->addReplyTo($f->cfg("mail.from"), $f->cfg("site.name"));
                $mail->Subject = $subject;
                //$mail->msgHTML(file_get_contents('message.html'), __DIR__);
                $mail->Body = $content;

                foreach ( $who as $u ) {
                    $mail->addAddress($u);
                }

                // Send
                $done = $mail->send();

                // Show errors only to a root.
                if ( !$done && $f->getUser()->isRoot() ) {
                    $f->setError($mail->ErrorInfo);
                }

                return $done;
            }

            $f->setWarning("MAIL_UNACTIVE");

            return false;
        }


    /**
     * Make the form to send an email.
     * 
     * @param string $action
     * @param string $method
     * 
     * @return string Html
     */
        public static function makeMailToForm(string $action, string $method="post", string $moreFields="") : string {
            return HtmlHelper::makeMailToForm($action, $method, $moreFields);
        }


}