<?php
namespace app\Helpers;
use app\Databases\DBFilters;

defined('_PWE') or die("Limited acces");

/**
 * Rules of the current context.
 */
class RulesHelper
{
    private string $context;
    private object $rulesUser;
    private object $rules;
    private array $roles = [];
    private array $userRoles = [];
    private bool $isRoot = false;
    private bool $isGuest = false;
    
    
    public function __construct(string $context, array $groups, \app\Objects\User $user=null) {
        $this->initRules($context, $groups, $user);
    }
    
    
    /**
     * Get full tree or rules related to a group.
     * @param int $gid 0 to return full tree.
     */
        public function getRules( int $gid=0 ) : object|null 
        {
            if ( $gid ) {
                return isset($this->rules->{$gid}) ? $this->rules->{$gid} : null;
            }

            return $this->rules;
        }
    
    
    /**
     * Define if an user can do something.
     * 
     * @param string $what
     * 
     * @return bool
     */
        public function canDo(string $what) : bool
        {
            if ( $this->isRoot ) {
                return true;
            }

            foreach ( $this->rules as $gid => $rules )
            {
                if ( in_array($gid, $this->userRoles) 
                    && isset($rules->{$what})
                    && $rules->{$what}
                ) {
                   return true; 
                }
            }

            return false;
        }
        
        
    /**
     * Define if the role id is a valid role.
     * 
     * @param int $rid
     * 
     * @return bool
     */
        public function isValidRole(int $rid) : bool {
            return array_key_exists($rid, $this->roles);
        }
        
        
    /**
     * Extract groups/rules tree related to current context 
     * (without values).
     * 
     * @return void
     */
        public function saveFromForm() : void
        {
            if ( !empty($this->rules) )
            {
                foreach( $this->rules as $gid => &$rules )
                {
                    foreach ( $rules as $rule => &$canDo )
                    {
                        $ident = "rules-" . $gid . "-" . $rule;
                        $vpost = array_key_exists($ident, $_POST) ? $_POST[$ident] : $canDo;
                        $canDo = $vpost ? true : false;                    
                    }
                }
                
                $this->saveRules();
            } 
            else {
                Factory::getInstance()->setError("RULES_BAD_CONTEXT");
            }
        }
        
        
    /**
     * Save rules tree into db.
     */
        private function saveRules() : void
        {
            $m = new \app\Models\RulesModel();
            $f = new DBFilters();
             $f->filters->isIn["context"] = [$this->context];
            
            $rules = new \stdClass();
             $rules->context = $this->context;
             $rules->cfg = json_encode($this->rules);
            
            if ( is_null($m->getItem($f)) ) 
            {
                $m->insertObject($rules);
                $done = true;
            } 
            else {
                $done = $m->updateObject($rules, "context");
            }
            
            if ($done) {
                Factory::getInstance()->setSuccess("RULES_SAVE_OK");
            } else {
                Factory::getInstance()->setWarning("RULES_SAVE_FAILED");
            }
        }
        
        
    /**
     * Get context rules from db.
     * @param string $context
     * 
     * @return object
     */
        private function getContextRules(string $context) : object|null 
        {
            $m = new \app\Models\RulesModel();
            $filters = new DBFilters();
             $filters->filters->isIn["context"] = [$context];
            
            return $m->getItem($filters);
        }
 
    
    /**
     * Init rules tree.
     * 
     * @param string $context
     * @param array $groups
     * @param \app\Objects\User $user
     * 
     * @return void
     */
        private function initRules(string $context, array $groups, \app\Objects\User &$user=null) : void
        {         
            // Bind groups
            foreach ( $groups as $g ) {
                $this->roles[$g->id] = $g;
            }

            $this->context = $context;
            $this->rules = new \stdClass();
            $this->rulesUser = new \stdClass();
            $path = Factory::getInstance()->path("form_rules") . $context . '.rules' . ".xml";
            
            $contextRules = $this->getContextRules($context); 
            if ( is_null($contextRules) ) {
                $contextRules = new \stdClass();
            } else {
                $contextRules = $contextRules->cfg;
            }

            $this->initUser($user);
            
            if ( file_exists($path) )
            {
                $xml = simplexml_load_file($path);
               
                foreach ( $groups as $g )
                {
                   $this->rules->{$g->id} = new \stdClass();
                   $this->rulesUser->{$g->id} = new \stdClass();
                   
                   if ( !isset($contextRules->{$g->id}) ) {
                       $contextRules->{$g->id} = new \stdClass();
                   }
                                      
                   foreach ( $xml->rule as $rule )
                   {
                       $name = (string)$rule["name"];
                       if ( !isset($contextRules->{$g->id}->{$name}) ) {
                           $contextRules->{$g->id}->{$name} = 0;
                       }
                       
                       $userCan = $this->isRoot || in_array($g->id, $this->userRoles);
                       $can = $contextRules->{$g->id}->{$name};
                       
                       $this->rules->{$g->id}->{$name} = $can;
                       $this->rulesUser->{$g->id}->{$name} = $userCan;
                   }
                }
            }
       }
       
       
   /**
    * Init user groups.
    * @param \app\Objects\User $user
    */
       private function initUser( \app\Objects\User &$user=null ) : void 
       {
           if ( is_null($user) ) {
               $user = Factory::getInstance()->getUser();
           }

           if ( $user->isGuest() && !count($user->getGroups()) ) {
                $user->groups = [$this->getGuestGroup()];
                $this->isGuest = true;
           } 

           if( !$user->isGuest() ) {
                $user->groups = array_merge($user->groups, [$this->getMemberGroup()]);
           }
           
           $this->userRoles = $user->getGroups();
           $this->isRoot = $user->isRoot();
       }


    /**
     * Get guest group id.
     * 
     * @return int Id of the guest role. 0=no role = need to init.
     */
       public function getGuestGroup() : int 
       {
            $groups = $this->getGroups(); 
            foreach ( $groups as $gr ) {
                if ($gr->isGuest) {
                    return $gr->id;
                }
            }

            return 0;
       }


    /**
     * Get member (loggedin) group id.
     * 
     * @return int Id of the members role (loggedin).
     */
       public function getMemberGroup() : int 
       {
            $groups = $this->getGroups(); 
            foreach ( $groups as $gr ) {
                if ($gr->isLoggedIn && $gr->protected) {
                    return $gr->id;
                }
            }

            return 0;
       }
    
    
    /**
     * Get available groups.
     * @return array
     */
       public function getGroups() : array {
           return $this->roles;
       }


    /**
    * Set roles (groups).
    * @param array $roles
    * @return void
    */
       public function setGroups(array $roles) : void {
            $this->roles = $roles;
       }
    
    
    /**
     * Get available groups.
     * @return array
     */
       public function getGroup(int $gid) : object|null {
           return array_key_exists($gid, $this->roles) 
                    ? $this->roles[$gid] : null;
       }
        
    
    /**
     * Get html related to rules configuration.
     * @return string (html)
     */
       public function makeRulesHtml(string $context, bool $enclose=true, bool $loadScripts=true) : string
       {
           $return = "";
           
           if ( !empty($context) )
           {
               $formID = "editRules" . $context;               
               $helper = new \app\Helpers\RulesHelper($context, $this->getGroups());
               $fields = \app\Helpers\FieldHelper::makeRules($helper);
               $fields .= \app\Helpers\FieldHelper::getInput("format", "reqFormat" . $context, "json", "", "hidden");
               $action = Factory::getInstance()->getUrl(\app\Enums\Page::JSON_RULES_SAVE, $context);
              
               // Scripts
               if ($loadScripts)
               {
                    Factory::getInstance()->addStyleFile("assets/css/form.rules.css");
                    Factory::getInstance()->addScriptFile("assets/js/form.rules.js");
                    Factory::getInstance()->addScriptDeclaration($this->makeScript($context, $formID));
                }

               // Insert CSRF Token
               $fields .= Factory::getInstance()->getTokenCSRF();
               $form = \app\Helpers\FormHelper::encloseFields(
                   $fields, $formID, $formID, $action, ' method="post" data-rules-form '
               );

               // Info about rules configuration.
               $info = $this->renderInfo();
               
               $return = $enclose ? $this->encloseRulesForm($info.$form) : $info.$form;
           }
           
           return $return;
       }


    /**
     * Get info related to the rules configuration.
     * 
     * @return string (html)
     */
       public function renderInfo() : string 
       {
            $txt = Factory::getInstance()->getTxt();
            $icon = IconsHelper::get("info", false, "me-2");
            $title = '<h4 class="alert-heading">' . $icon . $txt->get("RULES_INFO_TITLE") . '</h4>';
            $info1 = '<p>' . $txt->get("RULES_INFO1") . '</p>';
            $info2 = '<p class="m-0">' . $txt->get("RULES_INFO2") . '</p>';

            return '<div class="alert alert-info mt-3" role="alert">' . $title . $info1 . "<hr />" . $info2 .  '</div>';
       }
       
       
   /**
    * Make script to call with rules form..
    * 
    * @param string $context
    * @param string $formID
    * 
    * @return string
    */
       private function makeScript(string $context, string $formID) : string
       {
           return "PMRules = new PWFormRules({ " 
                    . " formID: '". $formID . "',"
                    . " context: '". $context . "'"
                    . "});";
               
       }
       
   
   /**
    * Enclose form in a collasped div + btn.
    * @param string $form
    * @return string
    */
       private function encloseRulesForm(string $form) 
       {
           return '<div class="pw-rules-part w-100 text-end">
                	<button 
                		class="btn btn-outline-info" 
                		type="button" 
                		data-bs-toggle="collapse" 
                		data-bs-target="#rulesPart" 
                		aria-expanded="false" 
                		aria-controls="rulesPart"
                	>' 
                	   . Factory::getInstance()->getTxt()->get("BTN_RULES") .
                  	'</button>
                	<div class="collapse text-start" id="rulesPart">'
                  	    . $form . 
                	'</div>
            	</div>';
       }
}

