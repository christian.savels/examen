<?php 
namespace app\Helpers;

defined('_PWE') or die("Limited acces");


class FileHelper
{
    
    /**
     * 
     * Apply image url on item.
     * 
     * @param object $o
     * 
     * @return void
     */
        public static function applyImage(object &$o, string $path, string $default) : void
        {
            // Image 
            $img = isset($o->attribs->image) ? $o->attribs->image->imgUrl : '';
            $ipath = Factory::getInstance()->getUrlFromPath($path);
           
            if ( empty($img) ) {
                $o->image =  Factory::getInstance()->getRootUrl() . $default;
            } else {
                $o->image = $ipath . $img;
            }
        }


    /**
     * Check if upload new image is needed.
     * Also check if remove old asked.
     * 
     * @param object $item
     * @param \app\Models\AModel $model
     * 
     * @return void
     */
        public static function uploaditemImgOnSave( object $item, \app\Models\AModel $model, string $path, string $fname )
        {
            $img = $item->attribs->image->imgUrl;
            $hasUpload = self::hasFileToUpload("imgUrl");            
            
            // Remove old 
            if ( !empty($img) && (Helper::getValue("remove_image", 0) || $hasUpload) ) {
                self::removeFile($path . $img);
                $img = "";
            }
            
            // Upload new
            if ($hasUpload)
            {
                $image = self::uploadImage
                (
                    "imgUrl", 
                    $path, 
                    $fname,
                    Factory::getInstance()->cfg("form.img_max_size"),
                    Factory::getInstance()->cfg("form.img_mime")
                ); 
                if ( !empty($image) ) {
                    $img = $image;
                }

                $item->attribs->image->imgUrl = $img;
                $item->attribs = json_encode($item->attribs);

                // Save new img ident on db.
                $model->saveObject($item);
            }
        }

    
    /**
     * Define if an upload is ready for a specific file.
     * 
     * @param string $ident
     * 
     * @return bool
     */
        public static function hasFileToUpload( string $ident ) : bool 
        {
            return isset($_FILES[$ident]) 
                    && isset($_FILES[$ident]["tmp_name"])
                    && !empty($_FILES[$ident]["tmp_name"]);
        }
    
        
    /**
     * Remove file.
     * 
     * @param string $path
     * 
     * @return bool
     */
        public static function removeFile( string $path ) : bool {
            return !is_dir($path) && is_file($path) && unlink($path);
        }
    

    /**
     * Upload image.
     * @param string $ident
     * @param string $toPath
     * @param string $forcedName
     * @param int $maxSize
     * @return string
     */
        public static function uploadImage( string $ident, string $toPath, string $forcedName=null, int $maxSize=0, array $mime=[] ) : string
        {
            $filename = "";

            if ( is_dir($toPath) ) 
            {            
                $factory = Factory::getInstance();
                            
                if ( isset($_FILES[$ident]) 
                      && isset($_FILES[$ident]["tmp_name"]) 
                      && isset($_FILES[$ident]["name"]) 
                      && $_FILES[$ident]["error"]===UPLOAD_ERR_OK
                ) {
                    if ( !$_FILES[$ident]["size"] ) {
                        $factory->setError("FILE_EMPTY", $_FILES[$ident]["name"]);
                    } elseif ( $maxSize && $_FILES[$ident]["size"]>$maxSize ) {
                        $factory->setError("FILE_TOO_BIG", $_FILES[$ident]["name"], $_FILES[$ident]["size"], $maxSize);
                    } elseif ( count($mime) && !in_array($_FILES[$ident]["type"], $mime) ) {
                        $factory->setError("BAD_MIME", $_FILES[$ident]["name"]);
                    }
                    else 
                    {
                        $ext = self::getRelatedExtension($_FILES[$ident]["type"]);
                        if ( !empty($ext) ) 
                        {
                            $name = !is_null($forcedName) ? $forcedName : $_FILES[$ident]["name"];
                            $finalPath = $toPath . $name . $ext;
                            if ( move_uploaded_file($_FILES[$ident]["tmp_name"], $finalPath) ) {
                                $filename = $name . $ext;
                            }
                        }
                    }
                }
            }
            
            return $filename;
        }
    
    
    /**
     * Get extension related to the mime.
     * @param string $mime
     * @return string
     */
        public static function getRelatedExtension(string $mime) : string
        {
            switch(strtolower($mime))
            {
                case 'image/gif' : 
                    return '.gif';
                case 'image/jpeg' : 
                    return '.jpg';
                case 'image/png' : 
                    return '.png';
                default : return "";
            }
        }
}