<?php
namespace app\Helpers;
use stdClass;

defined('_PWE') or die("Limited acces");


/**
 * 
 */
class JSONHelper
{
    public $done = false;
    public object $messages;
    public mixed $data = null;
    public mixed $pagination = null;
    public mixed $html = null;
    public mixed $filters = null;
    public mixed $redirect = null;
    public int $redirectDelay = 0;
    
    
    /**
     * Contructor.
     * @param bool $bindMsg If true, get msg from Factory.
     */
        public function __construct(mixed $done=null, mixed $data=null, PaginationHelper $pagination=null, bool $bindMsg=true) {
            $this->init($done, $data, $pagination, $bindMsg);
        }
            
    
    /**
     * Init helpeR.
     * 
     * @param mixed $done
     * @param mixed $data
     * @param bool $bindMsg
     */
        private function init(mixed $done=null, mixed $data=null, PaginationHelper $pagination=null, bool $bindMsg=true) 
        {
            $this->data = $data;
            $this->redirectDelay = (int)Factory::getInstance()->cfg("delay.redirect");

            $this->initPagination($pagination);
            $this->initDone($done, $bindMsg);
            $this->initMsg($bindMsg);
        }


    /**
     * 
     * Init pagination.
     * 
     * @param mixed $pagination
     * 
     * @return void
     */
        public function initPagination(PaginationHelper $pagination=null)
        {
            if ( !is_null($pagination) ) 
            {
                $this->pagination = new stdClass;
                $this->pagination->limit = $pagination->getLimit();
                $this->pagination->pages = $pagination->getPages();
                $this->pagination->count = $pagination->getCount();
                $this->pagination->text = Factory::getInstance()->getTxt()->get("PAG_LINK_PAGE");
            }
        }
        
    
    /**
     * Init done value.
     * 
     * @param mixed $done
     * @param bool $bindMsg
     */
        private function initDone( mixed $done, bool $bindMsg ) : void
        {
            if ( !is_null($done) ) {
                $this->done = $done;
            }
            elseif ( $bindMsg ) {
                $this->done = !Factory::getInstance()->hasErrors();
            }
        }
        
    
    /**
     * Init msg object.
     * @param bool $bind If true, get msg from Factory.
     */
        private function initMsg( bool $bind=false) 
        {
            $this->messages = new stdClass();
            
            if ($bind) {
                Factory::getInstance()->bindMsgTo($this->messages);
            }
            else 
            {
                $this->messages->infos = [];
                $this->messages->errors = [];
                $this->messages->success = [];
                $this->messages->warnings = [];
            }
        }
}

