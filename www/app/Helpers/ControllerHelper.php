<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

use Exception;

class ControllerHelper
{
    /**
     * Extract controller name from view value.
     * @param string $view
     * @return string
     */
        public static function extractName( string $view ) : string
        {
            $segments = explode("/", $view);
            return $segments[0];
        }
    
    
    /**
     * Get controller namespace related to the controller name.
     *
     * @param string $controllerName
     * @param bool $json True to load json controller.
     *
     * @return string | null
     */
        public function getNSController( string $controllerName, bool $json=false )
        {
            if ( ctype_alnum($controllerName) )
            {
                $pref = $json ? 'Json\\' : '';
                $controllerName = ucfirst($controllerName);
                $root = Factory::getInstance()->getRootPath();
                $nspace = '\\app\Controllers\\' . $pref . $controllerName . "Controller";
                $file = str_replace("\\\\", DIRECTORY_SEPARATOR, $nspace) . ".php";
                $path = $root . $file;
                
                return file_exists($path) ? $nspace : null;
            }
            
            return null;
        }
        
        
    /**
     * Get controller callable name.
     * Will check number of params. 
     * 
     * @param string $nspace
     * @param string $method
     * @param array $params
     * @throws \Exception
     * @return string|null
     */
        public function getController( string $nspace, string $method, array $params )
        {
            $callable = null;
            $c = null;
            $r = null;

            try 
            {
                $r = new \ReflectionMethod($nspace, $method);
                if ( count($params)>=$r->getNumberOfRequiredParameters() ) 
                {
                    if ( is_callable($method, true, $callable) && !empty($callable) ) {
                        $c = new $nspace($method);
                    }
                    else 
                    {
                        // TODO Not callable method
                        throw new Exception("Not callable controller method \n controller : " . $nspace . "\n method : " . $method );
                    }
                }
                else
                {
                    throw new Exception('PAS ASSEZ DE PARAMS !!! \n Required : ' . $r->getNumberOfRequiredParameters() 
                        . ' & given : ' . count($params));
                }
            } 
            catch (Exception $e) {
                throw new Exception('INVALID CONTROLLER TO LOAD : \n ' . $e->getMessage());
            }
            
            return $c;
        }
    
}

