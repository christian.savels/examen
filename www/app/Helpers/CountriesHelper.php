<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Countries/Continents
 * 
 * @author chris
 *
 */
class CountriesHelper
{
   private const FOLDER = "countries";
   private const FILE_CONTINENTS = "continents.json";
   private const FILE_COUNTRIES = "countries.json";
   private const FILE_LANGUAGES = "languages.json";
    
   
    /**
     * Get full contienents/countries tree.
     * 
     * @param bool $byContinent
     * 
     * @return object
     */
        public static function getFullTree( bool $byContinent=true ) : object
        {            
            if ( !$byContinent ) {
                return self::loadJSON(self::FILE_COUNTRIES);
            }
            
            $o = new \stdClass();
            $countries = self::loadJSON(self::FILE_COUNTRIES);                
            $continents = self::loadJSON(self::FILE_CONTINENTS);    

            foreach ( $continents as $contCode => $name )
            {
                $o->{$contCode} = new \stdClass();
                $o->{$contCode}->name = $name;
                $o->{$contCode}->countries = [];
                
                foreach( $countries as $countCode => $country )
                {
                    if ( $country->continent==$contCode ) {
                        $o->{$contCode}->countries[$countCode] = $country;
                    }
                }
            }        
        
            return $o;
        }


    /**
     * Get country object from its iso code.
     * 
     * @param string $iso Iso code to return.
     * 
     * @return object|null
     */
        public static function getCountry(string $iso) : object|null
        {
            $tree = self::getFullTree(0);

            return isset($tree->{$iso}) ? $tree->{$iso} : null;
        }


    /**
     * Get country objects from iso codes.
     * 
     * @param array $iso Iso codes to return.
     * 
     * @return array With iso as key.
     */
        public static function getCountries(array $iso) : array
        {
            $r = [];
            $tree = self::getFullTree(0);

            foreach ( $iso as $k )
            {
                if ( isset($tree->{$k}) ) {
                    $r[$k] = $tree->{$k};
                }
            }

            return $r;
        }
    
    
    /**
     * Return object related to the file.
     * 
     * @param string $path
     * @return object
     */
        private static function loadJSON(string $file) : object 
        {
            $path = Factory::getInstance()->path("json") . self::FOLDER . DS;
            return json_decode(file_get_contents($path . $file), false);
        }
    
}

