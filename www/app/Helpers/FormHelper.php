<?php
namespace app\Helpers;
use app\Objects\AObj;
use stdClass;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Form helper.
 * 
 * @author chris
 *
 */
class FormHelper
{      

    /**
     * 
     * Make an order by form.
     * 
     * @param string $formName
     * @param string $formID
     * @param string $formAction
     * @param AObj $obj
     * @param array $ignored
     * @param string $selectAttributes
     * 
     * @return string
     */
        public static function makeOrderBy(string $formName, string $formID, string $formAction, AObj $obj, string $value="", array $ignored=[], string $selectAttributes="")
        {
            $fields = FieldHelper::selectOrder($obj, $value, $ignored, $selectAttributes);

            return self::encloseFields($fields, $formName , $formID, $formAction);
        }


    /**
     * Get db contexts related to the context translations.
     * 
     * @param string $context
     * @param int $uid
     * @param string $file
     * @return array
     */
        public static function extractTranslableContexts(string $context, int $uid, string $file) : array
        {
            $tree = self::extractTree($file);
            $r = [];
            
            foreach ( $tree as $field )
            {
                if ( $field->translate ) {
                    $r[] = $context . '.' . $uid . '.' . $field->name;
                }
            }
            
            return $r;
        }
        
    
    /**
     * Save a common form.
     * 
     * @param \app\Objects\AObj $obj
     * @param \app\Models\AModel $model
     * @param array $xml
     * @param string $pk
     * @param array $ignoredFields
     * 
     * @return \app\Objects\AObj
     */
        public static function saveForm( 
            AObj $obj, 
            \app\Models\AModel $model, 
            array $xml,
            string $pk="id",
            array $ignoredFields=[]
        ) : AObj
        {
            $factory = Factory::getInstance();
            $langs = $factory->getTxt()->getLangs();
            $translated = []; // Fields to translate.
            $uroot = $factory->getUser()->isRoot();
            
            // Get edited role object.
            if ( empty($obj->id) ) 
            {
                $obj->id = Helper::getValue($pk, 0); // Item primary key.
                if ($obj->id) {
                    $obj->bindPublicVars( $model->getItemBy($pk, $obj->id) );
                }
            }
                    
            // Get fields to check.
            foreach ( $xml as $node )
            {
                if ( !in_array($node->name, $ignoredFields) 
                    && ( !$node->root || $uroot ) 
                ) 
                {
                    // Create attribs tree if needed.
                    if ( $node->attribs )
                    {
                        if ( !isset($obj->attribs) ) {
                            $obj->attribs = new stdClass;
                        }
                        if ( !isset($obj->attribs->{$node->attribs_tree}) ) {
                            $obj->attribs->{$node->attribs_tree} = new \stdClass();
                        }
                        if ( !isset($obj->attribs->{$node->attribs_tree}->{$node->name}) ) {
                            $obj->attribs->{$node->attribs_tree}->{$node->name} = null;
                        }
                    }
                    
                    // Translatable field.
                    if ( $node->translate )
                    {
                        foreach ( $langs as $lang )
                        {
                            $content = Helper::getValue($node->name . '-' . $lang, $node->default);
                            $required = $lang===Factory::getInstance()->cfg("lang.default");
                            $isEmpty = !strlen(trim($content));
                            
                            if ( $required && $isEmpty ) {
                                $factory->setError("REQUIRED_TRANSLATE", $factory->getTxt()->get("FIELD_" . $node->name));
                            }

                            // Only text value if not an html input.
                            if ( !$isEmpty && $node->type!="html" ) {
                                $content = htmlspecialchars(strip_tags($content));
                            }
                            
                            $o = new \app\Objects\Translated();
                             $o->required = $required;
                             $o->fk = $obj->id;
                             $o->name = $node->name;
                             $o->lang = $lang;
                             $o->content = $content;
                             $o->delete = $isEmpty;
                             
                            $translated[] = $o;
                        }
                    }
                    elseif ( $node->type!="image" )
                    {
                        $val = Helper::getValue($node->name, $node->default);
                        
                        // Check empty required fields.
                        if ( $node->required && !strlen(trim($val)) ) {
                            $factory->setError("REQUIRED_FIELD", $factory->getTxt()->get("FIELD_" . $node->name));
                        }  else {
                            $obj->{$node->name} = $val;
                        }
                    }
                }
            } 
            
            // Save item & assign id if new item & save ok.
            if ( !is_null($obj->attribs) && !is_string($obj->attribs) ) {
                $obj->attribs = json_encode($obj->attribs);
            }
            $saved = $model->saveObject($obj);
            if ( !$obj->id && $saved ) {
                $obj->id = $saved;
            }
                
            // Finalize translations
            $tmod = new \app\Models\TranslateModel();
            if ($obj->id && count($translated) )
            {            
                // Get existing 
                $contexts = [];
                $filters = new \app\Databases\DBFilters();
                 $filters->filters->isIn['context'] = [];
                 foreach ( $translated as $kt => $t ) 
                 { 
                    $context = $model->makeRoleIdent($obj->id, $t->name);
                    $translated[$kt]->fk = $obj->id;
                    $translated[$kt]->context = $context;
                    $contexts[$context] = 1; 
                 }
                 
                $filters->filters->isIn['context'] = array_keys($contexts);
                $translations = $tmod->getItems($filters); 
                
                foreach ( $translated as $t ) 
                {
                    $t->id = self::extractTranslationID($translations, $t);
                    if ( $t->id && $t->delete ) {
                        $tmod->deleteByPk($t->id);
                    } elseif ( !$t->delete ) {
                        $tmod->saveObject($t);
                    }
                }
            }
            
            return $obj;
        }
    
    
    /**
     * Get translation object id.
     * 
     * @param array $translations
     * @param \app\Objects\Translated $compare
     * 
     * @return number
     */
        private static function extractTranslationID( array $translations, \app\Objects\Translated $compare ) 
        {
            foreach ( $translations as $t )
            {
                if ( $t->context==$compare->context
                    && $t->lang==$compare->lang
                ) {
                    return $t->id;
                }
            }
            
            return 0;
        }
    
    
    /**
     * Make form with fields as content.
     * 
     * @param string $fields
     * @param string $formName
     * @param string $formID
     * @param string $action
     * @param string $attributes
     * 
     * @return string
     */
        public static function encloseFields(string $fields, string $formName , string $formID,
            string $action, string $attributes="") : string
        {
            if ( !str_contains($attributes, "method=") ) {
                $attributes .= ' method="post" ';
            }
            
            return '<form id="' . $formID . '"'
                    . ' name="' . $formName . '"'
                    . ' action="' . $action . '"'
                    . ' ' . $attributes
                    . ' >' . $fields . '</form>';
        }
    
    /**
     * 
     * @param string $formFile
     * @param object $values
     * @return array
     */
        public static function extractTree(string $formFile) : array
        {
            $fields = [];
            $path = Factory::getInstance()->path("form") . $formFile . ".xml";
            if ( file_exists($path) )
            {
                $xml = simplexml_load_file($path);                
                foreach ( $xml->fieldset as $fieldset )
                {
                    $fattribs = ((string)$fieldset["attribs"]=="true");
                    $ftranslate = ((string)$fieldset["translate"]=="true");
                    $fname = (string)$fieldset["name"];
                    $froot = ((string)$fieldset["root"]=="true");
                    
                    foreach ( $fieldset->field as $field )
                    {
                        $o = new stdClass();
                         $o->type = (string) $field["type"];
                         $o->name = (string) $field["name"];
                         $o->required = ((string) $field["required"] == "true");
                         $o->translate = ($ftranslate || (string) $field["translate"] == "true");
                         $o->attribs = ($fattribs || (string) $field["attribs"] == "true");
                         $o->attribs_tree = $o->attribs ? $fname : null;
                         $o->default = (string) $field["default"];
                         $o->root = ($froot || (string) $field["root"] == "true");

                        $fields[] = $o;
                    }
                }
            }
            
            return $fields;
        }
        
        
    /**
     * Extract a field value.
     * 
     * @param bool $fromAttribs
     * @param object $values
     * @param string $fieldsetname
     * @param string $fieldname
     * @param mixed $default
     * 
     * @return NULL|string
     */
        private static function extractValue( bool $fromAttribs, object $values, string $fieldsetname, string $fieldname, $default="" ) : string
        {
            $v = null;
            
            // Assign value
            if ( $fromAttribs && isset($values->attribs) )
            {
                if ( isset($values->attribs->{$fieldsetname})
                && isset($values->attribs->{$fieldsetname}->{$fieldname})
                ) {
                    $v = $values->attribs->{$fieldsetname}->{$fieldname};
                } elseif ( isset($values->attribs->{$fieldname}) ) {
                    $v = $values->attribs->{$fieldname};
                }
            }
            
            if ( is_null($v) )
            {
                if ( isset($values->{$fieldname}) ) {
                    $v = $values->{$fieldname};
                } else {
                    $v = $default;
                }
            }
            
            return $v;
        }

        
        
        

    /**
     * 
     * @param string $formFile
     * @param object $values
     * 
     * @return array
     */
        public static function extractFormFields(string $formFile, object $values=null, array $translations=[]) : array
        {
            $fields = [];
            $langs = Factory::getInstance()->getTxt()->getLangs();
            $path = Factory::getInstance()->path("form") . $formFile . ".xml";

            if ( file_exists($path) ) 
            {
                $xml = simplexml_load_file($path);
                if ( is_null($values) ) {
                    $values = new stdClass();
                }
                                
                // Prepare translations
                foreach ( $translations as &$t ) 
                {
                    $t->context = explode(".", $t->context);
                     $t->context = end($t->context);
                    $values->{$t->context . '-' . $t->lang} = $t->content;
                }
                                
                foreach ( $xml->fieldset as $fieldset )
                {
                    $fname = (string)$fieldset["name"];
                    $fattribs = ((string)$fieldset["attribs"]=="true");
                    $fields[$fname] = [];
                    $ftranslate = ((string)$fieldset["translate"]=="true");
                    $froot = ((string)$fieldset["translate"]=="true");
                    
                    foreach ( $fieldset->field as $field )
                    {
                        $attribs = ((string)$field["attribs"]=="true");
                        $name = (string)$field["name"];  
                        $required = ((string)$field["required"]=="true");
                        $default = (string)$field["default"];
                        $attributes = (string)$field["attributes"];
                        $translate = ((string)$field["translate"]=="true");
                        $root = ((string)$field["root"]=="true");
                        $type = (string)$field["type"];
                        $v = null;
                        $names = [];
                        $isRequired = [];
                        
                        if ( ( !$froot && !$root) || Factory::getInstance()->getUser()->isRoot() )
                        {
                            // Translatable field.
                            if ( $ftranslate || $translate )
                            {
                                foreach ( $langs as $lang ) 
                                {
                                    $names[] = $name . '-' . $lang;
                                    $isRequired[] = $lang==Factory::getInstance()->cfg("lang.default");
                                }
                            } else {
                                $names = [$name];
                                $isRequired[] = $required;
                            }
                            
                            foreach ( $names as $k => $name )
                            {
                                $label = '';
                                
                                // Assign value
                                $v = self::extractValue
                                ( 
                                    ($fattribs || $attribs ), 
                                    $values, $fname, $name, $default
                                );
    
                                // Make field
                                $fieldContent = FieldHelper::getField(
                                    $type,
                                    $name,
                                    $name,
                                    $v,
                                    (string)$field["class"],
                                    $isRequired[$k],
                                    $attributes
                                );
                                
                                // Make field label.
                                if ( $type!="hidden" ) {
                                    $label = FieldHelper::getLabel($name, $name, $isRequired[$k]);
                                }
                                
                                $fields[$fname][] = $label . $fieldContent;
                            }
                        }
                    }
                }
            }
        
        return $fields;
    }
    


    /**
     * 
     * Make from FormHelper::extractFormFields result.
     * 
     * @param array $fields
     * @return string
     */
        static public function makeFromExtractedFormFields( array $result, string $formName, string $formId, string $formAction ) : string 
        {
            $content = [];

            foreach ( $result as $section => $fields )
            {
                $content[] = '<div class="pw-form-fieldset fieldset-' . $section . '">';
                foreach ( $fields as $f ) {
                    $content[] = '<div class="pw-form-field">' . $f . '</div>';
                }
                $content[] = '</div>';
            }

            return self::encloseFields(
                implode('', $content),
                $formName, $formId, $formAction
            );
        }
 
}

