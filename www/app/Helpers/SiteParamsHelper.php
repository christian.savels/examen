<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Parameters related to the configuration.ini file.
 * 
 * @author chris
 *
 */
class SiteParamsHelper
{
    private const SEPARATOR = ';';

    private array $configuration;

    public function __construct() {
        $this->init();
    }


    /**
     * Init object.
     * @return void
     */
        private function init() {
            $this->configuration = parse_ini_file($this->getFilePath(), 1);
        }


    /**
     * Get path to the configuration.ini file.
     */
        private function getFilePath() : string {
            return Factory::getInstance()->getRootPath() . DS . "configuration.ini";
        }


    /**
     * Get value of configuration parameter.
     * @param string $what Searched attribute. Use "section.attribute" to get attribute from specific section.
     * @param mixed $default
     * @return mixed
     */
        public function cfg(string $what, $default=null) : mixed 
        {
            $what = explode(".", $what);

            // Get from specific section.
            if ( count($what)==2 )
            {
                if ( array_key_exists($what[0], $this->configuration)  
                    && array_key_exists($what[1], $this->configuration[$what[0]]) 
                ) {
                    return $this->configuration[$what[0]][$what[1]];
                } 
            } 
            // Search in whole tree et return first attribute with same name.
            else
            {
                foreach ( $this->configuration as $tree )
                {
                    foreach ( $tree as $attribute => $v )
                    {
                        if ( $attribute==$what[0] ) {
                            return $v;
                        }
                    }
                }
            }

            // TODO Attribut inexistant ! Log it...
            Factory::getInstance()->setError(
                "BAD_CONFIG_PARAM", implode(".", $what)
            );

            return $default;
        }


    /**
     * Get path related to $where.
     * 
     * @param string $where
     * @param bool $fromRoot True to prefix with root path.
     * @return string
     */
        public function path(string $where, bool $fromRoot=true) : string
        {
            $p = str_replace("/", DS, $this->cfg("path." . $where));
            return $fromRoot ? Factory::getInstance()->getRootPath() . DS . $p : $p;
        }


        public function save( array $data ) : bool
        {
            foreach ( $this->configuration as $section => $params )
            {
                if ( array_key_exists($section, $data) ) 
                {
                    foreach ( $params as $param => $value )
                    {
                        $fieldname = $this->getFieldName($section, $param, $value);

                        if ( array_key_exists($param, $data[$section]) )
                        {
                            if ( is_array($value) ) {
                                $this->configuration[$section][$param] = explode(self::SEPARATOR, $data[$section][$param][0]);
                            } else {
                                $this->configuration[$section][$param] = $data[$section][$param];
                            }
                        }


    /*
                        if ( array_key_exists($fieldname, $data) ) 
                        {
                            if ( is_array($value) ) {
                                $this->configuration[$section][$param] = explode(self::SEPARATOR, $data[$fieldname]);
                            } else {
                                $this->configuration[$section][$param] = $data[$fieldname];
                            }
                        } 
                        */
                    }
                }
            }

            if ( !$handle=fopen($this->getFilePath(), 'w') ) {
                $done = false;
            } 
            else 
            {
                $content = $this->convertConfiguration();
                $done = fwrite($handle, $content);
            }

            if ($done) {
                Factory::getInstance()->setSuccess("SAVE_OK");
            } else {
                Factory::getInstance()->setError("SAVE_FAILED");
            }

            return $done;
        }


    /**
     * Convert current configuration array to string.
     * Used to save updated configuration.ini tree.
     * 
     * @return string
     */
        private function convertConfiguration() : string
        {
            $out = "";

            foreach ( $this->configuration as $section => $params ) 
            {
                if ( !empty($out) ) {
                    $out .= "\r\n\r\n";
                }
                $out .= '[' . $section . ']';
                $out .= $this->appendParams($params);
            }

            return $out;
        }


    /**
     * Get stringified params.
     * @param array $params
     * @return string
     */
        private function appendParams(array $params ) : string
        {
            $out = "";

            foreach ( $params as $param => $value )
            {
                // String from array
                if ( is_array($value) ) 
                {
                    foreach ( $value as $v ) {
                        $out .= "\r\n" . $param . '[] = "' . $v. '"';;
                    }
                }
                // From string
                else {
                    $out .= "\r\n" . $param . ' = "' . $value . '"';
                }
            }

            return $out;
        }


    /**
     * 
     * Get formatted field name.
     * Use with form to edit site params.
     * 
     * @param string $section
     * @param string $param
     * @param mixed $value
     * 
     * @return string
     */
        public function getFieldName(string $section, string $param, mixed $value) : string 
        {
            $suf = is_array($value) ? '[]' : '';            
            return $section . '[' . $param . ']' . $suf;
        }

    /**
     * 
     * Get formatted field id.
     * Use with form to edit site params.
     * 
     * @param string $section
     * @param string $param
     * @param mixed $value
     * 
     * @return string
     */
        public function getFieldId(string $section, string $param) : string {
            return $section . '-' . $param;
        }


    /**
     * Get form to edit site configuration.
     * @return string
     */
        public function getForm() : string 
        {
            $t = Factory::getInstance()->getTxt();
            $h = [];
            $formUrl = Factory::getInstance()->getUrl(
                \app\Enums\Page::ADMIN_SAVE_CONFIG, null, true
            );

            // Alert
            $alert = '<p class="alert alert-warning">';
            $alert .= $t->get("EDIT_SITE_PARAMS_ALERT");
            $alert .= '</p>';
            $h[] = $alert;

             // Submit button.
             $h[] = FieldHelper::getInput(
                "submitParams1", "submitParams1", 
                $t->get("P_SUBMIT"), 
                "btn btn-lg btn-outline-success", "submit"
            );

            foreach ( $this->configuration as $section => $params )
            {
                $h[] = '<div class="section-params">';
                $h[] = '<h5>' . $t->get("P_SECTION_" . strtoupper($section)) . '</h5>';
                foreach ( $params as $param => $value )
                {
                    $type = str_contains($param, "pwd") ? "password" : "text";
                    $fieldid = $this->getFieldId($section, $param);
                    $fieldname = $this->getFieldName($section, $param, $value);
                    $v = is_array($value) ? implode(self::SEPARATOR, $value) : $value;
                   
                    $h[] = '<div class="pw-form-field">';
                    $h[] = FieldHelper::getLabel($fieldid, $fieldid);
                    $h[] = FieldHelper::getInput($fieldname, $fieldid, $v, "form-control", $type);
                    $h[] = '</div>';
                }

                $h[] = '</div>';
            }

            // Alert bis
            $h[] = $alert;
        
            // Submit button.
            $h[] = FieldHelper::getInput(
                "submitParams2", "submitParams2", 
                $t->get("P_SUBMIT"), 
                "btn btn-lg btn-outline-success", "submit"
            );

            return FormHelper::encloseFields(
                implode('', $h), "siteParamsForm", "siteParamsForm", $formUrl
            );
        }
}