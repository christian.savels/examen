<?php
namespace app\Helpers;
use DateTimeImmutable;

defined('_PWE') or die("Limited acces");

use \app\Enums\Page as Pg;

class Helper
{
    /**
     * Return only alpha numeric characters.
     * 
     * @param string $what
     * 
     * @return string
     */
        public static function alphaNumeric(string $what) : string {
            return preg_replace("/[^a-zA-Z0-9]/", "", $what);
        }


    /**
     * Get a formatted date.
     * 
     * @param string $date
     * @return string
     */
        public static function getDate(string $date) : string 
        {
            $date = new DateTimeImmutable($date);
            return $date->format(Factory::getInstance()->cfg("dates_format"));
        }


    /**
     * Redirect to the page.
     * 
     * @param string $location
     * 
     * @return void
     */
        public static function redirect( string $location=null, bool $p404=false ) : void
        {
            $response = null;
            if ( is_null($location) ) 
            {
                $location = Pg::PAGE_404;
                $p404 = true;
            }
            
            // Set reponse code
            if($p404) {
                $response = 404;
            }

            
            // Enqueue messages.
            $msg = new \stdClass();
            Factory::getInstance()->bindMsgTo($msg);
            $_SESSION['notify'] = $msg;
            
            header('Location: ' . $location, null, $response);
            exit;
        }
    
    
    /**
     * Define if string is a valid email.
     * 
     * @param string $str
     * 
     * @return bool
     */
        public static function isMail( string $str=null ) : bool {
            return $str!=null && filter_var($str, FILTER_VALIDATE_EMAIL);
        }
    
    
    /**
     * Get value from $_POST or $_GET.
     * Return $default if ident does not exist.
     * 
     * @param string $ident
     * @param $default
     * @return mixed
     */
        public static function getValue(string $ident, $default=null) : mixed
        {        
            if ( isset($_POST) && array_key_exists($ident, $_POST) ) {
                return $_POST[$ident];
            } 
            elseif ( isset($_GET) && array_key_exists($ident, $_GET) ) {
                return $_GET[$ident];
            } 
            
            return $default;
        }


    /**
     * 
     * Set item to show it in a list.
     * 
     * @param object $o
     * 
     * @return void
     */
        public static function applyListParams( object &$o, array $ignore=[] ) : void
        {
            foreach ( ["title", "description", "email"] as $k )
            {
                if ( !in_array($k, $ignore) ) 
                {
                    // Set max length.
                    $ident = "list." . $k . "_len";
                    $len = Factory::getInstance()->cfg($ident);
                    if ( isset($o->{$k}) && $len && strlen($o->{$k})>$len ) {
                        $o->{$k} = substr($o->{$k}, 0, $len) . '...';
                    }
                }
            }
        }
}

