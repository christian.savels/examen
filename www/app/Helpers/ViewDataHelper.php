<?php 
namespace app\Helpers;

defined('_PWE') or die("Limited acces");


class ViewDataHelper
{
    /**
     * DB Filters.
     * 
     * @param \app\Databases\DBFilters $dbFilters
     */
        public \app\Databases\DBFilters $dbFilters;

    /**
     * Page to show.
     * Used to reload items with filters/pagination.
     * 
     * @param \app\Enums\Page $page
     */
        public \app\Enums\Page $page;     
        
    /**
     * Related object (to show in the list)
     * 
     * @param \app\Objects\AObj $obj
     */
        public \app\Objects\AObj $obj;
      
    /**
     * If true, make section rules form.
     * 
     * @param bool $makeRules
     */
        public bool $makeRules = false;         

    /**
     * If true, make export link(s).
     * Links must be initialized.
     * 
     * @param bool $makeExport
     */
        public bool $makeExport = false;   

    /**
     * If true, make toolbar options.
     * 
     * @param bool $makeToolBar
     */
        public bool $makeToolBar = false;   

    /**
     * If true, make order by object.
     * 
     * @param bool $makeOrderBy
     */
        public bool $makeOrderBy = false;   

    /**
     * If true, make filters html form.
     * 
     * @param bool $makeFilters
     */
        public bool $makeFilters = false;   

    /**
     * If filters are currently active.
     * 
     * @param string|null $filtersAlert
     */
        public string|null $filtersAlert = null;   

    /**
     * Export html.
     * 
     * @param string|null $export
     */
        public string|null $export = null;   
        
    /**
     * Pagination.
     * 
     * @param \app\Helpers\PaginationHelper|null $pagination 
     */
    
        public \app\Helpers\PaginationHelper|null $pagination = null;
            
    /**
     * Filters (for html).
     * 
     * @param string|null $filters 
     */

        public string|null $filters = null;            

    /**
     * Filters form name.
     * 
     * @param string $filters_formName
     */
        public string $filters_formName = "filtersForm";            
    
    /**
     * If true, make section rules form.
     * 
     * @param array
     */
        public array $items = [];    
        
    /**
     * Items count.
     * 
     * @param int
     */
        public int $itemsCount = 0;

    /**
     * Ignored filters.
     * Used to generate html.
     * 
     * @param array
     */
        public array $filters_ignore = [];

    /**
     * Order by (for html).
     * @param object|null
     */
        public object|null $orderBy = null;

    /**
     * Order by attributes.
     * 
     * @param string 
     */
        public string|null $orderBy_attribs = "data-pw-order-by";

    /**
     * Order by ignored.
     * 
     * @param array 
     */
        public array $orderBy_ignore = [];

    /**
     * Order by
     * 
     * @param string 
     */
        public string $orderBy_id = "cobitems";

    /**
     * Order by form name.
     * 
     * @param string 
     */
        public string $orderBy_formName = "cobitems";

    /**
     * Order by form id.
     * 
     * @param string 
     */
        public string $orderBy_formID = "cobitems";

    /**
     * Url to get json export.
     * 
     * @param string|null
     */
        public string|null $exportUrlJSON = null;

    /**
     * Url to get csv export.
     * 
     * @param string|null
     */
        public string|null $exportUrlCSV = null;

    /**
     * Toolbar options.
     * 
     * @param \app\Helpers\ToolbarHelper|null
     */
        public \app\Helpers\ToolbarHelper|null $options = null;

    /**
     * Rules form.
     * 
     * @param string|null
     */
        public string|null $rules = null;



   
        public function __construct( \app\Objects\AObj $obj, \app\Enums\Page $page ) {
            $this->initObj($obj, $page);
        }


        private function initObj( \app\Objects\AObj $obj, \app\Enums\Page $page ) : void 
        {
            $this->obj = $obj;
            $this->page = $page;

            $this->pagination = new \app\Helpers\PaginationHelper();
            $this->dbFilters = new \app\Databases\DBFilters($this->pagination);
        }


    /**
     * Render view data. 
     * 
     * @param \app\Controllers\AController $controller Related controller.
     * @param bool $setCookies If true, save forms values into a cookie with the same name of the form.
     * 
     * @return void
     */
        public function init(\app\Controllers\AController $controller) : void
        {
            $factory = \app\Helpers\Factory::getInstance();
            $filtersHelper = $controller->getFiltersHelper($this->filters_formName);
            
            $this->dbFilters->filters->makeFromPost($filtersHelper);

            // Get pagination count.
            $this->dbFilters->behavior->count = true;
            $this->pagination->setCount($controller->getModel()->getItems($this->dbFilters));

            // Get items
            $this->dbFilters->behavior->count = false;
            $this->dbFilters->behavior->orderBy = new \app\Helpers\OrderByHelper($this->orderBy_formName);
            $this->items = $controller->getModel()->getItems($this->dbFilters);
            $this->itemsCount = count($this->items);
            
            // Order by elements.
            if ($this->makeOrderBy) 
            {
                $this->orderBy = new \stdClass;
                $this->orderBy->id = $this->orderBy_id;
                $this->orderBy->html = \app\Helpers\FormHelper::makeOrderBy
                (
                    $this->orderBy_formName, 
                    $this->orderBy_formID, 
                    $factory->getUrl($this->page, null, true),
                    $this->obj,
                    $this->dbFilters->behavior->orderBy->getVal(),
                    $this->orderBy_ignore, 
                    $this->orderBy_attribs                   
                );
            }

            // Filters related to xml file.
            if ($this->makeFilters)
            {
                $this->filters = $filtersHelper->render(
                    $this->filters_formName,
                    $factory->getUrl($this->page, null, true), 
                    $this->filters_ignore,
                    $this->makeOrderBy ? $this->orderBy->html : ''
                );
            }
            
            // Make alert (filters are active)
            $this->filtersAlert = $filtersHelper->renderAlert($this->dbFilters->filters->filtersActive);

            // Export 
            if ($this->makeExport && ( !empty($this->exportUrlJSON) || !empty($this->exportUrlCSV)) )
            {
                $this->export = \app\Helpers\ExportHelper::render(
                    $this->exportUrlJSON,
                    $this->exportUrlCSV,
                );
            }

            // Make page options.
            if ($this->makeToolBar) {
                $this->options = $controller->getToolbarManage();
            }

            // Make rules form
            if ($this->makeRules) {
                $this->rules = $controller->rules->makeRulesHtml($controller->getContext());
            }

            // Unset unused 
            unset($this->dbFilters);
        }
}