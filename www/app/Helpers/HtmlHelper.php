<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

/**
 * 
 * HTML Maker.
 * 
 * @author chris
 *
 */
class HtmlHelper
{
    public static function makeOffcanvas(string $id, string $title, string $linkTitle, string $content, bool $show=false) : string
    {
        $show = $show ? ' show' : '';
        $offcanvas = '<div class="offcanvas offcanvas-start ' . $show . '"'
        .' tabindex="-1"'
        .' id="' . $id . '"'
        .' aria-labelledby="' . $id . 'Label" >'
        .'<div class="offcanvas-header">'
        .'<h5 class="offcanvas-title" id="' . $id . 'Label">' . $title . '</h5>'
        .'<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="' . Factory::getInstance()->getTxt()->get("CLOSE") . '"></button>'
        .'</div>'
        .'<div class="offcanvas-body">' . $content . '</div>'
        .'</div>';
            
        // Collapse link
        $link = '<a class="btn btn-light btn-lg"'
            .' data-bs-toggle="offcanvas"'
            .' href="#' . $id . '"'
            .' role="button"'
            .' aria-controls="' . $id . '" >'
            . $linkTitle
            . '</a>';

        return $link . $offcanvas;
    }


    /**
     * Make an alert.
     * 
     * @param string $content Alert content.
     * @param string $style CSS To apply (like "alert-info").
     * @param bool $dismiss True = dismissable.
     * @param string $attributes More attributes.
     * 
     * @return string Html
     */
        public static function makeAlert(
            string $content, string $style="alert-info", bool $dismiss=false, string $attributes=""
        ) : string 
        {
            $h = "";
            $d = "";

            // Close button.
            if ($dismiss) 
            {
                $style .= " alert-dismissible fade show";
                $d = '<button type="button"'; 
                $d .= ' class="btn-close"';
                $d .= ' data-bs-dismiss="alert"'; 
                $d .= ' aria-label="' . Factory::getInstance()->getTxt("CLOSE_ALERT") . '"';
                $d .= '></button>';
            }

            // Make alert.
            $h .= '<div class="alert ' . $style . '"'; 
            $h .= ' role="alert" ' . $attributes; 
            $h .= '>';
            $h .= $content . $d;
            $h .= '</div>';

            return $h;
        }


    /**
     * Make a lang chooser.
     * 
     * @param array $langs List of languages.
     * 
     * @return string Html.
     */
        public static function makeLangChooser(array $langs) : string
        {
            $t = Factory::getInstance()->getTxt();
            $html = '<a class="nav-link dropdown-toggle pw-menu-drop-lang" href="#" role="button"' 
                    .' data-bs-toggle="dropdown" aria-expanded="false" >'
                    . IconsHelper::get("lang", 0, "text-main-color")
                    . $t->get("MENU_LANG")
                    . '</a>'
                    . '<ul class="dropdown-menu pw-menu-ul-lang">';

            foreach ( $langs as $lg )
            {
                $lang = new \app\Objects\PWLang($lg);   
                $html .= '<li>'
                        . '<a class="dropdown-item" href="#" data-pw-lang="' . $lang->code . '">'
                        . '<img src="' . $lang->icon . '" />'
                        . ' ' . $lang->countryTitle
                        . '</a>'
                        . '</li>';
            }

            $html .= '</ul>';

            return $html;
        }


    /**
     * Make link to collaspse element.
     * Based on Bootstrap 5.3.
     * 
     * @param string $target Id of target to collapse (without #).
     * @param string $title Text of the link.
     * @param bool $expanded True to expand on load.
     * 
     * @return string (html)
     
        public static function makeCollapseLink(string $target, string $title, string $linkStyle="", bool $expanded=false) : string
        {
            $ex = $expanded ? "true" : "false";

            return '<a class="' . $linkStyle . '"'
                    . ' data-bs-toggle="collapse"'
                    . ' href="#' . $target . '"'
                    . ' role="button"'
                    . ' aria-expanded="' . $expanded . '"'
                    . ' aria-controls="' . $target . '"'
                    . '>' . $title . '</a>';
        }
*/

    /**
     * Make the form to send an email.
     * 
     * @param string $action
     * @param string $method
     * 
     * @return string
     */
        public static function makeMailToForm(string $action, string $method="post", string $moreFields="") : string
        {
            $html = [];
            $html[] = '<form action="' . $action . '" method="' . $method . '">';
            $html[] = $moreFields;
            $html[] = FieldHelper::getInput("subject", null, '', 'form-control', 'text', true, null, Factory::getInstance()->getTxt()->get("MAIL_SUBJECT"));
            $html[] = FieldHelper::textarea("content", null, '', 'form-control textarea-mail', true, 'rows="10"', Factory::getInstance()->getTxt()->get("MAIL_BODY"));
            $html[] = '<button type="submit" class="btn btn-success btn-lg mx-auto" data-pw-submit >' . IconsHelper::get("mailto", 0, "fa-2x") . '</button>';
            $html[] = '<button type="button" class="btn btn-danger btn-lg mx-auto" data-pw-cancel >' . IconsHelper::get("cancel", 0, "fa-2x") . '</button>';
            $html[] = '</form>';

            return implode('', $html);
        }




    /**
     * Make a table.
     * 
     * @param array $thead
     * @param array $tbody
     * @param array $attributes
     * @return string
     */
        public static function makeTable( array $thead=[], array $tbody=[], array $attributes=[] ) : string
        {
            $table = ['<table ' . implode(' ', $attributes) . '>'];
            
            // Head
            if ( count($thead) ) 
            {
                $table[] = "<thead>";
                $table[] = "<tr>";
                foreach ( $thead as $h ) {
                    $table[] = "<th>" . $h .  "</th>";
                }
                $table[] = "</tr>";
                $table[] = "</thead>";
            }
            
            // Body
            $table[] = "<tbody>";            
            foreach ( $tbody as $columns )
            {
                $table[] = "<tr>";
                foreach ( $columns as $c ) {
                    $table[] = "<td>" . $c .  "</td>";
                }
                $table[] = "</tr>";
            }            
            $table[] = "</tbody>";        
            $table[] = '</table>';
            
            return implode('', $table);
        }
        
        
    /**
     * Make a confirmation box.
     * @param string $modalID
     * @return string
     */
        public static function confirmBox( string $modalID ) : string 
        {
            $t = Factory::getInstance()->getTxt();
    
            return '<div id="' . $modalID . '" class="pw-confirm alert alert-warning" data-pw-confirm="0"  >
                <h4 class="alert-heading" data-pw-confirm-title>' . $t->get("CONFIRM_TITLE") . '</h4>
                ' . $t->get("CONFIRM_DESC") . '
                <hr />
                <div data-confirm-content></div>
                <div class="text-start">
                <button data-pw-confirm-it class="btn btn-lg btn-success">' . $t->get("OUI") . '</button>
                <button data-pw-cancel-it class="btn btn-lg btn-danger">' . $t->get("NON") . '</button>
                </div>
                </div>';
        }
    
    
    /**
     * 
     * Make a simple list.
     * 
     * @param array $values
     * @param string $ulClass
     * @param string $liClass
     * 
     * @return string
     */
        public static function makeList(array $values, string $ulClass="list-group list-group-flush", string $liClass="list-group-item list-group-item-light") : string
        {
            $h = '';
            if (count($values) ) 
            {
                if ( strlen($ulClass) ) {
                    $ulClass = 'class="' . $ulClass . '"';
                }
                if ( strlen($liClass) ) {
                    $liClass = 'class="' . $liClass . '"';
                }

                foreach ( $values as $v ) 
                {
                    if ( is_object($v) ) {
                        $v = $v->title;
                    }
                    $h .= '<li ' . $liClass . '>' . $v . '</li>';
                }

                $h = '<ul ' . $ulClass . '>' . $h . '</ul>';
            }

            return $h;
        }
    
}

