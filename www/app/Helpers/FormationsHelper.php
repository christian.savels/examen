<?php
namespace app\Helpers;

defined('_PWE') or die("Limited acces");

/**
 * Formations helper.
 */
class FormationsHelper
{
    /**
     * Make a list item html.
     * 
     * @param \app\Objects\Formation $item
     * 
     * @return string Html.
     */
        public static function makeListItem(\app\Objects\Formation $item, string $tmpl="formation.list", bool $setBg=true) : string 
        {
            // Apply course background image.
            if ($setBg)
            {
                Factory::getInstance()->addStyleDeclaration(
                    '[data-pw-item="' . $item->id . '"] > .card-formation > .card-body {'
                    . "background-image: url('" . $item->image . "');"
                    . "}"
                );
            }

            return TemplatesHelper::get($tmpl, $item);
        }    
}