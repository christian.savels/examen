<?php 
namespace app\Databases;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Behavior properties of db query.
 * 
 * @author chris
 *
 */
class DbBehavior
{
    public bool $count = false;
    public bool $forceOR = false;
    public bool $translateTitle = false;
    public bool $translateDescription = false;
    public int $limit = 0;
    public int $offset = 0;
    public array $select = []; // If !count => select *.
    public array $join = [];   
    public bool $fetchAssoc = false;
    public bool $fetchColumn = false;
    public bool $joinGroups = false; // Used with users.

    public \app\Helpers\OrderByHelper|null $orderBy = null;
    
    
    public function __construct(\app\Helpers\PaginationHelper $pagination=null) {
        $this->init($pagination);
    }
    
    
    /**
     * Init filters behavior.
     * 
     * @param \app\Helpers\PaginationHelper $pagination
     */
        private function init(\app\Helpers\PaginationHelper $pagination=null) : void 
        {
            if ( !is_null($pagination) ) 
            {
                $this->limit = $pagination->getLimit();
                $this->offset = $pagination->getOffset();
            }
        }
}