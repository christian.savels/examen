<?php 
namespace app\Databases;

use app\Helpers\Factory;
use stdClass;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Filters properties of db where queries.
 * 
 * @author chris
 *
 */
class DbWhere
{
    public array $notIn = [];               // Key=field & value=possible values.
    public array $isIn = [];                // Key=field & value=array of possible values.
    public array $isNull = [];              // Value=field
    public array $isNotNull = [];           // Value=field
    public array $begins = [];              // Key=field & value=possible values.
    public array $contains = [];            // Key=field & value=array of possible values.
    public array $where = [];               // More where to apply.
    public bool $filtersActive = false;     // Define if filters had been used.
    

    /**
     * Used to return translated fields.
     * Do not confuse with $this->lang
     * that will force filter field lang=$this->lang.
     * 
     * @return array
     */
        public function getLangs() : array 
        {
            $f = Factory::getInstance();
            $t = new \app\Helpers\Text($f->cfg("lang.default"));
            return array_unique(
              array_merge([$f->getUser()->getLang()], $t->getLangs())
            );
        }
        
        
    /**
     * Bind filters from item filters.
     * 
     * @param \app\Helpers\FiltersHelper $filters
     * 
     * @return void
     */
        public function makeFromPost( \app\Helpers\FiltersHelper $filters ) : void
        {
            if ( is_null(($fpost = \app\Helpers\Helper::getValue("filters", null))) ) {
                $fpost = new stdClass;
            } else {
                $fpost = json_decode($fpost, false);
            }

            $toCook = new stdClass; // To record into the cookie.
            $fromCook = $this->getFromCookie($filters->getFormName());

            foreach ( $filters->getFilters() as $fname => $f )
            {
                $iname = $filters->makeInputName($fname); 
                $v = isset($fpost->{$iname}) ? $fpost->{$iname} : null;
            
                if ( is_null($v) && isset($fromCook->{$iname}) && !empty($fromCook->{$iname}) ) {
                    $v = $fromCook->{$iname};
                } 

                $toCook->{$iname} = $v;

                if ( !is_null($v) && strlen(trim($v)) )
                {                    
                    $this->filtersActive = true;

                    if ( empty($f->attribs) ) 
                    {
                        switch($f->type) 
                        {
                            case "search":
                                    $this->contains[$fname] = $v; 
                                break;
                            default:
                                    $this->isIn[$fname] = [$v];
                                break;
                        }
                    }
                    else 
                    {
                        /*
                         * NOTE :
                         * MySQL n'est PAS insensible à la casse lorsqu'on analyse
                         * un attribut JSON ! (seul cas que je connaisse).
                         * 
                         * ==> LOWER sur request
                         * ==> strtolower sur value recherchée.
                         * 
                         * Both !!!
                         */ 
                        
                        // Search in attribs tree.
                        $this->where[] = "LOWER(JSON_UNQUOTE(" 
                                        . "JSON_EXTRACT(a.attribs, '$." . $f->attribs . "." . $fname . "')" 
                                        . ")) LIKE " . Factory::getInstance()->getDBo()->quote(strtolower($v) . '%');
                    }
                }
            }

            // Record cookie values.
            if ( !empty($filters->getFormName()) ) {
                \app\Helpers\CookiesHelper::setValue($filters->getFormName(), json_encode($toCook) );
            }
        }


    /**
     * Get values related to the filters form from cookie.
     * 
     * @param string $formName Form name.
     * 
     * @return object
     */
        public function getFromCookie($formName) : object
        {
            $fromCook = \app\Helpers\CookiesHelper::getValue($formName, new stdClass);
            if ( is_string($fromCook) ) {
                $fromCook = json_decode($fromCook);
            }

            return is_string($fromCook) ? json_decode($fromCook) : $fromCook;
        }
} ?>