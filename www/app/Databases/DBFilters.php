<?php 
namespace app\Databases;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Filter properties must be valid db fields.
 * 
 * @author chris
 *
 */
class DBFilters
{
    public DbBehavior $behavior;
    public DbWhere $filters;
    
    public function __construct( \app\Helpers\PaginationHelper $pagination=null )
    {
        $this->behavior = new DbBehavior($pagination);
        $this->filters = new DbWhere();
    }
    
    
    
}