<?php
namespace app\Databases;

defined('_PWE') or die("Limited acces");

use PDO;
use PDOException;
use app\Helpers\Factory;


class Mysqli implements \app\Interfaces\iDb
{
    protected const CHARSET = "utf8";
    protected static PDO|null $db = null;
    
    
    /**
     * @return PDO|null
     */
    public static function connect() : PDO|null
    {
        if ( !self::$db )
        {
            $f = Factory::getInstance();
            
            try
            {
                self::$db = new PDO(
                    'mysql:dbname=' . $f->cfg("db.name")
                    . ';host=' . $f->cfg("db.host")
                    . ';charset=' . self::CHARSET, $f->cfg("db.user"), $f->cfg("db.pwd")
                    );
                self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch (PDOException $e) {
                die ('DB Error : ' . $e->getMessage());
            }
        }
        
        return self::$db;
    }
    
}

