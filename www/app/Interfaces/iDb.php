<?php
namespace app\Interfaces;

defined('_PWE') or die("Limited acces");

use PDO;
use PDOException;

interface iDb
{
    /**
     * Get connected DB object.
     * @return PDO|null
     */
        public static function connect() : PDO|null;
}

