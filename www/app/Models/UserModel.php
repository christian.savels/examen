<?php
namespace app\Models;

use app\Databases\DbFilters;

defined('_PWE') or die("Limited acces");


class UserModel extends AModel
{
    protected string $table = "pw_user";
    
    
    public function __construct() {
        parent::__construct("user");
    }

    
    /**
     * Get list of users related to a formation.
     * 
     * @param int $fid FOrmation id.
     * @param bool $registered IF true, return registered users (to the form.). False, not registered yet.
     * 
     * @return array
     */
        public function getRelatedToFormation( int $fid, bool $registered=true ) : array
        {
            $c = $registered ? "IN" : "NOT IN";
            $sq = "SELECT uid FROM " . $this->table_form_user
                . " WHERE fid=" . $fid;
            $f = new DBFilters();
             $f->filters->where[] = "a.id " . $c . " (" . $sq . ")";

            return $this->getItems($f);
        }
    
    
    /**
     * Get user enroled into a specific role.
     * 
     * Check if db context BEGINS with $contexts values.
     * 
     * @param string $contextLike
     * @param bool $userInRole If true, return users in group. If false, user not in group.
     * 
     * @return array
     */
        public function getRelatedToRoleContexts(string $contextLike, bool $userInRole=true ) : array 
        {
            $items = [];
            $q = "";
            
            // Get users enroled.
            if ( $userInRole )
            {
                $q = "SELECT r.context AS role, r.id as gid, r.context, u.*" 
                    . " FROM pw_role AS r"
                    . " INNER JOIN pw_role_user AS ru"
                    . " ON ru.roleid = r.id"
                    . " INNER JOIN pw_user AS u"
                    . " ON u.id = ru.userid"
                    . " WHERE r.context LIKE ?"
                    . " GROUP BY ru.roleid, u.id"
                    . " ORDER BY r.context ASC, u.lastname ASC"
                ;
            }
            // Get user non enroled.
            else 
            {
                $q = "SELECT u.* FROM pw_user AS u
                        WHERE u.id NOT IN((
                        	SELECT ru.userid
                        	FROM pw_role AS r
                        	INNER JOIN pw_role_user AS ru
                    		 ON ru.roleid=r.id
                        	WHERE r.context LIKE ?"
                       . " GROUP BY ru.roleid
                           ORDER BY u.lastname ASC
                        ));";
            }

            $ps = $this->db->prepare($q);
             $ps->execute([$contextLike . "%"]);
             
            $items = $ps->fetchAll(\PDO::FETCH_OBJ);
            foreach ( $items as &$user ) {
                $user = new \app\Objects\User($user);
            }  
            
            return $items;
        }
    
    
    /**
     * @override
     */
        public function getItems( DBFilters $filters=null, string $convertTo=null ) : array|int
        {        
            // No selection ?
            if ( !count($filters->behavior->select) ) {
                $filters->behavior->select[] = "a.*";
            }
            
            // Ignore if user assigned to roles ?
            if ( isset($filters->filters->notIn["role"]) && !empty($filters->filters->notIn["role"])) 
            {
                $roles = implode(",", $filters->filters->notIn["role"]);
                $filters->filters->where[] = "a.id NOT IN("
                                            . "(SELECT userid FROM " . $this->table_ugroup . " WHERE roleid IN (" . $roles . ")))"
                                    ; 
            }
            
            // Join groups ?
            if ( $filters->behavior->joinGroups ) {
                $this->setJoinGroupsQuery($filters);
            }
            
            // Get items.
            $items = parent::getItems($filters);
            
            // Do some tasks with items before return.
            if ( !$filters->behavior->count && !$filters->behavior->fetchColumn)
            {
                foreach ( $items as $k => &$item ) 
                {
                    // Decode json attribs.
                    if ( isset($item->attribs) ) 
                    {
                        $item->attribs = json_decode($item->attribs, false);
                        if ( is_null($item->attribs) ) {
                            $item->attribs = new \stdClass();
                        }
                    }
                    
                    // Groups.
                    if( isset($item->user_groups) ) 
                    {
                        $item->groups = explode(',', $item->user_groups);
                        unset($item->user_groups);
                    } 
                    else {
                        $item->groups = [];
                    }
                    
                    // Convert object
                    if ( !is_null($convertTo) ) {
                        $items[$k] = new $convertTo($item);
                    }
                }
            }
            
            return $items;
        }
    
        
    /**
     * Make query to join user groups.
     * @param DBFilters $filters
     */
        private function setJoinGroupsQuery(DBFilters &$filters) : void 
        {
            $q = "(select GROUP_CONCAT(roleid SEPARATOR ',')"
                . " FROM " . $this->table_ugroup
                . " WHERE userid=a.id"
                . ") AS user_groups";
            
            // Join groups
            if ( !in_array($q, $filters->behavior->select) ) {
                $filters->behavior->select[] = $q;
            }
        }
        
    
    /**
     * @override 
     */
        public function getItem( DBFilters $filters ) : object|null
        {            
            if ( !count($filters->behavior->select) ) {
                $filters->behavior->select[] = "a.*";
            }
                        
            // Join session
            $filters->behavior->select[] = "sess.sessionid AS token_session";
            $filters->behavior->select[] = "sess.time as token_time";
            $filters->behavior->join[] = "LEFT JOIN " . \app\Helpers\SessionHelper::TABLE . " AS sess"
                                        . " ON sess.userid=a.id" ;
            
            // Join groups
            $this->setJoinGroupsQuery($filters);
            
            return parent::getItem($filters);
        }
}

