<?php
namespace app\Models;
use app\Helpers\Factory;


defined('_PWE') or die("Limited acces");

use PDO;
use \app\Enums\ErrorCode as Err;
use \app\Databases\DbFilters;

abstract class AModel
{
    public readonly string $role;    
    protected readonly PDO $db;
    protected string $table;
    protected string $table_translate = "pw_translate";
    protected string $table_user = "pw_user";
    protected string $table_role = "pw_role";
    protected string $table_ugroup = "pw_role_user";
    protected string $table_form = "pw_formation";
    protected string $table_form_courses = "pw_formation_course";
    protected string $table_form_user = "pw_formation_user";
    protected array $tableSchema = [];
    
    
    protected function __construct(string $role) 
    {
        $this->db = Factory::getInstance()->getDBo();
        $this->role = $role;
    }
    
    
    /**
     * Get item with id.
     * 
     * @param int $uid
     * @return object|null
     */
        public function getById( int $uid ) : object|null {
            return $this->getItemBy("id", $uid);
        }
        
        
    /**
      * Apply query to get translated fields.
     */
        protected function applyGetTranslated( array $toTranslate, DBFilters &$filters )
        {
            if ( isset($filters->filters->isIn["id"]) && count($filters->filters->isIn["id"]) )
            {
                $langs = $this->arrayToString($filters->filters->getLangs());
                foreach ( $toTranslate as $section )
                {
                    $what = $this->makeRoleIdent($filters->filters->isIn["id"][0], $section);
                    $filters->behavior->select[] = $this->queryTranslate($what, $langs, $section);
                }
            }
        }


        protected function extractWhere( DBFilters $filters, array &$where, array &$values ) : void
        {
            $langs = $this->arrayToString($filters->filters->getLangs());

            // Init where(s).
            foreach ( $filters->filters as $pname => $pval )
            {
                if ( !is_null($pval) )
                {
                    if ( $pname=="contains" && count($filters->filters->{$pname}) )
                    {
                        foreach ( $pval as $field => $fval )
                        {
                            $fval = trim($fval);
                            if ( strlen($fval) )
                            {
                                if ( in_array($field, ['title', 'description', 'content']) ) 
                                {
                                    $where[] = $this->__queryTranslateByRelated($field, $langs) . ' LIKE ?';
                                    $values[] = '%' . $fval . "%";
                                }
                                elseif ( $this->isValidColumn($field) ) 
                                {
                                    $where[] = 'a.' . $field . " LIKE ?";
                                    $values[] = '%' . $fval . "%";
                                }
                            }
                        }
                    } 
                    elseif ( $pname=="begins" && count($filters->filters->{$pname}) )
                    {
                        foreach ( $pval as $field => $fval )
                        {
                            if ( $this->isValidColumn($field) ) 
                            {
                                $where[] = 'a.' . $field . " LIKE ?";
                                $values[] = $fval . "%";
                            }
                        }
                    } 
                    elseif ( ($pname=="isNull" || $pname=="isNotNull") && count($pval) )
                    {       
                        foreach ( $pval as $field )
                        {
                            if ( $this->isValidColumn($field) ) {
                                $where[] = 'a.' . $field . ($pname=="isNull" ? " IS NULL" : " IS NOT NULL");
                            }
                        }
                    }
                    elseif ( $pname=="notIn" && count($pval) )
                    {
                        foreach ( $pval as $field => $fvalues ) 
                        {
                            if ( count($fvalues) && $this->isValidColumn($field) )
                            {
                                $v = implode(',', array_fill(0, count($fvalues), '?') );
                                $this->appendValues($fvalues, $values);
                                $where[] = 'a.' . $field . " NOT IN(" . $v . ")";
                            }
                        }
                    }
                    elseif ( $pname=="isIn" && count($pval) )
                    {
                        foreach ( $pval as $field => $fvalues ) 
                        {
                            if ( count($fvalues) && $this->isValidColumn($field) )
                            {
                                $v = implode(',', array_fill(0, count($fvalues), '?') );
                                $this->appendValues($fvalues, $values);
                                $where[] = 'a.' . $field . " IN(" . $v . ")";
                            }
                        }
                    }
                    elseif ( $this->isValidColumn($pname) )
                    {
                        // Arrays
                        if ( is_array($pval) && count($pval) )
                        {
                            $v = implode(',', array_fill(0, count($pval), '?') );
                            $where[] = "a." . $pname . " IN(" . $v . ")";
                            $this->appendValues($pval, $values);
                        }
                        // String/numbers
                        elseif ( !is_array($pval) )
                        { 
                            $where[] = "a." . $pname . "=?";
                            $values[] = $pval;
                        }
                    } 
                    else
                    {
                        switch($pname) 
                        {
                            default : break;
                        }
                    }
                } 
            }
        }
    
    
    /**
     * Get items with column=value.
     * 
     * @param DBFilters $filters.
     * 
     * @return array|int
     */
        public function getItems( DBFilters $filters=null ) : array|int 
        {
            if ( is_null($filters) ) {
                $filters = new DBFilters();
            }
            
            $items = [];
            $selectWhat = ""; 
            $where = $filters->filters->where;
            $values = [];
                                   
            if ( $filters->behavior->count ) {
                $selectWhat = "count(*) as count";
            } 
            else 
            {
                // Forced select.
                if ( count($filters->behavior->select) ) {
                    $selectWhat = implode(",", $filters->behavior->select);
                } else {
                    $selectWhat = "*";
                }
            }
           
            // Init where(s).
            $this->extractWhere($filters, $where, $values);
            
            // Init final query
            $q = "SELECT " . $selectWhat . " FROM " . $this->table . " AS a";
            
            // Join queries.           
            foreach ( $filters->behavior->join as $j ) {
                $q .= ' ' . $j;
            }
                        
            // Apply where(s).
            if ( count($where) ) {
                $q .= " WHERE " . implode($filters->behavior->forceOR ? " OR " : " AND ", $where);
            }

            // Order by
            if ( !is_null($filters->behavior->orderBy) ) 
            {
                $col = $filters->behavior->orderBy->getCol();
                $dir = $filters->behavior->orderBy->getDir();
                if ( in_array($col, ['title', 'description']) || $this->isValidColumn($col) ) {
               
                    $q .= " ORDER BY " . $col . ' ' . $dir;
                }
            }
            
            // Limit/offset
            if ( !$filters->behavior->count
                && ($filters->behavior->limit || $filters->behavior->offset) 
            ) {
                $q .= " LIMIT " . $filters->behavior->offset . ", " . $filters->behavior->limit;
            }
            
            // Get items
            try {
                $p = $this->db->prepare($q);
                $p->execute($values);                
            } 
            catch (\Throwable $th) 
            {
                if ( !is_null($p->errorCode()) && $p->errorCode() > 0) 
                {
                    echo "\n\nError code : " . $p->errorCode() . "\n\n";
                    echo "\n\nQuery : " . $p->queryString . "\n\n" ;
                    echo "\n\ErrorInfos : " ;

                    var_dump($p->errorInfo());
                    
                    exit;
                }
            }

            $fetch = PDO::FETCH_OBJ;
            if ( $filters->behavior->fetchColumn ) {
                $fetch = PDO::FETCH_COLUMN;
            } elseif ( $filters->behavior->fetchAssoc ) {
                $fetch = PDO::FETCH_ASSOC;
            }
          
            $items = $p->fetchAll($fetch);

            //echo "------------------------------------------------------------------\n\n" . $p->queryString . "\n\n";
            
            // Return count
            if ( $filters->behavior->count ) {
                return count($items) ? $items[0]->count : 0;
            }

            return $items;
        }
        
        
    /*
     * Adding elements in order at the end of $to.
     * 
     * @param array $values Values to add at the end.
     * @param array $to Final array.
     */
        protected function appendValues( array $values, array &$to ) : void
        {
            foreach ( $values as $v ) {
                $to[] = $v;
            }
        }
    
    
    /**
     * Get item with column=value.
     * @param string $column
     * @param string $val
     */
        public function getItemBy( string $column, string $val ) : object|null
        {
            $f = new DBFilters();            
            if ( $this->isValidColumn($column) ) 
            {                
                $f->filters->isIn[$column] = [$val];
                $f->behavior->limit = 1;
                
                return $this->getItem($f);
            }
            
            return null;
        }
        
        
    /**
     * Get item related to filters.
     * 
     * @param DBFilters filters
     * 
     * @return object|null
     */
        public function getItem( DBFilters $filters ) : object|null
        {
            $filters->behavior->limit = 1;
            $items = $this->getItems($filters);    
            
            return count($items) ? $items[0] : null;
        }
    
    
   /**
    * Define if column is a valid column (check table schema).
    * @return bool
    */
        protected function isValidColumn( string $col=null ) : bool {
            return !empty($col) && array_key_exists($col, (array)$this->getTableSchema());
        }
    
    
    /**
     * Insert db item from object.
     * 
     * @param object $o  Data.
     * 
     * @return int Insert id.
     */
        public function insertObject( object $o ) : int
        {
            $insertid = 0;
            $prepare = $this->extractPrepare($o);
            if ( count($prepare->keys) ) 
            {
                $query = "INSERT INTO " . $this->table
                        . " SET " . implode(", ", $prepare->keys);
                
                $dbp = $this->db->prepare($query);
                if( $dbp->execute($prepare->values) ) {
                    $insertid = $this->db->lastInsertId();
                } else {
                    $this->log("insertObject", $dbp->errorCode(), $dbp->errorInfo() );
                }
            }
            
            return $insertid;
        }
        
        
    /**
     * Save object to db.
     */
        public function saveObject( object $o, string $pk="id" ) : int 
        {
            if ( $o->{$pk} ) {
                return $this->updateObject($o, $pk) ? $o->id : 0;
            } else {
                return $this->insertObject($o);
            }
        }
        
        
    
    
    /**
     * Update db item from object.
     * 
     * @param object $o  Data.
     * @param string $pk Primary key name.
     * 
     * @return bool
     */
        public function updateObject( object $o, string $pk="id" ) : bool
        {
            $done = false;
            $prepare = $this->extractPrepare($o, $pk);

            if ( count($prepare->keys) && !is_null($prepare->pkValue) && $this->isValidColumn($pk) ) 
            {
                $query = "UPDATE " . $this->table
                         . " SET " . implode(", ", $prepare->keys) 
                         . " WHERE " . $pk . "=?"
                         . " LIMIT 1";
                
                $prepare->values[] = $prepare->pkValue;
                $dbp = $this->db->prepare($query);
                $done = $dbp->execute((array)$prepare->values);
                                
                if (!$done) {
                    $this->log("updateObject", $dbp->errorCode(), $dbp->errorInfo());
                }
            }            
            
            return $done;
        }
        
    
    /**
     * 
     * @param object $o
     * @param string $pk Set to "null" to use with insert.
     * @return object
     */
        protected function extractPrepare(object $o, string $pk=null) : object
        {
            $r = new \stdClass();
              $r->pkValue = null;
              $r->keys = [];
              $r->values = [];
            
            // Check valid keys.
            foreach ( $o as $k => $v )
            {
                if ( !is_null($pk) && $k==$pk ) {
                    $r->pkValue = $v;
                }
                elseif ( $this->isValidColumn($k) )
                {
                    $r->keys[] = $k . "=?";
                    $r->values[] = $v;
                }
            }
            
            return $r;
        }
        
        
    /**
     * Get table schema.
     * @return object
     */
        public function getTableSchema() : array
        {
            if ( !count($this->tableSchema) ) 
            {
                $q =
                "SELECT COLUMN_NAME"
                . " FROM INFORMATION_SCHEMA.COLUMNS "
                . " WHERE TABLE_SCHEMA=?" 
                . " AND TABLE_NAME=?" 
                ;
                
                $p = $this->db->prepare($q);
                $p->execute([Factory::getInstance()->cfg("db.name"), $this->table]);
                $columns = $p->fetchAll();
                foreach ( $columns as $c ) {
                    $this->tableSchema[$c["COLUMN_NAME"]] = null;
                }
                
                if ( !count($this->tableSchema) ) { // Log table does not exists.
                    $this->log("getTableSchema", Err::DB_INVALID_TABLE->value, $p->errorInfo() );
                }
            }
            
            return $this->tableSchema;
        }
        
        
    /**
     * Log errors
     * @param string $from
     * @param string $errorCode
     * @param array $errorInfo
     */
        private function log( string $from, mixed $errorCode, array $errorInfo ) : void
        {
            if ( $errorCode instanceof Err ) {
                $errorCode = $errorCode->value;
            }
            
            $from = get_class($this) . '::' . $from;
            \app\Helpers\LogsHelper::logDbErrors(
                $from,
                $errorCode,
                $errorInfo
            );
        }
        
        
    /**
     * Return quoted value.
     * 
     * @param string $value
     * 
     * @return string
     */
        protected function q(string $value) : string {
            return $this->db->quote($value);
        }
        
        
    /**
     * Make formated role ident.
     */
        public function makeRoleIdent( int $id, string $section=null ) : string
        {
            $a = [$this->role, $id];
            if ( !is_null($section) ) {
                $a[] = $section;
            }
            
            return implode(".", $a);
        }
        
        
    /**
     * Quote array values & return stringyfied value..
     * 
     * @param array $toQuote
     * 
     * @return string
     */
        protected function arrayToString( array $toQuote ) : string
        {
            foreach ( $toQuote as &$tq ) {
                $tq = $this->q($tq);
            }
            
            return implode(",", $toQuote);
        }
        
   
    /**
     * Delete item(s) related to the filters.
     */
        public function delete(DBFilters $filters, string $table=null) : int
        {
            $count = 0;
            $where = [];
            $values = [];
            
            // Init table.
            if ( is_null($table) ) {
                $table = $this->table;
            }

            // Apply where queries.
            $this->extractWhere($filters, $where, $values);

            // Secured, only query with where clauses is executed.
            // to prevent delete all table entries.
            if ( count($where) ) 
            {
                $where = implode(" AND ", $where);
                $q = ["DELETE FROM " . $table . " AS a"];
                 $q[] = "WHERE " . $where;

                $p = $this->db->prepare(implode(' ', $q));
                $p->execute($values);
                 
                $count = $p->rowCount();
            }

            return $count;
        }

        
    /**
     * Delete item with primary key.
     */
        public function deleteByPk( mixed $pk, string $pkIdent="id", string $table=null) : int
        {
            if ( $this->isValidColumn($pkIdent) ) 
            {
                if ( !is_array($pk) ) {
                    $pk = [$pk];
                }

                // Init filters.
                $f = new DBFilters();
                 $f->filters->isIn[$pkIdent] = $pk;

                return $this->delete($f, $table);
            }
           
            return 0;
        }
        
        
    /**
     * Delete translations related to contexts.
     */
        public function deleteTranslated( array $contexts ) : int
        {
            $m = new TranslateModel();            
            $filters = new DBFilters();
             $filters->filters->isIn["context"] = $contexts;
            
            return $m->delete($filters);
        }
        
        
    /**
     * Make query to get translation.
     * 
     * @param string $what
     * @param string $langs
     * @param string $joinAlias
     * 
     * @return string
     */
        protected function queryTranslateByRelated( string $section, string $langs, string $joinAlias="", string $related="a.id", string $role=null) : string 
        {
            if ( empty($joinAlias) ) {
                $joinAlias = $section;
            }

            return $this->__queryTranslateByRelated($section, $langs, $related, $role) 
                                . ' AS ' . $joinAlias;
        }

        
    /**
     * 
     * Make common query to extract translation.
     * 
     * @param string $section
     * @param string $langs
     * @param string $related
     * @return string
     */
        protected function __queryTranslateByRelated(string $section, string $langs, string $related="a.id", string $role=null) : string
        {
            if ( empty($role) ) {
                $role = $this->role;
            }

            return '('
                . 'SELECT COALESCE(content, ' . $this->q("#No " .$section) . ')'
                . ' FROM ' . $this->table_translate
                . ' WHERE context = CONCAT(' . $this->q($role . '.') . ', ' . $related . ', ' . $this->q('.' . $section) . ')'
                . ' AND lang IN(' . $langs . ')'
                . ' ORDER BY field(lang, ' . $langs . ')'
                . ' LIMIT 1 '
                . ')';
        }
        
        
    /**
     * Make query to get translation.
     * 
     * @param string $what
     * @param string $langs
     * @param string $joinAlias
     * 
     * @return string
     */
        protected function queryTranslate( string $what, string $langs, string $joinAlias ) : string 
        {
            return '('
                    . 'SELECT content'
                    . ' FROM ' . $this->table_translate
                    . ' WHERE context=' . $this->q($what)
                    . ' AND lang IN(' . $langs . ')'
                    . ' ORDER BY field(lang, ' . $langs . ')'
                    . ' LIMIT 1 '
                . ') AS ' . $joinAlias;
        }
}

