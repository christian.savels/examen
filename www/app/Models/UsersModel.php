<?php
namespace app\Models;

use app\Databases\DbFilters;
use app\Objects\AObj;

defined('_PWE') or die("Limited acces");


class UsersModel extends AModel
{
    protected string $table = "pw_user";

    
    public function __construct() {
        parent::__construct("user");
    }

    
    /**
     * @override
     */
        public function getItems( DBFilters $filters=null, string $convertTo=null ) : array|int
        {        
            // No selection ?
            if ( !count($filters->behavior->select) ) {
                $filters->behavior->select[] = "a.*";
            }
            
            // Join groups ?
            if ( $filters->behavior->joinGroups ) {
                $this->setJoinGroupsQuery($filters);
            }

            // Is student ?
            $q = "( SELECT COUNT(*) FROM " . $this->table_ugroup . " 
                    WHERE userid=a.id 
                    AND roleid IN 
                        (SELECT id FROM " . $this->table_role . " WHERE context LIKE '%.students')
                    ) AS isStudent";
            $filters->behavior->select[] = $q;

            // Is teacher ?
            $q = "( SELECT COUNT(*) FROM " . $this->table_ugroup . "  
                    WHERE userid=a.id 
                    AND roleid IN 
                        (SELECT id FROM " . $this->table_role . " WHERE context LIKE '%.teachers')
                    ) AS isTeacher";
            $filters->behavior->select[] = $q;
            
            // Get items.
            $items = parent::getItems($filters);
            
            // Do some tasks with items before return.
            if ( !$filters->behavior->count )
            {
                foreach ( $items as $k => &$item ) 
                {
                    // Decode json attribs.
                    $item->attribs = json_decode($item->attribs, false);
                    if ( is_null($item->attribs) ) {
                        $item->attribs = new \stdClass();
                    }
                    
                    // Groups.
                    if( isset($item->user_groups) ) 
                    {
                        $item->groups = explode(',', $item->user_groups);
                        unset($item->user_groups);
                    } 
                    else {
                        $item->groups = [];
                    }
                    
                    // Convert object
                    if ( !is_null($convertTo) ) {
                        $items[$k] = new $convertTo($item);
                    }
                }
            }
            
            return $items;
        }
    
        
    /**
     * Make query to join user groups.
     * @param DBFilters $filters
     */
        private function setJoinGroupsQuery(DBFilters &$filters) : void 
        {
            $q = "(select GROUP_CONCAT(roleid SEPARATOR ',')"
                . " FROM " . $this->table_ugroup
                . " WHERE userid=a.id"
                . ") AS user_groups";
            
            // Join groups
            if ( !in_array($q, $filters->behavior->select) ) {
                $filters->behavior->select[] = $q;
            }
        }
        
    
    /**
     * 
     * @override 
     */
        public function getItem( DBFilters $filters ) : object|null
        {            
            if ( !count($filters->behavior->select) ) {
                $filters->behavior->select[] = "a.*";
            }
                        
            // Join session
            $filters->behavior->select[] = "sess.sessionid AS token_session";
            $filters->behavior->select[] = "sess.time as token_time";
            $filters->behavior->join[] = "LEFT JOIN " . \app\Helpers\SessionHelper::TABLE . " AS sess"
                                . " ON sess.userid=a.id" ;
            
            // Join groups
            $this->setJoinGroupsQuery($filters);
            
            return parent::getItem($filters);
        }
}

