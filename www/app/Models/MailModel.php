<?php
namespace app\Models;

use app\Databases\DbFilters;
use app\Objects\AObj;

defined('_PWE') or die("Limited acces");


class MailModel extends AModel
{
    public function __construct() {
        parent::__construct("mail");
    }

    
    /**
     * Get mails of all published users.
     */
        public function getMails() : array
        {        
            $q = "SELECT DISTINCT(u.email)"      
                . " FROM pw_user as u"
                . " WHERE u.published=1"
                ;

            $p = $this->db->prepare($q);
            $p->execute();
            
            return $p->fetchAll(\PDO::FETCH_COLUMN);
        }


    /**
     * Get mails from section.
     * 
     * @return array Array of mails.
     */
        public function getSection( string $section ) : array
        {        
            if ( in_array($section, ['teachers', 'students']) )
            {
                $q = "SELECT DISTINCT(u.email)"      
                    . " FROM pw_role as r"
                    . " INNER JOIN pw_role_user as ru"
                    . " ON ru.roleid = r.id "
                    . " INNER JOIN pw_user as u"
                    . " ON u.id = ru.userid "
                    . " AND u.published=1"
                    . " WHERE r.context LIKE " . $this->q("course.%." . $section)
                    ;

                $p = $this->db->prepare($q);
                $p->execute();
                
                return $p->fetchAll(\PDO::FETCH_COLUMN);
            }   
            return [];
        }
    
}

