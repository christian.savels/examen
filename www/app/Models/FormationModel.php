<?php
namespace app\Models;

use app\Databases\DbFilters;
use app\Helpers\Factory;

defined('_PWE') or die("Limited acces");


class FormationModel extends AModel
{
    protected string $table = "pw_formation";
    
    
    public function __construct() {
        parent::__construct("formation");
    }
        
                
   
    /**
     * @override
     */
        public function getItem( DBFilters $filters ) : object|null
        {
            if ( !count($filters->behavior->select) ) {
                $filters->behavior->select[] = "a.*";
            }
            
            //$this->applyGetTranslated(["title", "description", "content"], $filters);

            return parent::getItem($filters);
        }
  
        
    /**
     * @override
     */
        public function getItems( DBFilters $filters=null ) : array|int
        {
            if ( is_null($filters) ) {
                $filters = new DBFilters();
            }

            if ( !$filters->behavior->count )
            {               
                if ( !count($filters->behavior->select) ) {
                    $filters->behavior->select[] = "a.*";
                }
                $langs = $this->arrayToString($filters->filters->getLangs());

                // Join item translations.
                $toTranslate = ["title", "description", "content"];
                foreach ( $toTranslate as $tt )
                {
                    $q = $this->queryTranslateByRelated($tt, $langs);
                    // Avoid double entries (in case of reuse filters when user changes count value for ex.).
                    if ( !in_array($q, $filters->behavior->select) ) {
                        $filters->behavior->select[] = $q;
                    }
                }

                // Join degree title
                $qd = $this->queryTranslateByRelated(
                    "title", $langs, "degree_title", "a.degree", "degree"
                );
                if ( !in_array($qd, $filters->behavior->select) ) {
                    $filters->behavior->select[] = $qd;
                }

                // Current user is student ?
                $uid = $this->q(Factory::getInstance()->getUser()->id);
                $filters->behavior->select[] = "(SELECT COUNT(fu.uid) " 
                . " FROM " . $this->table_form_user . ' AS fu'
                . " WHERE fu.uid=" . $uid . " AND fu.fid=a.id) AS isStudent";
            }
            
            return parent::getItems($filters);
        }        
}

