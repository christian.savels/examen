<?php
namespace app\Models;

use app\Databases\DbFilters;
use PDO;

defined('_PWE') or die("Limited acces");


class FormationCoursesModel extends AModel
{
    protected string $table = "pw_formation_course";
    protected string $table_course = "pw_course";
    
    
    public function __construct() {
        parent::__construct("formation");
    }



    /**
     * Get pending formations id including the course.
     * 
     * @param int $cid Course id.
     * @param int $uid User id.
     * 
     * @return array Array of ids.
     */
        public function getPendingFormationsRelatedToCourse(int $cid, int $uid) : array 
        {
            $q = "SELECT a.fid FROM " . $this->table . " AS a"
                . " INNER JOIN " . $this->table_form_user . " AS fu"
                . " ON fu.fid=a.fid AND fu.uid=?"
                . " WHERE a.cid=?";
            $p = $this->db->prepare($q);
            $p->execute([$uid, $cid]);
            
            return $p->fetchAll(PDO::FETCH_COLUMN);
        }



    /**
     * Get all students roles related to the formation.
     * (= related courses roles).
     * 
     * @param int $fid Formation id.
     * 
     * @return array
     */
        public function getStudentsRoles(int $fid) : array 
        {
            $q = "SELECT r.id
                    FROM " . $this->table . " AS fc
                    INNER JOIN pw_role AS r
                    ON r.context = CONCAT(" . $this->q("course.") . ", fc.cid, " . $this->q('.students') . ")
                    WHERE fc.fid=?";

            $p = $this->db->prepare($q);
            $p->execute([$fid]);
            
            return $p->fetchAll(PDO::FETCH_COLUMN);
        }
}

