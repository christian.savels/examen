<?php
namespace app\Models;

use app\Databases\DbFilters;
use app\Objects\AObj;

defined('_PWE') or die("Limited acces");


class FooterModel extends AModel
{
    public function __construct() {
        parent::__construct("footer");
    }

    
    /**
     * @override
     */
        public function getItems( DBFilters $filters=null ) : array|int
        {        
            $q = "SELECT 'students' as role, COUNT(DISTINCT(u.id)) AS total"       // Count students
                . " FROM pw_role as r"
                . " INNER JOIN pw_role_user as ru"
                . " ON ru.roleid = r.id AND ru.published=1"
                . " INNER JOIN pw_user as u"
                . " ON u.id = ru.userid "
                . " AND u.published=1"
                . " WHERE r.context LIKE 'course.%.students'"
                . " UNION ALL "
                . " SELECT 'teachers' as role, COUNT(DISTINCT(ru.userid)) AS total "    // Count teachers
                . " FROM pw_role as r"
                . " INNER JOIN pw_role_user as ru"
                . " ON ru.roleid = r.id "
                . " INNER JOIN pw_user as u"
                . " ON u.id = ru.userid "
                . " AND u.published=1"
                . " WHERE r.context LIKE 'course.%.teachers'"
                . " UNION ALL "
                . " SELECT 'formations' as role, COUNT(f.id) AS total "                 // Count formations
                . " FROM pw_formation as f"
                . " WHERE f.published=1"
                . "  UNION ALL "
                . " SELECT 'courses' as role, COUNT(c.id) AS total"                     // Count courses
                . " FROM pw_course as c"
                . " WHERE c.published=1";

            $p = $this->db->prepare($q);
            $p->execute();
            
            return $p->fetchAll(\PDO::FETCH_OBJ);
        }
    
}

