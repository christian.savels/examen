<?php
namespace app\Models;

use app\Databases\DbFilters;

defined('_PWE') or die("Limited acces");


class DegreeModel extends AModel
{
    protected string $table = "pw_degree";
    protected string $table_form = "pw_formation";                                                          
   
    public function __construct() {
        parent::__construct("degree");
    }
    
    
    /**
     * @override
     */
    public function getItem( DBFilters $filters ) : object|null
    {
        if ( !count($filters->behavior->select) ) {
            $filters->behavior->select[] = "a.*";
        }
        
        $this->applyGetTranslated(["title", "description"], $filters);
        
        return parent::getItem($filters);
    } 
    
    
    /**
     * @override
     */
        public function getItems( DBFilters $filters=null ) : array|int
        {
            if ( is_null($filters) ) {
                $filters = new DBFilters();
            }
            if ( !count($filters->behavior->select) ) {
                $filters->behavior->select[] = "a.*";
            }
            $langs = $this->arrayToString($filters->filters->getLangs());
            
            // Join translations.
            $filters->behavior->select[] = $this->queryTranslateByRelated(
                "title", $langs
            );
            $filters->behavior->select[] = $this->queryTranslateByRelated(
                "description", $langs
            );
            
            // Count formations
            $filters->behavior->select[] = "(SELECT COUNT(id) FROM "
                . $this->table_form
                . " WHERE degree=a.id) as count_inside";                
                
            return parent::getItems($filters);
        }
        
    
}