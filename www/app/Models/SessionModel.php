<?php
namespace app\Models;
use app\Databases\DBFilters;

defined('_PWE') or die("Limited acces");


class SessionModel extends AModel
{
    protected string $table = "pw_session";
    
    public function __construct() {
        parent::__construct("session");
    }


    /**
     * Delete user session.
     * 
     * @param int $uid
     * @return int
     */
        public function logout(int $uid) : int
        {
            $f = new DBFilters();
             $f->filters->isIn['userid'] = [$uid];
             $f->behavior->limit = 1;

            return $this->delete($f);
        }
}

