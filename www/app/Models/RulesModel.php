<?php
namespace app\Models;

use app\Databases\DbFilters;

defined('_PWE') or die("Limited acces");


class RulesModel extends AModel
{
    protected string $table = "pw_config";
    
    
    public function __construct() {
        parent::__construct("config");
    }
    
    /**
     * @override
     */
        public function getItem( DBFilters $filters ) : object|null
        {
            $item = parent::getItem($filters);
            if ( !is_null($item) ) {
                $item->cfg = json_decode($item->cfg, false);
            }
            
            return $item;
        }
}

