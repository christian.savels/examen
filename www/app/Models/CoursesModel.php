<?php
namespace app\Models;

use app\Databases\DbFilters;
use app\Enums\Status;
use app\Helpers\Factory;
use PDO;

defined('_PWE') or die("Limited acces");


class CoursesModel extends AModel
{
    protected string $table = "pw_course";
    
    
    public function __construct() {
        parent::__construct("course");
    }


    /**
     * Get courses without assigned teacher.
     * 
     * @return array
     */
        public function getWithoutTeacher() : array
        {
            $filters = new DBFilters();
            $langs = $this->arrayToString($filters->filters->getLangs());

            $q = 'SELECT c.*,'
                . $this->queryTranslateByRelated('title', $langs, "title", "c.id") 
                . ' FROM pw_course AS c'
                . ' INNER JOIN pw_role AS r'
                . ' ON r.context = CONCAT(\'course.\', c.id, \'.teachers\')'
                . ' LEFT JOIN pw_role_user AS ru'
                . ' ON ru.roleid = r.id'
                . ' WHERE ru.roleid IS NULL AND c.published!=?';
            
            $p = $this->db->prepare($q);
            $p->execute([Status::ARCHIVED->value]);
                
            return $p->fetchAll(PDO::FETCH_OBJ);
        }


    /**
     * Get pending students.
     * 
     * @param int $course Default=0.
     * 
     * @return array If course!=0 Course pending, 0= all pending.
     */
        public function getPending(int $course=0) : array
        {
            $filters = new DBFilters();
            $langs = $this->arrayToString($filters->filters->getLangs());
            $values = [];
            
            if ($course) 
            {
                $w = '?';
                $values[] = $course;
            } 
            else {
                $w = '%';
            }

            $q = "SELECT "
                 . "u.id as userid,"
                 . "concat(u.lastname, ' ', u.firstname) as username," 
                 . "r.id as role,"
                 . "SUBSTRING_INDEX(SUBSTRING_INDEX(r.context, '.', 2), '.', -1) as courseId,"
                 . "("
                . 'SELECT content'
                . ' FROM ' . $this->table_translate
                . " WHERE context = CONCAT('course.', SUBSTRING_INDEX(SUBSTRING_INDEX(r.context, '.', 2), '.', -1), '.title')"
                . ' AND lang IN(' . $langs . ')'
                . ' ORDER BY field(lang, ' . $langs . ')'
                . ' LIMIT 1 '
                . ") as courseTitle"
                . " FROM pw_role_user AS ru "
                . " INNER JOIN pw_role AS r"
                . " ON r.id=ru.roleid AND r.context LIKE CONCAT('course.', '" . $w . "', '.students')"
                . " INNER JOIN pw_user AS u"
                . " ON u.id=ru.userid"
                . " WHERE ru.published=0";                
            
            $p = $this->db->prepare($q);
            $p->execute($values);
                
            return $p->fetchAll(PDO::FETCH_OBJ);
        }

    
    /**
     * Enrole users to courses related to a formation.
     * 
     * @param array $users Array of userids.
     * @param array $roles Array of roles.
     * 
     * @return int|bool Number of inserts or false.
     */
        public function enroleUsersToCourses(array $users, array $roles, int $status=1) : int|bool
        {
            if ( count($roles) && count($users) ) 
            {
                $q = "INSERT " . (!$status ? 'IGNORE': '' ) . " INTO " . $this->table_ugroup . "(roleid, userid, published) VALUES ";
                $sq = [];
                $values = [];

                foreach ( $roles as $r ) 
                {
                    foreach ( $users as $uid ) 
                    {
                        $sq[] = "(?,?,?)";
                        $values[] = $r;
                        $values[] = $uid;
                        $values[] = $status;
                    }
                }

                $q .= implode(",", $sq);      
                
                /*
                 * Dans le cas où la validation se fait alors qu'il y a déjà une demande en cours.
                 * Possible lorsque la validation se fait via l'enrolement à une formation.
                 */ 
                    if($status) {
                        $q .= "ON DUPLICATE KEY UPDATE published=?";
                        $values[] = $status;
                    }

                $p = $this->db->prepare($q);
                $done = $p->execute($values);

                return $done ? $p->rowCount() : false;
            }

            return 0;
        }



    /**
     * Get related users.
     * 
     * @param int $cid Course id.
     * @param string $target "teachers" or "students".
     * 
     * @return array
     */
        public function getRelatedUsers(int $cid, string $target) : array 
        {
            if ( in_array($target, ["teachers", "students"]) ) 
            {
                $q = "SELECT * FROM " . $this->table . " AS a"
                    . " INNER JOIN " . $this->table_role . " AS r" 
                    . " ON r.context = CONCAT(" . $this->q("course.") . ", a.id, " . $this->q("." . $target) . ")" 
                    . " INNER JOIN " . $this->table_ugroup . " AS ru"
                    . " ON ru.roleid=r.id" 
                    . " INNER JOIN " . $this->table_user . " AS u"
                    . " ON u.id = ru.userid"
                    . " WHERE a.id=?" 
                    . " GROUP BY u.id";

                $p = $this->db->prepare($q);
                $p->execute([$cid]);
                    
                return $p->fetchAll(PDO::FETCH_OBJ);
            }

            return [];
        }


    /**
     * Get courses where user is enroled.
     * 
     * @param int $uid User id.
     * @param string $what Can be "students" or "teachers"
     * 
     * @return array
     */
        public function getRelatedToUser(int $uid, string $what="students") 
        {
            if ( in_array($what, ["students", "teachers"]) ) 
            {
                $dbf = new DBFilters();
                $langs = $this->arrayToString($dbf->filters->getLangs());
                $qt = $this->__queryTranslateByRelated("title", $langs, "c.id") . " AS title";
                $qt1 = $this->__queryTranslateByRelated("title", $langs, "c1.id") . " AS title";

                $q = "SELECT c.*, IF(ru.published=0, 1, 0) AS pending, " . $qt 
                    ." FROM pw_course AS c"
                    ." INNER JOIN pw_role AS r"
                    ."  ON r.context = CONCAT(" . $this->q("course.") . ", c.id, " . $this->q("." . $what) . ")"
                    ." INNER JOIN pw_role_user AS ru"
                    ."  ON ru.userid=?"
                    ."  AND ru.roleid=r.id";

                $p = $this->db->prepare($q);
                $p->execute([$uid]);
                    
                return $p->fetchAll(PDO::FETCH_OBJ);
            }

            return [];
        }        


    /**
     * Get courses related to the formation.
     * 
     * @param int $formationID Formation id.
     * @param DbFilters $filters Filters.
     * 
     * @return int|array
     */
        public function getFromFormation( int $formationID, DBFilters $filters ) : mixed
        {
            $filters->filters->where[] = "a.id IN "
                                        . "("
                                        . "SELECT cid FROM " . $this->table_form_courses
                                        . " WHERE fid=" . $this->q($formationID)
                                        . ")";

            return $this->getItems($filters);
        }
    

    /**
     * 
     * Get courses related to a formation.
     * 
     * @param int $formationID
     * @param bool $coursesInForm If true, return courses already assigned to the formation.
     * 
     * @return array
     */
        public function getRelatedToFormation(int $formationID, bool $coursesInForm=true ) : array 
        {
            $items = [];
            $q = "";
            $filters = new DBFilters();
            $filters->filters->makeFromPost(new \app\Helpers\FiltersHelper("courses.course", "filtersFormationCourses"));
            $langs = $this->arrayToString($filters->filters->getLangs());
            
            // Get assigned courses.
            if ( $coursesInForm )
            {
                $q = "SELECT a.*,"
                    . $this->queryTranslateByRelated('title', $langs, "title") . ','
                    . $this->queryTranslateByRelated('description', $langs, "description")
                    . " FROM " . $this->table . " AS a"
                    . " WHERE a.id IN "
                    . "("
                    . "SELECT cid FROM " . $this->table_form_courses
                    . " WHERE fid=" . $formationID
                    . ") ORDER BY title ASC";
                ;
            }
            // Get non assigned courses.
            else 
            {
                $q = "SELECT a.*,"
                    . $this->queryTranslateByRelated('title', $langs, "title") . ','
                    . $this->queryTranslateByRelated('description', $langs, "description")
                    . " FROM " . $this->table . " AS a"
                    . " WHERE a.id NOT IN "
                    . "("
                    . "SELECT cid FROM " . $this->table_form_courses
                    . " WHERE fid=" . $formationID
                    . ")";
                ;
            }
           
            $ps = $this->db->prepare($q);
             $ps->execute();
             
            $items = $ps->fetchAll(\PDO::FETCH_OBJ);
            foreach ( $items as &$o ) {
                $o = new \app\Objects\Course($o);
            }  
            
            return $items;
        }
    
    
    public function removeUsersFromContext( string $context, array $uids, bool $begins=true )
    {
        if ( count($uids) ) 
        {
            $rp = str_repeat("?,", count($uids)-1) . "?";
            $sq = ($begins) ? " LIKE ?" : "=?";
            $q = "DELETE ru.* FROM " . $this->table_role . " AS r"
                . " INNER JOIN " . $this->table_ugroup . " AS ru"
                . " ON ru.roleid = r.id"
                . " AND ru.userid IN(" . $rp . ")"
                . " WHERE r.context" . $sq;
               
            $uids[] = $context . '%';
            $ps = $this->db->prepare($q);
            $ps->execute($uids);
            
            return $ps->rowCount();
        }
    }
        
    
   
    /**
     * @override
     */
        public function getItem( DBFilters $filters ) : object|null
        {
            if ( !count($filters->behavior->select) ) {
                $filters->behavior->select[] = "a.*";
            }
            
            return parent::getItem($filters);
        }
  
        
    /**
     * @override
     */
        public function getItems( DBFilters $filters=null ) : array|int 
        {
            if ( is_null($filters) ) {
                $filters = new DBFilters();
            }
            if ( !count($filters->behavior->select) ) {
                $filters->behavior->select[] = "a.*";
            }
            $langs = $this->arrayToString($filters->filters->getLangs());         
            
            // Join translations.
            if ( !$filters->behavior->count )
            {
                $filters->behavior->select[] = $this->queryTranslateByRelated(
                    "title", $langs
                );
                $filters->behavior->select[] = $this->queryTranslateByRelated(
                    "description", $langs
                ); 
                $filters->behavior->select[] = $this->queryTranslateByRelated(
                    "content", $langs
                ); 
                      
            
            // Current user is student ?
            $userid = Factory::getInstance()->getUser()->id;
            $filters->behavior->select[] = "(SELECT COUNT(sa.userid) FROM " 
                                            . $this->table_ugroup . ' AS sa'
                                            . ' INNER JOIN ' . $this->table_role . ' AS sr'
                                            . ' ON sr.context=CONCAT(' . $this->q('course.') . ', a.id, ' . $this->q('.students') . ')'
                                            . ' AND sr.id=sa.roleid'
                                            . " WHERE sa.userid=" . $userid . ") AS isStudent";

            // Current user is teacher ?
            $filters->behavior->select[] = "(SELECT COUNT(sa.userid) FROM " 
                                            . $this->table_ugroup . ' AS sa'
                                            . ' INNER JOIN ' . $this->table_role . ' AS sr'
                                            . ' ON sr.context=CONCAT(' . $this->q('course.') . ', a.id, ' . $this->q('.teachers') . ')'
                                            . ' AND sr.id=sa.roleid'
                                            . " WHERE sa.userid=" . $userid . ") AS isTeacher";

            // Join teacher(s) name.
           
                
                //$filters->behavior->select[] = "GROUP_CONCAT(CONCAT(tu.lastname, ' ', tu.firstname) SEPARATOR '---' ) AS teacher";
                $filters->behavior->select[] = "tr.id as roleTeachers"; 
                $filters->behavior->select[] = "tr2.id as roleStudents"; 
                $filters->behavior->select[] = "(SELECT GROUP_CONCAT(CONCAT(tu.lastname, ' ', tu.firstname) SEPARATOR ', ' ) as teacher
                                                FROM pw_role AS tr
                                                LEFT JOIN pw_role_user AS tru
                                                ON tru.roleid=tr.id
                                                LEFT JOIN pw_user AS tu
                                                ON tu.id = tru.userid
                                                WHERE tr.context=CONCAT('course.',a.id,'.teachers')
                                                GROUP BY a.id
                                                ) AS teacher
                                                "; 
                
                // Students
                $context = 'CONCAT(' . $this->q('course.') . ', a.id, ' . $this->q('.students') . ')';
                $filters->behavior->join[] = "INNER JOIN " . $this->table_role . " AS tr2"
                                            . " ON tr2.context=" . $context;
                // Teachers
                $context = 'CONCAT(' . $this->q('course.') . ', a.id, ' . $this->q('.teachers') . ')';
                $filters->behavior->join[] = "INNER JOIN " . $this->table_role . " AS tr"
                                            . " ON tr.context=" . $context;
                                            

                
                                            
            }
            
            return parent::getItems($filters);
        }
        
        
}

