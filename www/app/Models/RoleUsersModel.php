<?php
namespace app\Models;

use app\Databases\DbFilters;
use PDO;
use stdClass;

defined('_PWE') or die("Limited acces");


class RoleUsersModel extends AModel
{
    protected string $table = "pw_role_user";
    
    public function __construct() {
        parent::__construct("role.users");
    }

    
    /**
     * Get users related to roles id.
     * @param array $roles
     */
        public function getUsers( array $roles ) : array
        {        
            $q = 'SELECT a.roleid as rid, u.id, u.attribs, u.firstname, u.lastname, u.created_on';
            $q .= ' FROM ' . $this->table_ugroup . ' AS a';
            $q .= ' INNER JOIN ' . $this->table_user . ' AS u';
            $q .= ' ON u.id=a.userid';
            $q .= ' WHERE a.roleid IN(?)';
        
            $p = $this->db->prepare($q);
            $p->execute([implode(',', $roles)]);

            return $p->fetchAll(PDO::FETCH_OBJ);
        }


    /**
     * Valid pending students.
     * 
     * @param array Entries to valid.
     * 
     * @return bool Query status.
     */
        public function validPending( array $toValid ) : bool
        {
            $q = "UPDATE " . $this->table . " SET published=? WHERE ";
            $w = [];
            $values = [1];

            foreach ( $toValid as $item )
            {
                $w[] = "(roleid=? AND userid=?)";
                $values[] = $item->roleid;
                $values[] = $item->userid;
            }

            $q .= implode(" OR ", $w);
            $p = $this->db->prepare($q);
            
            return $p->execute($values);
        }


    /**
     * Get users enroled in group.
     */
        public function getEnroled(int $roleid) : array
        {
            $q = "SELECT u.* FROM pw_role as r 
            INNER JOIN pw_role_user as ru
            on ru.roleid=r.id
            INNER JOIN pw_user as u
            on u.id = ru.userid
            WHERE r.id=?";

            $p = $this->db->prepare($q);
            $p->execute([$roleid]);

            return $p->fetchAll(PDO::FETCH_OBJ);
        }

    /**
     * Get teachers from roleid (manually enroled inside the main role)
     * and from courses.%.teachers roles.
     * 
     * @param int $roleid
     * 
     * @return array
     */
        public function getTeachers(int $roleid) : array
        {
            $q = "SELECT u.* FROM pw_role as r 
            INNER JOIN pw_role_user as ru
            on ru.roleid=r.id
            INNER JOIN pw_user as u
            on u.id = ru.userid
            WHERE r.id=?
            GROUP BY u.id
            
            UNION 

            SELECT u.* FROM pw_role as r  
            INNER JOIN pw_role_user as ru
            on ru.roleid=r.id
            INNER JOIN pw_user as u
            on u.id = ru.userid
            WHERE r.context LIKE ?
            GROUP BY u.id"
            ;

            $p = $this->db->prepare($q);
            $p->execute([$roleid, 'course.%.teachers']);

            return $p->fetchAll(PDO::FETCH_OBJ);
        }


    /**
     * Get users enroled in group.
     */
        public function getEnroledMails(int $roleid) : array
        {
            $q = "SELECT u.email FROM pw_role as r 
            INNER JOIN pw_role_user as ru
            on ru.roleid=r.id
            INNER JOIN pw_user as u
            on u.id = ru.userid
            WHERE r.id=?";

            $p = $this->db->prepare($q);
            $p->execute([$roleid]);

            return $p->fetchAll(PDO::FETCH_COLUMN);
        }


    /**
     * Get users from role context.
     */
        public function getContextMails(string $context) : array
        {
            $q = "SELECT u.email FROM pw_role as r 
            INNER JOIN pw_role_user as ru
            on ru.roleid=r.id
            INNER JOIN pw_user as u
            on u.id = ru.userid
            WHERE r.context LIKE ?";

            $p = $this->db->prepare($q);
            $p->execute([$context]);

            return $p->fetchAll(PDO::FETCH_COLUMN);
        }
    
    
    public function enroleUser( int $userID, string $context ) : bool
    {
        $q = "INSERT INTO " . $this->table
        . " SELECT (SELECT id FROM " . $this->table_role . " WHERE context=?), ?"
        . " WHERE NOT EXISTS ("
            . " SELECT 1 FROM " . $this->table 
            . " WHERE roleid=(SELECT id FROM " . $this->table_role . " WHERE context=?) AND userid=?"
        . ")";

        $p = $this->db->prepare($q);

        $p->execute([$context, $userID, $context, $userID]);

        return $p->rowCount();
    }

    

    /**
     * Unenrole user from all roles.
     * 
     * @param int $uid
     * @return int
     */
        public function unenroleUser(int $uid) : int
        {
            $f = new DBFilters();
            $f->filters->isIn['userid'] = [$uid];

            return $this->delete($f);
        }
    
    
    /**
     * Delete users in a partial context but not in specific contexts.
     * 
     * @param array $contexts
     * 
     * @return int Affected rows.
     */
        public function deleteFromContext(string $validContextBegins) : int
        {
            $validContextBegins = $validContextBegins . "%";
            $groupIn = "(SELECT id FROM ". $this->table_role . " AS g" 
                    . " WHERE g.context LIKE ?)";
            $q = "DELETE FROM " . $this->table . " AS a"
                . " WHERE roleid IN " . $groupIn;
             
            $sp = $this->db->prepare($q);
            $sp->execute([$validContextBegins]);
            
            return $sp->rowCount();
        }
            
}

