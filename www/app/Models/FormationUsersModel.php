<?php
namespace app\Models;

use app\Databases\DbFilters;
use PDO;

defined('_PWE') or die("Limited acces");


class FormationUsersModel extends AModel
{
    protected string $table = "pw_formation_user";
    
    
    public function __construct() {
        parent::__construct("formation");
    }


    /**
     * 
     * Get formations where user is enroled.
     * 
     * @param int $uid
     * @return array
     */
        public function getStudentFormations(int $uid)
        {
            $dbf = new DBFilters();
            $langs = $this->arrayToString($dbf->filters->getLangs());
            $qt = $this->__queryTranslateByRelated("title", $langs, "f.id") . " AS title";

            $q = "SELECT f.*, IF(fu.published=0, 1, 0) AS pending, " . $qt
                . " FROM " . $this->table . " AS fu"
                . " INNER JOIN " . $this->table_form . " AS f"
                . " ON f.id=fu.fid"
                . " WHERE fu.uid=?";

            $p = $this->db->prepare($q);
            $p->execute([$uid]);
                
            return $p->fetchAll(PDO::FETCH_OBJ);
        }



    /**
     * Get only students registered to a the formation.
     * Not students following course included in the formation.
     * 
     * @param int $fid
     * @param bool $notIn
     * @param bool $idsOnly
     * @return array
     */
        public function getRelatedStudents(int $fid, bool $notIn=false, bool $idsOnly=false) 
        {
            if (!$notIn) 
            {
                $q = "SELECT a.published AS request_status, " . $this->q("students") . " AS role, u.*"
                    . " FROM " . $this->table . " AS a"
                    . " INNER JOIN " . $this->table_user . " AS u"
                    . " ON u.id=a.uid"
                    . " WHERE a.fid=? GROUP BY u.id";
            }
            else 
            {
                $q = "SELECT " . $this->q("students") . " AS role, a.*"
                    . " FROM pw_user AS a"
                    . " WHERE a.id NOT IN("
                    . " SELECT uid FROM pw_formation_user WHERE fid=? GROUP BY a.id)";
            }

            $p = $this->db->prepare($q);
            $p->execute([$fid]);
            
            $items = $p->fetchAll(PDO::FETCH_OBJ);
            if ( !$idsOnly ) {
                return $items;
            }
            else 
            {
                $r = [];
                foreach ( $items as $item ) {
                    $r[] = $item->id;
                }
                return $r;
            }
        }


        
    /**
     * Valid pending students.
     * 
     * @param array Entries to valid. Key=fid & values users id.
     * 
     * @return bool Query status.
     */
        public function validPending( array $toValid ) : bool
        {
            $q = "UPDATE " . $this->table . " SET published=? WHERE ";
            $w = [];
            $values = [1];

            foreach ( $toValid as $fid => $users )
            {
                foreach( $users as $uid )
                {
                    $w[] = "(fid=? AND uid=? AND published=?)";
                    $values[] = $fid;
                    $values[] = $uid;
                    $values[] = 0;
                }
            }

            $q .= implode(" OR ", $w);
            $p = $this->db->prepare($q);
            
            return $p->execute($values);
        }


    /**
     * Get pendings students.
     * 
     * 
     * @return array
     */
        public function getPending() 
        {
            $q = "SELECT a.fid, f.uid, u.id AS userid, concat(u.lastname, ' ', u.firstname) as username"
                . " FROM " . $this->table . " AS a"
                . " INNER JOIN " . $this->table_form . " AS f"
                . " ON f.id=a.fid"
                . " INNER JOIN " . $this->table_user . " AS u"
                . " ON u.id=a.uid"
                . " WHERE a.published=?";
        
            $p = $this->db->prepare($q);
            $p->execute([0]);
            
            return $p->fetchAll(PDO::FETCH_OBJ);
        }


    /**
     * 
     * Get teachers related to the formation.
     * 
     * @param int $formation
     * @return array
     */
        public function getRelatedTeachers(int $formation) 
        {
            $q = "SELECT " . $this->q("teachers") . " AS role, r.context, u.*
                    FROM pw_formation_course AS fc
                    INNER JOIN pw_role AS r
                    ON r.context = CONCAT(" . $this->q("course.") . ", fc.cid, " . $this->q('.teachers') . ")
                    INNER JOIN pw_role_user AS ru
                    ON ru.roleid = r.id
                    INNER JOIN pw_user AS u
                    ON u.id=ru.userid
                    WHERE fc.fid=?
                    GROUP BY u.id";

            $p = $this->db->prepare($q);
            $p->execute([$formation]);
            
            return $p->fetchAll(PDO::FETCH_OBJ);
        }


    /**
     * 
     * Enrole user into the formation.
     * 
     * @param int $formationID
     * @param int $userID
     * @param int $published
     * @return bool
     */
        public function enroleUser( int $formationID, int $userID, int $published ) : bool
        {
            $q = "INSERT INTO " . $this->table
                . " SELECT ?, ?, ?"
                . " WHERE NOT EXISTS ("
                    . " SELECT 1 FROM " . $this->table 
                    . " WHERE uid=? AND fid=?"
                . ")";

            $p = $this->db->prepare($q);
            $p->execute([$formationID, $userID, $published, $userID, $formationID]);

            return $p->rowCount();
        }
 
    
    /**
     * Update db item from object.
     * 
     * @param int $fid Formation uniq id.
     * @param array $users Users uniq id.
     * @param int $status New status.
     * 
     * @return bool
     */
        public function updateRegistration( int $fid, array $users, int $status ) : bool
        {
            $done = false;
            $prepare = new \stdClass();
              $prepare->pkValue = null;
              $prepare->keys = [];
              $prepare->values = [$status, $fid];

              foreach ( $users as $uid ) {
                 $prepare->values[] = $uid;
              }

            $values = array_fill(0, count($users), "?");
            $query = "UPDATE " . $this->table
                    . " SET published=?" 
                    . " WHERE fid=? AND uid IN(" . implode(",", $values) . ")";
            
            $dbp = $this->db->prepare($query);
            $done = $dbp->execute($prepare->values);
                            
            if (!$done) {
                $this->log("updateRegistration", $dbp->errorCode(), $dbp->errorInfo());
            }
            
            return $done;
        }
    

    /**
     * Cancel (pending) registration to a formation.
     * (delete registration)
     * 
     * @param int $fid Formation uniq id.
     * @param array $users Users uniq id.
     * 
     * @return bool
     */
        public function cancelPendingRegistration( int $fid, array $users ) : bool
        {
            $done = false;
            $prepare = new \stdClass();
              $prepare->pkValue = null;
              $prepare->keys = [];
              $prepare->values = [$fid];

              foreach ( $users as $uid ) {
                 $prepare->values[] = $uid;
              }

            $values = array_fill(0, count($users), "?");
            $query = "DELETE FROM " . $this->table
                    . " WHERE fid=? AND published=0 AND uid IN(" . implode(",", $values) . ")";
            
            $dbp = $this->db->prepare($query);
            $done = $dbp->execute($prepare->values);
                            
            if (!$done) {
                $this->log("cancelRegistration", $dbp->errorCode(), $dbp->errorInfo());
            }
            
            return $done;
        }


    /**
     * Unenrole formation students.
     * 
     * @param int $fid Formation id.
     * 
     * @return int
     */
        public function unenroleStudents(int $fid) : int
        {
            $f = new DBFilters();
             $f->filters->isIn['fid'] = [$fid];

            return $this->delete($f);
        }


    /**
     * Unenrole user from all formations.
     * 
     * @param int $uid User id.
     * 
     * @return int
     */
        public function unenroleUser(int $uid) : int
        {
            $f = new DBFilters();
             $f->filters->isIn['uid'] = [$uid];

            return $this->delete($f);
        }
        
}

