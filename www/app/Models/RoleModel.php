<?php
namespace app\Models;

use app\Databases\DbFilters;

defined('_PWE') or die("Limited acces");


class RoleModel extends AModel
{
    protected string $table = "pw_role";
    
    
    public function __construct() {
        parent::__construct("role");
    }


    /** 
     * Get roles related to courses.
     * 
     * @param array $courses Ids of courses.
     * @param string $target Null or "students" or "teachers".
     * 
     * @return array 
    */
        public function getRolesRelatedToCourses( array $courses, string $target=null ) : array 
        {
            $q = 'SELECT a.*';
            $q .= ' FROM ' . $this->table . ' AS a';

            // Filter by context.
            $tmp = [];
            $values = [];
            foreach ( $courses as $cid ) 
            {
                if ( !empty($target) && in_array($target, ["teachers", "students"]) ) {
                    $tmp[] = "?";
                    $values[] = 'course.' . $cid . '.' . $target;
                } else {
                    $tmp[] = "a.context LIKE ?";
                    $values[] = 'course.' . $cid . '.%';
                }
            }

            if ( !empty($target) && in_array($target, ["teachers", "students"]) ) {
                $q .= ' WHERE a.context IN(' . implode(',', $tmp) . ')';
            } else {
                $q .= ' WHERE (' . implode(' OR ', $tmp) . ')';
            }
            
            $p = $this->db->prepare($q);
            $p->execute($values);

            return $p->fetchAll(\PDO::FETCH_OBJ);
        }
    
    
    /**
     * Get table with enroled users.
     */
        public function getTableEnroled() : string {
            return $this->table_ugroup;
        }
        
    
    /**
     * Delete users from group.
     */
        public function cleanGroup( int $rid ) : int
        {
            $q = "DELETE FROM " . $this->table_ugroup . " WHERE roleid=?";
            $p = $this->db->prepare($q);
            $p->execute([$rid]);
            
            return $p->rowCount();
        }
            
    
    /**
     * Delete users from group.
     */
        public function unenrole( mixed $role, array $users=[] ) : int
        {
            if ( !is_array($role)  ) {
                $role = [$role]; 
            }
            $sq = " roleid IN(" . str_repeat("?,", count($role)-1) . "?)";
            $sq1 = count($users) ? " AND userid IN(" . str_repeat("?,", count($users)-1) . "?)" : "";
            $q = "DELETE FROM " . $this->table_ugroup 
                . " WHERE " . $sq . $sq1;
            $p = $this->db->prepare($q);
             $p->execute(array_merge($role, $users));
            
            return $p->rowCount();
        }
        
    
    
    /**
     * @override
     */
        public function getItem( DBFilters $filters ) : object|null
        {
            if ( !count($filters->behavior->select) ) {
                $filters->behavior->select[] = "a.*";
            }
            
           // $this->applyGetTranslated(["title", "description"], $filters);

            return parent::getItem($filters);
        }
  
        
    /**
     * @override
     */
        public function getItems( DBFilters $filters=null ) : array|int
        {
            if ( is_null($filters) ) {
                $filters = new DBFilters();
            }

            if ( !$filters->behavior->count ) 
            {
                if ( !count($filters->behavior->select) ) {
                    $filters->behavior->select[] = "a.*";
                }

                $langs = $this->arrayToString($filters->filters->getLangs());
                
                // Join translations.
                $filters->behavior->select[] = $this->queryTranslateByRelated(
                    "title", $langs
                );
                $filters->behavior->select[] = $this->queryTranslateByRelated(
                    "description", $langs
                );
                
                // Count users
                $filters->behavior->select[] = "(SELECT COUNT(userid) FROM "
                    . $this->table_ugroup
                    . " WHERE roleid=a.id) as count_users";                    
            }
     
            return parent::getItems($filters);
        }
        
        
}

