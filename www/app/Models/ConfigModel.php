<?php
namespace app\Models;
use app\Databases\DBFilters;
use app\Objects\RulesObj;

defined('_PWE') or die("Limited acces");


class ConfigModel extends AModel
{
    protected string $table = "pw_config";
    
    public function __construct() {
        parent::__construct("config");
    }


    /**
     * Get rules by context.
     * 
     * @param DbFilters $filters
     * 
     * @return array Array of RulesObj.
     */
        public function getByContext( DBFilters $filters=null ) : array
        {
            $items = $this->getItems($filters);
            $r = [];

            foreach ( $items as $it ) {
                $r[$it->context] = new RulesObj($it);
            }

            return $r;
        } 
}

