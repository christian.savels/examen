<?php
namespace app\Models;

use app\Databases\DbFilters;

defined('_PWE') or die("Limited acces");


class TranslateModel extends AModel
{
    protected string $table = "pw_translate";

    public function __construct() {
        parent::__construct("translate");
    }
      

    /**
     * 
     * Get translation related to a partiel context.
     * 
     * @param string $context
     * 
     * @return array
     */
        public function getWhereContextBegins( string $context ) : array
        {
            // Get all translations.
            $filters = new DBFilters();
            $filters->filters->begins['context'] = $context;
        
            return $this->getItems($filters);
        }
        
    
}

