<?php 
namespace app\Objects;

defined('_PWE') or die("Limited acces");


class Role extends AObj
{
    /**
     * Unique id.
     *
     *  @var int
     */
        public $id = 0;
       
    /**
     * Role is protected. Only roots can delete it.
     * 
     * @var boolean
     */
        public $protected = false;      

    /**
     * If true, assign users to roles is allowed.
     * 
     * @var boolean
     */
        public $enrolable = false;       
    
    /**
     * Default role for guests (unidentiied users).
     * 
     * @var boolean
     */
        public $isGuest = false;

    /**
     * Role for all loggedin users.
     * False only for the default guest role.
     * 
     * @var boolean
     */
           public $isLoggedIn = true;
       
    /**
     * Teachers.
     *
     * @var boolean
     */
        public $teachers = false;         

    /**
     * Students.
     * 
     * @var boolean
     */
        public $students = false;     

    /**
     * Title
     * 
     * @var string
     */
        public $title;
    
    /**
     * Description
     * 
     * @var string
     */
        public $description;

        
    /**
     * 
     * @param object $dbObj Public values to bind to $this.
     */
        public function __construct( object $dbObj=null ) {
            parent::__construct($dbObj);
        }
}