<?php 
namespace app\Objects;

defined('_PWE') or die("Limited acces");


class Course extends AObj
{
    /**
     * Db id.
     * @var integer
     */
        public $id = 0;
        
    /**
     * Unique id.
     * @var string
     */
        public $uid;
       
    /**
     * Title
     * @var string
     */
        public $title;
    
    /**
     * Description
     * @var string
     */
        public $description;

    /**
     * Long description.
     * @var string
     */
        public $content;
        
    /**
     * The course begins on.
     * @var date
     */
        public $start_on;
    
    /**
     * The course ends on.
     * @var date
     */
        public $end_on;
        
    /**
     * Formation id.
     * @var int
     */
        public $formation;


    /**
     * Teacher lastname-firstname.
     */
        public string $teacher = "";

    /**
     * Current user is student ?
     */
        public bool $isStudent = false;

    /**
     * Current user is teacher ?
     */
        public bool $isTeacher = false;

    /**
     * Teacher(s) role id.
     */
        public int $roleTeachers = 0;

    /**
     * Students role id.
     */
        public int $roleStudents = 0;
        
        
    /**
     * 
     * @param object $dbObj Public values to bind to $this.
     */
        public function __construct( object $dbObj=null ) {
            parent::__construct($dbObj);
        }
}