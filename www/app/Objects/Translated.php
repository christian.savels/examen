<?php 
namespace app\Objects;

defined('_PWE') or die("Limited acces");


class Translated extends AObj
{
    /**
     * Uniq id.
     * @var integer
     */
        public $id = 0;
        
    /**
     * Foreign key
     * @var integer
     */
        public $fk = 0;
       
    /**
     * Content
     * @var string
     */
        public $content;
    
    /**
     * Context
     * @var string
     */
        public $context;

    /**
     * If true, will be deleted.
     *           
     * @var bool
     */
        public bool $delete=false;
        
    /**
     * Lang
     * @var string
     */
        public $lang;
        public $name;
        
    /**
     * For default language.
     * @var boolean
     */
        public bool $required=false;
        
        
    /**
     * 
     * @param object $dbObj Public values to bind to $this.
     */
        public function __construct( object $dbObj=null ) {
            parent::__construct($dbObj);
        }
}