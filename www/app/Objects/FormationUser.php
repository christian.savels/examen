<?php 
namespace app\Objects;

defined('_PWE') or die("Limited acces");


class FormationUser extends AObj
{
    /**
     * Unique id.
     * @var integer
     */
        public $fid = 0;

    /**
     * Unique id.
     * 
     * @var string
     */
        public $uid = 0;

        
    /**
     * 
     * @param object $dbObj Public values to bind to $this.
     */
        public function __construct( object $dbObj=null ) {
            parent::__construct($dbObj);
        }
}