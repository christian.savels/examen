<?php 
namespace app\Objects;

defined('_PWE') or die("Limited acces");


class Formation extends AObj
{
    /**
     * Unique id.
     * @var integer
     */
        public int $id = 0;

    /**
     * Unique id.
     * 
     * @var string
     */
        public string $uid = "";

    /**
     * Degree
     * 
     * @var int
     */
        public int $degree = 0;
        
        public string $degree_title = "";
       
    /**
     * Title
     * @var string
     */
        public $title;
    
    /**
     * Short description (text)
     * @var string
     */
        public $description;

    /**
     * Full description. (html)
     * @var string
     */
        public $content;


    /**
     * Edit link url.
     * @var mixed
     */
        public $editLink;

    /**
     * Define if current user is registered to the formation.
     * 0=no & 1=yes.
     */
        public int $isStudent = 0;
        
        
    /**
     * 
     * @param object $dbObj Public values to bind to $this.
     */
        public function __construct( object $dbObj=null ) {
            parent::__construct($dbObj);
        }
}