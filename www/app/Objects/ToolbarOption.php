<?php 
namespace app\Objects;

defined('_PWE') or die("Limited acces");


class ToolbarOption extends AObj
{       
    /**
     * Title (on hover).
     * @var string
     */
        public $title = null;
       
    /**
     * Option text.
     * @var string
     */
        public $text = null;
       
    /**
     * Action on click.
     * @var string
     */
        public $action = null;
       
    /**
     * Go to page on clic.
     * @var \app\Enums\Page
     */
        public $page = null;
       
    /**
     * Go url
     * @var string
     */
        public $url = null;
       
    /**
     * Option icon.
     * @var string
     */
        public $icon = null;
       
    /**
     * Option style (class="").
     * @var string
     */
        public $class = null;
        
    /**
     * Define if user has to confirm action before.
     * If null=no confirm, if string, ask value before.
     * 
     * @var string
     */
        public $confirm = null;
        
    /**
     * Additionnal attributes to place on the option container.
     * @var mixed
     */
        public $boxAttributes = null;
        
    /**
     * Additionnal attributes to place on the option link.
     * @var mixed
     */
        public $linkAttributes = null;
        
        
    /**
     * Button content (title & icon).
     * @var string
     */
        public $content = null;
    
        
    /**
     * @param object $dbObj Public values to bind to $this.
     */
        public function __construct( object $dbObj=null ) {
            parent::__construct($dbObj);
        }
}