<?php 
namespace app\Objects;

defined('_PWE') or die("Limited acces");


class Degree extends AObj
{
    /**
     * Unique id.
     * @var integer
     */
        public $id = 0;
       
        
    /**
     * Title
     * @var string
     */
        public $title;
    
    /**
     * Description
     * @var string
     */
        public $description;
        
        
    /**
     * 
     * @param object $dbObj Public values to bind to $this.
     */
        public function __construct( object $dbObj=null ) {
            parent::__construct($dbObj);
        }
}