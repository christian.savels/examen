<?php 
namespace app\Objects;

defined('_PWE') or die("Limited acces");


class Footer 
{
    /**
     * Count of courses, formations, ...
     */
        public array $counts = [];
        
    /**
     * 
     * @param array $counts Count of courses, formations, ...
     */
        public function __construct( array $counts ) {
            $this->counts = $counts;
        }
}