<?php 
namespace app\Objects;
use SimpleXMLElement;

defined('_PWE') or die("Limited acces");

/**
 * Common list filter.
 */
class Filter extends AObj
{
    public string $name;
    public string $type;
    public string $value;
    public string $style;
    public string $hint;


    public function __construct(SimpleXMLElement $fieldObj) {
        $this->init($fieldObj);
    }
    

    /**
     * Init used values.
     * 
     * @param SimpleXMLElement $fieldObj
     * @return void
     */
        private function init(SimpleXMLElement $fieldObj) : void
        {
            $this->name = (string) $fieldObj["name"];
            $this->type = (string) $fieldObj["filter"];
            $this->value = (string) $fieldObj["filter_default"];
            $this->style = trim((string) $fieldObj["class"] . ' form-control');
            $this->hint = trim((string) $fieldObj["filter_hint"]);
            $this->attribs = (string) $fieldObj["attribs"];
        }
}