<?php 
namespace app\Objects;

defined('_PWE') or die("Limited acces");

/**
 * Simple lang object.
 */
class PWLang 
{
    /**
     * Code, like "en-GB".
     */
        public string|null $code = null;  

    /**
     * Country code
     */
        public string|null $countryCode = null;  

    /**
     * Country title
     */
        public string|null $countryTitle = null;  

    /**
     * Lang icon.
     */
        public string|null $icon = null; 


        public function __construct(string $code) {
            $this->init($code);
        }


    /**
     * Init object from code.
     */
        private function init(string $code) : void 
        {
            $t = \app\Helpers\Factory::getInstance()->getTxt();

            $this->code = $code;
            $this->countryCode = $t->getCountryFromCode($code);
            $this->countryTitle = $t->getCountryTitle($this->countryCode);
            $this->icon = $t->getImgFromCountry($this->countryCode);
        }
}