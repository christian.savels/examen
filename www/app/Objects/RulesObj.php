<?php 
namespace app\Objects;

defined('_PWE') or die("Limited acces");

/**
 * Rules object.
 */
class RulesObj extends AObj
{
    /**
     * Rules context
     *
     *  @var string
     */
        public string $context = "";
       
    /**
     * Rules.
     * 
     * @var string|object
     */
        public string|object $cfg = "";      


    /**
     * 
     * @param object $dbObj Public values to bind to $this.
     */
        public function __construct( object $dbObj=null ) 
        {
            parent::__construct($dbObj);   
            
            if ( is_string($this->cfg) && !empty($this->cfg) ) {
                $this->cfg = json_decode($this->cfg);
            }
        }

        
    /**
     * Get configuration object.
     * 
     * @return object
     */
        public function getCfg() : object {
            return is_string($this->cfg) ? json_decode($this->cfg) : $this->cfg;
        }
}