<?php 
namespace app\Objects;
use app\Helpers\Factory;

defined('_PWE') or die("Limited acces");


class User extends AObj
{
    /**
     * Unique id.
     * @var int
     */
        public $id = 0;     
        
    /**
     * Session token.
     * @var string
     */
        public $token_session;
        
    /**
     * Token created time.
     * @var string
     */
        public $token_time;
        
    /**
     * Access level.
     * Default=1=guest.
     * 
     * @var int
     * 
     */
        public $access = 1;
        
    /**
     * User groups (roles) .
     * @var array
     */
        public $groups = [];
        
    /**
     * Date of creation.
     * @var string SQL DateTime.
     */
        public $created_on;
        
    /**
     * User name.
     * @var string
     */
        public $username;
        
    /**
     * Firstname
     * @var string
     */
        public $firstname;
        
    /**
     * Lastname
     * @var string
     */
        public $lastname;

    /**
     * Email.
     * @var string
     */
        public $email = null;
        
    /**
     * Language.
     * @var string
     */
        public $lang = null;        

    /**
     * User has to reset his password.
     * @var mixed True to reset.
     */
        public $resetpwd = null;
        
        public $role = null;
        
        
    /**
     * 
     * @param object $dbUser Public values to bind to $this.
     */
        public function __construct( object $dbUser=null ) {
            parent::__construct($dbUser);
        }
        
        
    /**
     * Get user language.
     * @return string
     */
        public function getLang() : string {
            return is_null($this->lang) ? Factory::getInstance()->cfg("lang.default") : $this->lang;
        }

        
    /**
     * Define if user is a guest (not identified).
     * 
     * @return bool
     */
        public function isGuest() : bool {
            return $this->id==0;
        }
        
        
    /**
     * Define if user is a guest (not identified).
     * 
     * @return bool
     */
        public function isRoot() : bool {
            return \app\Enums\Access::isRoot($this->access);
        }
        
        
    /**
     * Get used group id(s).
     * @return array
     */
        public function getGroups() : array 
        {
            if ( is_null($this->groups) ) {
                $this->groups = [];
            }
            if ( is_string($this->groups) ) {
                $this->groups = explode(',', $this->groups);
            }
            
            return $this->groups;
        }


    /**
     * Get full address.
     * 
     * @return string
     */
        public function getAddressFull() : string 
        {
            if ( is_string($this->attribs) ) {
                $this->attribs = json_decode($this->attribs);
            }

            return $this->attribs->address->number . ', '
                    . $this->attribs->address->street . "<br />"
                    . $this->attribs->address->zip . ' ' . $this->attribs->address->city
                    . ' (' . $this->attribs->address->country . ')';
        }

} ?>