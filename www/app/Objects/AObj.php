<?php
namespace app\Objects;

defined('_PWE') or die("Limited acces");


abstract class AObj
{
    public int $published = 0;

    public mixed $attribs = null;
    public string $link = "#";

    
    /**
     * 
     * @param object $o Bind related public var.
     */
        protected function __construct( object $o=null )
        {
            if ( !is_null($o) ) {
                $this->bindPublicVars($o);
            }
        }
        
        
    /**
     * Apply item attribs on main tree.
     * 
     * @param bool $removeAfter True tu unset attribs before application on main tree.
     */
        public function applyAttribsTree(bool $removeAfter=false) : void
        {
            if ( isset($this->attribs) ) 
            {
                if ( is_string($this->attribs) ) {
                    $this->attribs = json_decode($this->attribs, false);
                }
                foreach ( $this->attribs as $k => $v ) {
                    $this->{$k} = $v;
                }
                if ($removeAfter) {
                    unset($this->attribs);
                }
            }
        }
        
        
    /**
     * Bind only public var.
     * @param object $o
     */
        public function bindPublicVars( object $o ) 
        {
            $rf = new \ReflectionObject($this);
            $vars = $rf->getProperties(\ReflectionProperty::IS_PUBLIC);
            foreach ( $vars as $v )
            {
                if ( isset($o->{$v->getName()}) ) {
                    $this->{$v->getName()} = $o->{$v->getName()};
                }
            }
            
            if ( isset($this->attribs) && is_string($this->attribs) ) {
                $this->attribs = json_decode($this->attribs, false);
            }
        }


    /**
     * 
     * Define if item is published.
     * Related to Status.
     * 
     * @return bool
     */
        public function isPublished() : bool {
            return $this->published == \app\Enums\Status::PUBLISHED->value;
        }
}