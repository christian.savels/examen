<?php
namespace app\Controllers\interfaces;
use app\Helpers\JSONHelper;

defined('_PWE') or die("Limited acces");

/**
 * 
 * JSON Roles controller interface.
 * 
 * @author chris
 *
 */
interface RolesJControllerInterface
{
        
    /**
     * 
     * Manage roles.
     * 
     * @param mixed $page
     * 
     * @return JSONHelper
     */
        public function manage(mixed $page=null) : JSONHelper;

}

