<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * User controller.
 * 
 * @author chris
 *
 */
interface UserControllerInterface
{
    /**
     * User logout.
     */
        public function logout();
        
        
    /**
     * User account (private data).
     */
        public function account();
}

