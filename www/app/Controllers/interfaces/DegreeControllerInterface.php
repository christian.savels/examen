<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Degree controller interface.
 * 
 * @author chris
 *
 */
interface DegreeControllerInterface extends CrudInterface
{
}