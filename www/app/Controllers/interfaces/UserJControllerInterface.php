<?php
namespace app\Controllers\interfaces;
use app\Helpers\JSONHelper;

defined('_PWE') or die("Limited acces");

/**
 * 
 * User controller.
 * 
 * @author chris
 *
 */
interface UserJControllerInterface
{
    /**
     * User logout.
     */
        public function logout() : JSONHelper;
        
        
    /**
     * User account (save private data).
     */
        public function account() : JSONHelper;
        

    /**
     * Export user data.
     * 
     * @param string $format Export to format...
     */
        public function export($format="") : void;
}

