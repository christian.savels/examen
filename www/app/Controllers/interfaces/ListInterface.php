<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * List interface.
 * 
 * @author chris
 *
 */
interface ListInterface 
{
    /**
     * Items list.
     * 
     * @param mixed $page
     *  
     * @return mixed
     */
        public function list(mixed $page=null) : mixed;  


    /**
     * Export data.
     */
        public function export(string $format) : void;
}