<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

use app\Helpers\JSONHelper;

/**
 * 
 * JSON Formation controller.
 * 
 * @author chris
 *
 */
interface FormationJControllerInterface 
{
    /**
     * Register user to a formation.
     * 
     * @param int $uid
     * 
     * @return JSONHelper
     */
        public function register(int $uid) : JSONHelper;     


    /**
     * Courses related to the formation.
     * 
     * @param int $uid Formation id.
     * 
     * @return JSONHelper
     */
        public function courses(int $uid=0) : JSONHelper;    
        

    /**
     * List of users to assign to a formation.
     * 
     * @return JSONHelper
     */
        public function enrole() : JSONHelper;
        

    /**
     * Unassign users from the formation.
     * 
     * @return JSONHelper
     */
        public function unenrole() : JSONHelper;


    /**
     * Assign users to a formation.
     * 
     * @return JSONHelper
     */
        public function enroleto() : JSONHelper;


    /**
     * List of courses to assign to a formation.
     * 
     * @return JSONHelper
     */
        public function enrolecourse() : JSONHelper;     


    /**
     * Unassign courses from the formation.
     * 
     * @return JSONHelper
     */
        public function unenrolecourse() : JSONHelper;


    /**
     * Assign courses to a formation.
     * 
     * @return JSONHelper
     */
        public function enrolecourseto() : JSONHelper;


    /**
     * 
     * Clean courses associations.
     * 
     * @return JSONHelper
     */
        public function cleancourses() : JSONHelper;


    /**
     * Clean students associations.
     * 
     * @return JSONHelper
     */
        public function cleanstudents() : JSONHelper; 

        
    /**
     * Return form to mail to teachers.
     * @return JSONHelper
     */
        public function mailformteachers() : JSONHelper;
        
        
    /**
     * Return form to mail to students.
     * @return JSONHelper
     */
        public function mailformstudents() : JSONHelper;        
        

    /**
     * Validate or cancel pending users.
     * 
     * @return JSONHelper
     */
        public function validPending() : JSONHelper;


    /**
     * Send mail to selected users.
     * @return void
     */
        public function mailto() : JSONHelper;
}