<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Formation interface.
 * 
 * @author chris
 *
 */
interface FormationControllerInterface extends CrudInterface
{
    /**
     * Page to register an user to a formation.
     * 
     * @param int $uid
     * 
     * @return mixed
     */
        public function register(int $uid) : mixed;     
        
}