<?php
namespace app\Controllers\interfaces;
use app\Helpers\JSONHelper;

defined('_PWE') or die("Limited acces");

/**
 * 
 * JSON Course controller interface.
 * 
 * @author chris
 *
 */
interface CourseJControllerInterface extends CrudInterface
{  
    /**
     * Users related.
     * 
     * @return JSONHelper
     */
        public function users() : JSONHelper;


    /**
     * Register current user to a course.
     * 
     * @return JSONHelper
     */
        public function register(int $cid) : JSONHelper;


    /**
     * Return html of selectable table of users
     * who can be enroled.
     * 
     * @return JSONHelper
     */
        public function enrole() : JSONHelper;


    /**
     * Remove users from course.
     * 
     * @return JSONHelper
     */
        public function unenrole() : JSONHelper;


    /**
     * Enrole users to role.
     * 
     * @return JSONHelper
     */
        public function enroleto() : JSONHelper;
        

    /**
     * Clean a course.
     * 
     *  - Remove all course roles except students & teachers.
     *  - Remove all course users (teachers & students).
     *  
     * @return JSONHelper
     */
        public function clean() : JSONHelper;

        
    /**
     * Return form to mail to teachers.
     * 
     * @return JSONHelper
     */
        public function mailformteachers() : JSONHelper;

        
    /**
     * Return form to mail to students.
     * 
     * @return JSONHelper
     */
        public function mailformstudents() : JSONHelper;
        

    /**
     * Send mail to selected users.
     * 
     * @return JSONHelper
     */
        public function mailto() : JSONHelper;
}