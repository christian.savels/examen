<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Course controller interface.
 * 
 * @author chris
 *
 */
interface CourseControllerInterface extends CrudInterface
{
    /**
     * Page register to a course confirmation.
     * 
     * @param int $cid Course id.
     * 
     * @return object
     */
        public function register(int $cid) : object;
}