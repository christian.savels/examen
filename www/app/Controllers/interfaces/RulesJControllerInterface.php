<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * JSON Rules controller interface.
 *  
 * @author chris
 *
 */
interface RulesJControllerInterface 
{
    /**
     * Save rules form.
     */
        public function save(string $context) : \app\Helpers\JSONHelper;     
}

