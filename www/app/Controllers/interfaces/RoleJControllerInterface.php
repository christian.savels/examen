<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * JSON Role controller interface.
 * 
 * @author chris
 *
 */
interface RoleJControllerInterface extends CrudInterface
{
    /**
     * Users related to the a role.
     */
        public function users();
        
        
    /**
     * Clean role users.
     */
        public function clean();
        
        
    /**
     * Return data to enrole.
     */
        public function enrole();
        
        
    /**
     * Add selected users to role.
     * Return related users into json.
     */
        public function enroleto();
        
        
    /**
     * Unenrole users in a group.
     */
        public function unenrole();
        
}

