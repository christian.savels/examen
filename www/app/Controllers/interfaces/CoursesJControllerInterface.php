<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

use app\Helpers\JSONHelper;

/**
 * 
 * Courses controller interface.
 * 
 * @author chris
 *
 */
interface CoursesJControllerInterface extends ListInterface
{
    /**
     * Manage courses.
     */
        public function manage() : JSONHelper;

        
    /**
     * Valid/cancel pending students.
     */
        public function validPending() : JSONHelper;
}