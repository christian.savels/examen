<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

use app\Helpers\JSONHelper;

/**
 * 
 * JSON Users controller interface.
 * 
 * @author chris
 *
 */
interface UsersJControllerInterface 
{
    /**
     * Manage users.
     * 
     * @param mixed $page
     * 
     * @return JSONHelper
     */
        public function manage(mixed $page=null) : JSONHelper;   


    /**
     * Apply edit user.
     * 
     * @return JSONHelper
     */
        public function apply() : JSONHelper;


    /**
     * Form to edit an user.
     * 
     * @param int $uid User id.
     * 
     * @return JSONHelper
     */
        public function edit(int $uid=0) : JSONHelper;


    /**
     * Form to create an user.
     * 
     * @return JSONHelper
     */
        public function create() : JSONHelper;


    /**
     * Delete selected users.
     * 
     * @return JSONHelper
     */
        public function delete() : JSONHelper;


    /**
     * Save a new user.
     * 
     * @return JSONHelper
     */
        public function signup() : JSONHelper;


    /**
     * Get html containing more information on the selected user.
     * Will be placed in a modal by js scripts.
     * 
     * @return JSONHelper 
    */
        public function more() : JSONHelper;


    /**
     * Modify the access of the user. (root, basic)
     * 
     * @return JSONHelper
     */
        public function access() : JSONHelper;


    /**
     * Send mail to selected users.
     * 
     * @return JSONHelper
     */
        public function mailto() : JSONHelper;
}