<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Roles controller interface. 
 * 
 * @author chris
 *
 */
interface RolesControllerInterface extends ListInterface
{
        
    /**
     * Liste des rôles.
     * Manage roles.
     */
        public function manage(mixed $page=null);
        
}

