<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * JSON Formations controller.
 * 
 * @author chris
 *
 */
interface FormationsJControllerInterface extends ListInterface
{   
    /**
     * Manage formations.
     * @return void
     */
        public function manage();  
}