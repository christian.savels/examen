<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Degrees controller interface.
 * 
 * @author chris
 *
 */
interface DegreesControllerInterface extends ListInterface
{
    /**
     * Manage degrees.
     */
        public function manage();
}