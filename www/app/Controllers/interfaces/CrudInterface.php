<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Item interface.
 * 
 * @author chris
 *
 */
interface CrudInterface
{
    /**
     * Delete an item.
     * 
     * @param int $uid
     */
        public function delete(int $uid=0);
        
        
    /**
     * Edit/create an item.
     * 
     * @param int $uid Item uniq id.
     */
        public function edit(int $uid=0); 


    /**
	 * Item details (public).
	 *
	 * @param int $uid
	 * @param mixed $page
     * 
	 * @return mixed
	 */
        public function details(int $uid = 0, mixed $page=null) : mixed;
}