<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Formations controller.
 * 
 * @author chris
 *
 */
interface FormationsControllerInterface extends ListInterface
{
    /**
     * Courses related to the formation.
     * @return void
     */
       // public function courses();

    /**
     * Manage formations.
     * @return void
     */
        public function manage();  
        
    /**
     * List of users to assign to a formation.
     */
      //  public function enrole();     

    /**
     * Unassign users from the formation.
     */
       // public function unenrole();

    /**
     * Assign users to a formation.
     */
        //public function enroleto();

    /**
     * List of courses to assign to a formation.
     */
        //public function enrolecourse();     

    /**
     * Unassign courses from the formation.
     */
        //public function unenrolecourse();

    /**
     * Assign courses to a formation.
     */
        //public function enrolecourseto();

    /**
     * 
     * Clean courses associations.
     */
        //public function cleancourses();

    /**
     * Clean students associations.
     */
        //public function cleanstudents();
        
    /**
     * Formation details.
     * 
     * @param int $uid
     * @param mixed $page (of courses)
     * 
     * @deprecated Use FormationController instead.
     */
        //public function formation(int $uid=0,  mixed $page=null);    

    /**
     * Register user to a formation.
     * 
     * @param int $uid
     * @return void
     * @deprecated Use FormationController instead.
     */
       // public function register(int $uid);     

        
    /**
     * Return form to mail to teachers.
     * @return void|string
     */
       //public function mailformteachers();
        
        
    /**
     * Return form to mail to students.
     * @return void|string
     */
        //public function mailformstudents();


    /**
     * Send mail to selected users.
     * @return void
     */
        //public function mailto();
}