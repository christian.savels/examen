<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Home controller.
 * 
 * @author chris
 *
 */
interface HomeControllerInterface
{
    /**
     * Home page.
     */
        public function home();
}

