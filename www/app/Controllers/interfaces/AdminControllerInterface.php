<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Admin controller interface.
 * 
 * @author chris
 *
 */
interface AdminControllerInterface
{
    /**
     * Admin home page.
     */
        public function admin() : mixed;
}

