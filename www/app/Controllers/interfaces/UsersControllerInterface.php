<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Users controller interface.
 * 
 * @author chris
 *
 */
interface UsersControllerInterface 
{
    /**
     * Manage users.
     */
        public function manage(mixed $page=null);   
}