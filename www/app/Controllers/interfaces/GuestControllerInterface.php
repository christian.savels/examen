<?php
namespace app\Controllers\interfaces;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Guests controller.
 * 
 * @author chris
 *
 */
interface GuestControllerInterface
{
    /**
     * User login.
     */
        public function login();
        
    /**
     * User signup.
     */
        public function signup();
}

