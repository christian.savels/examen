<?php
namespace app\Controllers\interfaces;

/**
 * 
 * Both html controller & json controller have to include all methods.
 * 
 * @author chris
 *
 */
interface RulesControllerInterface 
{
    /**
     * Save rules form.
     */
        public function save(string $context) : \app\Helpers\JSONHelper;     
}

