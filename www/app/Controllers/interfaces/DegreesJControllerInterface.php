<?php
namespace app\Controllers\interfaces;
use app\Helpers\JSONHelper;

defined('_PWE') or die("Limited acces");

/**
 * 
 * JSON Degrees controller interface.
 * 
 * @author chris
 *
 */
interface DegreesJControllerInterface extends ListInterface
{
    
    /**
     * Manage degrees.
     */
        public function manage() : JSONHelper;
}