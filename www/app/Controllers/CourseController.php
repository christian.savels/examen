<?php
namespace app\Controllers;

defined('_PWE') or die("Limited acces");

use app\Enums\Page;
use app\Helpers\FileHelper;
use app\Objects\Course;
use stdClass;
use app\Helpers\Factory;


/**
 * 
 * Controller of a course views.
 * 
 * @author chris
 *
 */
class CourseController extends \app\Controllers\AController
implements \app\Controllers\interfaces\CourseControllerInterface 
{   
    protected string $context = "courses";
    protected string $context_item = "course";

    
    /**
     * @Override
     */
        public function getRelatedObject(object $obj=null ) : Course|null {
            return new Course($obj); 
        }

    
    /**
     * @Override
     */
        public function register(int $cid) : object
        {
            $factory = Factory::getInstance();
            $data = new stdClass();
             $data->course = \app\Helpers\CoursesHelper::checkCourseRegistration($cid, 1);
             $data->cancelLink = $factory->getUrl(Page::COURSE, $cid);
             
             // Apply url image.
             FileHelper::applyImage(
                $data->course, 
                $factory->path("img_courses"), 
                $factory->cfg("img.courses_default")
            );

            return $data;
        }


    /**
     * @Override
     */
        public function details(int $uid = 0, mixed $page=null) : mixed 
        {
            $factory = Factory::getInstance();
            $data = null;

            if ( !$uid ) {
                $uid = \app\Helpers\Helper::getValue($this->context_item, 0);
            }
            if ( $uid )
            {
                $item = new Course($this->getModel()->getById($uid));

                // Bad !
                if ( !$item->id || !$item->isPublished() ) 
                {
                    $factory->setError("BAD_COURSE", $uid);
                    $this->redirect(Page::COURSES);
                }
                else 
                {
                    // Data to return
                    $data = new stdClass();
                     $data->course = $item;                  

                    // Set register url
                    $data->course->urlRegister = $factory->getUrl(
                        Page::COURSES_REGISTER, $data->course->id
                    );

                    // Apply url image.
                    FileHelper::applyImage(
                        $data->course, 
                        $factory->path("img_courses"), 
                        $factory->cfg("img.courses_default")
                    );
                    
                }
            } 
            else {
                $factory->setError("BAD_FORMATION", $uid);
            }

            return $data;
        }
        
        
    /**
     * @override
     */
        public function edit(int $id=0) : mixed
        {            
            if ( ($item=$this->isEditable($id))!==false )
            {
                $translated = [];
                $mod = $this->getModel();
                $data = new stdClass();
                 $data->item = !$item ? new Course() : $item;
                 $data->langs = Factory::getInstance()->getTxt()->getLangs();
                
                if ($id)
                {
                    $data->item = new Course($mod->getItemBy("id", $id));
                    
                    // Bad id, redirect to new item form.
                    if ( !$data->item->id )
                    {
                        Factory::getInstance()->setError("BAD_ITEM_ID", $id);
                        $this->redirect(Page::COURSES_EDIT);
                    }
                    
                    // Get all translations.
                    $translated = $this->getModelTranslate()->getWhereContextBegins(
                        $this->getModel()->role . '.' . $id . '.'
                    );
                }

                // Image 
                FileHelper::applyImage(
                    $data->item, 
                    Factory::getInstance()->path("img_courses"), 
                    Factory::getInstance()->cfg("img.courses_default")
                );
                
                $data->fields = \app\Helpers\FormHelper::extractFormFields
                (
                    $this->context . "." . $this->context_item,
                    $data->item,
                    $translated
                );               
            
                return $data;
            }
        }
        
        
    /**
     * @override
     */
        public function delete(int $uid=0) : void { die; }
  
    
    /**
     * @override
     */
        public function getModel() {
            return new \app\Models\CoursesModel();
        } 
                

    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::COURSES_MANAGE);
        }
}