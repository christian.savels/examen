<?php
namespace app\Controllers;

use app\Databases\DBFilters;
use app\Enums\Page;
use app\Enums\Status;
use app\Helpers\FileHelper;
use app\Objects\AObj;
use app\Objects\Course; 
use app\Objects\Formation;
use stdClass;

defined('_PWE') or die("Limited acces");

use app\Helpers\Factory;


/**
 * 
 * Formation controller.
 * 
 * @author chris
 *
 */
class FormationController extends \app\Controllers\AController
    implements \app\Controllers\interfaces\FormationControllerInterface 
{   
    protected string $context = "formations";
    protected string $context_item = "formation";

    
    /**
     * {@inheritDoc}
     * @see \app\Controllers\AController::getModel()
     */
        public function getModel() {
            return new \app\Models\FormationModel();
        } 

    
    /**
     * @Override
     */
        public function getRelatedObject(object $obj=null ) : Formation|null {
            return new Formation($obj); 
        }
    

	/**
	 * @override
	 */
        public function details(int $uid = 0, mixed $page=null) : mixed
        {
            $factory = Factory::getInstance();
            $data = null;

            if (!$uid) {
                $uid = \app\Helpers\Helper::getValue($this->context_item, 0);
            }
            if ($uid)
            {
                $item = new Formation($this->getModel()->getById($uid));

                // Bad !
                if ( !$item->id || !$item->isPublished() ) 
                {
                    $factory->setError("BAD_FORMATION", $uid);
                    $this->redirect(Page::FORMATIONS);
                }
                else 
                {
                    $pagination = new \app\Helpers\PaginationHelper($page);
                    $m = new \app\Models\CoursesModel();
                    $fh = new \app\Helpers\FiltersHelper("courses.course", "filtersFormationCourses");
                    $f = new DBFilters($pagination);
                     $f->filters->makeFromPost($fh);
                     $f->filters->isIn["published"] = [Status::PUBLISHED->value];
                     $f->filters->isIn["fid"] = [$uid];
                    
                    // Get count for pagination
                    $f->behavior->count = true;
                    $pagination->setCount($m->getFromFormation($uid, $f)); 
                    $f->behavior->count = false;
                    
                    // Data to return
                    $orderBy = \app\Helpers\FormHelper::makeOrderBy
                    (
                        "cobitems" . $this->context . "Courses", 
                        "cobitems" . $this->context . "Courses", 
                        $factory->getUrl(Page::FORMATIONS, null, true),
                        new Formation(),
                        "",
                        ["published", "editLink", "attribs", "id", "degree_title"], 
                        "data-pw-formations-order"                    
                    );
                    $data = new stdClass();
                     $data->formation = $item;
                     $data->courses = $m->getFromFormation($uid, $f);
                     $data->countCourses = count($data->courses);
                     $data->pagination = $pagination;
                     $data->filters = $fh->render(
                        "filtersFormationCourses",
                         $factory->getUrl(Page::FORMATION_COURSES, null, true), 
                         ["published", "content", "degree_title"],
                         $orderBy
                     );
                     $data->orderBy = new stdClass;
                     $data->orderBy->id = "cobitems" . $this->context . "Courses";
                     

                    // Set register url
                    $data->formation->urlRegister = $factory->getUrl(
                        Page::FORMATIONS_REGISTER, $data->formation->id
                    );

                    // Apply url image.
                    FileHelper::applyImage(
                        $data->formation, 
                        $factory->path("img_formations"), 
                        $factory->cfg("img.formations_default")
                    );
                    
                    // Finalize items.
                    foreach ( $data->courses as &$o )
                    {
                        $o = new Course($o);
                        \app\Helpers\Helper::applyListParams($o);
                        
                        // Apply url image.
                        FileHelper::applyImage(
                            $o, 
                            $factory->path("img_courses"), 
                            $factory->cfg("img.courses_default")
                        );
                    }            
                }
            }  
            else {
                $factory->setError("BAD_FORMATION", $uid);
            }

            return $data;
        }
    

    /**
     * @override
     */
        public function register(int $uid) : mixed
        {
            $factory = Factory::getInstance();
            $data = null;
            $item = null;

            if ( !$uid || is_null(($item=$this->getModel()->getById($uid))) ) {
                $factory->setError("BAD_ITEM_ID", $uid);
            } elseif ( $factory->getUser()->isGuest() ) {
                $factory->setError("ERROR_GUEST", $item->title);
            } 
            
            if ( !$factory->hasErrors() ) 
            {
                $f = new DBFilters();
                 $f->filters->isIn["published"] = [Status::PUBLISHED->value];
                $m = new \app\Models\CoursesModel();
                $courses = $m->getFromFormation($uid, $f);
                   
                // Data to return
                $data = new stdClass();
                 $data->formation = new Formation($item);
                 $data->formation->link = $factory->getUrl(Page::FORMATIONS_FORMATION, $data->formation->id);
                 $data->courses = new stdClass;
                  $data->courses->registered = [];
                  $data->courses->notyet = [];

                  foreach ( $courses as &$o ) 
                  {
                     $o = new Course($o);
                     if ( $o->isStudent ) {
                         $data->courses->registered[] = $o->title;
                     } else {
                         $data->courses->notyet[] = $o->title;
                     }
                  }

                // Apply image.
                FileHelper::applyImage(
                    $data->formation, 
                    $factory->path("img_formations"), 
                    $factory->cfg("img.formations_default")
                );
            }
            else 
            {
                if ( is_null($item) ) {
                    $this->redirect(Page::FORMATIONS);                    
                } else {
                    $this->redirect(Page::FORMATIONS_FORMATION, $uid);
                }
            }

            return $data;
        }
            

    /**
     * @override
     */
        public function edit(int $id=0)
        {
            if ( $this->rules->canDo("manage") )
            {
                $factory = Factory::getInstance();
                $translated = [];
                $data = new stdClass();
                 $data->formation = new Formation();
                 $data->langs = Factory::getInstance()->getTxt()->getLangs();

                if ($id)
                {
                    // Get related formation.
                    $data->formation = new Formation($this->getModel()->getById($id));
                    
                    // Bad id, redirect to formations list.
                    if ( !$data->formation->id )
                    {
                        Factory::getInstance()->setError("BAD_ITEM_ID", $id);
                        $this->redirect(Page::FORMATIONS);
                    }

                    // Get all translations.
                    $translated = $this->getModelTranslate()->getWhereContextBegins(
                        $this->getModel()->role . '.' . $id . '.'
                    );
                }
                
                $data->fields = \app\Helpers\FormHelper::extractFormFields
                (
                    $this->context . "." . $this->context_item,
                    $data->formation,
                    $translated
                );

                
                // Apply url image.
                FileHelper::applyImage(
                    $data->formation, 
                    $factory->path("img_formations"), 
                    $factory->cfg("img.formations_default")
                );
            
                return $data;
            }

            $this->redirect();
        }


    /**
     * @override
     */
        function delete(int $uid = 0) { die; }
            

    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::FORMATIONS_MANAGE);
        }
}