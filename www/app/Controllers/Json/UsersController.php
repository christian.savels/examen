<?php
namespace app\Controllers\Json;

defined('_PWE') or die("Limited acces");

use stdClass;
use app\Databases\DBFilters;
use app\Enums\Page;
use app\Helpers\JSONHelper;
use app\Enums\Access;
use app\Helpers\IconsHelper;
use app\Helpers\MailHelper;
use app\Helpers\UserHelper;
use app\Helpers\Factory;
use app\Helpers\Helper;
use app\Objects\User;
use app\Helpers\FormHelper;
use app\Helpers\FileHelper;


/**
 * 
 * JSON Users controller.
 * 
 * @author chris
 *
 */
class UsersController extends \app\Controllers\AController 
    implements \app\Controllers\interfaces\UsersJControllerInterface
{    
    protected string $context = "users";
    protected string $context_item = "user";


     /**
     * @Override
     */
        public function more() : JSONHelper 
        {
            $factory = Factory::getInstance();
            $data = $html = null;

            if ( $this->rules->canDo("manage") ) 
            {
                $uid = intval(Helper::getValue($this->context_item, 0));
                $user = null;

                if ( $uid && !is_null(($user=$this->getModel()->getById($uid))) ) 
                {                 
                    // Groups 
                    // Apply groups title.
                    $groups = [];
                    foreach ( $user->groups as &$g ) 
                    {
                        $n = $this->rules->getGroup($g);
                        if ( !is_null($n) ) {
                            $groups[] = $n->title;
                        }
                    }

                    // Get student formations
                    $m = new \app\Models\FormationUsersModel();
                    $formations = $m->getStudentFormations($uid);
                    
                    // Get student/teacher courses 
                    $m = new \app\Models\CoursesModel();
                    $student = $m->getRelatedToUser($uid);   

                    // Get teacher courses 
                    $teacher = $m->getRelatedToUser($uid); // TODO Get teacher not students.

                    $user = new User($user);
                    UserHelper::initAvatar($user);
                    
                    $html = UserHelper::makeStudentCard($user, $groups, $formations, $teacher, $student);
                }
                else {
                    $factory->setError("BAD_ID");
                }
            } 
            else {
                $factory->setError("RESTRICTED_ACCESS");
            }

            $json = new JSONHelper();
             $json->data = $data;
             $json->html = $html;
            
            return $json;    
        }


     /**
     * @Override
     */
        public function delete() : JSONHelper
        {
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("manage") ) 
            {
                $deleted = 0;
                $users = json_decode(Helper::getValue("users", null), true);
                if ( !is_array($users) ) {
                    $users = [$users];
                }

                if (count($users)) 
                {
                    $m = $this->getModel();
                    $mfu = new \app\Models\FormationUsersModel();
                    $mru = new \app\Models\RoleUsersModel();
                    $ms = new \app\Models\SessionModel();
                    $mu = new \app\Models\UserModel();

                    foreach ( $users as $uid )
                    {
                        $u = $m->getById($uid);
                        if ( is_null($u) ) {
                            $factory->setWarning("BAD_ID", $uid);
                        } 
                        else 
                        {
                            $o = new User($u);
                            // Delete my own account is not allowed.
                            if ( $uid==$factory->getUser()->id ) {
                                $factory->setWarning("CANT_DELETE_OWN");
                            }
                            // Only root can delete a root.
                            elseif ( $o->isRoot() && !$factory->getUser()->isRoot() ) {
                                $factory->setWarning("CANT_DELETE_ROOT");
                            }
                            else
                            {
                                $mfu->unenroleUser($o->id);             // Remove formations relations.
                                $mru->unenroleUser($o->id);             // Remove roles relations (also courses).
                                $ms->logout($o->id);                    // Remove session
                                $deleted += $mu->deleteByPk($o->id);    // Remove user
                            }
                        }
                    }

                    $factory->setInfo("ITEMS_DELETED", $deleted);
                }
                else {
                    $factory->setError("NO_SELECTION");
                }
            } 
            else {
                $factory->setError("RESTRICTED_ACCESS");
            }

            return new JSONHelper();            
        }
    
             
    /**
     * @Override
     */
        public function getRelatedObject(object $obj=null ) : User|null {
            return new User($obj);
        }
        
    
    /**
     * @override
     */
        public function getModel() : \app\Models\UsersModel {
            return new \app\Models\UsersModel(); 
        }
    
    
    /**
     * @override
     */
        public function manage(mixed $page=null) : JSONHelper
        {
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("manage") )
            {
                $data = new \app\Helpers\ViewDataHelper(new User(), Page::USERS_MANAGE);
                 $data->orderBy_formName = "cobitemsManage" . $this->context;
                 $data->filters_formName = "filtersManage" . $this->context;
                 $data->init($this);

                 // Job on items
                 foreach ( $data->items as &$item ) 
                 {
                    unset($item->password);

                    // Apply address attribs on main tree.
                    foreach ( $item->attribs->address as $k => $v ) {
                        $item->{$k} = $v;
                    }

                    // Current user is root, load form to change access.
                    if ( $factory->getUser()->isRoot() ) 
                    {
                        $item->access = \app\Helpers\FieldHelper::makeAccessSelect(
                            "changeUserAccess", "changeUserAccess", $item->access
                        );
                    } 
                    // Only manager, just show the access title.
                    else {
                        $item->access = $this->txt("ACCESS_" . $item->access);
                    }
                    
                    // Apply list params
                    Helper::applyListParams($item); 
                    
                    // Convert status to icon.
                    $item->published_icon = IconsHelper::get("user-status-" . $item->published);

                    // Teacher ? Student ?
                    $item->isTeacher = $item->isTeacher ? IconsHelper::get("teachers") : "-";
                    $item->isStudent = $item->isStudent ? IconsHelper::get("students") : "-";
                 }
            }
        
            return new JSONHelper(null, $data->items, $data->pagination);
        }

    
    /**
     * @override
     */
        public function edit(int $uid=0) : JSONHelper
        {
            $factory = Factory::getInstance();
            $html = null;
            $data = null;

            if ( $this->rules->canDo("manage") ) 
            {
                $m = new \app\Models\UserModel();
                if ( !$uid ) {
                    $uid = Helper::getValue($this->context_item, 0);
                }
                        
                // Get user & bind it to form.
                $data = new User($m->getById($uid));

                // Define action
                $action = $data->id ? Page::USERS_EDIT_APPLY : Page::USERS_SIGNUP;
                $xml = "users.edit";
                
                // Edit a root ?
                if ( $data->isRoot() && !$factory->getUser()->isRoot() ) {
                    $factory->setError("EDIT_ROOT");
                } 
                else 
                {
                    $html = FormHelper::makeFromExtractedFormFields
                    (
                        FormHelper::extractFormFields($xml, $data),
                        "formEditUserData", "formEditUserData", 
                        $factory->getUrl($action)
                    );
            
                    // Avatar full url.
                    if ( !empty($data->attribs->social->avatar) ) 
                    {
                        $url = $factory->getUrlFromPath($factory->path("avatar"));
                        $data->attribs->social->avatar = $url . $data->attribs->social->avatar;
                    }
                }                            
            } 
            else {
                $factory->setError("RESTRICTED_ACCESS");
            }

            $json = new JSONHelper();
             $json->data = $data;
             $json->html = $html;
            
            return $json;
        }
        

    /**
     * @override
     */
        public function create() : JSONHelper
        {
            $factory = Factory::getInstance();
            $data = null;
            $html = null;

            if ($this->rules->canDo("manage")) 
            {
                $data = new stdClass;
                $html = FormHelper::makeFromExtractedFormFields
                (
                    FormHelper::extractFormFields(
                        $this->context . ".create"
                    ), "formNewUser", "formNewUser", 
                    $factory->getUrl(Page::USERS_SIGNUP, null, true)
                );
            } else {
                $factory->setError("RESTRICTED_ACCESS");
            }

            $json = new JSONHelper();
             $json->data = $data;
             $json->html = $html;
            
            return $json;
        }
    
    
    /**
     * @override
     */
        public function apply() : JSONHelper
        {
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("manage") ) 
            {
                $uid = intval(Helper::getValue("id", 0));
                $usr = $this->getModel()->getById($uid);

                if ( !is_null($usr) )
                {
                    // Collect old data before apply newest.
                    $access = $usr->access;
                    $ignore = ["remove_avatar", "pwd_secure"];
                    $xml = FormHelper::extractTree("users.edit");

                    foreach ( $xml as $node )
                    {
                        // Create attribs tree if needed.
                        if ( $node->attribs ) 
                        {
                            if ( !isset($usr->attribs->{$node->attribs_tree}) ) {
                                $usr->attribs->{$node->attribs_tree} = new stdClass();
                            }
                            if ( !isset($usr->attribs->{$node->attribs_tree}->{$node->name}) ) {
                                $usr->attribs->{$node->attribs_tree}->{$node->name} = null;
                            }
                        }
                        
                        /*
                        * Manage avatar.
                        * Upload new avatar and/or remove old.
                        */ 
                        if ( $node->name=="avatar" ) 
                        {
                            $av = &$usr->attribs->{$node->attribs_tree}->avatar;
                            $hasUpload = FileHelper::hasFileToUpload($node->name);
                            // Remove old avatar
                            if ( !empty($av) && (Helper::getValue("remove_avatar", 0) || $hasUpload) ) {
                                FileHelper::removeFile($factory->path("avatar") . $av);
                                $av = "";
                            }
                            
                            // Upload new
                            if ($hasUpload)
                            {
                                $avatar = FileHelper::uploadImage
                                (
                                    $node->name, 
                                    $factory->path("avatar"), 
                                    "avatar-" . $usr->id,
                                    Factory::getInstance()->cfg("form.img_max_size"),
                                    Factory::getInstance()->cfg("form.img_mime")
                                ); 
                                if ( !empty($avatar) ) {
                                    $av = $avatar;
                                }
                            }
                        }
                        
                        /*
                        * Others fields.
                        */
                        elseif ( !in_array($node->name, $ignore) ) 
                        {
                            $val = Helper::getValue($node->name, $node->default);
                            
                            // Check empty required fields.
                            if ( $node->required && !strlen(trim($val)) ) {
                                $factory->setError("REQUIRED_FIELD", $this->txt("FIELD_" . $node->name));
                            } 
                            else 
                            {
                                // To attribs
                                if ( $node->attribs ) {                                
                                    $usr->attribs->{$node->attribs_tree}->{$node->name} = $val;
                                }
                                else 
                                {
                                    if ( !in_array($node->name, ["username", "email", "pwd_secure", "pwd", "pwd2", "avatar"]) ) {
                                        $usr->{$node->name} = $val;
                                        
                                    }
                                }
                            }                    
                        }                    
                    } 

                    // Check access (only root is allowed to modiy access).
                    if ( !$factory->getUser()->isRoot() ) {
                        $usr->access = $access;
                    }
                    
                    // Check uniq fields if value changed.
                    $toCheck = ["username", "email"];
                    foreach ( $toCheck as $tc )
                    {
                        $v = trim(Helper::getValue($tc, ""));
                        if ( empty($v) ) {
                            $factory->setError("REQUIRED_FIELD", $this->txt("FIELD_" . $tc));
                        }
                        elseif ( $v!=$usr->{$tc} ) 
                        {
                            $filters = new DBFilters();
                            $filters->filters->isIn[$tc] = [$v];
                            $u = $this->getModel()->getItem($filters);
                            if ( is_null($u) ) {
                                $usr->{$tc} = $v;
                            } else {
                                $factory->setError("FIELD_" . $tc . "_UNAVAILABLE");
                            }
                        } 
                        else {
                            $usr->{$tc} = $v;
                        }
                    }
                    
                    // Check new password
                    $pwd = trim(Helper::getValue("pwd", ""));
                    $pwd2 = trim(Helper::getValue("pwd2", ""));
                    if ( !empty($pwd) ) 
                    {
                        if ( $pwd===$pwd2 ) 
                        {
                            // Check easy password
                            if ( strtolower($pwd)==strtolower($usr->username) ) {
                                $factory->setError("PWD_EQ_USERNAME");
                            } elseif ( strtolower($pwd)==strtolower($usr->email) ) {
                                $factory->setError("PWD_EQ_EMAIL");
                            }
                            else 
                            {
                                $usr->password = password_hash($pwd, PASSWORD_DEFAULT);
                                $factory->setInfo("PWD_CHANGED_HAVE_TO_RESET");
                                $usr->resetpwd = 1;
                            }
                        } 
                        else 
                        {
                            $factory->setError("PASSWORD_BAD");    
                            $factory->setError("PASSWORD2_BAD");    
                        }
                    }
                    
                    // Save valid changes.
                    $usr->attribs = json_encode($usr->attribs);
                    if ( $this->getModel()->updateObject($usr) ) {
                        $factory->setSuccess("SAVE_OK");
                    } else {
                        $factory->setError("SAVE_FAILED");
                    }
                }
                else {
                    $factory->setError("BAD_ID", $uid);
                }
            }
            else {
                $factory->setError("RESTRICTED_ACCESS");
            }

            return new JSONHelper();
        }


    /**
     * @override
     */
        public function signup() : JSONHelper
        { 
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("manage") ) 
            {
                $o = new stdClass();
                 $o->username = trim(Helper::getValue("username", ""));
                 $o->firstname = trim(Helper::getValue("firstname", ""));
                 $o->lastname = trim(Helper::getValue("lastname", ""));
                 $o->email = trim(Helper::getValue("email", ""));
                 $o->password = Helper::getValue("pwd", ""); // No trim to allow whites in pwd.
                 $o->password2 = Helper::getValue("pwd2", ""); // No trim to allow whites in pwd.
                
                // Check empty values.
                foreach ( $o as $k => $v ) 
                {
                    if ( !strlen($v) ) {
                        $factory->setError(strtoupper($k) . "_BAD");
                    } elseif ( $k=="email" && !filter_var($v, FILTER_VALIDATE_EMAIL) ) {
                        $factory->setError("EMAIL_BAD");
                    }
                }

                // Not required.
                $o->birthday = trim(Helper::getValue("birthday", ""));
                $o->lang = trim(Helper::getValue("lang", ""));
                 
                // Do
                if ( !$factory->hasErrors() )
                {
                    // Check existing user
                    if ( !UserHelper::isExistingUser($o->username, $o->email) )
                    { 
                        // Check easy password
                        if ( strtolower($o->password)==strtolower($o->username) ) {
                            $factory->setError("PWD_EQ_USERNAME");
                        }
                        if ( strtolower($o->password)==strtolower($o->email) ) {
                            $factory->setError("PWD_EQ_EMAIL");
                        }
                        
                        if ( UserHelper::isValidPassword($o->password) && !$factory->hasErrors() )
                        {
                            // Hash password
                            $o->password = password_hash($o->password, PASSWORD_DEFAULT);
                            unset($o->password2);
                            
                            // Check lang validity
                            $o->lang = trim(Helper::getValue("lang", $factory->cfg("lang.default")));
                            if ( !$factory->getTxt()->isValid($o->lang) ) {
                                $o->lang = $factory->cfg("lang.default");
                            }
                            
                            // Record user.
                            $userid = $this->getModel()->insertObject($o);
                            if (!$userid) {
                                $factory->setError("SIGNUP_ERROR_WHILE");
                            } 
                            else 
                            {
                                $factory->setSuccess("SIGNUP_SUCCESS", $userid);
                                $model = new \app\Models\UserModel();
                               
                                // Apply normal tree on db entry. (attribs, ...).
                                 $user = FormHelper::saveForm(
                                    new User($model->getById($userid)),
                                    $model,
                                    FormHelper::extractTree("user.account"),
                                    "id",
                                    ["remove_image", "pwd_secure"]
                                );   
                                
                                if ( !$user->id ) {
                                    $factory->setWarning("INIT_FAILED");
                                }
                            }
                        }
                    }
                    else {
                        $factory->setError("EXISTING_USER");
                    }
                }                
            }
            else {
                $factory->setError("RESTRICTED_ACCESS");
            }
    
            return new JSONHelper();
        }
        
    
    /**
     * @override
     */
        public function access() : JSONHelper 
        {
            $factory = Factory::getInstance();

            // Only root can change access level.
            if ( $factory->getUser()->isRoot() )
            {
                $m = $this->getModel();
                $u = null;
                $uid = intval(Helper::getValue("user", 0));
                $a = intval(Helper::getValue("access", -1));

                // Valid user ?
                if ( !is_null(($u=$m->getById($uid))) ) 
                {
                    // Valid access ?
                    if ( !is_null(($a=Access::related($a))) )
                    {
                        // Always keep root access, user cannot change his own access.
                        if ( $factory->getUser()->id==$u->id ) {
                            $factory->setError("CANT_CHANGE_OWN_ACCESS");
                        } 
                        else 
                        {
                            $u->access = $a->value;
                            if ( !is_string($u->attribs) ) {
                                $u->attribs = json_encode($u->attribs);
                            }
                            if ( $m->saveObject($u) ) {
                                $factory->setSuccess("ACCESS_CHANGED"); 
                            } else {
                                $factory->setError("ACCESS_CHANGE_ERROR"); 
                            }
                        }
                    } 
                    else {
                        $factory->setError("INVALID_ACCESS");
                    }
                }
                else {
                    $factory->setError("BAD_ID");
                }
            } else {
                $factory->setError("RESTRICTED_ACCESS");
            }

            return new JSONHelper();
        }


        
    /**
     * @override
    */
        public function mailUsersForm() : JSONHelper
        {
            $factory = Factory::getInstance();
            $html = null;

            if ( $this->canDo("mailto") )
            {
                $html = MailHelper::makeMailToForm(
                    $factory->getUrl(Page::USERS_MAIL_TO, null, true)
                );
            }
            else {
                $factory->setError("RESTRICTED_ACTION");
            }

            $json = new JSONHelper();
            $json->html = $html;

            return $json;
        } 
            

            
    /**
     * @override
     */
        public function mailto() : JSONHelper
        {
            $factory = Factory::getInstance();

            if ( $this->canDo("mailto") )
            {
                $users = Helper::getValue("users");
                $subject = Helper::getValue("subject", "");
                $content = Helper::getValue("content", "");

                if ( empty($users) ) {
                    $factory->setError("MAIL_NO_USERS");
                }
                elseif ( empty(trim($subject)) ) {
                    $factory->setError("MAIL_NO_SUBJECT");
                }
                elseif ( empty(trim($content)) ) {
                    $factory->setError("MAIL_NO_CONTENT");
                }
                else 
                {
                    $mails = [];
                    $users = json_decode($users, true);
                    if ( is_string($users) ) {
                        $users = [$users];
                    }

                    $f = new DBFilters();
                    $f->filters->isIn["id"] = $users;
                    $tmp = $this->getModel()->getItems($f);
                    foreach ( $tmp as $u ) {
                        $mails[] = $u->email;
                    }

                    if ( MailHelper::mailTo($mails, $subject, $content) ) {
                        $factory->setSuccess("MAIL_SENT", count($mails));
                    }
                }
            }
            else {
                $factory->setError("RESTRICTED_ACTION");
            }

            return new JSONHelper();
        }

        
    /**
     * @override
     */
        protected function guestCanAccess() : bool {
            return false;
        }


    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::USERS_MANAGE);
        }
}