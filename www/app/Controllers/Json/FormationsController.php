<?php
namespace app\Controllers\Json;

defined('_PWE') or die("Limited acces");

use app\Enums\Page;
use app\Enums\Status;
use app\Helpers\FileHelper;
use app\Helpers\JSONHelper;
use app\Objects\Formation;
use app\Helpers\Factory;
use app\Enums\Page as Pg;
use app\Helpers\Helper;


/**
 * 
 * Controller related to the formations.
 * 
 * @author chris
 * 
 */
class FormationsController extends \app\Controllers\AController 
    implements \app\Controllers\interfaces\FormationsJControllerInterface
{    
    protected string $context = "formations";
    protected string $context_item = "formation";
    

    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : Formation|null {
            return new Formation($obj); 
        }

    
    /**
     * @override
     */
        public function getModel() : \app\Models\FormationModel {
            return new \app\Models\FormationModel();
        }


	/**
	 * @override
	 */
        public function list(mixed $page = null) : mixed
        { 
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("access") )
            {
                $data = new \app\Helpers\ViewDataHelper(new Formation(), Pg::FORMATIONS);
                 $data->dbFilters->filters->isIn["published"] = [Status::PUBLISHED->value];
                 $data->orderBy_formName = "cobitems" . $this->context;
                 $data->filters_formName = "filters" . $this->context;
                 $data->init($this);

                 // Finalize items.
                 foreach ( $data->items as &$o )
                 {
                    $o = new Formation($o);
                    Helper::applyListParams($o);

                    // Apply url image.
                    FileHelper::applyImage(
                        $o, 
                        $factory->path("img_formations"), 
                        $factory->cfg("img.formations_default")
                    );

                    // Link to...
                    $o->link = $factory->getUrl(Pg::FORMATIONS_FORMATION, $o->id);
                 }
            }
        
            return new JSONHelper(null, $data->items, $data->pagination);
        }
        

	/**
	 * @override
	 */
        public function manage() 
        {
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("manage") )
            {
                $data = new \app\Helpers\ViewDataHelper(new Formation(), Pg::FORMATIONS_MANAGE);
                 $data->orderBy_formName = "cobitemsManage" . $this->context;
                 $data->filters_formName = "filtersManage" . $this->context;
                 $data->init($this);

                 // Job on items
                 foreach ( $data->items as &$item ) 
                 {
                    $item = new Formation($item);
                     $item->editLink = $factory->getUrl(Pg::FORMATIONS_EDIT, $item->id);
                
                    Helper::applyListParams($item);
                 }

                 return new JSONHelper(null, $data->items, $data->pagination);
            }
        
            return new JSONHelper();
        }


    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::FORMATIONS_MANAGE);
        }
}