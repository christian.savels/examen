<?php
namespace app\Controllers\Json;

defined('_PWE') or die("Limited acces");

use stdClass;
use app\Helpers\JSONHelper;
use app\Models\CoursesModel;
use app\Objects\Course;
use app\Databases\DBFilters;
use app\Enums\Page;
use app\Helpers\MailHelper;
use app\Helpers\Factory;
use app\Enums\Page as Pg;
use app\Helpers\FormHelper;
use app\Helpers\Helper;


/**
 * 
 * Controller related to the courses.
 * 
 * @author chris
 *
 */
class CourseController extends \app\Controllers\AController 
    implements \app\Controllers\interfaces\CourseJControllerInterface
{    
    protected string $context = "courses";
    protected string $context_item = "course";


    /**
     * @Override
     */
        public function getRelatedObject(object $obj=null ) : Course {
            return new Course($obj); 
        }

        
    /**
     * @Override
     */
        public function register(int $cid) : JSONHelper 
        {
            $factory = Factory::getInstance();
            $course = \app\Helpers\CoursesHelper::checkCourseRegistration($cid, 0);

            if ( $course->id )
            {
                $role = $course->roleStudents;
                $uid = $factory->getUser()->id;
 
                if ( $this->getModel()->enroleUsersToCourses([$uid], [$role], 0)===1) 
                {
                    $factory->setSuccess("REGISTRATION_OK");
                    $factory->setInfo("REGISTRATION_NOW_PENDING");
                }
            }

            $json = new JSONHelper();
             $json->redirect = $factory->getUrl(Page::COURSE, $cid);

            return $json;
        }

    
    /**
     * @override
     */
        public function clean() : JSONHelper 
        {
            $uid = Helper::getValue($this->context_item, 0);
            $data = null;
        
            if ( $this->isEditable($uid) )
            {
                $factory = Factory::getInstance();
                $context =  $this->context_item . '.' . $uid . '.';
                $rmod = new \app\Models\RoleModel();
                $rumod = new \app\Models\RoleUsersModel();
                $filters = new DBFilters();

                // Exclude all users from courses.
                $factory->setInfo("USERS_UNENROLED", $rumod->deleteFromContext($context));
                
                // Remove extra roles.
                $contexts = [$context . "teachers", $context . "students"];
                $filters->filters->begins['context'] = $context;
                $filters->filters->notIn["context"] = $contexts;
                $factory->setInfo("XTRA_ROLES_DELETED", $rmod->delete($filters));
                
                // Get list of users after clean (should be empty).
                // Will force js to reload (empty) list.
                $data = $this->users();
            }

            return new JSONHelper(null, $data);
        }
    
    
    /**
     * @override
     */
        public function getModel() : CoursesModel {
            return new CoursesModel();
        }
        
        
    /**
     * @override
     */
        public function enroleto() : JSONHelper
        {
            $data = null;
            $targets = ['add-teacher' => 'teachers', 'add-student' => 'students'];
            $target = Helper::getValue("action", "bad");
            $uid = Helper::getValue($this->context_item, 0);
            $users = Helper::getValue("users", null);
            
            if ( !empty($users) && array_key_exists($target, $targets) && $this->isEditable($uid) )
            {  
                $rmodel = new \app\Models\RoleModel();
                $model = new \app\Models\RoleUsersModel();
                $target = $targets[$target];
                $fllters = new DBFilters();
                 $fllters->filters->isIn['context'] = [$this->context_item . '.' . $uid . '.' . $target];
              
                $g = $rmodel->getItem($fllters);
                if ( !is_null($g) ) 
                {
                    $users = json_decode($users, true);
                    if ( !is_array($users) ) {
                        $users = [$users];
                    }
                    
                    if ( count($users) ) 
                    {
                        $o = new stdClass();
                         $o->roleid = $g->id;
                        
                        foreach ( $users as $user )
                        {
                            $o->userid = intval($user);
                            $model->insertObject($o);
                        }
                        
                        Factory::getInstance()->setSuccess("USERS_ADDED_TO_" . strtoupper($target));
                    
                        $data = $this->users()->data;
                    }
                }
            }
            
            return new JSONHelper(null, $data);
        }
    

    /**
     * @override
     */
        public function enrole() : JSONHelper
        {
            $uid = Helper::getValue($this->context_item, 0);
            $context = Helper::getValue("context", "");
            $data = null;
            $html = null;
           
            if ( $this->isEditable($uid) && in_array($context, ['add-teacher', 'add-student']) )
            {          
                $umod = new \app\Models\UserModel();
                $sufx = $context=="add-teacher" ? "teachers" : "students";
                $data = $umod->getRelatedToRoleContexts(
                     $this->context_item . '.' . $uid . '.' . $sufx, false
                );    
                
                foreach ( $data as &$user )
                {
                    $user->role = $this->convertRole($user->role);
                     $user->applyAttribsTree(true);
                }

                $html = \app\Helpers\UserHelper::makeSelectableUsers(
                    $data, $this->rules->getGroups()
                );
            }
            
            $json = new JSONHelper();
             $json->html = $html;
            
            return $json;
        }
        
        
    /**
     * @override
     */
        public function unenrole() : JSONHelper
        {
            $uid = Helper::getValue($this->context_item, 0);
            $users = Helper::getValue("users", null);   
            
            if ( !empty($users) && $this->isEditable($uid) )
            {          
                json_decode($users, true);
                if ( !is_array($users) ) {
                    $users = [$users];
                }
                
                $deleted = $this->getModel()->removeUsersFromContext(
                    $this->context_item . '.' . $uid . '.', 
                    $users
                );
                
                if ( !$deleted ) {
                    Factory::getInstance()->setError("NO_USERS_UNENROLED");
                } else {
                    Factory::getInstance()->setInfo("USERS_UNENROLED", $deleted);
                }
            }          
            
            return new JSONHelper();
        }
        
        
    
    /**
     * @override
     */
        public function users() : JSONHelper
        {                 
            $uid = Helper::getValue($this->context_item, 0);
            $data = null;
            
            if ( $this->isEditable($uid) )
            {
                $umod = new \app\Models\UserModel();
                $users = $umod->getRelatedToRoleContexts(
                    $this->context_item . '.' . $uid . '.', true
                );    
                
                $data = new stdClass();
                 $data->teachers = [];
                 $data->students = [];
                foreach ( $users as $user )
                {
                    // Get tree branch related to the role section. (teachers, students)
                    $s = explode('.', $user->role);
                    $r = end($s);
                    
                    // Convert role to human readable ^^.
                    $user->role = $this->convertRole($user->role);
                    $user->applyAttribsTree();
                    
                    \app\Helpers\UserHelper::initAvatar($user);
                    
                    $data->{$r}[] = $user;
                }               
            }

            return new JSONHelper(null, $data);
        }
        
        
    /**
     * COnvert role to human readable.
     * 
     * @param string $role
     * 
     * @return string
     */
        private function convertRole(string $role=null) : string 
        {
            if ( is_null($role) ) {
                $role = "";
            }
            $segments = explode('.', $role);
            return $this->txt("ROLE_" . end($segments));
        }
        
        
    /**
     * @override
     */
        public function delete(int $uid=0) : JSONHelper
        {
            $factory = Factory::getInstance();
            
            if ( !$uid ) {
                $uid = Helper::getValue($this->context_item, 0);
            }            
            if ( $this->isEditable($uid) )
            {
                // Associated to a formation ? (protected)
                $fmod = new \app\Models\FormationCoursesModel();
                $f = new DBFilters();
                 $f->filters->isIn["cid"] = [$uid];
                $associations = $fmod->getItems($f);
                if ( count($associations) ) 
                {
                    $formations = [];
                    foreach ( $associations as $fo ) {
                        $formations[] = $fo->uid;
                    }
                    $factory->setError("COURSES_NOT_DELETABLE", implode(', ', $formations));
                }
                // Delete course & related.
                else 
                {
                    $context = $this->context_item . '.' . $uid .  '.';
                    $rmod = new \app\Models\RoleModel();
                    $f = new DBFilters();
                    $f->behavior->orderBy = new \app\Helpers\OrderByHelper("");
                    $f->behavior->select = ["a.id"];
                    $f->filters->begins['context'] = $context;
                    $roles = $rmod->getItems($f);
                    if ( count($roles) ) 
                    {
                        $rids = [];                     
                        foreach( $roles as $r ) {
                            $rids[] = $r->id;
                        }
                        
                        // 1 -> Delete related users from pw_role_user
                        $unenroled = $rmod->unenrole($rids);
                        if ($unenroled) {
                            $factory->setInfo("USERS_UNENROLED", $unenroled);
                        }
                        
                        // 2 -> Delete roles related to course.
                        $deleted = $rmod->deleteByPk($rids);
                        if ($deleted) {
                            $factory->setInfo("ROLES_DELETED", $deleted);
                        } else {
                            $factory->setError("ROLES_NOT_DELETED");
                        }
                    }
                    
                    // 3 -> Delete related translations.
                    $contexts = FormHelper::extractTranslableContexts(
                        $this->context_item, $uid, "courses.course"
                    );
                    if ( count($contexts) ) {
                        $factory->setInfo("TRANSLATIONS_DELETED", $rmod->deleteTranslated($contexts));
                    }
                    
                    // 4 -> Delete course
                    if ( $this->getModel()->deleteByPk($uid) ) {
                        $factory->setInfo("COURSE_DELETED");
                    } else {
                        $factory->setInfo("COURSE_NOT_DELETED");
                    }
                 }
            }
            
            return new JSONHelper();
        }
                
                
    /**
     * @override
     */
        public function edit(int $id=0) : JSONHelper
        {
            $factory = Factory::getInstance();
            $item = new Course();
        
            if ( $this->rules->canDo("manage") )
            {
                $model = $this->getModel();

                if ( !$id ) {
                    $id = Helper::getValue("id");
                }                
                
                // Get edited item.
                if ($id)
                {
                    $item = $model->getById($id);
                    if ( is_null($item) ) {
                        $factory->setError("COURSES_BAD_COURSE", $id);
                    }
                }
                
                // Save form
                if ( !$factory->hasErrors() ) 
                {
                    $item = FormHelper::saveForm
                    (
                        new Course(),
                        $model,
                        FormHelper::extractTree($this->context . "." . $this->context_item),
                        "id",
                        ["remove_image"]
                    );
                                
                    if ($item->id) 
                    {
                        if ( is_string($item->attribs) ) {
                            $item->attribs = json_decode($item->attribs , false);
                        }
                        
                        // Check image.
                        \app\Helpers\FileHelper::uploaditemImgOnSave(
                            $item, $model, $factory->path("img_courses"), $this->context_item . "-" . $item->id
                        );
                        
                        // In case of new course, create groups teachers/students related to the course.
                        if(!$id)
                        {
                            $rmod = new \app\Models\RoleModel();
                            $role = new stdClass();
                           
                             // Students group
                             $role->context = $this->context_item . '.' . $item->id .  '.students';
                             $role->protected = 0;
                              $rmod->insertObject($role);
                              
                             // Teachers group
                             $role->context = $this->context_item . '.' . $item->id .  '.teachers';
                              $rmod->insertObject($role);
                        }                        
                        
                        $factory->setSuccess("COURSE_SAVE_OK", $item->id);
                    } 
                    else {
                        $factory->setError("COURSE_SAVE_FAILED");
                    }
                }
            }
     
            // Prepare return
            if ( $factory->getFormat()=="json" ) 
            {
                // Message about auto redirect.
                if ( !$factory->hasErrors() ) {
                    $factory->setInfo("REDICT_ON", (int)$factory->cfg("delay.redirect") / 1000);
                }

                // Json
                $json = new JSONHelper();
                if ( $json->done ) 
                {
                    $json->redirectDelay = $factory->cfg("delay.redirect");
                    $json->redirect = $factory->getUrl(Page::COURSES_MANAGE);
                }

                return $json;
            }
           
            $this->redirect(Pg::COURSES);
        }
        
        
    /**
     * @override
     */
        public function mailto() : JSONHelper
        {
            $factory = Factory::getInstance();

            if ( $this->canDo("mailto") )
            {
                $m = $this->getModel();
                $item = Helper::getValue($this->context_item, 0);
                $target = Helper::getValue("target", "");
                $subject = Helper::getValue("subject", "");
                $content = Helper::getValue("content", "");

                if ( empty($item) || is_null(($item=$m->getById($item))) ) {
                    $factory->setError("MAIL_BAD_FORMATION");
                }
                elseif ( !in_array($target, ["teachers", "students"]) ) {
                    $factory->setError("MAIL_BAD_TARGET");
                }
                elseif ( empty(trim($subject)) ) {
                    $factory->setError("MAIL_NO_SUBJECT");
                }
                elseif ( empty(trim($content)) ) {
                    $factory->setError("MAIL_NO_CONTENT");
                }
                else 
                {
                    $tmp = $m->getRelatedUsers($item->id, $target);
                    $mails = [];

                    foreach ( $tmp as $u ) {
                        $mails[] = $u->email;
                    }

                    MailHelper::mailTo($mails, $subject, $content);

                    $factory->setSuccess("MAIL_SENT", count($mails));
                }
            }
            else {
                $factory->setError("RESTRICTED_ACTION");
            }

            return new JSONHelper();
        }


    /**
     * @override
     */
        public function mailformstudents() : JSONHelper {
            return $this->mailform("students", Page::COURSES_MAIL_TO);
        }


    /**
     * @override
     */
        public function mailformteachers() : JSONHelper {
            return $this->mailform("teachers", Page::COURSES_MAIL_TO);
        }

            
    /**
     * @Override
     */
        public function details(int $uid=0, mixed $page=null) : mixed { die; }
        
        
    /**
     * {@inheritDoc}
     * @see \app\Controllers\AController::guestCanAccess()
     */
        protected function guestCanAccess(): bool {
            return false;
        }
    
        
    /**
     * {@inheritDoc}
     * @see \app\Controllers\AController::isRootAccess()
     */
        protected function isRootAccess(): bool {
            return false;
        }
        
        
    /**
     * {@inheritDoc}
     * @see \app\Controllers\AController::loggedCanAccess()
     */
        protected function loggedCanAccess(): bool {
            return true;
        }
                

    /**
     * {@inheritDoc}
     * @see \app\Controllers\AController::getLinkManage()
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::COURSES_MANAGE);
        }
}