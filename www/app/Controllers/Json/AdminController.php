<?php
namespace app\Controllers\Json;

defined('_PWE') or die("Limited acces");

use app\Helpers\Factory;
use app\Enums\Page as Pg;
use app\Helpers\JSONHelper;

/**
 * 
 * Controller for the admin forms and json requests.
 * 
 * @author chris
 *
 */ 
class AdminController extends \app\Controllers\AController  
    implements \app\Controllers\interfaces\AdminControllerJInterface 
{    
    protected string $context = "admin";
    protected string $context_item = "admin";
    
  

    /**
     * @override
     */
        public function getModel() : object {
            return new \stdClass(); 
        }
        
        
    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : \app\Objects\AObj|null {
            return null; 
        }

        
    /**
     * @override
     */
        protected function isRootAccess() : bool {
            return true;
        }


    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Pg::ADMIN);
        }
}