<?php
namespace app\Controllers\Json;
use app\Helpers\JSONHelper;

defined('_PWE') or die("Limited acces");

use app\Helpers\Factory;
use app\Enums\Page as Pg;
use app\Objects\Degree;


/**
 * 
 * Controller of the degrees view.
 * 
 * @author chris
 *
 */
class DegreesController extends \app\Controllers\AController
    implements \app\Controllers\interfaces\DegreesJControllerInterface 
{   
    protected string $context = "degrees";
    protected string $context_item = "degree";
    
    
    /**
     * @override
     */
        public function getModel() : \app\Models\DegreeModel {
            return new \app\Models\DegreeModel();
        }
    

    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : Degree|null {
            return new Degree($obj); 
        }
    
    
    /**
     * @override
     */
        public function list(mixed $page=null) : mixed { die; }
        
        
    /**
     * @override
     */
        public function manage() : JSONHelper
        { 
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("manage") )
            {
                $data = new \app\Helpers\ViewDataHelper(new Degree(), Pg::DEGREES_MANAGE);
                 $data->orderBy_formName = "cobitemsManage" . $this->context;
                 $data->filters_formName = "filtersManage" . $this->context;
                 $data->init($this);

                 // Job on items
                 foreach ( $data->items as &$item ) {
                    $item->link = $factory->getUrl(Pg::DEGREES_EDIT, $item->id);
                 }
            }
        
            return new JSONHelper(null, $data->items, $data->pagination);
        }
        

    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Pg::DEGREES_MANAGE);
        }       
   
}