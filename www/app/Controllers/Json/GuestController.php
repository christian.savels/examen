<?php
namespace app\Controllers\Json;

defined('_PWE') or die("Limited acces");

use app\Enums\UserStatus;
use app\Helpers\FormHelper;
use DateTimeImmutable;
use app\Helpers\Factory;
use app\Helpers\Helper;
use app\Objects\User;
use app\Enums\Page as Pg;
use app\Helpers\UserHelper;


/**
 * 
 * Controller of the guest forms and json requests.
 * 
 * @author chris
 *
 */
class GuestController extends \app\Controllers\AController 
    implements \app\Controllers\interfaces\GuestControllerInterface
{    
    
    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : User|null {
            return new User($obj); 
        }
        
    
    /**
     * @override
     */
        public function login()
        {
            $factory = Factory::getInstance();
            if ( $factory->getUser()->isGuest() ) 
            {
                $identifier = trim(Helper::getValue("identifier", ""));
                $pwd = Helper::getValue("pwd", ""); // No trim to allow whites in pwd.
                if ( !empty($identifier) && !empty(trim($pwd)) )
                {
                    // Query filters
                    $filters = new \app\Databases\DBFilters();
                     if ( Helper::isMail($identifier) ) {
                        $filters->filters->isIn["email"] = [$identifier];
                     } else {
                         $filters->filters->isIn["username"] = [$identifier];
                     }
                    
                    // Get user.
                    $usr = $this->getModel()->getItem($filters);

                    // Good login
                    if ( !is_null($usr) && password_verify($pwd, $usr->password) )
                    {
                        // User id banned ?
                        if ( $usr->published==UserStatus::BANNED->value ) {
                            $factory->setError("YOUR_ARE_BANNED");
                        } 
                        else 
                        {
                            $modelSession = new \app\Models\SessionModel();
                            $time = new DateTimeImmutable("now");
                            $token = \app\Helpers\SessionHelper::makeToken();
                            
                            // Record token to db
                            $t = new \stdClass();
                             $t->sessionid = $token;
                             $t->userid = $usr->id;
                             $t->time = $time->format("Y-m-d H:i:s");

                            // Update previous session
                            if ( !is_null($usr->token_session) ) 
                            {
                                $usr->token_session = $token;
                                $usr->token_time = $t->time;
                                $modelSession->updateObject($t, "userid");
                            }
                            // Record new session.
                            else {
                                $modelSession->insertObject($t);
                            }
                            
                            $usr = new User($usr);
                            $factory->setUser($usr);                             
                            \app\Helpers\SessionHelper::destroyFor($usr);
                            $factory->setSuccess("LOGIN_SUCCESS", $usr->firstname);

                            // Reset pwd alert
                            if ( $usr->resetpwd ) {
                                $factory->setWarning("RESET_YOUR_PWD");
                            }
                         }  
                    }
                    // Bad login
                    else {
                        $factory->setError("LOGIN_BAD");
                    }
                } else {
                    $factory->setError("EMPTY_FIELDS");
                }
            } else {
                $factory->setError("ALREADY_LOGGEDIN");
            }
            
            // Finalize return
            $json = new \app\Helpers\JSONHelper();
            if ($json->done) {
                $json->redirect = $factory->getUrl(Pg::USER_ACCOUNT);
            }
            
            return $json;
        }

    
    /**
     * @override
     */
        public function signup() : object
        {
            $factory = Factory::getInstance();
            
            if ( !$factory->cfg("site.register_active") ) {
                $factory->setError("SIGNUP_DISABLED");
            } elseif ( !$factory->getUser()->isGuest() ) {
                $factory->setError("SIGNUP_ALREADY_LOGGEDIN");
            }
            else
            {
                $o = new \stdClass();
                 $o->username = trim(Helper::getValue("username", ""));
                 $o->firstname = trim(Helper::getValue("firstname", ""));
                 $o->lastname = trim(Helper::getValue("lastname", ""));
                 $o->email = trim(Helper::getValue("email", ""));
                 $o->password = Helper::getValue("pwd", ""); // No trim to allow whites in pwd.
                 $o->password2 = Helper::getValue("pwd2", ""); // No trim to allow whites in pwd.
                 
                // Check empty values.
                foreach ( $o as $k => $v ) 
                {
                    if ( !strlen($v) ) {
                        $factory->setError(strtoupper($k) . "_BAD");
                    }
                }
                
                if ( !$factory->hasErrors() )
                {
                    // Check existing user
                    if ( !UserHelper::isExistingUser($o->username, $o->email) )
                    {
                        // Check easy password
                        if ( strtolower($o->password)==strtolower($o->username) ) {
                            $factory->setError("PWD_EQ_USERNAME");
                        }
                        if ( strtolower($o->password)==strtolower($o->email) ) {
                            $factory->setError("PWD_EQ_EMAIL");
                        }
                        
                        if ( UserHelper::isValidPassword($o->password) && !$factory->hasErrors() )
                        {
                            // Hash password
                            $o->password = password_hash($o->password, PASSWORD_DEFAULT);
                            unset($o->password2);
                            
                            // Check lang validity
                            $o->lang = trim(Helper::getValue("lang", $factory->cfg("lang.default")));
                            if ( !$factory->getTxt()->isValid($o->lang) ) {
                                $o->lang = $factory->cfg("lang.default");
                            }
                            
                            // Record user.
                            $userid = $this->getModel()->insertObject($o);
                            if (!$userid) {
                                $factory->setError("SIGNUP_ERROR_WHILE");
                            } 
                            else 
                            {
                                $factory->setSuccess("SIGNUP_SUCCESS");
                                $model = new \app\Models\UserModel();
                               
                                // Apply normal tree branches on db entry. (attribs, ...).
                                FormHelper::saveForm(
                                    new User($model->getById($userid)),
                                    $model,
                                    FormHelper::extractTree("user.account"),
                                    "id",
                                    ["remove_image", "pwd_secure"]
                                );   
                            }
                        }
                    }
                    else {
                        $factory->setError("EXISTING_USER");
                    }
                }
            }
        
            // Make return
            $json = new \app\Helpers\JSONHelper();
            if ( $json->done ) {
                $json->redirect = $factory->getUrl(Pg::USER_LOGIN);
            }
            
            return $json;
        }
    
    
    /**
     * @override
     */
        public function getModel() : \app\Models\UserModel {
            return new \app\Models\UserModel(); 
        }
         
        
    /**
     * {@inheritDoc}
     * @see \app\Controllers\AController::guestCanAccess()
     */
        protected function guestCanAccess(): bool {
            return true;
        }
    
        
    /**
     * {@inheritDoc}
     * @see \app\Controllers\AController::isRootAccess()
     */
        protected function isRootAccess(): bool {
            return false;
        }
        
        
    /**
     * {@inheritDoc}
     * @see \app\Controllers\AController::loggedCanAccess()
     */
        protected function loggedCanAccess(): bool {
            return false;
        }
                

    /**
     * {@inheritDoc}
     * @see \app\Controllers\AController::getLinkManage()
     */
        public function getLinkManage() : string {
            return "";
        }
    
}