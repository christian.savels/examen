<?php
namespace app\Controllers\Json;
use app\Models\AModel;
use app\Models\MailModel;

defined('_PWE') or die("Limited acces");

use app\Enums\Page;
use app\Enums\Status;
use app\Helpers\FileHelper;
use app\Helpers\JSONHelper;
use app\Objects\Formation;
use app\Helpers\Factory;
use app\Enums\Page as Pg;
use app\Helpers\Helper;


/**
 * 
 * Controller for mass mails.
 * 
 * @author chris
 * 
 */
class MailController extends \app\Controllers\AController 
    
{    
    protected string $context = "mails";
    protected string $context_item = "mail";
    

    /**
     * @override
     */
    public function getMailForm() : JSONHelper
    {
        $target = Helper::getValue("mailto", "");
        
        if ( in_array($target, ['all', 'teachers', 'students']) ) {
            return $this->mailform($target, Pg::ADMIN_MAIL_TO);
        } else {
            Factory::getInstance()->setError("RESTRICTED_ACTION");
        }

        return new JSONHelper();
    }

    
    /**
     * @override
     */
    public function mailto() : JSONHelper
    {
        $factory = Factory::getInstance();
        $m = $this->getModel();
        $target = Helper::getValue("mailto", "");
        $subject = Helper::getValue("subject", "");
        $content = Helper::getValue("content", "");

        if ( !in_array($target, ["all", "teachers", "students"]) ) {
            $factory->setError("MAIL_BAD_TARGET");
        }
        elseif ( empty(trim($subject)) ) {
            $factory->setError("MAIL_NO_SUBJECT");
        }
        elseif ( empty(trim($content)) ) {
            $factory->setError("MAIL_NO_CONTENT");
        }
        else 
        {    
            if ( in_array($target, ["teachers", "students"]) ) {
                $mails = $m->getSection($target);
            } else {
                $mails = $m->getMails();
            }

            \app\Helpers\MailHelper::mailTo($mails, $subject, $content);

            $factory->setSuccess("MAIL_SENT", count($mails));
        }
        

        return new JSONHelper();
    }


    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : \app\Objects\AObj|null {
            return null; 
        }

    
    /**
     * @override
     */
        public function getModel() : MailModel {
            return new MailModel();
        }


    /**
     * @override
     */
        protected function isRootAccess(): bool {
            return true;
        }


    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::ADMIN);
        }
}