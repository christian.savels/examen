<?php
namespace app\Controllers\Json;

defined('_PWE') or die("Limited acces");

use app\Helpers\JSONHelper;
use app\Helpers\Factory;
use app\Enums\Page as Pg;


/**
 * 
 * Controller for the admin forms and json requests.
 * 
 * @author chris
 *
 */
class ConfigController extends \app\Controllers\AController 
    
{    
    protected string $context = "config";
    protected string $context_item = "config";
    

    /**
     * @override
     */
        public function getModel() : object {
            return new \stdClass(); 
        }
        
        
    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : \app\Objects\AObj|null {
            return null; 
        }
        

    
    /**
     * Save updated configuration(.ini).
     * @return JSONHelper
     */
        public function save() 
        {
            $factory = Factory::getInstance();

            if ( $factory->getUser()->isRoot() ) { 
                $factory->getSiteParams()->save($_POST);
            } else {
                $factory->setError("RESTRICTED_ACCESS");
            }

            return new JSONHelper();
        }
        
        
    /**
     * @override
     */
        protected function isRootAccess(): bool {
            return true;
        }
        

    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Pg::ADMIN);
        }
}