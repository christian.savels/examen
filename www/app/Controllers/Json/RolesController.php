<?php
namespace app\Controllers\Json;

defined('_PWE') or die("Limited acces");

use app\Helpers\JSONHelper;
use app\Objects\Role;
use app\Helpers\Factory;
use app\Enums\Page as Pg;


/** 
 * 
 * Controller related to the roles.
 * 
 * @author chris
 *
 */
class RolesController extends \app\Controllers\AController 
    implements \app\Controllers\interfaces\RolesJControllerInterface
{    
    protected string $context = "roles";
    protected string $context_item = "role";
        
    
    /**
     * @override
     */
        public function getModel() : object {
            return new \app\Models\RoleModel();
        }
        
        
    /**
     * @Override
     */
        public function getRelatedObject(object $obj=null ) : Role|null {
            return new Role(); 
        }
        
        
    /**
     * @Override
     */
        public function manage(mixed $page=null) : JSONHelper
        {
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("manage") )
            {
                $data = new \app\Helpers\ViewDataHelper(new Role(), Pg::ROLES_MANAGE);
                 $data->dbFilters->filters->isNull[] = "context";
                 $data->orderBy_formName = "cobitemsManage" . $this->context;
                 $data->filters_formName = "filtersManage" . $this->context;
                  $data->init($this);

                 // Job on items
                 foreach ( $data->items as &$item ) 
                 {
                    $item = new Role($item);
                     $item->link = $factory->getUrl(Pg::ROLES_ROLE_EDIT, $item->id);
                 }
            }
        
            $json = new JSONHelper(null, $data->items, $data->pagination);

            return $json;
        }
                
        
    /**
     * @override
     */
        protected function guestCanAccess(): bool {
            return false;
        }
    
            

    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Pg::ROLES_MANAGE);
        }
}