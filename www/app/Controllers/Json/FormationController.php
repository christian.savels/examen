<?php
namespace app\Controllers\Json;

use app\Databases\DBFilters;
use app\Enums\Page;
use app\Enums\Status;
use app\Enums\UserStatus;
use app\Helpers\FileHelper;
use app\Helpers\Helper;
use app\Helpers\JSONHelper;
use app\Models\CoursesModel;
use app\Models\FormationCoursesModel;
use app\Models\FormationUsersModel;
use app\Models\RoleModel;
use app\Models\UsersModel;
use app\Objects\Course;
use app\Objects\Formation;
use Exception;
use stdClass;

defined('_PWE') or die("Limited acces");

use app\Helpers\Factory;


/**
 * 
 * JSON Formation controller.
 * 
 * @author chris
 *
 */
class FormationController extends \app\Controllers\AController 
    implements \app\Controllers\interfaces\FormationJControllerInterface  
{   
    protected string $context = "formations";
    protected string $context_item = "formation";

    
    /**
     * @override
     */
        public function getModel() {
            return new \app\Models\FormationModel();
        } 

    
    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : Formation|null {
            return new Formation($obj); 
        }

    
    /**
     * @override
     */
        public function delete(int $uid=0) 
        {
            if ( !$uid ) {
                $uid = Helper::getValue('formation', 0);
            }
                
            $factory = Factory::getInstance();
            if ( $this->isEditable($uid) )
            {
                $factory->getTxt()->loadFiles(["formations.default"]);
                $umod = new FormationUsersModel();
                $model = $this->getModel();

                // Check associated students.
                $related = $umod->getRelatedStudents($uid);
                if( count($related) ) {
                    $factory->setError("ERR_STUDENTS_RELATED", count($related));
                }
                else 
                {
                    // Remove courses associations.
                    $cmod = new FormationCoursesModel();
                    $filters = new DBFilters();
                    $filters->filters->isIn['fid'] = [$uid];
                    Factory::getInstance()->setInfo("FORMATION_UNENROLED_COURSES", $cmod->delete($filters));

                    // Remove enroled students
                    Factory::getInstance()->setInfo("FORMATION_UNENROLED", $umod->delete($filters));

                    // Delete related translations.
                    $contexts = \app\Helpers\FormHelper::extractTranslableContexts(
                        $this->context_item, $uid, $this->context . "." . $this->context_item
                    );
                    $factory->setInfo("TRANSLATIONS_DELETED", $model->deleteTranslated($contexts));

                    // Remove formation
                    unset($filters->filters->isIn['fid']);
                    $filters->filters->isIn['id'] = [$uid];

                    if ( $this->getModel()->delete($filters) ) {
                        Factory::getInstance()->setSuccess("FORMATION_DELETED");
                    } else {
                        Factory::getInstance()->setError("FORMATION_NOT_DELETED");
                    }
                }
            }
            
            return new JSONHelper();
        }
                
        
    /**
     * @override
     */
        public function edit(int $id=0)
        {
            $factory = Factory::getInstance();
        
            if ( $this->rules->canDo("manage") )
            {
                if ( !$id ) {
                    $id = Helper::getValue("id", 0);
                }

                $factory->getTxt()->loadFiles(["formations.default"]);
                $item = null;
                $model = $this->getModel();
                $uid = Helper::getValue("uid", null);
                
                // Get edited item.
                if ( $id )
                {
                    $item = $model->getById($id);
                    if ( is_null($item) ) {
                        $factory->setError("BAD_ITEM_ID", $id);
                    }
                }
                $item = new Formation($item);

                // UID check
                if ( empty(trim($uid)) ) {
                    $factory->setError("UID_REQUIRED");
                }
                else
                {
                    $f = new DBFilters();
                     $f->filters->isIn["uid"] = [$uid];
                     $f->filters->notIn["id"] = [$id];
                    $check = $model->getItem($f);
                    if ( !is_null($check) ) {
                        $factory->setError("UID_NOT_UNIQUE", $uid, $check->title);
                    }
                }
                
                // Save form
                if ( !$factory->hasErrors() ) 
                {
                    $item = \app\Helpers\FormHelper::saveForm(
                        new Formation(),
                        $model,
                        \app\Helpers\FormHelper::extractTree($this->context . "." . $this->context_item),
                        "id",
                        ["remove_image"]
                    );                    
                    
                    if ( $item->id )
                    {
                        if ( is_string($item->attribs) ) {
                            $item->attribs = json_decode($item->attribs, false);
                        }

                        // Check image.
                        FileHelper::uploaditemImgOnSave(
                            $item, $model, $factory->path("img_formations"), $this->context_item . "-" . $item->id
                        );
                        
                        $factory->setSuccess("FORMATION_SAVE_OK", $item->id);
                    } 
                    else {
                        $factory->setError("FORMATION_SAVE_FAILED");
                    }
                }
            } else {
                $factory->setError("RESTRICTED_ACTION");
            }

            if ( !$factory->hasErrors() ) {
                $factory->setInfo("REDICT_ON", (int)$factory->cfg("delay.redirect") / 1000);
            }

            $json = new JSONHelper();
            if ( $json->done && $item->id ) 
            {
                $json->redirectDelay = $factory->cfg("delay.redirect");
                $json->redirect = $factory->getUrl(Page::FORMATIONS_MANAGE, $item->id);
            }

            return $json;
        }
        
    
    /**
     * @override
     */
        public function register(int $uid) : JSONHelper
        {
            $factory = Factory::getInstance();
            $item = null;
            $factory->getTxt()->loadFiles(["formations.default"]);

            // Errors check.
            if ( !$uid || is_null(($item=$this->getModel()->getById($uid))) ) {
                $factory->setError("BAD_ITEM_ID", $uid);
            } elseif ( $factory->getUser()->isGuest() ) {
                $factory->setError("ERROR_GUEST", $item->title);
            } elseif ( $item->published!=Status::PUBLISHED->value ) {
                $factory->setError("UNAVAILABLE_FORMATION", $item->title);
            }
            
            // Do
            if ( !$factory->hasErrors() ) 
            {
                $item = new Formation($item);

                // Check if user is already registered.
                $f = new DBFilters();
                 $f->filters->isIn["fid"] = [$uid];
                 $f->filters->isIn["uid"] = [$factory->getUser()->id];
                $m = new FormationUsersModel();
                $reg = $m->getItem($f);
                if ( !is_null($reg) ) 
                {
                    if ( $reg->published==Status::PUBLISHED->value ) {
                        $factory->setError("WAS_REGISTERED", $item->title);
                    } else {
                        $factory->setError("IS_PEDING", $item->title);
                    }
                } 
                else 
                {
                    // Attach user to the formation
                    $o = new stdClass;
                     $o->fid = $uid;
                     $o->uid = $factory->getUser()->id;
                     $o->published = Status::UNPUBLISHED->value;
                    $m->insertObject($o);

                    $factory->setSuccess("REGISTERED_NOW_PENDING", $item->title);
                }
            }

            return new JSONHelper();
        } 

        


	/**
	 * @override
	 */
        public function enrole() : JSONHelper
        {
            $uid = Helper::getValue($this->context_item, 0);
            $data = [];

            if ( $this->isEditable($uid) )
            {
                Factory::getInstance()->getTxt()->loadFiles(["formations.default"]);
                $cmod = new FormationUsersModel();
                $data = $cmod->getRelatedStudents($uid, true);    
                foreach ( $data as &$o ) {
                    $o = new \app\Objects\User($o); 
                     $o->applyAttribsTree(true);
                }
            }
            
            $json = new JSONHelper();
             $json->html = \app\Helpers\UserHelper::makeSelectableUsers($data); 
            
            return $json;
        }


        
	
	/**
	 * Unassign courses from the formation.
	 * @return mixed
	 */
        public function unenrolecourse() : JSONHelper 
        {
            $data = null;
            $uid = Helper::getValue($this->context_item, 0);

            if ( $this->isEditable($uid) )
            { 
                Factory::getInstance()->getTxt()->loadFiles(["formations.default"]);
                $courses = Helper::getValue("courses", null);
                $courses = json_decode($courses, true);
                if ( !is_array($courses) ) {
                    $courses = [$courses];
                }
                
                if ( count($courses) ) 
                {
                    $model = new FormationCoursesModel();
                    $filters = new DBFilters();
                    $filters->filters->isIn['fid'] = [$uid];
                    $filters->filters->isIn['cid'] = $courses;
                    
                    Factory::getInstance()->setInfo("FORMATION_UNENROLED_COURSES", $model->delete($filters));
                } else {
                    Factory::getInstance()->setError("NO_SELECTED_COURSE");
                }

                return $this->courses();
            }

            return new JSONHelper(null, $data);
        }



	/**
	 * @override
	 */
        public function enrolecourse() : JSONHelper 
        {
            $uid = Helper::getValue($this->context_item, 0);
            $data = null;
           
            if ( $this->isEditable($uid) )
            {    
                Factory::getInstance()->getTxt()->loadFiles(["formations.default"]);     
                $cmod = new CoursesModel();
                $data = $cmod->getRelatedToFormation($uid, false);    
                foreach ( $data as &$o ) {
                    $o = new Course($o);
                }
            }
            
            $json = new JSONHelper();
            $json->html = \app\Helpers\CoursesHelper::makeSelectableCourses($data);
            
            return $json;
        }
        
    
    /**
     * @override
     */
        public function unenrole() : JSONHelper
        {
            $data = null;
            $uid = Helper::getValue($this->context_item, 0);

            if ( $this->isEditable($uid) )
            { 
                Factory::getInstance()->getTxt()->loadFiles(["formations.default"]);
                $users = Helper::getValue("users", null);
                $users = json_decode($users, true);
                if ( !is_array($users) ) {
                    $users = [$users];
                }
                
                if ( count($users) ) 
                {
                    $model = new FormationUsersModel();
                    $filters = new DBFilters();
                      $filters->filters->isIn['fid'] = [$uid];
                      $filters->filters->isIn['uid'] = $users;
                    
                    Factory::getInstance()->setInfo("FORMATION_UNENROLED", $model->delete($filters));

                    return $this->courses();
                } else {
                    Factory::getInstance()->setError("NO_SELECTED_USERS");
                }
            }

            return new JSONHelper(null, $data);
        }

        
    /**
     * @override
     */
	    public function enroleto() : JSONHelper
        {
            $data = null;
            $uid = Helper::getValue($this->context_item, 0);

            if ( $this->isEditable($uid) )
            { 
                Factory::getInstance()->getTxt()->loadFiles(["formations.default"]);
                $cmod = new CoursesModel();
                $umod = new FormationUsersModel();
                $rmod = new \app\Models\RoleUsersModel();

                // Users to enrole to.
                $users = Helper::getValue("users", null);
                $users = json_decode($users, true);
                if ( !is_array($users) ) {
                    $users = [$users];
                }
                
                if ( count($users) ) 
                {
                    // Get related courses
                    $f = new DBFilters();
                     $f->filters->isIn["fid"] = [$uid];
                    $courses = $cmod->getRelatedToFormation($uid);

                    foreach ( $users as $u )
                    {
                        // Enrole to formation table
                        $umod->enroleUser($uid, $u, Status::PUBLISHED->value);

                        // Enrole student to each formation course group.
                        foreach ($courses as $c) {
                            $rmod->enroleUser($u, "course." . $c->id . ".students");
                        }
                    }
                    
                    Factory::getInstance()->setSuccess("STUDENTS_ENROLED_TO", $uid);
                
                    $data = $this->courses()->data;
                }
            }
            
            return new JSONHelper(null, $data);
        }

        
    /**
     * @override
     */
	    public function enrolecourseto() : JSONHelper
        {
            $data = null;
            $uid = Helper::getValue($this->context_item, 0);

            if ( $this->isEditable($uid) )
            { 
                Factory::getInstance()->getTxt()->loadFiles(["formations.default"]);
                $fum = new FormationUsersModel();
                $cum = new CoursesModel();
                $rm = new RoleModel();
                $ids = [];
                $fusers = $fum->getRelatedStudents($uid, 0, 1); // Formations users.
                $courses = Helper::getValue("courses", null);
                $courses = json_decode($courses, true);
                if ( !is_array($courses) ) {
                    $courses = [$courses];
                }
                $dbf = new DBFilters();
                 $dbf->filters->isIn['id'] = $courses;
                  $courses = $cum->getItems($dbf); // Get valid courses.

                 // vaR_dump( $courses );
                
                if ( count($courses) ) 
                {
                    $model = new FormationCoursesModel();
                    $o = new stdClass();
                     $o->fid = $uid;

                    foreach ( $courses as $c )
                    {
                        $o->cid = $c->id;
                        $model->insertObject($o);
                        $ids[] = $c->id;
                    }

                    Factory::getInstance()->setSuccess("COURSES_ENROLED_TO", $uid);

                    // Enrole formation students to these courses
                    $cum->enroleUsersToCourses($fusers, $rm->getRolesRelatedToCourses($ids, 'students'));


                    $data = $this->courses()->data;
                }
            }
            
            return new JSONHelper(null, $data);
        }

        
    /**
     * @override
     */
        public function cleancourses() : JSONHelper
        {
            $uid = Helper::getValue($this->context_item, 0);

            if ( $this->isEditable($uid) )
            {
                Factory::getInstance()->getTxt()->loadFiles(["formations.default"]);
                $model = new FormationCoursesModel();
                $filters = new DBFilters();
                $filters->filters->isIn['fid'] = [$uid];
                
                Factory::getInstance()->setInfo("FORMATION_UNENROLED_COURSES", $model->delete($filters));
                
                return $this->courses();
            }

            return new JSONHelper();
        }


    /**
     * @override
     */
        public function cleanstudents() : JSONHelper
        {
            $uid = Helper::getValue($this->context_item, 0);

            if ( $this->isEditable($uid) )
            {
                Factory::getInstance()->getTxt()->loadFiles(["formations.default"]);
                $model = new FormationUsersModel();
                
                Factory::getInstance()->setInfo("FORMATION_UNENROLED_STUDENTS", $model->unenroleStudents($uid));
                
                return $this->courses();
            }

            return new JSONHelper();
        }

        
    /**
     * @override
     */
        public function courses(int $uid=0) : JSONHelper
        {
            $data = $pagination = null;

            if ( !$uid ) {
                $uid = Helper::getValue($this->context_item, 0);
            }            
         
            $f = $this->getModel()->getById($uid);
            if ( !is_null($f) )
            {
                Factory::getInstance()->getTxt()->loadFiles(["formations.default"]);
                $umod = new FormationUsersModel();
                $cmod = new CoursesModel();
                $page = \app\Helpers\PaginationHelper::getPageFromPost();
                $pagination = new \app\Helpers\PaginationHelper($page);
                $fh = new \app\Helpers\FiltersHelper("courses.course", "filtersFormationCourses");
                $f = new DBFilters($pagination);
                $f->filters->makeFromPost($fh);
                
                // Get count for pagination
                $f->behavior->count = true;
                $pagination->setCount($cmod->getFromFormation($uid, $f)); 
                $f->behavior->count = false;
                $f->behavior->orderBy = new \app\Helpers\OrderByHelper("cobitems" . $this->context . "Courses");

                $data = new stdClass;
                $data->users = new stdClass;
                $data->users->teachers = [];
                $data->users->students = [];
                $data->courses = $cmod->getFromFormation($uid, $f);
                $data->pending = 0;

                // Get teachers & students related to the formation.
                $users = array_merge(
                    $umod->getRelatedTeachers($uid), $umod->getRelatedStudents($uid)
                );
                
                // Finalize courses.
                foreach ( $data->courses as &$o ) {
                    $o = new Course($o);
                        Helper::applyListParams($o);
                        
                        // Apply url image.
                        FileHelper::applyImage(
                            $o, 
                            Factory::getInstance()->path("img_courses"), 
                            Factory::getInstance()->cfg("img.courses_default")
                        );
                }

                // Make user & check pending
                foreach ( $users as &$o ) 
                {
                    // Extract role from context.
                    $role = $o->role;

                    // Students is pending ?
                    if ( $role=="students" ) 
                    {
                        $pending = $o->request_status==Status::UNPUBLISHED->value;
                        if ($pending) {
                            $data->pending++;
                        }
                    }

                    // Set final object in appropriate branch.
                    $o = new \app\Objects\User($o);
                    $o->applyAttribsTree();
                    \app\Helpers\UserHelper::initAvatar($o);
                    if ( $role=="students" ) {
                        $o->pending = intval($pending);
                    }

                    $data->users->{$role}[] = $o;
                }
            } else {
                Factory::getInstance()->setError("BAD_ITEM");
            }

            return new JSONHelper(null, $data, $pagination);
        }
        

    /**
     * @override
     */
        public function validPending() : JSONHelper
        {
            $factory = Factory::getInstance();
            $data = null;

            if ( $this->rules->canDo("manage") )
            {
                $factory->getTxt()->loadFiles(["formations.default"]);
                $pending = Helper::getValue("pendingUser", []);
                $valid = Helper::getValue("valid", null);

                if ( !is_null($valid) && is_array($pending) )
                {
                    $valid = strtolower($valid)==='false' ? 0 : 1;
                    $mc = new CoursesModel();
                    $mfc = new FormationCoursesModel();
                    $mru = new FormationUsersModel();
                    $toDo = [];
                    $toMail = [];

                    foreach ( $pending as $str )
                    {
                        $seg = explode('.', $str);
                        if ( count($seg)==2 && intval($seg[0]) && intval($seg[1]) )
                        {
                            if ( !array_key_exists($seg[1], $toDo) ) {
                                $toDo[$seg[1]] = [];
                            }

                            $toDo[$seg[1]][] = $seg[0];
                            $toMail[] = $seg[0];
                        }
                    }

                    if ( count($toDo) ) 
                    {     
                        // Valid pending       
                        if ($valid)
                        {            
                            $c = $mru->validPending($toDo);
                            $factory->setInfo("PENDING_STUDENTS_VALIDATED", $c);
                            $mailSubject = 'PENDING_VALIDATED_MAIL_SUBJECT';
                            $mailBody = 'PENDING_VALIDATED_MAIL_CONTENT';
                        } 
                        // Cancel pending
                        else 
                        {
                            $c = $this->cancelPending($toDo);
                            $factory->setInfo("PENDING_STUDENTS_CANCELED", $c);
                            $mailSubject = 'PENDING_CANCELED_MAIL_SUBJECT';
                            $mailBody = 'PENDING_CANCELED_MAIL_CONTENT';
                        }

                        if ($c)
                        {
                            // Get users mail.
                            $mu = new \app\Models\UserModel();
                            $dbf = new DBFilters();
                             $dbf->behavior->select[] = "email";
                             $dbf->behavior->fetchColumn = true;
                             $dbf->filters->isIn['id'] = $toMail;
                             $toMail = $mu->getItems($dbf);

                            // Send mail (if mailing system is active)
                            \app\Helpers\MailHelper::mailTo($toMail, $mailSubject, $mailBody);

                            // Enrole students to formation courses.
                            if ($valid)
                            {
                                foreach ( $toDo as $fid => $users ) {
                                    $mc->enroleUsersToCourses($users, $mfc->getStudentsRoles($fid));
                                }
                            }

                            $data = $pending;
                        }
                    }
                }                    
            }

            return new JSONHelper(null, $data);
        }


    /**
     * Cancel pendings.
     * 
     * @param array $toDo Array with keys = fids & values = uids.
     */
        private function cancelPending( array $toDo ) : int
        {
            $m = new FormationUsersModel();
            $dbf = new DBFilters();
             $dbf->filters->isIn['published'] = [0];
             foreach ( $toDo as $fid => $users ) 
             {
                $dbf->filters->isIn['fid'] = [$fid];
                foreach ( $users as $uid ) {
                     $dbf->filters->isIn['uid'][] = $uid;
                }
             }
                 
            return $m->delete($dbf);
        }

        
    /**
     * @override
     */
        public function mailto() : JSONHelper
        {
            $factory = Factory::getInstance();
            $factory->getTxt()->loadFiles(["formations.default"]);

            if ( $this->canDo("mailto") )
            {
                $m = $this->getModel();
                $mfu = new FormationUsersModel();
                $formation = Helper::getValue("formation", 0);
                $target = Helper::getValue("target", "");
                $subject = Helper::getValue("subject", "");
                $content = Helper::getValue("content", "");

                if ( empty($formation) || is_null(($formation=$m->getById($formation))) ) {
                    $factory->setError("MAIL_BAD_FORMATION");
                }
                elseif ( !in_array($target, ["teachers", "students"]) ) {
                    $factory->setError("MAIL_BAD_TARGET");
                }
                elseif ( empty(trim($subject)) ) {
                    $factory->setError("MAIL_NO_SUBJECT");
                }
                elseif ( empty(trim($content)) ) {
                    $factory->setError("MAIL_NO_CONTENT");
                }
                else 
                {    
                    if ( $target=="teachers" ) {
                        $tmp = $mfu->getRelatedTeachers($formation->id);
                    } else {
                        $tmp = $mfu->getRelatedStudents($formation->id);
                    }
                    
                    $mails = [];

                    foreach ( $tmp as $u ) {
                        $mails[] = $u->email;
                    }

                    \app\Helpers\MailHelper::mailTo($mails, $subject, $content);

                    $factory->setSuccess("MAIL_SENT", count($mails));
                }
            }
            else {
                $factory->setError("RESTRICTED_ACTION");
            }

            return new JSONHelper();
        }


    /**
     * @override
     */
        public function mailformstudents() : JSONHelper {
            return $this->mailform("students", Page::FORMATIONS_MAIL_TO);
        }


    /**
     * @override
     */
        public function mailformteachers() : JSONHelper {
            return $this->mailform("teachers", Page::FORMATIONS_MAIL_TO);
        }

            

    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::FORMATIONS_MANAGE);
        }



}