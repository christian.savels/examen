<?php
namespace app\Controllers\Json;

defined('_PWE') or die("Limited access");

use app\Enums\Page as Pg;
use app\Helpers\Factory;
use app\Helpers\Helper;
use app\Helpers\JSONHelper;
use app\Helpers\UserHelper;
use app\Objects\User;

/**
 * 
 * JSON User Controller.
 * 
 * @author chris
 *
 */
class UserController extends \app\Controllers\AController 
    implements \app\Controllers\interfaces\UserJControllerInterface
{    
    protected string $context = "user";
    protected string $context_item = "account";


    /**
     * @Override
     */
        public function getRelatedObject(object $obj=null ) : User|null {
            return new User();
        }


    /**
     * @Override
     */
        public function export($format="") : void
        {
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("export") )
            {
                $c = new \app\Controllers\UserController();
                $data = $c->account();
                $tree = [];
                
                // Filter attributes from items trees.
                $tmp = [
                    "formations" => $data->formations, 
                    "student" => $data->student, 
                    "teacher" => $data->teacher
                ];
                
                foreach ( $tmp as $k => $items )
                {
                    $tree[$k] = [];
                    foreach ( $items as $item )
                    {
                        $o = new \stdClass;
                        $o->id = $item->id;
                        $o->title = $item->title;
                        $o->link = $item->link;

                        $tree[$k][] = $o;
                    }
                }

                // Clean user params.
                unset(
                    $data->user->token_csrf, 
                    $data->user->token_time,
                    $data->user->token_session
                );
                
                $tree["account"] = $data->user;

                // Download it.
                \app\Helpers\ExportHelper::export("json", $tree);
            } 
            else {
                $factory->setError("CANT_EXPORT");
            }
        }
        
    
    
    /**
     * @Override
     * @todo Retourner un JSONHelper.
     */
        public function logout() : JSONHelper
        {
            \app\Helpers\SessionHelper::logout();
            
            if ( !Factory::getInstance()->hasErrors() ) {
                Factory::getInstance()->setSuccess("NOW_LOGGEDOUT");
            }

            $json = new JSONHelper();
            $json->redirect = Factory::getInstance()->getUrl(Pg::HOME);

            return $json;
        }
    
    
    /**
     * @Override
     */
        public function getModel() : \app\Models\UserModel {
            return new \app\Models\UserModel(); 
        }
        
    
    /**
     * @Override
     */
        public function account() : JSONHelper
        {
            $factory = Factory::getInstance();           
            $usr = $this->getModel()->getById($factory->getUser()->id);

            // Edit only if valid secure pwd provided.
            $secure = Helper::getValue("pwd_secure", "");
            if ( !is_null($usr) 
                && strlen(trim($secure)) 
                && password_verify($secure, $usr->password)
            ) 
            {
                UserHelper::updateFromPost($usr);
                
                $model = $this->getModel();
                if ( $model->updateObject($usr) ) {
                    $factory->setSuccess("SAVE_OK");
                } else {
                    $factory->setError("SAVE_FAILED");
                }
                
            }
            else {
                $factory->setError("BAD_SECURE");
            }

            return new JSONHelper();
        }

        
     /**
     * Define if guest can access.
     * 
     * @return bool
     */
        protected function guestCanAccess() : bool {
            return false;
        }
    
                
    /**
     * @Override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Pg::USERS_MANAGE);
        }
    
}