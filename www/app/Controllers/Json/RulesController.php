<?php
namespace app\Controllers\Json;
use app\Databases\DBFilters;

defined('_PWE') or die("Limited acces");

use app\Helpers\Factory;
use app\Helpers\RulesHelper;


/**
 * 
 * JSON Controller for rules management.
 * 
 * @author chris
 *
 */
class RulesController extends \app\Controllers\AController
    implements \app\Controllers\interfaces\RulesJControllerInterface 
{        
   
    /**
     * @override
     */
        public function save(string $context) : \app\Helpers\JSONHelper
        {
            // Get rules related to context
            $f = new DBFilters();
             $f->filters->isNull[] = "context";
            $m = new \app\Models\RoleModel();
            $rules = new RulesHelper($context, $m->getItems($f));
            
            if ( $rules->canDo("rules") ) {
                $rules->saveFromForm();
            } else {
                Factory::getInstance()->setError("RULES_CANT_MANAGE");
            }

            return new \app\Helpers\JSONHelper();
        }
    
    
    /**
     * @override
     */
        protected function guestCanAccess(): bool {
            return false;
        }
    
    
    /**
     * @override
     */
        public function getModel() {
            return new \app\Models\RulesModel();
        } 

    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : \app\Objects\AObj|null {
            return null; 
        }
                

    /**
     * @override
     */
        public function getLinkManage() : string {
            return "";
        }
}