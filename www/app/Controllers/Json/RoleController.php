<?php
namespace app\Controllers\Json;
use app\Enums\UserStatus;
use app\Helpers\JSONHelper;
use app\Helpers\ViewHelper;
use app\Objects\Role;

defined('_PWE') or die("Limited acces");

use app\Helpers\Factory;
use app\Enums\Page as Pg;
use app\Helpers\FormHelper;
use app\Helpers\Helper;


/**
 * 
 * Controller related to the roles.
 * 
 * @author chris
 *
 */
class RoleController extends \app\Controllers\AController 
    implements \app\Controllers\interfaces\RoleJControllerInterface
{    
    protected string $context = "roles";
    protected string $context_item = "role";
        
    
    /**
     * @override
     */
        public function getModel() : object {
            return new \app\Models\RoleModel();
        }
        
        
    /**
     * @Override
     */
        public function getRelatedObject(object $obj=null ) : Role|null {
            return new Role(); 
        }
        
        
    /**
     * @override
     */
        public function enroleto() : JSONHelper
        {
            $factory = Factory::getInstance();
            $uid = Helper::getValue('role', 0);
            
            if ( ($role=new Role($this->isEditable($uid)))!==false )
            {
                if ( $role->enrolable )
                {
                    $users = Helper::getValue('users', null);
                    if ( !is_null($users) ) 
                    {
                        $model = new \app\Models\RoleUsersModel();
                        $users = json_decode($users);
                        if ( count($users) )
                        {
                            $o = new \stdClass();
                            $o->roleid = $uid;
                            
                            foreach ( $users as $user )
                            {
                                $o->userid = $user;
                                $model->insertObject($o); 
                            }
                        } 
                        else {
                            $factory->setError("NO_SELECTED_USERS");
                        }
                    } 
                } 
                else {
                    $factory->setError("NOT_ENROLABLE");
                }
            }
            
            $json = new JSONHelper();
             $json->data = $this->users()->data;;
             
            return $json;
        }        
        
    
    /**
     * @override
     */
        public function enrole(int $uid=0) : JSONHelper
        {
            if ( !$uid ) {
                $uid = Helper::getValue('role', 0);
            }

            $factory = Factory::getInstance();
            $pagination = new \app\Helpers\PaginationHelper();
            $r = new JSONHelper(null, null, $pagination); // return
             $r->items = [];             
            
            if ( $this->isEditable($uid) )  
            {
                $model = new \app\Models\UserModel();
                $f = new \app\Databases\DBFilters($pagination);     
                 $f->behavior->joinGroups = true;
                 $f->filters->isIn["published"] = [UserStatus::PUBLISHED->value]; 
                 $f->filters->notIn["role"] = [$uid]; 
                $r->items = $model->getItems($f); 
                
                $f->behavior->joinGroups = false;
                $f->behavior->count = true;
                $pagination->setCount($model->getItems($f)); 

                $r->count = $pagination->getCount();
            } 
            else {
                $factory->setError("CANT_EDIT_ROLE");
            }
            
            foreach ( $r->items as &$item )
            {
                $item = new \app\Objects\User($item);
                 $item->applyAttribsTree(true);
            }
          
            // Get users list html.
            if ( is_null(Helper::getValue("pagination", null)) )
            {
                $r->pagination = $pagination;
                
                // Filters related to xml file.
                $filters = new \app\Helpers\FiltersHelper("users.user", "");
                $r->filters = $filters->render(
                    "filtersRoles",
                    $factory->getUrl(Pg::USERS_USER, null, true), 
                    ["published"]
                );
                
                // Order by elements.
                $r->orderBy = new \stdClass;
                $r->orderBy->id = "cobitems" . $this->context;
                $r->orderBy->html = FormHelper::makeOrderBy(
                    "cobitems" . $this->context, 
                    "cobitems" . $this->context, 
                    $factory->getUrl(Pg::ROLES_ENROLE, null, true),
                    new \app\Objects\User(),
                    "",
                    [], "data-pw-courses-order"                    
                ); 

                $vh = new ViewHelper();
                //$r->html = $vh->getViewHTML("users", "modal", $r);  
                $r->html = \app\Helpers\UserHelper::makeSelectableUsers(
                    $r->items                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             , $this->rules->getGroups()
                );
            }
            else 
            {
                $r->initPagination($pagination);
                $r->data = $r->items;
            }            

            // Unset unused fields.
            unset(
                //$r->items, 
                $r->orderBy, $r->filters);
             
            return $r;
        }


    /**
     * @override
     */
        public function unenrole() : JSONHelper
        {
            $factory = Factory::getInstance();
            $uid = Helper::getValue('role', 0);
            if ( $this->isEditable($uid) )
            {
                $users = Helper::getValue("users", null);
                if ( !empty($users) ) 
                {
                    $users = json_decode($users, true);
                    if ( count($users) ) {
                        $unenroled = $this->getModel()->unenrole($uid, $users);
                        if ($unenroled) {
                            $factory->setInfo("USERS_UNENROLED", $unenroled);
                        } else {
                            $factory->setWarning("USERS_UNENROLED", $unenroled);
                        }
                    }
                }
            }
            
            return new JSONHelper();
        }
        
        
    /**
     * @override
     */
        public function clean() : JSONHelper
        {
            $factory = Factory::getInstance();
            $uid = Helper::getValue('role', 0);
            if ( $this->isEditable($uid) )
            {
                if ( $this->rules->getGroup($uid)->count_users ) {
                    $factory->setInfo("ROLE_USERS_CLEANED", $this->getModel()->cleanGroup($uid));
                }
                else {
                    $factory->setInfo("ROLE_USERS_CLEANED", 0);
                }
            }
            
            return new JSONHelper();
        }
        
    
    /**
     * @override
     */
        public function users() : JSONHelper
        {                 
            $uid = Helper::getValue('role', 0);
            $data = null;
            
            if ( ($role=$this->isEditable($uid)) ) 
            {
                $m = new \app\Models\RoleUsersModel();

                // Get teachers assigned manually and also real teachers.
                if ( $role->teachers ) 
                {
                    
                    $data = $m->getTeachers($uid);    
                } 
                // Get users related to the role.
                else {
                    $data = $m->getUsers([$uid]);    
                }
                     
                // Job on users.
                foreach ( $data as &$item )
                {
                    // Bind attribs to main tree.
                    $item->attribs = json_decode($item->attribs, false);
                    foreach( $item->attribs as $k => $v ) {
                        $item->{$k} = $v;
                    }
                    
                    $item->social->avatar = \app\Helpers\UserHelper::getAvatarUrl($item->social->avatar);
                    
                    // Unset unused attributes.
                    unset($item->attribs);
                }
       
            }

            return new JSONHelper(null, $data);
        }
        
        
    /**
     * @override
     */
        public function delete(int $uid=0) : JSONHelper
        {
            $factory = Factory::getInstance();

            if ( $this->canDo("manage") ) 
            {
                if ( !$uid ) {
                    $uid = Helper::getValue('role', 0);
                }
                
                if ( $this->isEditable($uid) )
                {
                    $g = $this->rules->getGroup($uid);

                    if ( $g->protected && !$factory->getUser()->isRoot() ) {
                        $factory->setError("ROLE_IS_PROTECTED", $g->count_users, $g->title, $g->id);
                    }
                    elseif ( $g->count_users ) {
                        $factory->setError("ROLE_NOT_EMPTY_ERROR", $g->count_users, $g->title, $g->id);
                    }
                    else
                    {
                        $model = $this->getModel();
                        // Delete related translations.
                        $contexts =  FormHelper::extractTranslableContexts(
                            $this->context_item, $uid, "roles.role"
                            );
                        $factory->setInfo("TRANSLATIONS_DELETED", $model->deleteTranslated($contexts));
                        
                        if ( $model->deleteByPk($uid) ) {
                            $factory->setSuccess("ROLE_DELETED", $g->title, $g->id);
                        } else {
                            $factory->setSuccess("ROLE_DELETED_FAILED", $g->title, $g->id);
                        }
                    }
                }
            } 
            else {
                $factory->setError("RESTRICTED_ACTION");
            }
            
            return new JSONHelper();
        }
                
        
    /**
     * @override
     */
        public function manage(mixed $page=null) : JSONHelper
        {
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("manage") )
            {
                $data = new \app\Helpers\ViewDataHelper(new Role(), Pg::ROLES_MANAGE);
                 $data->dbFilters->filters->isNull[] = "context";
                 $data->filters_formName = "filtersManage" . $this->context_item;
                 $data->init($this);

                 // Job on items
                 foreach ( $data->items as &$item ) 
                 {
                    $item = new Role($item);
                     $item->link = $factory->getUrl(Pg::ROLES_ROLE_EDIT, $item->id);
                 }
            }
        
            return new JSONHelper(null, $data->items, $data->pagination);
        }
                
        
    /**
     * Save edited item. 
     */
        public function edit(int $id=0)
        {
            $factory = Factory::getInstance();
        
            if ( $this->rules->canDo("manage") )
            {
                if ( !$id ) {
                    $id = Helper::getValue("role");
                }
                
                // Check group access
                if ($id)
                {
                    $g = $this->rules->getGroup($id);
                    if ( is_null($g) ) {
                        $factory->setError("ROLES_BAD_ROLE", $id);
                    }
                    if ( $g->protected && !$factory->getUser()->isRoot() ) {
                        $factory->setError("PROTECTED_ROLE_ERROR");
                    }                    
                }
                
                if ( !$factory->hasErrors() ) 
                {
                    $role = FormHelper::saveForm(
                        new Role(),
                        $this->getModel(),
                        FormHelper::extractTree("roles.role"),
                    );
                                
                    if ($role->id) {
                        Factory::getInstance()->setSuccess("ROLE_SAVE_OK", $role->id);
                    } else {
                        Factory::getInstance()->setError("ROLE_SAVE_FAILED");
                    }
                }
            }
                        
            $json = new JSONHelper();
             $json->redirect = Factory::getInstance()->getUrl(Pg::ROLES_MANAGE);
             
            return $json;
        }
        

    /**
     * Get form to send a mail to users of a group.
     * 
     * @return \app\Helpers\JSONHelper
     */
        public function mailformroles() : JSONHelper{
            return $this->mailform("roles", Pg::ROLES_MAIL_TO);
        }


    /**
     * Try to send a mail to users group.
     */
        public function mailto() 
        {
            $factory = Factory::getInstance();

            if ( $this->canDo("mailto") )
            {
                $m = $this->getModel();
                $mfu = new \app\Models\RoleUsersModel();
                $role = Helper::getValue("role", 0);
                $subject = Helper::getValue("subject", "");
                $content = Helper::getValue("content", "");
                $mail = [];

                if ( empty($role) || is_null(($role=$m->getById($role))) ) {
                    $factory->setError("MAIL_BAD_ROLE");
                }
                elseif ( empty(trim($subject)) ) {
                    $factory->setError("MAIL_NO_SUBJECT");
                }
                elseif ( empty(trim($content)) ) {
                    $factory->setError("MAIL_NO_CONTENT");
                }
                else 
                {    
                    $role = new Role($role);
                    if ( $role->isGuest ) {
                        $factory->setError("CANT_MAIL_GUESTS");
                    } 
                    elseif ( $role->teachers ) {
                        $mails = $mfu->getContextMails("course.%.teachers");
                    }
                    elseif ( $role->students ) {
                        $mails = $mfu->getContextMails("course.%.students");
                    }
                    else {
                        $mails = $mfu->getEnroledMails($role->id);
                    }
                   
                    if ( \app\Helpers\MailHelper::mailTo($mails, $subject, $content) ) {
                        $factory->setSuccess("MAIL_SENT", count($mails));
                    }                    
                }
            }
            else {
                $factory->setError("RESTRICTED_ACTION");
            }

            return new JSONHelper();
        }


    /**
     * @override
     */
        public function details(int $uid = 0, mixed $page=null) : mixed { die; }

        
    /**
     * @override
     */
        protected function guestCanAccess(): bool {
            return false;
        }
    
        
    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Pg::ROLES_MANAGE);
        }
}