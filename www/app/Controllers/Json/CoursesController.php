<?php
namespace app\Controllers\Json;

defined('_PWE') or die("Limited acces");

use app\Helpers\JSONHelper;
use app\Models\CoursesModel;
use app\Objects\Course;
use app\Helpers\Factory;
use app\Enums\Page as Pg;
use app\Enums\Page;
use app\Enums\Status;
use app\Databases\DBFilters;
use app\Helpers\Helper;
use app\Models\RoleUsersModel;
use app\Models\UserModel;

/**
 * 
 * JSON Controller related to the courses.
 * 
 * @author chris
 *
 */
class CoursesController extends \app\Controllers\AController 
    implements \app\Controllers\interfaces\CoursesJControllerInterface
{    
    protected string $context = "courses";
    protected string $context_item = "course";


    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : Course|null {
            return new Course($obj); 
        }


    /**
     * @override
     */
        public function validPending() : JSONHelper
        {
            $factory = Factory::getInstance();
            $data = null;

            if ( $this->rules->canDo("manage") )
            {
                $pending = Helper::getValue("pendingUser", []);
                $valid = Helper::getValue("valid", null);
                
                if ( !is_null($valid) && is_array($pending) )
                {
                    $mru = new RoleUsersModel();
                    $toDo = [];
                    $toMail = [];

                    foreach ( $pending as $str )
                    {
                        $seg = explode('.', $str);
                        if ( count($seg)==2 && intval($seg[0]) && intval($seg[1]) )
                        {
                            $o = new \stdClass;
                                $o->userid = $seg[0];
                                $o->roleid = $seg[1];

                            $toDo[] = $o;
                            $toMail[] = $seg[0];
                        }
                    }

                    if ( count($toDo) ) 
                    {
                        $c = $mru->validPending($toDo);
                        $factory->setInfo("PENDING_STUDENTS_VALIDATED", $c);

                        // Mail
                        if ($c)
                        {
                            // Get users mail.
                            $mails = [];
                            $mu = new UserModel();
                            $dbf = new DBFilters();
                                $dbf->behavior->select[] = "email, attribs";
                                $dbf->filters->isIn['id'] = $toMail;
                            $users = $mu->getItems($dbf);
                            foreach( $users as $u ) {
                                $mails[] = $u->email;
                            }

                            // Send mail
                            \app\Helpers\MailHelper::mailTo(
                                $mails, 
                                $this->txt("PENDING_VALIDATED_MAIL_SUBJECT"), 
                                $this->txt("PENDING_VALIDATED_MAIL_CONTENT")
                            );

                            $data = $pending;
                        }
                    }
                }                    
            }

            return new JSONHelper(null, $data);
        }


    /**
     * @Override
     */
        public function list(mixed $page=null) : JSONHelper
        {
            $factory = Factory::getInstance();
            if ( $this->rules->canDo("access") )
            {
                $data = new \app\Helpers\ViewDataHelper(new Course(), Pg::COURSES);
                 $data->filters_ignore = ["published"];
                 $data->dbFilters->filters->isIn["published"] = [Status::PUBLISHED->value];
                 $data->orderBy_formName = "cobitems" . $this->context;
                 $data->filters_formName = "filters" . $this->context;
                 $data->init($this);

                 // Job on items
                 foreach ( $data->items as &$item ) 
                 {
                    $item = new Course($item);
                     $item->link = $factory->getUrl(Pg::COURSE, $item->id);
                    
                    Helper::applyListParams($item);

                    // Apply url image.
                    \app\Helpers\FileHelper::applyImage(
                        $item, 
                        $factory->path("img_courses"), 
                        $factory->cfg("img.courses_default")
                    );
                 }
            }
        
            $json = new JSONHelper(null, $data->items, $data->pagination);

            return $json;
        }
    
    
    
    /**
     * @override
     */
        public function getModel() : CoursesModel {
            return new CoursesModel();
        }
        
        
    /**
     * @override
     */
        public function manage() : JSONHelper 
        {
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("manage") )
            {
                $data = new \app\Helpers\ViewDataHelper(new Course(), Pg::COURSES_MANAGE);
                 $data->dbFilters->filters->notIn["published"] = [Status::ARCHIVED->value];
                 $data->orderBy_formName = "cobitemsManage" . $this->context;
                 $data->filters_formName = "filtersManage" . $this->context;
                 $data->init($this);

                 // Job on items
                 foreach ( $data->items as &$item ) 
                 {
                    $item = new Course($item);
                     $item->link = $factory->getUrl(Pg::COURSES_EDIT, $item->id);
                 }
            }
        
            return new JSONHelper(null, $data->items, $data->pagination);
        }

        
    /**
     * @override
     */
        public function mailformstudents() {
            return $this->mailform("students", Page::COURSES_MAIL_TO);
        }


    /**
     * @override
     */
        public function mailformteachers() {
            return $this->mailform("teachers", Page::COURSES_MAIL_TO);
        }
                

    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::COURSES_MANAGE);
        }
}