<?php
namespace app\Controllers\Json;

defined('_PWE') or die("Limited acces");

use app\Helpers\JSONHelper;
use app\Helpers\Factory;
use app\Helpers\FormHelper;
use app\Enums\Page as Pg;
use app\Objects\Degree;


/**
 * 
 * Controller of the degree view.
 * 
 * @author chris
 *
 */
class DegreeController extends \app\Controllers\AController
    implements \app\Controllers\interfaces\DegreeJControllerInterface 
{   
    protected string $context = "degrees";
    protected string $context_item = "degree";
    
    
    /**
     * @override
     */
        public function getModel() : \app\Models\DegreeModel {
            return new \app\Models\DegreeModel();
        }
    

    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : Degree|null {
            return new Degree($obj); 
        }
        
    
    /**
     * @override
     */
        public function details(int $uid = 0, mixed $page = null): mixed { die; }
    
    
    /**
     * @override
     */
        public function delete(int $uid = 0) : JSONHelper
        { 
            if ( !$uid ) {
                $uid = \app\Helpers\Helper::getValue($this->context_item, 0);
            }
            $factory = Factory::getInstance();
            
            if ( $this->rules->canDo("manage") )
            {
                if ($uid)
                {
                    $factory->getTxt()->loadFiles(['degrees.default']);
                    $model = $this->getModel();
                    $item = $model->getById($uid);
                    
                    // Delete only if the degree does not contain any learning.
                    if ( !$item->count_inside )
                    {
                        // Delete related translations.
                        $contexts =  FormHelper::extractTranslableContexts(
                            $this->context_item, $uid, $this->context . "." . $this->context_item
                        );
                        $factory->setInfo("TRANSLATIONS_DELETED", $model->deleteTranslated($contexts));
                        
                        if ( $model->deleteByPk($uid) ) {
                            $factory->setSuccess("DEGREE_DELETED", $item->title, $item->id);
                        } else {
                            $factory->setSuccess("DEGREE_DELETED_FAILED", $item->title, $item->id);
                        }
                    }
                    else {
                        $factory->setError("DEGREE_NOT_EMPTY_ERROR", $item->count_inside, $item->title, $item->id);
                    }
                } else {
                    $factory->setError("DEGREES_BAD_DEGREE", $uid);
                }
            } else {
                $factory->setError("CANT_DO");
            }
            
            return new JSONHelper();        
        }
    

    /**
     * @override
     */
        public function edit(int $id=0) : JSONHelper
        {
            $factory = Factory::getInstance();
            
            if ( $this->rules->canDo("manage") )
            {
                if ( !$id ) {
                    $id = \app\Helpers\Helper::getValue("degree");
                }
                
                $model = $this->getModel();
                
                // Check group access
                if ( $id )
                {
                    $degree = $model->getById($id);
                    if ( is_null($degree) ) {
                        $factory->setError("DEGREES_BAD_DEGREE", $id);
                    }
                }
                
                if ( !$factory->hasErrors() )
                {
                    $degree = FormHelper::saveForm(
                        new Degree(),
                        $model,
                        FormHelper::extractTree("degrees.degree"),
                    );
                    
                    if ($degree->id) {
                        Factory::getInstance()->setSuccess("DEGREE_SAVE_OK", $degree->id);
                    } else {
                        Factory::getInstance()->setError("SAVE_FAILED");
                    }
                }
            }
            
            $json = new JSONHelper();
             $json->redirect = Factory::getInstance()->getUrl(Pg::DEGREES_MANAGE);
            
            return $json;
        }
 
        
    
    /**
     * @override
     */
        protected function guestCanAccess(): bool {
            return false;
        }
    
    
    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Pg::DEGREES_MANAGE);
        }       
   
}