<?php
namespace app\Controllers;

defined('_PWE') or die("Limited acces");

use app\Enums\Page;
use app\Enums\Status;
use app\Helpers\FileHelper;
use app\Objects\Course;
use app\Helpers\Factory;


/**
 * 
 * Controller of the courses views.
 * 
 * @author chris
 *
 */
class CoursesController extends \app\Controllers\AController
implements \app\Controllers\interfaces\CoursesControllerInterface 
{   
    protected string $context = "courses";
    protected string $context_item = "course";

    
    /**
     * 
     * @Override
     * @return \app\Objects\AObj|null
     */
        public function getRelatedObject(object $obj=null ) : Course|null {
            return new Course($obj); 
        }
            
        
    /**
     * List of courses (public).
     * 
     * @Override
     * @param mixed $page Page to load.
     */
        public function list(mixed $page=null) : mixed
        {
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("access") )
            {
                $data = new \app\Helpers\ViewDataHelper(new Course(), Page::COURSES);
                 $data->makeOrderBy = true;
                 $data->orderBy_formName = "cobitems" . $this->context;
                 $data->orderBy_ignore = ["published", 'start_on', 'end_on', 'formation', 
                 'teacher', 'isStudent', 'isTeacher', 'roleTeachers', 'roleStudents'];
                 $data->makeFilters = true;
                 $data->filters_formName = "filters" . $this->context;
                 $data->filters_ignore = ['published', 'content'];
                 $data->makeExport = $this->rules->canDo("export");
                 $data->exportUrlCSV =  $factory->getUrl(Page::COURSES_EXPORT_CSV);
                 $data->exportUrlJSON = $factory->getUrl(Page::COURSES_EXPORT_JSON);
                 $data->dbFilters->filters->isIn["published"] = [Status::PUBLISHED->value];
                 $data->init($this);

                 // Job on items
                 foreach( $data->items as &$item)
                 {
                    $item = new Course($item);
                    \app\Helpers\Helper::applyListParams($item);

                    // Apply url image.
                    FileHelper::applyImage(
                        $item, 
                        $factory->path("img_courses"), 
                        $factory->cfg("img.courses_default")
                    );

                    // Link to item.
                    $item->link = $factory->getUrl(Page::COURSE, $item->id);
                 }

                return $data;
            }

            $factory->setError("NO_ACCESS");
            $this->redirect();
        }


    /**
     * @override
     */
        public function manage() : mixed
        {
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("manage") )
            {
                $data = new \app\Helpers\ViewDataHelper(new Course(), Page::COURSES_MANAGE);
                 $data->makeOrderBy = true;
                 $data->orderBy_formName = "cobitemsManage" . $this->context;
                 $data->orderBy_ignore = ["description", "content"];
                 $data->makeFilters = true;
                 $data->filters_formName = "filtersManage" . $this->context;
                 $data->makeToolBar = true;
                 $data->makeExport = $this->rules->canDo("export");
                 $data->exportUrlCSV =  $factory->getUrl(Page::COURSES_EXPORT_CSV);
                 $data->exportUrlJSON = $factory->getUrl(Page::COURSES_EXPORT_JSON);
                 $data->makeRules = $this->rules->canDo("rules");
                 $data->dbFilters->filters->notIn["published"] = [Status::ARCHIVED->value];
                 $data->init($this);

                 $data->withoutTeacher = $this->getModel()->getWithoutTeacher();
                 $data->pending = $this->getModel()->getPending();

                 // Job on items
                 foreach ( $data->items as &$item ) 
                 {
                    $item = new Course($item);
                     $item->link = $factory->getUrl(Page::COURSES_EDIT, $item->id);
                 }


                return $data;
            }

            $factory->setError("NO_ACCESS");
            $this->redirect();
        }
        
        
    /**
     * @override
     */
        public function getToolbarManage() : \app\Helpers\ToolbarHelper
        {
            $tb = new \app\Helpers\ToolbarHelper("coursesToolbar");
            
            // Add course.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "COURSES_ADD";
            $o->page = Page::COURSES_EDIT;
            $o->icon = "add";
            $o->class = "btn-outline-light";
            $tb->addOption($o);
            
            // Delete selected course.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "COURSES_DELETE";
            $o->page = null; // js.
            $o->icon = "delete";
            $o->boxAttributes = 'data-pw-course-action';
            $o->action = "delete";
            $o->confirm = "CONFIRM_DELETE_COURSE";
            $o->class = "btn-outline-light text-danger";
            $tb->addOption($o);
            
            // Empty course (delete all users).
            $o = new \app\Objects\ToolbarOption();
            $o->title = "COURSES_DO_EMPTY";
            $o->icon = "clean";
            $o->class = "btn-outline-light"; 
            $o->boxAttributes = 'data-pw-course-action';
            $o->action = "clean";
            $o->confirm = "CONFIRM_CLEAN_COURSE";
            $tb->addOption($o);
            
            // Add a student
            $o = new \app\Objects\ToolbarOption();
            $o->title = "COURSES_ADD_STUDENT";
            $o->icon = "add-student";
            $o->class = "btn-outline-light"; 
            $o->boxAttributes = 'data-pw-course-action';
            $o->action = "add-student";
            $tb->addOption($o, 2);
            
            // Add a teacher
            $o = new \app\Objects\ToolbarOption();
            $o->title = "COURSES_ADD_TEACHER";
            $o->icon = "add-teacher";
            $o->class = "btn-outline-light"; 
            $o->boxAttributes = 'data-pw-course-action';
            $o->action = "add-teacher";
            $tb->addOption($o, 2);
            
            // Remove selected user from course.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "COURSES_REMOVE_USERS";
            $o->icon = "unenrole";
            $o->class = "btn-outline-light"; 
            $o->boxAttributes = 'data-pw-course-action';
            $o->confirm = "CONFIRM_REMOVE_USERS";
            $o->action = "unenrole";
            $tb->addOption($o, 2);

             
            // Mail to teachers.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "COURSES_MAIL_TO_TEACHERS";
            $o->icon = "mailtoteachers";
            $o->class = "btn-outline-light"; 
            $o->boxAttributes = 'data-pw-course-action';
            $o->action = "mailformteachers";
            $tb->addOption($o, 3);

            // Mail to students.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "COURSES_MAIL_TO_STUDENTS";
            $o->icon = "mailtostudents";
            $o->class = "btn-outline-light"; 
            $o->boxAttributes = 'data-pw-course-action';
            $o->action = "mailformstudents";
            $tb->addOption($o, 3);
            
            return $tb;
        }
        
    
    /**
     * @override
     */
        public function getModel() {
            return new \app\Models\CoursesModel();
        } 
                

    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::COURSES_MANAGE);
        }
}