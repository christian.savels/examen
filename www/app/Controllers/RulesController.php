<?php
namespace app\Controllers;

use app\Helpers\Factory;
use app\Objects\AObj;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Controller of the rules.
 * 
 * @author chris
 *
 */
class RulesController extends \app\Controllers\AController
    implements \app\Controllers\interfaces\RulesControllerInterface 
{       
        public function form() : string { die; }
 
        public function edit(int $uid = 0) { die; }
     
        public function save(string $context) : \app\Helpers\JSONHelper { die; }
        
        public function delete(int $uid = 0) { die; }

             
    /**
     * 
     * @Override
     * @return \app\Objects\AObj|null
     */
        public function getRelatedObject(object $obj=null ) : AObj|null {
            return null; 
        }
        
    
    /**
     * {@inheritDoc}
     * @see \app\Controllers\AController::guestCanAccess()
     */
        protected function guestCanAccess(): bool {
            return false;
        }
    
    
    /**
     * {@inheritDoc}
     * @see \app\Controllers\AController::isRootAccess()
     */
        protected function isRootAccess(): bool {
            return false;
        }
        
    
    /**
     * {@inheritDoc}
     * @see \app\Controllers\AController::loggedCanAccess()
     */
        protected function loggedCanAccess(): bool {
            return true;
        }
  
    
    /**
     * {@inheritDoc}
     * @see \app\Controllers\AController::getModel()
     */
        public function getModel() {
            return new \app\Models\RulesModel();
        }
                

    /**
     * {@inheritDoc}
     * @see \app\Controllers\AController::getLinkManage()
     */
    public function getLinkManage() : string {
        return "";
    } 
}