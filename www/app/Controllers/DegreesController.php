<?php
namespace app\Controllers;
use app\Databases\DBFilters;
use app\Enums\Page;
use app\Enums\Status;
use app\Objects\Degree;

defined('_PWE') or die("Limited acces");

use app\Helpers\Factory;


/**
 * 
 * Controller of the user view.
 * 
 * @author chris
 *
 */
class DegreesController extends \app\Controllers\AController
    implements \app\Controllers\interfaces\DegreesControllerInterface 
{   
    protected string $context = "degrees";
    protected string $context_item = "degree";
    

    
    /**
     * 
     * @Override
     * @return \app\Objects\AObj|null
     */
        public function getRelatedObject(object $obj=null ) : Degree|null {
            return new Degree($obj); 
        }

    
    /**
     * Edit a degree.
     * 
     * @param int $uid UID of the degree to edit.     
     */    
        public function edit(int $uid = 0)
        {
            $data = null;
            
            if ( $this->rules->canDo("manage") )
            {
                $id = !$uid ? \app\Helpers\Helper::getValue("id") : $uid;
            
                $translated = [];
                $mod = $this->getModel();
                $data = new \stdClass();
                $data->degree = null;
                $data->langs = Factory::getInstance()->getTxt()->getLangs();
                
                if ($id)
                {
                    $filters = new DBFilters();
                    $filters->filters->begins['context'] = $mod->role . '.' . $id . '.';
                    
                    $data->degree = $mod->getItemBy("id", $id);
                    
                    // Bad id, redirect to new item form.
                    if ( !$data->degree->id )
                    {
                        Factory::getInstance()->setError("BAD_ITEM_ID", $id);
                        $this->redirect(Page::DEGREES_EDIT);
                    }
                    
                    $translated = $this->getModelTranslate()->getItems($filters);
                }
                
                $data->fields = \app\Helpers\FormHelper::extractFormFields
                (
                    $this->context . ".degree",
                    $data->degree,
                    $translated
                );
            }
            
            return $data;
        }
    

    /**
     * Show a list of available degrees.
     * 
     * @override
     * @param mixed $page Page to load.
     
        public function list(mixed $page=null) : mixed
        {
            $factory = Factory::getInstance();
             
            // Access to the list restricted ?
            if ( $this->rules->canDo("access") ) 
            {
                $pagination = new \app\Helpers\PaginationHelper($page);
                $m = $this->getModel();
                $f = new DBFilters($pagination);
                 $f->filters->isIn["published"] = [Status::PUBLISHED->value];

                // Get count for pagination
                $f->behavior->count = true;
                $pagination->setCount($m->getItems($f));
                $f->behavior->count = false;

                // Make final return.
                $data = new \stdClass;
                 $data->items = $m->getItems($f);
                 $data->pagination = $pagination;

                // Filters related to xml file.
                $filters = $this->getFiltersHelper();
                $data->filters = $filters->render(
                    "filtersCourses",
                    $factory->getUrl(Page::COURSES, null, true), 
                    ["published"]
                );
                 
                // Order by elements.
                $data->orderBy = new \stdClass;
                 $data->orderBy->id = "cobitems" . $this->context;
                 $data->orderBy->html = \app\Helpers\FormHelper::makeOrderBy
                 (
                     "cobitems" . $this->context, 
                     "cobitems" . $this->context, 
                     $factory->getUrl(Page::COURSES, null, true),
                     new Degree(),
                     "", [], "data-pw-courses-order"                    
                 );

                // Export 
                if ( $this->rules->canDo("export") )
                {
                    $data->export = \app\Helpers\ExportHelper::render(
                        $factory->getUrl(Page::DEGREES_EXPORT_JSON),
                        $factory->getUrl(Page::DEGREES_EXPORT_CSV),
                    );
                }

                // Job on items
                foreach( $data->items as &$item)
                {
                    $item = new Degree($item);
                    \app\Helpers\Helper::applyListParams($item);
                }
            
                return $data;
            }

            $factory->setError("ACCESS_RESTRICTED");
            $this->redirect();
        }*/

        public function list(mixed $page=null) : mixed { die; }

    
    
    /**
     * @override
     */
        public function manage() : mixed
        {
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("manage") )
            {
                $data = new \app\Helpers\ViewDataHelper(new Degree(), Page::DEGREES_MANAGE);
                 $data->makeOrderBy = true;
                 $data->orderBy_ignore = ['published'];
                 $data->orderBy_formName = "cobitemsManage" . $this->context;
                 $data->makeFilters = true;
                 $data->filters_formName = "filtersManage" . $this->context;
                 $data->makeToolBar = true;
                 $data->makeExport = $this->rules->canDo("export");
                 $data->exportUrlCSV =  $factory->getUrl(Page::DEGREES_EXPORT_CSV);
                 $data->exportUrlJSON = $factory->getUrl(Page::DEGREES_EXPORT_JSON);
                 $data->makeRules = $this->rules->canDo("rules");
                 $data->init($this);

                // Job on items
                foreach ( $data->items as &$item ) {
                    $item->link = $factory->getUrl(Page::DEGREES_EDIT, $item->id);
                }

                return $data;
            }

            $factory->setError("NO_ACCESS");
            $this->redirect();
        }


    /**
    * Make degrees toolbar (manage).
    *
    * @return \app\Helpers\ToolbarHelper
    */
        public function getToolbarManage() : \app\Helpers\ToolbarHelper
        {
            $tb = new \app\Helpers\ToolbarHelper("degreesToolbar");
            
            // Add new item.
            $o = new \app\Objects\ToolbarOption();
             $o->title = "DEGREES_ADD"; 
             $o->page = Page::DEGREES_EDIT;
             $o->icon = "add";
             $o->class = "btn-outline-primary";
            $tb->addOption($o);

            // Delete selection.
            $o = new \app\Objects\ToolbarOption();
             $o->title = "DEGREES_DELETE";
             $o->icon = "delete";
             $o->boxAttributes = 'data-pw-item-action';
             $o->action = "delete";
             $o->confirm = "CONFIRM_DELETE_DEGREE";
             $o->class = "btn-outline-danger";
            $tb->addOption($o);

            return $tb;
        }
        
    
    /**
     * @override
     */
        public function getModel() : \app\Models\DegreeModel {
            return new \app\Models\DegreeModel();
        }
        
        
    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::DEGREES_MANAGE);
        }
   
}