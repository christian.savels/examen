<?php
namespace app\Controllers;
use app\Enums\Page;
use app\Helpers\Factory;
use app\Helpers\IconsHelper;
use app\Objects\AObj;
use app\Objects\User;

defined('_PWE') or die("Limited acces");


class HomeController extends \app\Controllers\AController
    implements \app\Controllers\interfaces\HomeControllerInterface 
{        
  
    
    /**
     * Homepage actions.
     * Default.
     */
        public function home() : object
        {
            $data = new \stdClass();
              $data->topCarousel = $this->makeTopCarouselList();
              $data->topInfoList = $this->makeTopInfoList();
              $data->aboutUs = $this->makeAboutUs();
            
            return $data;
        }


    /**
     * Make elements of about us part.
     * 
     * @return object
     */
        private function makeAboutUs() : object 
        {
            $f = Factory::getInstance();
            $dir = $f->path("img_home_about_us", false);
            
            $o = new \stdClass;
              $o->title = $this->txt("ABOUT_TITLE");
              $o->slogan = $this->txt("ABOUT_SLOGAN");
              $o->content = $this->txt("ABOUT_CONTENT");
              $o->sign = [$this->txt("ABOUT_SIGN1"), $this->txt("ABOUT_SIGN2")];
              $o->img = $f->getUrlFromPath($dir) . '/aboutus.png';
              $o->imgAlt = $this->getTitle("ABOUT_SLOGAN");

            return $o;
        }


    /**
     * Make information placed on top of the page, 
     * inside the carousel, including images.
     * 
     * @return array
     */
        private function makeTopCarouselList() : array 
        {
            $factory = Factory::getInstance();
            $dir = $factory->path("img_home_carousel", false);
            $root = $factory->getUrlFromPath($dir) . '/';
            $images = array_diff(
                scandir($factory->path("img_home_carousel")), 
                array('..', '.')
            );
            $data = [];
            
            foreach ( $images as $img )
            {
                if ( str_contains($img, "homeTop")) 
                {
                    $segments = explode(".", $img);
                    $o = new \stdClass;
                     $o->img = $root . $img;
                     $o->titleAttribute = $this->getTitle("CAROUSEL_TITLE_" . strtoupper($segments[0]));
                     $o->title = $this->txt("CAROUSEL_TITLE_" . strtoupper($segments[0]));
                     $o->desc = $this->txt("CAROUSEL_DESC_" . strtoupper($segments[0]));
                
                    $data[] = $o;
                }
            }

            return $data;
        }


    /**
     * Make information placed on top of the page, 
     * under the hero part.
     * 
     * @return string
     */
        private function makeTopInfoList() : array 
        {
            $data = ['Modern', 'Social', 'Skill', 'Win'];
            $r = [];
            
            foreach ( $data as $item )
            {
                $o = new \stdClass;
                 $o->icon = 'homeTopInfo' . $item;
                 $o->title = 'TOP_INFO_TITLE_' . strtoupper($item);
                 $o->desc = 'TOP_INFO_DESC_' . strtoupper($item);

                $r[] = $o;
            }

            return $r;
        }
        
        
        public function getModel() {}
        
    
    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : AObj|null {
            return null; 
        }   
            

    /**
     * @override
     */
        public function getLinkManage() : string {
            return "";
        }
}

