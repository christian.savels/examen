<?php
namespace app\Controllers;
use app\Databases\DBFilters;
use app\Enums\Page;
use app\Objects\Role;

defined('_PWE') or die("Limited acces");

use app\Helpers\Factory;


/**
 * 
 * Controller of the user view.
 * 
 * @author chris
 *
 */
class RoleController extends \app\Controllers\AController
    implements \app\Controllers\interfaces\RoleControllerInterface 
{   
    protected string $context = "roles";
    protected string $context_item = "role";
    
        
    /**
     * @Override
     */
        public function getRelatedObject(object $obj=null ) : Role|null {
            return new Role(); 
        }
    

    /**
     * @Override
     */
        public function edit(int $id=0) : mixed
        {
            $data = null;
            if ( $this->rules->canDo("manage") )
            {
                $translated = [];
                $mod = new \app\Models\RoleModel();
                $data = new \stdClass();
                 $data->role = null;
                 $data->langs = Factory::getInstance()->getTxt()->getLangs();
                
                if ($id)
                {
                    $filters = new DBFilters();
                     $filters->filters->begins['context'] = $mod->role . '.' . $id . '.';
                    
                    $data->role = new Role($mod->getItemBy("id", $id));
                    
                    // Bad id, redirect to new item form.
                    if ( !$data->role->id )
                    {
                        Factory::getInstance()->setError("BAD_ITEM_ID", $id);
                        $this->redirect(Page::ROLES_ROLE_EDIT);
                    }
                    
                    $translated = $this->getModelTranslate()->getItems($filters);
                }
                
                $data->fields = \app\Helpers\FormHelper::extractFormFields
                (
                    $this->context . ".role",
                    $data->role,
                    $translated
                );
            }

            return $data;
        }
        
        
    /**
     * @Override
     */   
        public function details(int $uid = 0, mixed $page=null) : mixed { die; }   
        

    /**
     * @Override
     */
        public function delete(int $uid=0) : void { die; }
    
      
    /**
     * @override
     */
        protected function guestCanAccess(): bool {
            return false;
        }
    
    
    /**
     * @Override
     */
        public function getModel() {
            return new \app\Models\RoleModel();
        }
             

    /**
     * @Override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::ROLES_MANAGE);
        }
}