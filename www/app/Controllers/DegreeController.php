<?php
namespace app\Controllers;

defined('_PWE') or die("Limited acces");

use app\Databases\DBFilters;
use app\Enums\Page;
use app\Objects\Degree;
use app\Helpers\Factory;


/**
 * 
 * Controller of the user view.
 * 
 * @author chris
 *
 */
class DegreeController extends \app\Controllers\AController
    implements \app\Controllers\interfaces\DegreeControllerInterface 
{   
    protected string $context = "degrees";
    protected string $context_item = "degree";
    

    /**
     * @Override
     */
        public function getRelatedObject(object $obj=null ) : Degree|null {
            return new Degree($obj); 
        }

    
    /**
     * @Override  
     */    
        public function edit(int $uid = 0) : mixed
        {
            if ( $this->rules->canDo("manage")  )
            {
                $id = !$uid ? \app\Helpers\Helper::getValue("id") : $uid;
            
                $translated = [];
                $mod = $this->getModel();
                $data = new \stdClass();
                $data->degree = null;
                $data->langs = Factory::getInstance()->getTxt()->getLangs();
                
                if ($id)
                {
                    $filters = new DBFilters();
                    $filters->filters->begins['context'] = $mod->role . '.' . $id . '.';
                    
                    $data->degree = $mod->getItemBy("id", $id);
                    
                    // Bad id, redirect to new item form.
                    if ( !$data->degree->id )
                    {
                        Factory::getInstance()->setError("BAD_ITEM_ID", $id);
                        $this->redirect(Page::DEGREES_EDIT);
                    }
                    
                    $translated = $this->getModelTranslate()->getItems($filters);
                }
                
                $data->fields = \app\Helpers\FormHelper::extractFormFields
                (
                    $this->context . ".degree",
                    $data->degree,
                    $translated
                );
            }
            
            return $data;
        }


    /**
     * @Override
     */
        public function details(int $uid = 0, mixed $page = null) : mixed { die; }


    /**
     * @Override
     */
        public function delete(int $uid = 0) : void { die; }
    

    /**
     * @Override
     */
        public function getModel() : \app\Models\DegreeModel {
            return new \app\Models\DegreeModel();
        }
        
        
    /**
     * @Override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::DEGREES_MANAGE);
        }
   
}