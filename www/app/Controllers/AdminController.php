<?php
namespace app\Controllers;

use app\Databases\DBFilters;
use app\Enums\Page;
use app\Helpers\IconsHelper;
use stdClass;

defined('_PWE') or die("Limited acces");

use app\Helpers\Factory;


/**
 * 
 * Controller of the admin (root) view.
 * 
 * @author chris
 *
 */
class AdminController extends \app\Controllers\AController
    implements \app\Controllers\interfaces\AdminControllerInterface 
{   
    protected string $context = "admin";
    protected string $context_item = "admin";
    
    
    /**
     * @override
     */
        public function admin() : mixed
        {
            $factory = Factory::getInstance();
            $data = null;
            $controllers = [
                new \app\Controllers\Json\UsersController(),
                new \app\Controllers\Json\CoursesController(),                
                new \app\Controllers\Json\FormationsController(), 
                new \app\Controllers\Json\DegreesController(),             
                new \app\Controllers\Json\RolesController()                
            ];
                                                
            foreach ( $controllers as $c )
            {
                if ( is_null($data) ) {
                    $data = new stdClass;
                }
                    if ( !isset($data->sections) ) {
                    $data->sections = [];
                }

                $f = new DBFilters();
                    $f->behavior->count = true;

                $o = new stdClass;
                    $o->title = $this->txt("PART_" . $c->getContext());
                    $o->count = $c->getModel()->getItems($f);
                    $o->icon = IconsHelper::get($c->getContext());
                    $o->link = $c->getLinkManage();
                    $o->link_title = $this->txt("LINK_" . $c->getContext());

                $data->sections[$c->getContext()] = $o;
            }

            // Rules
            $rules = $this->getRulesTree($controllers);
            if ( !empty($rules) ) {
                $data->rules = $rules;
            }

            // Site params (edit form for ROOT only)
            if ( $factory->getUser()->isRoot() ) {
                $data->siteParams = $factory->getSiteParams()->getForm();
            }

            // Redirect if restricted access.
            if ( empty($data) ) 
            {
                $factory->setError("RESTRICTED_ACCESS");
                $this->redirect();
            }

            return $data;
        }


    /**
     * Make rules part.
     * 
     * @param array $controllers
     * 
     * @return array
     */
        private function getRulesTree( array $controllers ) : array
        {
            Factory::getInstance()->addStyleFile("assets/css/form.rules.css");

            $o = [];

            foreach ( $controllers as $c )
            {
                if ( $c->canDo("rules") ) 
                {
                    $context = $c->getContext();
                    $o[$context] = new stdClass;
                    $o[$context]->context = $context;
                    $o[$context]->title = $this->txt("PART_" . $context);
                    $o[$context]->tree = $this->rules->makeRulesHtml($context, false, false);
                    $o[$context]->icon = IconsHelper::get($context);
                }
            }

            return $o;
        }


    /**
     * @override
     */
        public function save() { die; }

        
    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : \app\Objects\AObj|null {
            return null; 
        }
    
    
    /**
     * @override
     */
        protected function isRootAccess(): bool {
            return true;
        }
  
    
    /**
     * @override
     */
        public function getModel() {}  
                

    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::ADMIN);
        }  
}