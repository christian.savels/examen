<?php
namespace app\Controllers;

defined('_PWE') or die("Limited acces");

use app\Objects\User;

/**
 * 
 * Controller of the guest view.
 * 
 * @author chris
 *
 */
class GuestController extends \app\Controllers\AController
    implements \app\Controllers\interfaces\GuestControllerInterface 
{    
    
    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : User|null {
            return new User($obj); 
        }
    
    
    /**
     * Login page.
     */
        public function login() {}
        
    
    /**
     * Sign up page.
     */
        public function signup() {}
        
    
    /**
     * @override
     */
        public function getModel() : \app\Models\UserModel {
            return new \app\Models\UserModel();
        }
        
    
    /**
     * @override
     */
        protected function loggedCanAccess(): bool {
            return false;
        }
                

    /**
     * @override
     */
        public function getLinkManage() : string {
            return "";
        }
        
}