<?php
namespace app\Controllers;
use app\Models\FormationUsersModel;

defined('_PWE') or die("Limited acces");

use app\Enums\Page;
use app\Enums\Status;
use app\Helpers\FileHelper;
use app\Objects\Formation;
use app\Helpers\Factory;


/**
 * 
 * Formations controller.
 * 
 * @author chris
 *
 */
class FormationsController extends \app\Controllers\AController
implements \app\Controllers\interfaces\FormationsControllerInterface 
{    
    protected string $context = "formations";
    protected string $context_item = "formation";

    
    /**
     * @override
     */
        public function getModel() {
            return new \app\Models\FormationModel();
        } 

    
    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : Formation|null {
            return new Formation($obj); 
        }
    
    
    /**     
     * @override
     */
        public function list(mixed $page=null) : mixed
        { 
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("access") )
            {
                $data = new \app\Helpers\ViewDataHelper(new Formation(), Page::FORMATIONS);
                 $data->makeOrderBy = true;
                 $data->orderBy_formName = "cobitems" . $this->context; 
                 $data->orderBy_ignore = ['published', 'id', 'degree_title', 'isStudent'];
                 $data->makeFilters = true;
                 $data->filters_formName = "filters" . $this->context;
                 $data->makeExport = $this->rules->canDo("export");
                 $data->exportUrlCSV =  $factory->getUrl(Page::FORMATIONS_EXPORT_CSV);
                 $data->exportUrlJSON = $factory->getUrl(Page::FORMATIONS_EXPORT_JSON);
                 $data->dbFilters->filters->isIn["published"] = [Status::PUBLISHED->value];
                 $data->init($this);
                
                 // Finalize items.
                 foreach ( $data->items as &$o )
                 {
                    $o = new Formation($o);
                    \app\Helpers\Helper::applyListParams($o);

                    // Link to item.
                    $o->link = $factory->getUrl(Page::FORMATIONS_FORMATION, $o->id);

                    // Apply url image.
                    FileHelper::applyImage(
                        $o, 
                        $factory->path("img_formations"), 
                        $factory->cfg("img.formations_default")
                    );
                 }

                return $data;
            }

            $factory->setError("NO_ACCESS");
            $this->redirect();
        } 


    /**
     * @override
     */
        public function manage()
        {
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("manage") )
            {
                $data = new \app\Helpers\ViewDataHelper(new Formation(), Page::FORMATIONS_MANAGE);
                 $data->makeOrderBy = true;
                 $data->orderBy_formName = "cobitemsManage" . $this->context;
                 $data->makeFilters = true;
                 $data->filters_formName = "filtersManage" . $this->context;
                 $data->makeToolBar = true;
                 $data->makeExport = $this->rules->canDo("export");
                 $data->exportUrlCSV =  $factory->getUrl(Page::FORMATIONS_EXPORT_CSV);
                 $data->exportUrlJSON = $factory->getUrl(Page::FORMATIONS_EXPORT_JSON);
                 $data->makeRules = $this->rules->canDo("rules");
                 $data->init($this);

                 // Items init
                 foreach ( $data->items as &$o ) 
                 {
                    $o = new Formation($o);
                     \app\Helpers\Helper::applyListParams($o);
                 }

                 // Get pending registrations
                 $m = new FormationUsersModel();
                 $data->pending = $m->getPending();

                return $data;
            }

            $factory->setError("NO_ACCESS");
            $this->redirect();
        }


     /**
     * @override
     */
        public function getToolbarManage() : \app\Helpers\ToolbarHelper
        {
            $tb = new \app\Helpers\ToolbarHelper("formationsToolbar");
            
            // Add new item.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "FORMATIONS_ADD"; 
            $o->page = Page::FORMATIONS_EDIT;
            $o->icon = "create";
            $o->class = "btn-outline-light";
            $tb->addOption($o);
            
            // Delete selected course.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "FORMATIONS_DELETE";
            $o->page = null; // js.
            $o->icon = "delete";
            $o->boxAttributes = 'data-pw-f-action';
            $o->action = "delete";
            $o->confirm = "CONFIRM_DELETE_FORMATION";
            $o->class = "btn-outline-light text-danger";
            $tb->addOption($o);
            
            // Add a course
            $o = new \app\Objects\ToolbarOption();
            $o->title = "FORMATIONS_ADD_COURSE";
            $o->icon = "add-course";
            $o->class = "btn-outline-light"; 
            $o->boxAttributes = 'data-pw-f-action';
            $o->action = "enrolecourse";
            $tb->addOption($o, 2);
            
            // Unassign selected courses of the formation.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "FORMATIONS_REMOVE_COURSES"; 
            $o->icon = "unenrolecourse";
            $o->class = "btn-outline-light";
            $o->boxAttributes = 'data-pw-course-action';
            $o->confirm = "CONFIRM_REMOVE_COURSES";
            $o->action = "unenrolecourse";
            $tb->addOption($o, 2);
            
            // Empty formation (unassign all courses).
            $o = new \app\Objects\ToolbarOption();
            $o->title = "FORMATIONS_DO_EMPTY_COURSES";
            $o->icon = "cleancourses";
            $o->class = "btn-outline-light";
            $o->boxAttributes = 'data-pw-f-action';
            $o->action = "cleancourses";
            $o->confirm = "CONFIRM_CLEAN_FORMATION";
            $tb->addOption($o, 2);   

            // Mail to teachers.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "FORMATIONS_MAIL_TO_TEACHERS";
            $o->icon = "mailtoteachers";
            $o->class = "btn-outline-light"; 
            $o->boxAttributes = 'data-pw-f-action';
            $o->action = "mailformteachers";
            $tb->addOption($o, 3);

            // Mail to students.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "FORMATIONS_MAIL_TO_STUDENTS";
            $o->icon = "mailtostudents";
            $o->class = "btn-outline-light"; 
            $o->boxAttributes = 'data-pw-f-action';
            $o->action = "mailformstudents";
            $tb->addOption($o, 3);

            // Validate pending registration.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "VALIDATE_PENDING";
            $o->icon = "validate-pending";
            $o->class = "btn-outline-light"; 
            $o->boxAttributes = 'data-pw-pending-action';
            $o->action = "validatePending";
            $tb->addOption($o, 4);

            // Cancel pending registration.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "CANCEL_PENDING";
            $o->icon = "cancel-pending";
            $o->class = "btn-outline-light"; 
            $o->boxAttributes = 'data-pw-pending-action';
            $o->action = "cancelPending";
            $tb->addOption($o, 4);

            // Add students
            $o = new \app\Objects\ToolbarOption();
            $o->title = "FORMATIONS_ADD_STUDENT";
            $o->icon = "add-student";
            $o->class = "btn-outline-light"; 
            $o->boxAttributes = 'data-pw-f-action';
            $o->action = "enrole";
            $tb->addOption($o, 5);
            
            // Unassign selected students from the formation.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "FORMATIONS_REMOVE_STUDENT";
            $o->icon = "unenrolestudent";
            $o->class = "btn-outline-light";
            $o->boxAttributes = 'data-pw-user-action';
            $o->confirm = "CONFIRM_REMOVE_COURSES";
            $o->action = "unenrole";
            $tb->addOption($o, 5);

            // Empty formation (unassign all students).
            $o = new \app\Objects\ToolbarOption();
            $o->title = "FORMATIONS_DO_EMPTY_STUDENTS";
            $o->icon = "cleanstudents";
            $o->class = "btn-outline-light";
            $o->boxAttributes = 'data-pw-f-action';
            $o->action = "cleanstudents";
            $o->confirm = "CONFIRM_CLEAN_STUDENTS";
            $tb->addOption($o, 5);

            return $tb;
        }


    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::FORMATIONS_MANAGE);
        }
}