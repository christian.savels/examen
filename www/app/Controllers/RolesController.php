<?php
namespace app\Controllers;
use app\Enums\Page;
use app\Objects\Role;

defined('_PWE') or die("Limited acces");

use app\Helpers\Factory;


/**
 * 
 * Controller of the user view.
 *  
 * @author chris
 *
 */
class RolesController extends \app\Controllers\AController
    implements \app\Controllers\interfaces\RolesControllerInterface 
{   
    protected string $context = "roles";
    protected string $context_item = "role";
    

    /**
     * @Override
     */
        public function getRelatedObject(object $obj=null ) : Role|null {
            return new Role(); 
        }
    
        
    /**
     * @override
     */ 
        public function manage(mixed $page=null)
        {
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("manage") )
            {
                $data = new \app\Helpers\ViewDataHelper(new Role(), Page::ROLES_MANAGE);
                 $data->makeOrderBy = true;
                 $data->orderBy_formName = "cobitemsManage" . $this->context;
                 $data->makeFilters = true;
                 $data->filters_formName = "filtersManage" . $this->context;
                 $data->makeToolBar = true;
                 $data->makeExport = $this->rules->canDo("export");
                 $data->exportUrlCSV =  $factory->getUrl(Page::ROLES_EXPORT_CSV);
                 $data->exportUrlJSON = $factory->getUrl(Page::ROLES_EXPORT_JSON);
                 $data->makeRules = $this->rules->canDo("rules");
                 $data->dbFilters->filters->isNull[] = "context";
                 $data->init($this);

                foreach ( $data->items as &$item )
                {
                    $item = new Role($item);
                     $item->link = $factory->getUrl(Page::ROLES_ROLE_EDIT, $item->id);
                }

                return $data;
            }

            $factory->setError("NO_ACCESS");
            $this->redirect();
        }

        
    /**
     * @override
     */
        public function getToolbarManage() : \app\Helpers\ToolbarHelper
        {
            $tb = new \app\Helpers\ToolbarHelper("rolesToolbar");
            
            // Add new item.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "ROLES_ADD"; 
            $o->page = Page::ROLES_ROLE_EDIT;
            $o->icon = "add";
            $o->class = "btn-outline-light";
            $tb->addOption($o);

            // Delete
            $o = new \app\Objects\ToolbarOption();
            $o->title = "ROLES_DELETE"; 
            $o->icon = "delete";
            $o->action = "delete";
            $o->boxAttributes = 'data-pw-role-action';
            $o->confirm = "CONFIRM_DELETE_ROLE";
            $o->class = "btn-outline-light text-danger";
            $tb->addOption($o);

            // Empty
            $o = new \app\Objects\ToolbarOption();
            $o->title = "ROLES_DO_EMPTY"; 
            $o->icon = "clean";
            $o->action = 'clean';
            $o->boxAttributes = 'data-pw-role-action';
            $o->confirm = "CONFIRM_EMPTY_ROLE";
            $o->class = "btn-outline-light";
            $tb->addOption($o);

            // Mail to group
            $o = new \app\Objects\ToolbarOption();
            $o->title = "ROLES_MAIL_TO_GROUP";
            $o->icon = "mailtorole";
            $o->class = "btn-outline-light"; 
            $o->boxAttributes = 'data-pw-role-action';
            $o->action = "mailformroles";
            $tb->addOption($o, 3);

            // Add users
            $o = new \app\Objects\ToolbarOption();
            $o->title = "ROLES_ADD_USERS"; 
            $o->action = "enrole";
            $o->icon = "enrole";
            $o->boxAttributes = 'data-pw-role-action';
            $o->class = "btn-outline-light";
            $tb->addOption($o, 2);

            // Remove users
            $o = new \app\Objects\ToolbarOption();
            $o->title = "ROLES_REMOVE_USERS"; 
            $o->action = "unenrole";
            $o->icon = "unenrole";
            $o->boxAttributes = 'data-pw-user-action';
            $o->confirm = "CONFIRM_UNENROLE";
            $o->class = "btn-outline-light";
            $tb->addOption($o, 2);

            return $tb;
        }
        
        
    /**
     * @Override
     */ 
        function list(mixed $page = null): mixed { die; }
     
    
    /**
     * @override
     */
        public function getModel() {
            return new \app\Models\RoleModel();
        }
             

    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::ROLES_MANAGE);
        }
}