<?php
namespace app\Controllers;

defined('_PWE') or die("Limited acces");

use stdClass;
use app\Helpers\UserHelper;
use app\Objects\User;
use app\Helpers\Factory;
use \app\Enums\Page as Pg;


/**
 * 
 * Controller of the user views.
 * 
 * @author chris
 *
 */
class UserController extends \app\Controllers\AController
    implements \app\Controllers\interfaces\UserControllerInterface 
{      
    protected string $context = "users";
    protected string $context_item = "user";
    
             
    /**
     * @Override
     */
        public function getRelatedObject(object $obj=null ) : User|null {
            return new User();
        }
    
    
    /**
     * @Override
     */
        public function logout()
        {
            $data = Factory::getInstance()->getUser();
            return $data;
        }    
        
    
    /**
     * @Override
     */
        public function getModel() : \app\Models\UserModel {
            return new \app\Models\UserModel();
        }
        
        
    /**
     * @override
     */
        public function account()
        {
            $factory = Factory::getInstance();        
            $data = new stdClass();
            $mf = new \app\Models\FormationUsersModel();
            $mc = new \app\Models\CoursesModel();  
            
            // Get current user.
            $usr = $factory->getUser();
            UserHelper::initAvatar($usr);
            unset($usr->password);

            // Get data related to the user.
            $data->formations = $mf->getStudentFormations($usr->id);
            $data->student = $mc->getRelatedToUser($usr->id);
            $data->teacher = $mc->getRelatedToUser($usr->id, "teachers");
            $data->fields = \app\Helpers\FormHelper::extractFormFields("user.account", $usr);
            $data->user = $usr;      
            
            // Set page link on items.
            foreach ( $data->formations as &$item ) {
                $item->link = $factory->getUrl(Pg::FORMATIONS_FORMATION, $item->id);
                $item->link_title = $this->getTitle("OPEN_THE_PAGE", $item->title);
            } 
            foreach ( $data->student as &$item ) {
                $item->link = $factory->getUrl(Pg::COURSE, $item->id);
                $item->link_title = $this->getTitle("OPEN_THE_PAGE", $item->title);
            }
            foreach ( $data->teacher as &$item ) {
                $item->link = $factory->getUrl(Pg::COURSE, $item->id);
                $item->link_title = $this->getTitle("OPEN_THE_PAGE", $item->title);
            }

            // Export profile
            $data->export = \app\Helpers\ExportHelper::render(
                $factory->getUrl(Pg::USER_EXPORT_JSON, null, true),
                null,
                $this->txt("EXPORT_PROFILE")
            );            
            
            return $data;
        }  

        
    /**
     * @override
     */
        protected function guestCanAccess() : bool {
            return false;
        }
    

    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Pg::USERS_MANAGE);
        }
        
}

