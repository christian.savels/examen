<?php 
namespace app\Controllers;

defined('_PWE') or die("Limited acces");

use app\Helpers\Factory;
use app\Helpers\LogsHelper;
use app\Helpers\Helper;
use app\Helpers\RulesHelper;
use app\Databases\DbFilters;
use app\Enums\Page;
use app\Enums\Status;
use app\Objects\AObj;

/**
 * 
 * Abstract controller.
 * 
 * Every public method (of a non json controlleur) 
 * should begin with : 
 * 
 *      "$this->checkRequest(method)";
 * 
 * Ceci sélectionnera le controlleur adéquat.
 * 
 * @author chris
 *
 */
abstract class AController
{    
    /**
     * Controller context.
     * @var string
     */
        protected string $context = "";

    /**
     * Item context.
     * @var string
     */
        protected string $context_item = "";

    /**
     * Actions where user must be manager for the section.
     */
        protected array $manageActions = ["manage", "edit", "delete", "enrole", "unenrole", "enroleto", "clean"];
    
    /**
     * Rules system, related to loaded controlleR.
     * @var RulesHelper
     */
        public RulesHelper $rules;

    
    /**
     * Public constructor.
     * Will perform check user access.
     * 
     * @param string $method Method to execute. Will be checked to restrict manage access.
     */
        public function __construct(string $method=null) 
        {
            $this->checkAccess();
            $this->makeRules();

            if ( !is_null($method) ) {
                $this->checkManageAction($method); 
            }
        }
   

    /**
     * Get object related to the controller.
     * 
     * @return AObj|null
     */
        public abstract function getRelatedObject( object $obj=null ) : AObj|null;
        
        
    /**
     * Get link to manage section.
     * @return string
     */
        public abstract function getLinkManage() : string;

            
   /**
     * Define if guest can access.
     * 
     * @return bool
     */
        protected function guestCanAccess() : bool {
            return true;
        }
            
    
    /**
     * Define if loggedin user can access.
     * 
     * @return bool
     */
        protected function loggedCanAccess(): bool {
            return true;
        }

    
    /**
     * Define if access is only for root.
     * 
     * @return bool
     */
        protected function isRootAccess(): bool {
            return false;
        }


    /**
     * Check if user can do an action.
     * @param string $what
     * @return bool
     */
        public function canDo( String $what ) : bool {
            return $this->rules->canDo($what);
        }


    /**
     * Export data.
     */
        public function export(string $format) : void
        {
            $factory = Factory::getInstance();

            if ( $this->rules->canDo("export") )
            {
                $m = $this->getModel();
                $fh = $this->getFiltersHelper("");

                // Apply filters.
                $f = new DBFilters();
                $f->filters->makeFromPost($fh);

                // Public user, only published items.
                if ( !$this->rules->canDo("manage") ) {
                    $f->filters->isIn["published"] = [Status::PUBLISHED->value];
                }  
                // Manager of the section, ignore only archived.
                else {
                    $f->filters->notIn["published"] = [Status::ARCHIVED->value];
                }

                // Get items list.
                $items = $m->getItems($f);
                
                // Finalize items.
                foreach ( $items as &$o )
                {
                    $o = $this->getRelatedObject($o);

                    // Attribs only if manager.
                    if ( !$this->rules->canDo("manage") ) {
                        unset($o->attribs);
                    } elseif( isset($o->attribs) ) {
                        $o->attribs = json_encode($o->attribs);
                    }

                    // Purge some attributes if user is not a root.
                    if (!$factory->getUser()->isRoot()) {
                        unset($o->token_session, $o->token_time);
                    }
                }

                // Download it.
                \app\Helpers\ExportHelper::export($format, $items);
            } 
            else {
                $factory->setError("CANT_EXPORT");
            }
        }

    
    /**
     * Check if user is allowed to access to this controller.
     */
        protected function checkAccess() : void
        {
            $usr = Factory::getInstance()->getUser();
            if 
            ( 
                ($this->isRootAccess() && !$usr->isRoot())
                || (!$this->guestCanAccess() & $usr->isGuest())
                || (!$this->loggedCanAccess() & !$usr->isGuest())
            )
            {
                Factory::getInstance()->setError("RESTRICTED_ACCESS");
                Helper::redirect("index.php");
            }              
        }


    /**
     * Check if action (method to execute) is only for manager
     * Redirect if user is not a manager in this section.
     * @param string $method
     */
        protected function checkManageAction(string $method) 
        {
             /**
             * Method access
             * Check if method is only for manager access.
             */ 
             if ( !empty($method) && in_array(strtolower($method), $this->manageActions) ) 
             {
                if ( !$this->canDo("manage") ) {
                    Factory::getInstance()->setError("RESTRICTED_ACCESS");
                    Helper::redirect("index.php");
                }
                else {
                    // Load manage language.
                    Factory::getInstance()->getTxt()->loadFiles(
                        [$this->context . ".manage"],
                        Factory::getInstance()->getUser()->getLang()
                    );
                }
            }
        }
    

    /**
     * Check if request is for json controller via $_POST.
     * Used when javascript is disabled only.
     * 
     * @param string $method
     * @param mixed ...$args
     * 
     * @return mixed 
     
        protected function checkRequest(string $method, ...$args) 
        {          
            // Javascript disabled and form submitted by post ?
            if ( $_POST && \app\Helpers\SessionHelper::isValidSession() )
            {
                $helper = new \app\Helpers\ControllerHelper();
                $ns = $helper->getNSController(Factory::getInstance()->getSection(), true);                                
                if ( !is_null($ns) ) 
                {
                    $c = $helper->getController($ns, $method, $args);
                    if ( !is_null($c) )
                    {
                        $data = $c->{$method}(...$args);
                        if ( isset($data->redirect) && !empty($data->redirect) ) {
                            Helper::redirect($data->redirect);
                        }
                        
                        return $data;
                    }
                }   
            } 
            else 
            {
                $vs = \app\Helpers\SessionHelper::isValidSession() ? 1 : 0;
                
                // TO DELETE (log debug).
                LogsHelper::log("logs/controllers.log", 
                    "AController.checkRequest",
                    [$method . " : isValidSession: " . $vs]
                );
            }
        }*/
     
        
    /**
     * Init rules related to the loaded controller.
     */
        protected function makeRules() 
        {
            if ( !empty($this->context) ) 
            {
                $m = new \app\Models\RoleModel();
                $f = new DBFilters();
                 $f->filters->isNull[] = "context";

                $this->rules = new RulesHelper($this->context, $m->getItems($f));                
            } 
        }
    
    
    /**
     * Check basic access to edit a group.
     *
     * @param int $uid
     * @param bool $redirect IF true, redirect on homepage when user is not allowed to edit.
     *
     * @return bool|object False if user is not allowed to edit item. Return editable object if user can edit.
     */
        protected function isEditable(int $uid, bool $redirect=false) : mixed
        {
            $factory = Factory::getInstance();
            
            if ( $this->rules->canDo("manage") )
            {
                if ($uid)
                {
                    $item = $this->getModel()->getById($uid);
                    if ( !is_null($item) )
                    {
                        if ( !isset($item->protected) || !$item->protected || $factory->getUser()->isRoot() ) {
                            return $item;
                        } else {
                            $factory->setError("PROTECTED_ITEM_ERROR");
                        }
                    } else {
                        $factory->setError("ITEMS_BAD_ITEM", $uid);
                    }
                }
                else {
                    $factory->setError("ITEMS_BAD_ITEM", $uid);
                }
            } else {
                $factory->setError("CANT_DO");
            }
            
            if ($redirect) 
            {
                Factory::getInstance()->setError("CANT_DO");
                Helper::redirect();
            }
            
            return false;
        }
        
    
    /**
     * Redirect to the page.
     * (shortcut)
     * 
     * @param \app\Enums\Page $page
     */
        protected function redirect( Page $page=null, string $suffix=null ) : void 
        {
            if ( is_null($page) ) {
                $loc = 'index.php';
            } else {
                $loc = Factory::getInstance()->getUrl($page, $suffix);
            }
            
            Helper::redirect($loc);
        }
        
        
    /**
     * Get the translate model.
     * 
     * @return \app\Models\TranslateModel
     */
        public function getModelTranslate() : \app\Models\TranslateModel {
            return new \app\Models\TranslateModel();
        }
            
    
    /**
     * Translate $ident (shortcut).
     *
     * @param string $ident
     * @param ...$vars
     *
     * @return string
     */
        public function txt( string $ident=null, ...$vars)
        {
            return Factory::getInstance()
                            ->getTxt()
                            ->get($ident, ...$vars);
        }    

        
    /**
     * Translate an element title (ex: <a title="">)
     * 
     * @param string|null $ident
     * @param array $vars
     * 
     * @return string
     */
        public function getTitle( string $ident=null, ...$vars)
        {
            return Factory::getInstance()
                            ->getTxt()
                            ->getTitle($ident, ...$vars);
        }    


    /**
     * 
     * Get filters helper related to the controller context/context_item.
     * 
     * @return \app\Helpers\FiltersHelper
     */
        public function getFiltersHelper(string $formName="") : \app\Helpers\FiltersHelper 
        {
            return new \app\Helpers\FiltersHelper(
                $this->context . "." . $this->context_item,
                $formName
            );
        }


    /**
     * Get controller context.
     * 
     * @return string
     */
        public function getContext() : string {
            return $this->context;
        }


    /**
     * Get controller context item.
     * 
     * @return string
     */
        public function getContextItem() : string {
            return $this->context_item;
        }
        

    /**
     * Prepare form to send a mail to target(s).
     * 
     * @param string $target
     * 
     * @return \app\Helpers\JSONHelper
     */
        protected function mailform(string $target, Page $page) : \app\Helpers\JSONHelper 
        {
            $factory = Factory::getInstance();
            $html = null;

            if ( $this->canDo("mailto") )
            {
                $html = \app\Helpers\MailHelper::makeMailToForm(
                    $factory->getUrl($page, null, true),
                    "POST",
                    \app\Helpers\FieldHelper::getInput("target", null, $target, "form-control", "hidden") 
                );
            }
            else {
                $factory->setError("RESTRICTED_ACTION");
            }

            $json = new \app\Helpers\JSONHelper();
            $json->html = $html;
            $json->title = $this->txt("MAIL_FORM_TITLE");

            return $json;
        }


    /**
     * Get toolbar related to the section.
     * 
     * @return \app\Helpers\ToolbarHelper|null
     */
        public function getToolbarManage() : \app\Helpers\ToolbarHelper|null {
            return null;
        }
    
}
?>