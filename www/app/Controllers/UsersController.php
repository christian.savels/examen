<?php
namespace app\Controllers;
use app\Helpers\Helper;

defined('_PWE') or die("Limited acces");

use app\Enums\Page;
use app\Helpers\Factory;
use app\Helpers\IconsHelper;
use app\Objects\User;

/**
 * 
 * Users controller.
 * 
 * @author chris
 *
 */
class UsersController extends \app\Controllers\AController
    implements \app\Controllers\interfaces\UsersControllerInterface 
{      
    protected string $context = "users";
    protected string $context_item = "user";
     
    
    /**
     * @override
     */
        public function manage(mixed $page=null)
        {
            $factory = Factory::getInstance();
 
            if ( $this->rules->canDo("manage") )
            {
                $data = new \app\Helpers\ViewDataHelper(new User(), Page::USERS_MANAGE);
                 $data->makeOrderBy = true;
                 $data->orderBy_formName = "cobitemsManage" . $this->context;
                 $data->orderBy_ignore = ['token_session', 'token_time', 'groups', 'reset_pwd', 'role'];
                 //$data->orderBy_formID = "cobitemsManage" . $this->context;
                 $data->makeFilters = true;
                 $data->filters_formName = "filtersManage" . $this->context;
                 $data->makeToolBar = true;
                 $data->makeExport = $this->rules->canDo("export");
                 $data->exportUrlCSV =  $factory->getUrl(Page::USERS_EXPORT_CSV);
                 $data->exportUrlJSON = $factory->getUrl(Page::USERS_EXPORT_JSON);
                 $data->makeRules = $this->rules->canDo("rules");
                 $data->init($this);
                 
                 // Job on items
                 foreach ( $data->items as &$item ) 
                 {
                    unset($item->password);

                    // Apply address attribs on main tree.
                    foreach ( $item->attribs->address as $k => $v ) {
                        $item->{$k} = $v;
                    }

                    // Apply groups title.
                    $groups = [];
                    foreach ( $item->groups as &$g ) 
                    {
                        $n = $this->rules->getGroup($g);
                        if ( !is_null($n) ) {
                            $groups[] = $n->title;
                        }
                    }

                    // Make groups list.
                    if ( !empty($groups) ) {
                       $item->groups = \app\Helpers\HtmlHelper::makeList($groups);
                    } else {
                       $item->groups = '<span class="d-block text-center">-</span>';
                    }

                    // Current user is root, load form to change access.
                    if ( $factory->getUser()->isRoot() ) 
                    {
                        $item->access = \app\Helpers\FieldHelper::makeAccessSelect(
                            "changeUserAccess" . $item->id, "changeUserAccess" . $item->id, $item->access
                        );
                    } 
                    // Only manager, just show the access title.
                    else {
                        $item->access = $this->txt("ACCESS_" . $item->access);
                    }

                    // Apply list params
                    Helper::applyListParams($item, ["email"]);
                    
                    // Convert status to icon.
                    $item->published_icon = IconsHelper::get("user-status-" . $item->published);
                    
                    // Teacher ? Student ?
                    $item->isTeacher = $item->isTeacher ? IconsHelper::get("teachers") : "-";
                    $item->isStudent = $item->isStudent ? IconsHelper::get("students") : "-";
                 }

                return $data;
            }

            $factory->setError("NO_ACCESS");
            $this->redirect();
        }

        
    /**
     * @override
     */
        public function getToolbarManage() : \app\Helpers\ToolbarHelper
        {
            $tb = new \app\Helpers\ToolbarHelper("usersToolbar");
            
            // Add new item.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "USERS_ADD"; 
            $o->icon = "create";
            $o->boxAttributes = 'data-pw-create-action';
            //$o->action = "create";
            $o->class = "btn-outline-light";
            $tb->addOption($o);
            
            // Delete selected users.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "USERS_DELETE";
            $o->icon = "delete";
            $o->boxAttributes = 'data-pw-item-action';
            $o->action = "delete";
            $o->confirm = "CONFIRM_DELETE_USERS";
            $o->class = "btn-outline-light text-danger";
            $tb->addOption($o);

            // Mail to selected users.
            $o = new \app\Objects\ToolbarOption();
            $o->title = "USERS_MAIL_TO";
            $o->icon = "mailto";
            $o->boxAttributes = 'data-pw-item-action';
            //$o->boxAttributes = 'data-pw-mail-action';
            $o->action = "mailform";
            $o->class = "btn-outline-light";
            $tb->addOption($o, 3);

            return $tb;
        }
        
        
    /**
     * @override
     */
        protected function guestCanAccess() : bool {
            return false;
        }

             
    /**
     * @override
     */
        public function getRelatedObject(object $obj=null ) : User|null {
            return new User($obj);
        }
    

    /**
     * @override
     */
        public function getModel() : \app\Models\UsersModel {
            return new \app\Models\UsersModel();
        }       
            

    /**
     * @override
     */
        public function getLinkManage() : string {
            return Factory::getInstance()->getUrl(Page::USERS_MANAGE);
        }
}

