<?php
namespace app\Enums;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Map of NS pages.
 * Non restrictif, les liens n'ont pas besoin d'être déclarés pour être utilisés.
 * Utilisé uniquement pour éviter de changer le code en cas de changement de logique des urls
 * et pour identifier les appels de liens via l'IDE.
 * 
 * @author chris
 *
 */
enum Page : string
{    
    case ADMIN = 'admin';
    case ADMIN_MAIL_TO = 'mail/mailto';
    case ADMIN_MAIL_FORM = 'mail/getMailForm';
    case ADMIN_SAVE_CONFIG = 'config/save';
    
    case JSON_RULES_FORM = "rules/form";
    case JSON_RULES_SAVE = "rules/save";

    case COURSE = 'course/details';
    case COURSES = 'courses/list';
    case COURSES_CLEAN = 'course/clean';
    case COURSES_DELETE = 'course/delete';
    case COURSES_EDIT = 'course/edit';
    case COURSES_ENROLE = 'course/enrole';
    case COURSES_ENROLE_TO = 'course/enroleto';
    case COURSES_EXPORT_CSV = 'courses/export/csv';
    case COURSES_EXPORT_JSON = 'courses/export/json';
    case COURSES_MAIL_FORM_STUDENTS = 'course/mailformstudents';
    case COURSES_MAIL_FORM_TEACHERS = 'course/mailformteachers';
    case COURSES_MAIL_TO = 'course/mailto';
    case COURSES_REGISTER = 'course/register';
    case COURSES_UNENROLE = 'course/unenrole';
    case COURSES_MANAGE = 'courses/manage';    
    case COURSES_USERS = 'course/users';    
    case COURSES_VALID_PENDING = 'courses/validPending';    
    
    case DEGREES_EDIT = 'degree/edit';
    case DEGREES = 'degrees/list';
    case DEGREES_MANAGE = 'degrees/manage';
    case DEGREES_DELETE = 'degree/delete';
    case DEGREES_EXPORT_CSV = 'degrees/export/csv';
    case DEGREES_EXPORT_JSON = 'degrees/export/json';

    
    case FORMATIONS = 'formations/list';
    case FORMATIONS_CLEAN = 'formation/clean';
    case FORMATION_COURSES = 'formation/courses';
    case FORMATIONS_DELETE = 'formation/delete';
    case FORMATIONS_EDIT = 'formation/edit';
    case FORMATIONS_ENROLE = 'formation/enrole';
    case FORMATIONS_ENROLE_COURSE = 'formation/enrolecourse';
    case FORMATIONS_ENROLE_TO = 'formation/enroleto';
    case FORMATIONS_ENROLE_COURSE_TO = 'formation/enrolecourseto';
    case FORMATIONS_EXPORT_CSV = 'formations/export/csv';
    case FORMATIONS_EXPORT_JSON = 'formations/export/json';
    case FORMATIONS_FORMATION = 'formation/details';
    case FORMATIONS_MAIL_FORM_STUDENTS = 'formation/mailformstudents';
    case FORMATIONS_MAIL_FORM_TEACHERS = 'formation/mailformteachers';
    case FORMATIONS_MAIL_FORM = 'formation/mailform';
    case FORMATIONS_MAIL_TO = 'formation/mailto';
    case FORMATIONS_REGISTER = 'formation/register';
    case FORMATIONS_UNENROLE = 'formation/unenrole';
    case FORMATIONS_UNENROLE_COURSE = 'formation/unenrolecourse';
    case FORMATIONS_MANAGE = 'formations/manage';
    case FORMATIONS_VALIDATE_PENDING = 'formation/validPending';
    case FORMATIONS_CANCEL_PENDING = 'formation/cancelPending';
    
   
    
    case HOME = 'home';
    
    case PAGE_404 = "home/page404";
    
    case ROLES_EXPORT_CSV = 'roles/export/csv';
    case ROLES_EXPORT_JSON = 'roles/export/json';
    case ROLES_ROLE_EDIT = 'role/edit';
    case ROLES_MAIL_FORM_ROLES = 'role/mailformroles';
    case ROLES_MAIL_TO = 'role/mailto';
    case ROLES_MANAGE = 'roles/manage';
    case ROLES_ROLE_USERS = 'role/users';
    case ROLES_ROLE_DO_EMPTY = 'role/clean';
    case ROLES_ROLE_DELETE = 'role/delete';
    case ROLES_ENROLE = 'role/enrole';
    case ROLES_ENROLE_TO = 'role/enroleto';
    case ROLES_UNENROLE = 'role/unenrole';
    
    case USER_SIGNUP = 'guest/signup';
    case USER_LOGIN = 'guest/login';
    case USER_LOGOUT = 'user/logout';
    case USER_ACCOUNT = 'user/account';
    case USER_EXPORT_JSON = 'user/export';

    
    case USERS_ACCESS = 'users/access';
    case USERS_CREATE = 'users/create';
    case USERS_DELETE = 'users/delete';
    case USERS_EDIT = 'users/edit';
    case USERS_EDIT_APPLY = 'users/apply';
    case USERS_SIGNUP = 'users/signup';
    case USERS_EXPORT_CSV = 'users/export/csv';
    case USERS_EXPORT_JSON = 'users/export/json';
    case USERS_MANAGE = 'users/manage';
    case USERS_MAIL_FORM = 'users/mailUsersForm';
    case USERS_MAIL_TO = 'users/mailto';
    case USERS_MORE = 'users/more';
    case USERS_USER = 'users/user';
    


    /**
     * Get value from case name.
     * 
     * @return string|null
     */
        public static function valueFromName(string $name) : string|null
        {
            foreach (self::cases() as $c) 
            {
                if( $name===$c->name ){
                    return $c->value;
                }
            }

            return null;
        }


    /**
     * Get case from case name.
     * 
     * @return string|null
     */
        public static function fromName(string $name) : Page|null
        {
            foreach (self::cases() as $c) 
            {
                if( $name===$c->name ){
                    return $c;
                }
            }

            return null;
        }
}