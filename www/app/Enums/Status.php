<?php
namespace app\Enums;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Map of valid status.
 * 
 * @author chris
 *
 */
enum Status : int
{
    case UNPUBLISHED = 0;
    case PUBLISHED = 1;
    case ARCHIVED = 2;
}