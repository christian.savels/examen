<?php
namespace app\Enums;

defined('_PWE') or die("Limited acces");

enum ErrorCode : string
{
    // Database
    case DB_INVALID_TABLE = "DB01";
    case DB_INVALID_COLUMN = "DB02";
    
    // Request received from outside.
    CASE REQUEST_ATTEMPT = "RA01";
}
?>

