<?php
namespace app\Enums;

defined('_PWE') or die("Limited acces");

enum Access : int
{
    // Basic user levels.
    case GUEST = 1; // Not logged in.
    case USER = 3;  // Logged in.
    case ROOT = 5;  // Admin user.
    
    
    /**
     * Define if level is root level.
     */
        public static function isRoot( int $level ) : bool {
            return self::ROOT->value==$level;
        }
        
    
    /**
     * Get related access.
     * $param int $level
     * @return Access|null Related access or null.
     */
        public static function related( int $level ) : Access|null
        {
            $cases = self::cases();
            $len = count($cases);
            $r = null;
            $i = 0;
            
            while ( is_null($r) && $i<$len ) 
            {
                if ( $cases[$i]->value==$level ) {
                    $r = $cases[$i];
                }
                $i++;
            }
            
            return $r;
        }
}
?>

