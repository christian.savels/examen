<?php
namespace app\Enums;

defined('_PWE') or die("Limited acces");

/**
 * 
 * Map of valid user status.
 * 
 * @author chris
 *
 */
enum UserStatus : int
{
    case PUBLISHED = 1;
    case BANNED = 0;
    case DEAD = -1;
}