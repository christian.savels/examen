<?php
defined('_PWE') or die("Limited acces");

spl_autoload_register
( 
    function ($class) 
    {
        $class = str_replace('\\', DIRECTORY_SEPARATOR, $class);
        require __DIR__ . '/' . $class . '.php';
        //require __DIR__ . '/' . strtolower($class) . '.php';
    }
);
?>