<?php 
defined('_PWE') or die("Limited acces");

$factory = \app\Helpers\Factory::getInstance();
$ztext = $factory->getTxt();    // Text tool (translator).
$content = "";                  // Page html.
$isJSON = $factory->getFormat()=="json";
$loadHtml = !$_POST && !$isJSON;

// Start html format.
if ($loadHtml)
{ ?>
   <!DOCTYPE html>
   <html lang="<?php echo $ztext->getLangISO() ?>" data-bs-theme="dark"><?php
}

/*
 * Inject content into $content.
 * If view needs some files, 
 * they will be added through the Factory.
 */
    require_once __DIR__ . '/view/content.php';
    
    // Page 404
    if ( is_null($content) ) {
        //\app\Helpers\Helper::redirect();
    }
    
    
    require_once __DIR__ . '/view/header.php';

// Start html format 
if ($loadHtml) 
{ 
    $bodyAttribs = $factory->getBodyAttributes(); ?>
	<body <?php echo implode(' ', $bodyAttribs) ?> ><?php
        require_once __DIR__ . '/view/menu.php'; ?>
    	<main><?php 
}

// Display content assigned from content.php.
echo trim($content);

// Html format elements.
if ($loadHtml)
{ ?>
	</main><?php 
    require_once __DIR__ . '/view/notify.php';
    require_once __DIR__ . '/view/footer.php';
    require_once __DIR__ . '/view/modal.php'; 
    require_once __DIR__ . '/view/templates.php'; 
   
    // Include common scripts
    require_once __DIR__ . '/view/scripts.php'; ?>
    
   </body>
   </html><?php
} ?>