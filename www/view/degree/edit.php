<?php
defined('_PWE') or die("Limited acces");

global $factory;

// Page properties.
$IDForm = "degreeForm";

// Script to execute on page ready.
$script = "const PWDEdit = new PWDegreeEdit"
    ."(
 			{
 				form: '" . $IDForm . "'
 			}
 		);";
    
// Add script
$factory->addScriptDeclaration($script);
?>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-lg-6 col-md-9 col-sm-12">
			<h2 class="page-title mt-5">
				<?php echo \app\Helpers\IconsHelper::get("degrees", 0, 'me-2') ?>
				<?php echo $this->txt("PAGE_TITLE") ?>
			</h2>

			<!-- Form -->
			<form
				id="<?php echo $IDForm ?>" 
				name="<?php echo $IDForm ?>" 
				method="post"
				action="<?php echo $factory->getUrl(\app\Enums\Page::DEGREES_EDIT) ?>"
			><?php 
			
				foreach ( $data->fields as $fieldsetName => $fieldset )
				{  ?>
					<div class="pw-form-fieldset fieldset-<?php echo $fieldsetName ?>"><?php 
					
						foreach ( $fieldset as $field ) 
						{ ?>
							<div class="pw-form-field">
								<?php echo $field; ?>
							</div><?php
						} ?>
					
					</div><?php 
				} ?>
				
				<!-- Submit form -->
				<div class="w-100 text-center my-5">
					<button 
						type="submit" 
						class="btn btn-success"
						class="submit-form my-4"
					><?php echo $this->txt("SUBMIT")?></button>
				</div>
				
				<!-- CSRF Token -->
				<?php echo $factory->getTokenCSRF() ?>
			</form>

		</div><!-- col -->
	</div><!-- row -->
</div><!-- container -->