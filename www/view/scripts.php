<?php 
defined('_PWE') or die("Limited acces"); 

// Get current language
if ( $factory->cfg("lang.multi") && !$factory->getUser()->isGuest() ) {
	$lang = $factory->getUser()->getLang();
} else {
	$lang = $factory->getTxt()->getLang();
}

// Get messages from session.
$messages = null;
if ( isset($_SESSION['notify']) && !empty($_SESSION['notify'])) 
{
    $messages = json_encode($_SESSION['notify']);
    unset($_SESSION['notify']);
}

// Files to include @ the end of the page (like bootstrap js).
$scripts = array_keys($factory->getAppendAfterDeclaration());
foreach ( $scripts as $s ) { ?>
    <script src="<?php echo $s ?>"></script><?php
} 

// Common script ?>
<script type="text/javascript">
     var PWE, PMRules;
     window.addEventListener("load", ()=> 
     {
		PWE = new PWExam(
			{
				tokenName: '<?php echo \app\Helpers\SessionHelper::TOKEN_CSRF ?>',
				notify: { 
					delay: <?php echo $factory->cfg("delay.notify") ?>,
					<?php if ( !empty($messages) ) : ?>
					messages: <?php echo $messages ?>,
					<?php endif; ?>
				},
				cookies: {
					validity: '<?php echo $factory->cfg("cookies.validity") ?>' /* in days */
				},
				delay: 2000,
				url: {
					rulesForm: '<?php echo $factory->getUrl( \app\Enums\Page::JSON_RULES_FORM) ?>',
					rulesSave: '<?php echo $factory->getUrl( \app\Enums\Page::JSON_RULES_SAVE) ?>'
				},
				elements: {
					modalID: '<?php echo $factory->cfg("modal_id") ?>',
				},
				language: {
					multi: <?php echo $factory->cfg("lang.multi") ?>,
					current: '<?php echo $factory->getTxt()->getLang() ?>'
				}
			}
		);
		
		<?php 
			// Include scripts declarations from view.
			echo implode(" ", array_keys($factory->getScriptsDeclaration())); 
		?>
     }); 
</script>