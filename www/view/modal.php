<?php 
use app\Helpers\Factory;
defined('_PWE') or die("Limited acces");
?>

<div id="<?php echo Factory::getInstance()->cfg("modal_id") ?>" class="pw-modal">
	<div class="pw-modal-body text-bg-dark">
    	<div class="pw-modal-header">
    		<!-- Title -->
            <h3 class="pw-modal-title my-3 mx-4"></h3>
            <div class="p-3">
            	<span class="fa-solid fa-xmark pw-modal-close fa-2x"></span>
            </div>
        </div>
        <!-- Content -->
        <div class="pw-modal-content"></div>
        
        <!-- Buttons -->
        <div class="pw-modal-buttons text-center p-4" data-pw-buttons-count></div>
    </div>
</div>