<?php 
defined('_PWE') or die("Limited acces");
?>

<!-- 
	The main container is the data-pw-toasts element.
	It should contain only one data-pw-toast.
-->
<div class="toasts-container-bottom-center" data-pw-toasts>
	<div class="p-3" data-pw-toast="0">
		<div class="row align-items-center">
			<!-- icon -->
			<div class="col-2 p-3">
				<!-- Icon alert -->
				<span data-pw-toast-icon="alert" class="fas fa-exclamation text-danger"></span>
				
				<!-- Icon info -->
				<span data-pw-toast-icon="info" class="fa fa-info text-info"></span>
				
				<!-- Icon warning -->
				<span data-pw-toast-icon="warning" class="far fa-bell text-warning"></span>
				
				<!-- Icon success -->
				<span data-pw-toast-icon="success" class="fas fa-check text-success"></span>

			</div>
			
			<!-- rest -->
			<div class="col">
				<div class="row">
					<div class="col">
						<h6 data-pw-title></h6>
					</div>			
					<div class="col-2">
						<!-- Close btn -->
						<button type="button" class="btn-close" aria-label="Close" data-pw-toast-close ></button>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<p data-pw-message>msg</p>	
					</div>							
				</div>
			</div>
		</div>
	</div>
</div>


