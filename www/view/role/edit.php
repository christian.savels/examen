<?php
defined('_PWE') or die("Limited acces");

global $factory;

// Page properties.
$title = is_null($data->role)  ? $this->txt('TITLE_NEW') : $this->txt('TITLE_EDIT', $data->role->title);
$IDForm = "roleForm";

// Script to execute on page ready.
$script = "const PWRole = new PWRoleEdit"
    ."(
 			{
 				form: '" . $IDForm . "'
 			}
 		);";
    
// Add scripts
$factory->addScriptDeclaration($script);
?>

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-9 col-sm-12">

			

			<div class="card my-5">
				<div class="card-header">
					<h2 class="page-title mt-2">
						<?php echo \app\Helpers\IconsHelper::get("roles", 0, "text-main-color") ?>
						<?php echo $this->txt( is_null($data->role) ? 'TITLE_NEW' : 'TITLE_EDIT');?>
					</h2>
				</div>
				<div class="card-body">
					
					<form
						id="<?php echo $IDForm ?>" 
						name="<?php echo $IDForm ?>" 
						method="post"
						action="<?php echo $factory->getUrl(\app\Enums\Page::ROLES_ROLE_EDIT) ?>"
					><?php 
					
						foreach ( $data->fields as $fieldsetName => $fieldset )
						{  ?>
						
							<div class="pw-form-fieldset fieldset-<?php echo $fieldsetName ?>"><?php 
							
								foreach ( $fieldset as $field ) 
								{ ?>
									<div class="pw-form-field">
										<?php echo $field; ?>
									</div><?php
								} ?>
							
							</div><?php 
						} ?>
						
						<!-- Submit form -->
						<div class="w-100 text-center">
							<button 
								type="submit" 
								class="btn btn-success my-4"
							><?php echo $this->txt("SUBMIT")?></button>
						</div>
						
					</form>
				</div>
			</div>
		</div><!-- col -->
	</div><!-- row -->

	<!-- CSRF Token -->
	<?php echo $factory->getTokenCSRF() ?>
</div><!-- container -->
	