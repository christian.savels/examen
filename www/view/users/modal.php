<?php 
use app\Enums\UserStatus;
use app\Helpers\IconsHelper;

defined('_PWE') or die("Limited acces");

/**
 * Page only loaded through a modal 
 * to select one or more users.
 */

global $factory, $ztext;
?>
	
<div class="row">

	<!-- Left part -->
	<div class="col col-sm-12 col-md-4 col-lg-3" >

		<!-- Order by -->
		<?php if ( isset($data->orderBy) ) {
			echo $data->orderBy->html;
		} ?>
	
		<!-- DB Filters -->
		<?php if ( isset($data->filters) ) : ?>
			<div data-pw-filters >
				<h5>
					<i class="fa-solid fa-filter"></i>
					<?php echo $ztext->get("FILTERS") ?>
				</h5>
				<?php echo $data->filters ?>
			</div>
		<?php endif; ?>
			
	</div>

	<!-- Right part, Users -->
	<div class="col col-sm-12 col-md-8 col-lg-9" data-pw-users >

		<!-- Items table -->
		<table class="table table-striped table-hover" >
			<thead>
				<tr>
					<th>
						<?php echo $ztext->get("TH_ID") ?>
					</th>
					<th>
						&nbsp;
					</th>
					<th>
						&nbsp;
					</th>
					<th>
						<?php echo $ztext->get("TH_NAME") ?>
					</th>
					<th>
						<?php echo $ztext->get("TH_EMAIL") ?>
					</th>
					<th>
						<?php echo $ztext->get("TH_LOCALITY") ?>
					</th>
					<th>
						
					</th>
				
					<th>
						<?php echo $ztext->get("TH_ACCESS") ?>
					</th>
					<th class="text-center">
						<?php echo $ztext->get("TH_STATUS") ?>
					</th>
					
				</tr>
			</thead>

			<!-- Items rows -->
			<tbody data-pw-items>

			<?php foreach ($data->items as $item) :  ?>

				<!-- An user -->
				<tr 
					data-pw-item="<?php echo $item->id ?>" 
					data-pw-item-published="<?php echo $item->published ?>" 
					data-pw-item-selected="0" 
				>
					<td data-pw-id>
						<?php echo $item->id ?>
					</td>
					<td 
						class="table-light text-center" 
						title="<?php echo $ztext->getTitle("TITLE_SELECT") ?>" 
						data-pw-select="<?php echo $item->id ?>"
					>
						<?php echo IconsHelper::get("select"); ?>
					</td>
					<td 
						class="table-secondary" 
						title="<?php echo $ztext->getTitle("TITLE_EDIT") ?>" 
						data-pw-edit="<?php echo $item->id ?>"
					>
						<?php echo IconsHelper::get("edit"); ?>
					</td>
					<td data-pw-user-edit="<?php echo $item->id ?>">
						<span data-pw-lastname><?php echo $item->lastname ?></span>
						<span data-pw-firstname><?php echo $item->firstname ?></span>
					</td>
					<td>
						<span class="fs-6" data-pw-email><?php echo $item->email ?></span>
					</td>
					<td>
						<span data-pw-address-zip><?php echo $item->address->zip ?></span>
						<span data-pw-address-city><?php echo $item->address->city ?></span>
						<span data-pw-address-country>(<?php echo $item->address->country ?>)</span>
					</td>
					<td>
						
					</td>
					
					<td>
						<span data-pw-access="<?php echo $item->id ?>"><?php echo $item->access ?></span>
					</td>
					<td class="text-center" data-pw-published-icon>
						
					</td>
					
				</tr>
				
			<?php endforeach; ?>
			</tbody>
		</table>

		<!-- Pagination (formations) -->
		<?php if ( isset($data->pagination) ) {
				echo $data->pagination->render("PAGINATION_ARIA");
			} ?>

	</div>
</div>

<!-- CSRF Token -->
<?php echo $factory->getTokenCSRF() ?>