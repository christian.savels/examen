<?php 
use app\Enums\UserStatus;
use app\Helpers\IconsHelper;

defined('_PWE') or die("Limited acces");

global $factory, $ztext;

// Load some templates
\app\Helpers\TemplatesHelper::load('user.manage', new \app\Objects\User());

// Script to execute on page ready.
$idConfirm = "confirmUsersAction";
$orderBy = isset($data->orderBy) ? "orderBy: '" . $data->orderBy->id . "'," : "";
$paginationScript = "null";
if ( isset($data->pagination) ) {
	$paginationScript = $data->pagination->getJSON();
}

$script = "const PWEUsers = new PWUsers"
  			."(
				{
					confirm: {id: '" . $idConfirm . "'},
					toolbar: '" . $data->options->id . "', 
					" . $orderBy . "
					pagination: '" . $paginationScript . "',
					url: {
						access: '" . $factory->getUrl(\app\Enums\Page::USERS_ACCESS, null, true) . "',
						mailform: '" . $factory->getUrl(\app\Enums\Page::USERS_MAIL_FORM, null, true) . "',
						more: '" . $factory->getUrl(\app\Enums\Page::USERS_MORE, null, true) . "',
						items: '" . $factory->getUrl(\app\Enums\Page::USERS_MANAGE, null, true) . "',
						edit: '" . $factory->getUrl(\app\Enums\Page::USERS_EDIT, null, true) . "',
						create: '" . $factory->getUrl(\app\Enums\Page::USERS_CREATE, null, true) . "',
						signup: '" . $factory->getUrl(\app\Enums\Page::USERS_SIGNUP, null, true) . "',
						apply: '" . $factory->getUrl(\app\Enums\Page::USERS_EDIT_APPLY, null, true) . "',
						delete: '" . $factory->getUrl(\app\Enums\Page::USERS_DELETE, null, true) . "',
					},
					templates: {
						item: 'user.manage'
					}
 				}
 			);";
    
    // Add script
$factory->addScriptDeclaration($script);
?>

<div class="container">

	<?php 
		// Rules form
		if ( isset($data->rules) ) : ?>
			<div class="page-actions">
				<?php echo $data->rules;?>
			</div>
	<?php endif ?>	
		
	<div class="row justify-content-center">
		
		<!-- Users -->
		<div class="col-md-9 col-sm-12" data-pw-users >
				
			<!-- Page title -->
			<h1 class="page-title text-center my-5">
				<?php echo IconsHelper::get("formations", 0, 'text-main-color'); ?>
				<?php echo $this->txt("PAGE_TITLE")?>
			</h1>

			<!-- Toolbar -->
			<?php echo $data->options->render() ?>

			<!-- Actions confirmation -->
			<?php echo \app\Helpers\HtmlHelper::confirmBox($idConfirm); ?>
			
			<!-- Alert active filters --><?php 
			if ( isset($data->filtersAlert) ) {
				echo $data->filtersAlert; 
			} ?>

			<!-- Items -->
			<table class="table table-striped table-hover table-dark " >
				<thead>
					<tr>
						<th><?php echo $ztext->get("TH_ID") ?></th>
						<th>&nbsp;</th>
						<th><?php echo $ztext->get("TH_NAME") ?></th>
						<th><?php echo $ztext->get("TH_LOCALITY") ?></th>
						<th class="text-center"><?php echo $ztext->get("TH_TEACHER") ?></th>
						<th class="text-center"><?php echo $ztext->get("TH_STUDENT") ?></th>
						<th><?php echo $ztext->get("TH_ACCESS") ?></th>
						<th class="text-center"><?php echo $ztext->get("TH_STATUS") ?></th>
					</tr>
				</thead>

				<!-- Users list -->
				<tbody data-pw-items>

				<?php foreach ($data->items as $item) {
					echo \app\Helpers\TemplatesHelper::get("user.manage", $item);
				} ?>
				</tbody>
			</table>

			<div class="row cols-lg-3 cols-sm-1 mt-4 justify-content-center">
				<!-- Export -->
				<?php if ( isset($data->export) ) { ?>
					<div class="col-auto box-export">
						<?php echo $data->export; ?>
					</div><?php
				 } 

				// Filters 
				if ( isset($data->filters) ) : ?>
					<div class="col-auto" data-pw-filters >
						<?php echo $data->filters ?>
					</div>
				<?php endif; 

				// Pagination (formations) 
				if ( isset($data->pagination) ) { ?>
					<div class="col"><?php
						echo $data->pagination->render("PAGINATION_ARIA"); ?>
					</div><?php
				}
				?>
			</div>

		</div>
	</div>

	<!-- Legend -->
	<ul class="list-group list-group-horizontal status-legend">
	<?php 
		// Table icons
		foreach ( ["select", "edit", "more", "students", "teachers"] as $ic )
		{ ?>
			<li class="list-group-item">
				<?php echo IconsHelper::get($ic); ?>
				<?php echo $ztext->get("LEGEND_TB_" . $ic); ?>
			</li><?php
		}
		// Users status.
		foreach ( UserStatus::cases() as $c )
		{ ?>
			<li class="list-group-item">
				<?php echo IconsHelper::get("user-status-" . $c->value); ?>
				<?php echo $ztext->get("LEGEND_USER_STATUS_" . $c->value); ?>
			</li><?php
		} ?>
	</ul>

	<!-- CSRF Token -->
	<?php echo $factory->getTokenCSRF() ?>

</div><!-- container -->