<?php 
use app\Helpers\IconsHelper;
defined('_PWE') or die("Limited acces");

global $factory, $ztext;



?>

<!-- Top Carousel -->
<div class="w-100">
   
        <div id="carouselTopPage" class="carousel slide">
            <div class="carousel-inner">

                <?php foreach ( $data->topCarousel as $i => $item ) : 

                        $styleIdent = "carousel-top-item-$i"; 
                        $factory->addStyleDeclaration(
                            "." . $styleIdent . '{ background-image:url("' . $item->img . '"); }'
                        ); ?>

                    <div class="carousel-item bg-cover <?php echo $styleIdent ?> <?php echo !$i ? 'active' : '' ?>">
                        
                        <div class="carousel-caption d-none d-md-block">
                            <h5 class="title"><?php echo $item->title ?></h5>
                            <p><?php echo $item->desc ?></p>
                        </div>
                    </div>

                <?php endforeach; ?>
                        
            </div><!-- inner -->

            <button class="carousel-control-prev" type="button" data-bs-target="#carouselTopPage" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselTopPage" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>

        </div><!-- #carouselTopPage -->
    
</div><!-- w-100 -->

<div class="pw-home-main w-100 bg-light pb-5">

    <!-- Top home info -->
    <div class="container ">
        <div class="row text-center">
            <?php foreach ($data->topInfoList as $i => $item ) : ?>
                <div class="col-lg-3 col-md-6">
                    <div class="top-info-item h-100">
                        <div class="top-info-icon ">
                            <?php echo IconsHelper::get($item->icon) ?>
                        </div>       
                        <div class=" ">
                            <h4 class="top-info-title"><?php echo $ztext->get($item->title) ?></h4>
                            <p><?php echo $ztext->get($item->desc) ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>


        <!-- About us -->
        <div class="pw-about">

            <h2 class="pw-about-title text-bg-light mt-5">
                <?php echo $data->aboutUs->title ?>
            </h2>
            <div class="pw-about-slogan"><?php echo $data->aboutUs->slogan ?></div>
            
            <div class="row align-items-center ">

                <div class="col-lg-7 col-sm-12 pw-about-content">
                    <?php echo $data->aboutUs->content ?>
                </div>
                <div class="col-lg-5 col-sm-12 ">
                    <div class="pw-about-img p-3 ">
                        <img 
                            src="<?php echo $data->aboutUs->img ?>" 
                            alt="<?php echo $data->aboutUs->imgAlt ?>" 
                        />
                    </div>
                </div>
            </div>

            <?php foreach ( $data->aboutUs->sign as $i => $s ) : ?>
                <div class="pw-about-sign<?php echo $i+1 ?>">
                    <?php echo $data->aboutUs->sign[$i] ?>
                </div>
            <?php endforeach; ?>
        </div><!-- end about us -->

    </div><!-- container -->

</div><!-- pw-home-main -->
