<?php 
use app\Objects\Degree;
defined('_PWE') or die("Limited acces");

global $factory, $ztext;

// Load some templates
\app\Helpers\TemplatesHelper::load('degree.manage', new Degree());

// Script to execute on page ready.
$idContent = "contentPart";
$idConfirm = "confirmDegreesAction";

// Script pagination
$paginationScript = "null";
if ( isset($data->pagination) ) {
	$paginationScript = $data->pagination->getJSON();
}
// Order by
$orderBy = isset($data->orderBy) ? "orderBy: '" . $data->orderBy->id . "'," : "";

// Script declaration
$script = "const PWED = new PWEDegrees"
    ."(
		{
			content: {id: '" . $idContent . "'},
			confirm: {id: '" . $idConfirm . "'},
			toolbar: '" . $data->options->id . "',
			pagination: '" . $paginationScript . "',
			" . $orderBy . "
			 url: {
				items: '" . $factory->getUrl(\app\Enums\Page::DEGREES_MANAGE) . "',
				delete: '" . $factory->getUrl(\app\Enums\Page::DEGREES_DELETE) . "'
			},
			templates: {
				item: 'degree.manage'
			}
		 }
 		);";
    
// Add script
$factory->addScriptDeclaration($script);
?>
<div id="<?php echo $idContent ?>" class="container mb-5" >

	<!-- Rules form -->
	<?php if ( isset($data->rules) ) : ?>
		<div class="page-actions">
			<?php echo $data->rules;?>
		</div>
	<?php endif ?>

	

	<div class="row justify-content-center">
		
		<div class="col-lg-6 col-md-9 col-sm-12">

			<!-- Page title -->
			<h1 class="page-title my-5">
				<?php echo \app\Helpers\IconsHelper::get("degrees"); ?>
				<?php echo $this->txt("PAGE_TITLE")?>
			</h1>

			<!-- Toolbar options -->
			<?php echo $data->options->render() ?>

			<!-- Confirm action -->
			<?php echo \app\Helpers\HtmlHelper::confirmBox($idConfirm); ?>

			<!-- Alert active filters --><?php 
			if ( isset($data->filtersAlert) ) {
				echo $data->filtersAlert; 
			} ?>

			<!-- Alert no result -->
			<div class="alert alert-warning" role="alert" 
				data-pw-items-count="<?php echo count($data->items) ?>"
			>
				<?php echo $ztext->get('NO_DEGREES'); ?>
			</div>

			<!-- List of degrees -->
			<div class="list-group mt-4" data-pw-degrees data-pw-items >
			
				<?php foreach ( $data->items as $item ) {
					echo \app\Helpers\TemplatesHelper::get("degree.manage", $item);
				} ?>

			</div><!-- items -->

			<!-- Pagination (formations) -->
			<?php
				if ( isset($data->pagination) ) {
					echo $data->pagination->render("PAGINATION_ARIA");
				}
			?>
			
			<div class="row cols-lg-2 cols-sm-1 mt-4">

				<!-- Filters -->
				<?php if ( isset($data->filters) ) : ?>
					<div class="col" data-pw-filters >
						<?php echo $data->filters ?>
					</div>
				<?php endif; ?>
			</div>

		</div><!-- col -->
	</div><!-- row -->

	

</div><!-- container -->


<!-- CSRF Token -->
<?php echo $factory->getTokenCSRF() ?>