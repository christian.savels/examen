<?php 
use app\Helpers\IconsHelper;
defined('_PWE') or die("Limited acces");

global $factory;

// Page properties.
$IDForm = "loginForm";
$IDIdent = "identifier";
$IDPwd = "pwd";

// Script to execute on page ready.
$script = "const PWLogin = new UserLogin"
        ."(
 			{
 				form: '" . $IDForm . "',
 				identifier: '" . $IDIdent . "',
 				pwd: '" . $IDPwd . "',
 			}
 		);";
    
// Add script
$factory->addScriptDeclaration($script);
?>
<div class="d-flex align-items-start flex-column">
	<div class="container">

		<!-- Title -->
		<h2 class="page-title my-4 text-center">
			<?php echo IconsHelper::get("login", 0, "me-2 text-main-color") ?>
			<?php echo $this->txt("PAGE_LOGIN_TITLE") ?>
		</h2>

		<div class="row justify-content-center">
			<div class="col-lg-6 col-sm-12">
				
				<!-- Info -->
				<div class="alert alert-light mb-4" role="alert">
					<?php echo IconsHelper::get("info", 0, "me-2 text-main-color") ?>
					<?php echo $this->txt("PAGE_LOGIN_DESC"); ?>
				</div>

				<form
					id="<?php echo $IDForm ?>" 
					name="<?php echo $IDForm ?>" 
					method="post"
					action="<?php echo $factory->getUrl(\app\Enums\Page::USER_LOGIN) ?>"
				>

					<!-- Identifier -->
					<div>
						<label for="<?php echo $IDIdent ?>">
							<?php echo $this->txt("LOGIN_IDENTIFIER") ?>
						</label>
						<input 
							id="<?php echo $IDIdent ?>" 
							name="<?php echo $IDIdent ?>" 
							type="text"
							class="form-control"
							required
						/>
					</div>
					
					<!-- Password -->
					<div>
						<label for="<?php echo $IDPwd ?>">
							<?php echo $this->txt("LOGIN_PWD") ?>
						</label>
						<input 
							id="<?php echo $IDPwd ?>" 
							name="<?php echo $IDPwd ?>" 
							type="password" 
							class="form-control"
							required
						/>
					</div>
					
					<!-- Submit form -->
					<div class="w-100 text-center">
						<button 
							type="submit" 
							class="btn btn-success submit-form my-4 w-100"
						>
							<?php echo IconsHelper::get("login", 0, "me-2") ?>
							<?php echo $this->txt("LOGIN_SUBMIT")?>
						</button>
					</div>
					
					<!-- CSRF Token -->
					<?php echo $factory->getTokenCSRF() ?>
				</form>
	</div>
	<div class="col-lg-4 col-sm-12">

			<!-- Inscription -->
					<div class="card w-100">
						<div class="card-header first-letter">
							<?php echo $this->txt("TITLE_REGISTER") ?>
						</div>
						<div class="card-body">
							<p class="card-text" role="alert">
								<?php echo $this->txt("DESC_REGISTER") ?>
							</p>							
						</div>
						<div class="card-footer text-body-secondary p-0">
							<a 
								href="<?php echo $factory->getUrl(\app\Enums\Page::USER_SIGNUP) ?>"
								class="btn bg-main-color text-light w-100 m-0"
							>
								<?php echo IconsHelper::get("signup", 0, "me-2") ?>
								<?php echo $this->txt("LINK_REGISTER") ?>
							</a>
						</div>
					</div>
				
			</div>
		</div>
	</div>
</div>