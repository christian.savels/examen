<?php 
defined('_PWE') or die("Limited acces");

global $factory, $ztext;

// Page properties.
$IDForm = "signForm";
$IDUsername = "username";
$IDPwd = "pwd";
$IDPwd2 = "pwd2";
$IDEmail = "email";
$IDLang = "lang";
$IDFirstname = "firstname";
$IDLastname = "lastname";

// Make the lang select form.
//$selectLang = $factory->getTxt()->getLangChooser();
$selectLang = $factory->getTxt()->getLangSelect($IDLang);
/*
$selectLang = $factory->getTxt()->makeSelect(
    $IDLang, $IDLang, "", "SIGNUP_LANG_HINT", $style=null
);*/

// Add script
$script = "const PWSignup = new UserSignup(" 
            . "{
 				form: '" . $IDForm . "',
 				username: '" . $IDUsername . "',
 				pwd: '" . $IDPwd . "',
 				pwd2: '" . $IDPwd2 . "',
 				email: '" . $IDEmail . "',
 				lang: '" . $IDLang . "',
 				firstname: '" . $IDFirstname . "',
 				lastname: '" . $IDLastname . "'
 			   }" 
            . ");";
$factory->addScriptDeclaration($script);
?>
<div class="container">
    
    <form
    	 id="<?php echo $IDForm ?>" 
    	 name="<?php echo $IDForm ?>" 
    	 method="post"
    	 action="<?php echo $factory->getUrl(\app\Enums\Page::USER_SIGNUP) ?>"
     >
        <div class="row justify-content-center">
       		<div class="col-lg-6 col-md-8 col-sm-12">
			   
				<h2 class="page-title my-4"><?php echo $this->txt("PAGE_SIGNUP_TITLE");?></h2>
				<p class="alert alert-light" role="alert"><?php echo $this->txt("PAGE_SIGNUP_DESC"); ?></p>

				<!-- Lang -->
				<label for="<?php echo $IDLang ?>">
					<?php echo $this->txt("SIGNUP_LANG") ?>
				</label>
				<?php echo $selectLang; ?>
				
				<div class="card mt-3">
					<div class="card-body">
						<!-- Username -->
						<div>
							<label for="<?php echo $IDUsername ?>">
								<?php echo $this->txt("SIGNUP_USERNAME") ?> *
							</label>
							<input 
								id="<?php echo $IDUsername ?>" 
								name="<?php echo $IDUsername ?>" 
								type="text"
								class="form-control"
								required
							/>
						</div>
					
						
						<!-- Email -->
						<div>
							<label for="<?php echo $IDEmail ?>">
								<?php echo $this->txt("SIGNUP_EMAIL") ?> *
							</label>
							<input 
								id="<?php echo $IDEmail ?>" 
								name="<?php echo $IDEmail ?>" 
								type="email"
								class="form-control"
								required
							/>
						</div>
					</div>
				</div>
				
				<div class="card mt-3">
					<div class="card-body">
						<!-- Password -->
						<div>
							<label for="<?php echo $IDPwd ?>">
								<?php echo $this->txt("SIGNUP_PWD") ?> *
							</label>
							<input 
								id="<?php echo $IDPwd ?>" 
								name="<?php echo $IDPwd ?>" 
								type="password" 
								class="form-control"
								required
							/>
						</div>
						
						<!-- Password confirm -->
						<div>
							<label for="<?php echo $IDPwd2 ?>">
								<?php echo $this->txt("SIGNUP_PWD2") ?> *
							</label>
							<input 
								id="<?php echo $IDPwd2 ?>" 
								name="<?php echo $IDPwd2 ?>" 
								type="password" 
								class="form-control"
								required
								placeholder="<?php echo $ztext->get("CONFIRM_PWD_HINT") ?>"
							/>
						</div>
					</div>
				</div>

				<div class="alert alert-warning alert-dismissible fade show mt-3" role="alert">
					<?php echo $this->txt("PWD_EASY_INFO", $factory->cfg("password_len_min")) ?>
					<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div>
				
				<div class="card mt-3">
					<div class="card-body">
						<!-- Firstname -->
						<div>
							<label for="<?php echo $IDFirstname ?>">
								<?php echo $this->txt("SIGNUP_FIRSTNAME") ?> *
							</label>
							<input 
								id="<?php echo $IDFirstname ?>" 
								name="<?php echo $IDFirstname ?>" 
								class="form-control"
								type="text"
								required
							/>
						</div>
						
						<!-- Lastname -->
						<div>
							<label for="<?php echo $IDLastname ?>">
								<?php echo $this->txt("SIGNUP_LASTNAME") ?> *
							</label>
							<input 
								id="<?php echo $IDLastname ?>" 
								name="<?php echo $IDLastname ?>" 
								type="text"
								class="form-control"
								required
							/>
						</div>
					</div>
				</div>
								
				<!-- Submit form -->
				<div class="w-100 text-center">
					<input 
						type="submit" 
						value="<?php echo $this->txt("SIGNUP_SUBMIT")?>"
						class="btn btn-success submit-form my-4"
					/>
				</div>

			</div><!-- col -->
		</div><!-- row -->
    	
    	<!-- CSRF Token -->
    	<?php echo $factory->getTokenCSRF() ?>
	</form>
    
    
    
</div>









