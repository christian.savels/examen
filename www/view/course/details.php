<?php 
use app\Helpers\Factory;
use app\Helpers\IconsHelper;

defined('_PWE') or die("Limited acces");

global $factory, $ztext;

$cstyle = '.bg-course {';
	$cstyle .= "background-image: url('" . $data->course->image . "');";
	$cstyle .= "}";
	Factory::getInstance()->addStyleDeclaration($cstyle);
?>

	<div class="container">

		<h1 class="page-title mt-4 mb-5 text-center"><?php echo $ztext->get("PAGE_TITLE"); ?></h1>

		<div class="card card-course mb-3" >
			<div class="row g-0">
				<div class="col-md-4 rounded-start bg-box">
					<div class="bg-inner bg-course bg-cover rounded-start"></div>
				</div>
				<div class="col-md-8">
					<div class="card-header">
						<h4 class="card-title m-0 first-letter"><?php echo $data->course->title; ?></h4>
					</div>
					<div class="card-body px-4">
						<p class="card-text"><?php echo $data->course->description; ?></p>
					</div>
					<div class="card-footer px-2">
						
						<ul class="list-group list-group-flush w-100 details-list">

							<!-- Begins on date -->
							<li class="list-group-item m-0">
								<?php echo IconsHelper::get("date-start", 0, "me-2") ?>
								<?php echo $ztext->get("BEGINS_ON", $data->course->start_on); ?>
							</li>
							
							<!-- End on date -->
							<li class="list-group-item ">
								<?php echo IconsHelper::get("date-end", 0, "me-2") ?>
								<?php echo $ztext->get("END_ON", $data->course->end_on); ?>
							</li>

							<?php
							// Current user is student ?
							if ( $data->course->isStudent )
							{ ?>
								<li class="list-group-item list-group-item-info">
									<?php echo IconsHelper::get("student", 0, "me-2") ?>
									<?php echo $ztext->get("COURSE_IS_STUDENT"); ?>
								</li><?php
							}

							// Current user is teacher ?
							if ( $data->course->isTeacher )
							{ ?>
								<li class="list-group-item list-group-item-info">
									<?php echo IconsHelper::get("teacher", 0, "me-2") ?>
									<?php echo $ztext->get("COURSE_IS_TEACHER"); ?>
								</li><?php
							}
							// Current teacher(s) 
							elseif ( !empty($data->course->teacher) ) 
							{ ?>
								<li class="list-group-item list-group-item-secondary">
									<?php echo IconsHelper::get("teacher", 0, "me-2") ?>
									<?php echo $data->course->teacher ?>
								</li><?php
							}

							// Inscription
							if ( !$data->course->isStudent )
							{ ?>
								<li class="list-group-item bg-main-color" data-pw-register>
									<a href="<?php echo $data->course->urlRegister ?>" 
										class="w-100 m-0 d-block text-light"
										title="<?php echo $ztext->getTitle("COURSE_INSCRIPTION") ?>"
									>
										<?php echo IconsHelper::get("registerto", 0, "me-2 text-light") ?>
										<?php echo $ztext->get("COURSE_INSCRIPTION") ?>
									</a>
								</li><?php
							} ?>						
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- Full html description -->
		<?php if ( !empty($data->course->content) ) : ?>
			<hr class="hr-center my-4" />
			<div class="course-content  text-bg-dark p-4 rounded-start">
				<h5 class="mb-3 text-start first-letter"><?php echo $ztext->get("CONTENT_TITLE"); ?></h5>
				<?php echo $data->course->content; ?>
			</div>
		<?php endif; ?>

	</div>

	<!-- CSRF Token -->
	<?php echo $factory->getTokenCSRF();
	?>
