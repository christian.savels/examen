<?php
defined('_PWE') or die("Limited acces");

global $factory;

// Page properties.
$IDForm = "coursForm";

// Script to execute on page ready.
$script = "const PWCE = new PWCourseEdit"
    ."(
 			{
 				form: '" . $IDForm . "'
 			}
 		);";
    
// Add script
$factory->addScriptDeclaration($script); ?>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-9 col-sm-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-md-9 col-sm-12 d-flex flex-column justify-content-center align-items-center">
							<h2 class="card-title">
								<?php echo $this->txt(!$data->item->id ? "PAGE_NEW_TITLE" : "PAGE_EDIT_TITLE");?>
							</h2>
							<?php if ( $data->item->id ) : ?>
							<h5 class="card-title">
								<?php echo $data->item->title ?>
							</h5>
							<?php endif; ?>
						</div>
						<div class="col-md-3 col-sm-12">
							<img src="<?php echo $data->item->image ?>" class="card-img-top" alt="Illustration" />

						</div>
					</div>
				</div>
				<div class="card-body">
					<!-- Form -->
					<form
						id="<?php echo $IDForm ?>" 
						name="<?php echo $IDForm ?>" 
						method="post"
						action="<?php echo $factory->getUrl(\app\Enums\Page::COURSES_EDIT) ?>"
						enctype="multipart/form-data"
					><?php 
					
						foreach ( $data->fields as $fieldsetName => $fieldset )
						{  ?>
						
							<div class="pw-form-fieldset fieldset-<?php echo $fieldsetName ?>"><?php 
							
								foreach ( $fieldset as $field ) 
								{ ?>
									<div class="pw-form-field">
										<?php echo $field; ?>
									</div><?php
								} ?>
							
							</div><?php 
						} ?>
						
						<!-- Submit form -->
						<div class="w-100 text-center">
							<button 
								type="submit" 
								class="btn btn-success submit-form my-4"
							>
								<?php echo $this->txt("SUBMIT")?>
							</button>
						</div>
						
						<!-- CSRF Token -->
						<?php echo $factory->getTokenCSRF() ?>						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>