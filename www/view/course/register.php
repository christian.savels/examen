<?php 
use app\Helpers\Factory;
use app\Helpers\IconsHelper;

defined('_PWE') or die("Limited acces");

global $factory, $ztext;

$idForm = "regForm";

// Add onReady script.
$script = "const PWReg = new PWERegisterTo('" . $idForm . "');";
$factory->addScriptDeclaration($script);
?>

<div class="container mb-5">
	<h1 class="page-title text-center mt-5"><?php echo $ztext->get("PAGE_TITLE") ?></h1>        
	<h5 class="text-center mb-5 first-letter"><?php echo $data->course->title ?></h5>        
							
	<form
		id="<?php echo $idForm ?>" 
		name="<?php echo $idForm ?>" 
		method="post"
		action="<?php echo $factory->getUrl(\app\Enums\Page::COURSES_REGISTER, $data->course->id) ?>"
	>
		<input type="hidden" name="course" value="<?php echo $data->course->id ?>" />

		<div class="row justify-content-center">
			<div class="col-lg-6 col-md-10 col-sm-12 ">

				<div class="card">
					<img 
						src="<?php echo $data->course->image ?>" 
						class="card-img-top" 
						alt="<?php echo htmlentities($data->course->title) ?>"
					>
					<div class="card-body px-4">
						
						<div class="alert alert-primary" role="alert">
							<?php echo IconsHelper::get("info", 0, "mx-0") ?>
							<?php echo $ztext->get("INFO1") ?>
						</div>

						<div class="alert alert-warning" role="alert">
						<?php echo IconsHelper::get("warning", 0, "mx-0") ?>
							<?php echo $ztext->get("INFO2") ?>
						</div>
							
						<ul class="list-group list-group-flush w-100">

							<!-- Begins on date -->
							<li class="list-group-item m-0">
								<?php echo IconsHelper::get("date-start", 0, "me-2") ?>
								<?php echo $ztext->get("BEGINS_ON", $data->course->start_on); ?>
							</li>
							
							<!-- End on date -->
							<li class="list-group-item ">
								<?php echo IconsHelper::get("date-end", 0, "me-2") ?>
								<?php echo $ztext->get("END_ON", $data->course->end_on); ?>
							</li>

						</ul>

						
					</div>
					<div class="card-footer">
						<div class="text-center btn-group d-flex" role="group">
							<button class="btn btn-success" type="submit">
								<?php echo $ztext->get("SUBMIT") ?>
							</button>
							<a class="btn btn-danger" 
								type="submit" 
								href="<?php echo $data->cancelLink ?>"
							>
								<?php echo $ztext->get("CANCEL") ?>
							</a>
						</div>
						
					</div>
				</div><!-- card -->
			
			</div><!-- col -->
		</div><!-- row -->

		<!-- CSRF Token -->
		<?php echo $factory->getTokenCSRF() ?>

	</form>
</div><!-- container -->