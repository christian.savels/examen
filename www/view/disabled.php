<?php 
defined('_PWE') or die("Limited acces");
global $factory;
?>


<p class="alert alert-danger">
	<span class="fw-bold"><?php echo $factory->cfg("site.name"); ?></span> 
	is currently disabled. Please try again later...
</p>