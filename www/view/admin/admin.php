<?php 
defined('_PWE') or die("Limited acces");

use app\Enums\Page;

global $factory, $ztext;

$script = "const PWAd = new PWEAdmin"
  			."(
				{
					url: {
                        mailform: '" . $factory->getUrl(Page::ADMIN_MAIL_FORM) . "',
                        mailto: '" . $factory->getUrl(Page::ADMIN_MAIL_TO) . "'
                    }
 				}
 			);";
    
    // Add script
$factory->addScriptDeclaration($script);
?>

<div class="w-100">

    <div class="container">

        <h1 class="page-title my-3"><?php echo $ztext->get("PAGE_TITLE") ?></h1>

        <div class="row">

            
            <!-- Edit advanced site params (root only) -->
            <?php if ( isset($data->siteParams) ) : ?>
                
            
                    <div class="col-12">
                        <div class="accordion bg-transparent" id="accSiteParams">
                            <div class="accordion-item bg-transparent">
                                <h2 class="accordion-header bg-transparent " id="headingSiteParams">
                                    <button 
                                        class="accordion-button collapsed bg-main-color text-light text-end" 
                                        type="button" 
                                        data-bs-toggle="collapse" 
                                        data-bs-target="#collapseSiteParams" 
                                        aria-expanded="false" 
                                        aria-controls="collapseSiteParams"
                                    >
                                        <span class="text-center w-100 pe-5 text-uppercase fw-lighter">
                                            <?php echo $ztext->get("SITE_PARAMS_EDIT") ?>
                                        </span>
                                    </button>
                                </h2>
                                <div 
                                    id="collapseSiteParams" 
                                    class="accordion-collapse collapse bg-dark" 
                                    aria-labelledby="headingSiteParams" 
                                    data-bs-parent="#accSiteParams"
                                >
                                    <div class="accordion-body">
                                        <?php echo $data->siteParams; ?>
                                    </div>
                                </div>
                            </div>
                        </div>      
                    </div>
                
            <?php endif; ?>

            <!-- Sections -->
            <?php if ( isset($data->sections) ) : ?>
                <div class="col-md-6">
                <h3><?php echo $ztext->get("PAGE_SECTIONS") ?></h3>
                <div class="list-group list-sections">
                    <?php foreach ( $data->sections as $item ) : ?>
                            <a href="<?php echo $item->link ?>" class="list-group-item list-group-item-action d-flex justify-content-between align-items-start no-deco">    
                                <div class="ms-2 me-auto d-inline">
                                    <h5> 
                                        <?php echo $item->icon ?>
                                        <?php echo $item->title ?>
                                    </h5>
                                    <span class="text-light fw-lighter"><?php echo $item->link_title ?></span>
                                </div>
                                <span class="badge <?php echo $item->count ? "bg-primary" : "bg-warning"; ?> rounded-pill"><?php echo $item->count ?></span>
                            </a>
                    <?php endforeach; ?>
                    </div><!-- ul -->
                </div><!-- col -->
            <?php endif; ?>

            <!-- Mail to -->
            <div class="col-md-6">
                <h3><?php echo $ztext->get("PAGE_MAIL_TO") ?></h3>

                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action" data-pw-mailto="all">
                        <div class="ms-2 me-auto">
                            
                                <h5>Global</h5>
                                <span>Envoyer un mail à tout le monde</span>
                           
                        </div>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action" data-pw-mailto="students">
                        <div class="ms-2 me-auto">
                           
                                <h5>Etudiants</h5>
                                <span>Envoyer un mail à tous les étudiants</span>
                           
                        </div>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action" data-pw-mailto="teachers">
                        <div class="ms-2 me-auto">
                           
                                <h5>Enseignants</h5>
                                <span>Envoyer un mail à tous les enseignants</span>
                            
                        </div>
                    </a>                
                </div>
                <div class="alert alert-info mt-3" role="alert">
                    <?php echo $ztext->get("MAIL_TO_INFO_ROLE", $factory->getUrl(Page::ROLES_MANAGE)); ?>
                </div>
            </div>

            <!-- Rules -->
            <?php if (isset($data->rules)):
            $i = 0; ?>

                <div class="col-12">
                    <h3 class="text-start mt-5"><?php echo $ztext->get("PAGE_RULES") ?></h3>
                    <div class="accordion mb-5" id="accordionRules">

                        <?php foreach ($data->rules as $item) : ?>
                            
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading<?php echo $item->context ?>">
                                <button class="accordion-button collapsed" 
                                        type="button" 
                                        data-bs-toggle="collapse" 
                                        data-bs-target="#collapse<?php echo $item->context ?>" 
                                        aria-expanded="false" 
                                        aria-controls="collapse<?php echo $item->context ?>"
                                >
                                    <?php echo $item->icon ?>
                                    <?php echo $item->title ?>
                                </button>
                                </h2>
                                <div id="collapse<?php echo $item->context ?>" 
                                    class="accordion-collapse collapse " 
                                    aria-labelledby="heading<?php echo $item->context ?>" 
                                    data-bs-parent="#accordionRules"
                                >
                                    <div class="accordion-body">
                                        <?php echo $item->tree; ?>
                                    </div>
                                </div>
                            </div>

                        <?php
                        $i++;
                        endforeach; 
                        ?>
                    </div>
                </div>

            <?php endif; ?>

        </div><!-- row -->
    </div>
</div>

<!-- CSRF Token -->
<?php echo $factory->getTokenCSRF() ?>