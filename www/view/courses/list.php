<?php 
use app\Helpers\IconsHelper;

defined('_PWE') or die("Limited acces");

global $factory, $ztext;

// LIST OF COURSES (PUBLIC)
$paginationScript = "null";
if ( isset($data->pagination) ) {
	$paginationScript = $data->pagination->getJSON(
		$factory->getUrl(\app\Enums\Page::COURSES)
	);
}

// Script to execute on page ready.
$orderBy = isset($data->orderBy) ? "orderBy: '" . $data->orderBy->id . "'," : "";
$script = "const PWCourses = new PWECourses"
  		 ."(
 			 {
				" . $orderBy . "
				url: { 
					items: '" . $factory->getUrl(\app\Enums\Page::COURSES, null, true) . "',
				},
				pagination: '" . $paginationScript . "',
				templates: {
					item: 'course.list'
				}
 			 }
 		 );";
  
// Add script
$factory->addScriptDeclaration($script);
?>

<div class="container my-0">

	<!-- Page title -->
	<h1 class="page-title my-5 text-center">
		<?php echo IconsHelper::get("courses"); ?>
		<?php echo $this->txt("PAGE_TITLE")?>
	</h1>
	
	<div class="row justify-content-center">

		<!-- Courses part -->
		<div class="col-12 col-lg-9" data-pw-courses>

			<!-- Alert active filters --><?php 
			if ( isset($data->filtersAlert) ) {
				echo $data->filtersAlert; 
			} ?>

			<!-- Count alert -->
			<p class="alert alert-secondary" data-pw-courses-count="<?php echo $data->itemsCount ?>">
				<?php echo IconsHelper::get("nothing", 0, "me-2") ?>
				<?php echo $ztext->get("NO_COURSES") ?>
			</p>
			
			<!-- List of filtered courses -->
			<div class="card m-0 p-0">
				<div class="card-body">
					<div class="row justify-content-center" data-pw-items >
						<?php 
						foreach ($data->items as $item) {
							echo \app\Helpers\CoursesHelper::makeListItem($item);
						}; ?>
					</div>
				</div>
			</div>
			
			<div class="row my-5 p-0 justify-content-center">

				<!-- Export -->
				<?php if ( isset($data->export) ) { ?>
					<div class="col-auto d-flex flex-column justify-content-center text-right box-export">
						<?php echo $data->export; ?>
					</div>
				<?php } ?>

				<!-- Filters / OrderBy -->
				<?php if ( isset($data->filters) ) : ?>
					<div class="col-auto d-flex flex-column justify-content-center text-right" data-pw-filters >
						<?php echo $data->filters ?>
					</div>
				<?php endif; ?>
				
				<!-- Pagination (courses) -->
				<?php if ( isset($data->pagination) ) : ?>
					<div class="col-auto d-flex flex-column justify-content-center align-items-end p-0 pe-4  m-0">
						<?php
							echo $data->pagination->render
							(
								"PAGINATION_ARIA", 
								$factory->getUrl(\app\Enums\Page::FORMATIONS)
						); ?>
					</div>
				<?php endif; ?>

			</div>

		</div>
		
	</div>


	<!-- Legend -->
	<div class="w-100 text-center mb-5">
		<div class="list-legend m-0 p-0 text-end">
			<div class="list-legend-item">
				<span><?php echo $ztext->get("LEGEND_IS_STUDENT") ?></span>
				<i class="fa-regular fa-circle is-student ms-2"></i>
			</div>
			<div class="list-legend-item">
				<span><?php echo $ztext->get("LEGEND_IS_TEACHER") ?></span>
				<i class="fa-regular fa-circle is-teacher ms-2"></i>
			</div>
		</div>
	</div>

</div><!-- container -->

<!-- CSRF Token -->
<?php echo $factory->getTokenCSRF() ?>