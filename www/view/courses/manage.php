<?php 
use app\Enums\Page;
use app\Helpers\Factory;
use app\Helpers\IconsHelper;

defined('_PWE') or die("Limited acces");

global $factory, $ztext;

// Load some templates
\app\Helpers\TemplatesHelper::load('manage.user');
\app\Helpers\TemplatesHelper::load('course.manage');

// Script to execute on page ready.
$idConfirm = "confirmCoursesAction";
$idContent = "coursesContentBox";
$orderBy = isset($data->orderBy) ? "orderBy: '" . $data->orderBy->id . "'," : "";
$paginationScript = "null";
if ( isset($data->pagination) ) {
	$paginationScript = $data->pagination->getJSON(
		$factory->getUrl(Page::COURSES_MANAGE)
	);
}

$script = "const PWCourses= new PWECourses"
  			."(
				{
					confirm: {id: '" . $idConfirm . "'},
					content: {id: '" . $idContent . "'},
					toolbar: '" . $data->options->id . "',
					" . $orderBy . "
					pagination: " . $paginationScript . ",
					url: {
						items: '" . $factory->getUrl(Page::COURSES_MANAGE, null, true) . "',
						users: '" . $factory->getUrl(Page::COURSES_USERS) . "',
						enrole: '" . $factory->getUrl(Page::COURSES_ENROLE) . "',
						unenrole: '" . $factory->getUrl(Page::COURSES_UNENROLE) . "',
						enroleto: '" . $factory->getUrl(Page::COURSES_ENROLE_TO) . "',
						mailformstudents: '" . $factory->getUrl(Page::COURSES_MAIL_FORM_STUDENTS, null, true) . "',
						mailformteachers: '" . $factory->getUrl(Page::COURSES_MAIL_FORM_TEACHERS, null, true) . "',
						delete: '" . $factory->getUrl(Page::COURSES_DELETE) . "',
						clean: '" . $factory->getUrl(Page::COURSES_CLEAN) . "'
					},
					templates: {
						item: 'course.manage',
						user: 'manage.user'
					}
 				}
 			);";
  
  // Add script
$factory->addScriptDeclaration($script);
$userSections = ["teacher", "student"];


?>

<div class="container" id="<?php echo $idContent ?>">

	<?php 
	// Rules form
	if ( isset($data->rules) ) : ?>
		<div class="page-actions">
		<?php echo $data->rules;?>
	</div>
	<?php endif ?>


	<!-- Page title -->
	<h1 class="page-title mb-5">
		<i class="fa-brands fa-windows"></i>
		<?php echo $this->txt("PAGE_TITLE")?>
	</h1>

	<!-- Toolbar -->
	<?php echo $data->options->render("") ?>

	<!-- Confirm action -->
	<?php echo \app\Helpers\HtmlHelper::confirmBox($idConfirm); ?>

	<div class="row ">
		
		<!-- Courses list -->
		<div class="col col-sm-12 col-md-4 col-md-4" data-pw-courses >
			
			<!-- Alert no recorded course -->
			<p class="alert alert-secondary" data-pw-courses-count="<?php echo count($data->items) ?>">
				<?php echo $ztext->get("NO_COURSES") ?>
			</p>
			
			<!-- Alert active filters --><?php 
			if ( isset($data->filtersAlert) ) {
				echo $data->filtersAlert; 
			} ?>
			
			<!-- Fast search in page list -->
			<div class="fast-search mb-2">
				<input 
					type="search" 
					placeholder="<?php echo $ztext->get("FAST_SEARCH_COURSES") ?>" 
					class="form-control search-input" 
					data-pw-search="courses"
				/>
			</div>
			
			<div class="list-group" data-pw-items>

			<?php foreach ($data->items as $item) {
					echo \app\Helpers\TemplatesHelper::get("course.manage", $item);
				}; ?>
			</div>

			<!-- Pagination (formations) -->
			<?php
				if ( isset($data->pagination) ) {
					echo $data->pagination->render("PAGINATION_ARIA");
				}
			?>
			
			<div class="row cols-lg-2 cols-sm-1 mt-4">
				<!-- Export -->
				<?php if ( isset($data->export) ) { ?>
					<div class="col box-export">
						<?php echo $data->export; ?>
					</div>
				<?php } ?>

				<!-- Filters -->
				<?php if ( isset($data->filters) ) : ?>
					<div class="col" data-pw-filters >
						<?php echo $data->filters ?>
					</div>
				<?php endif; ?>
			</div>


	
		</div>
		
		<!-- Users of selected list -->
		<div class="col col-sm-12 col-md-8 col-lg-8" data-pw-users >

			<!-- Alert courses without teacher -->
			<?php if ( count($data->withoutTeacher) ) : ?>
				<div class="alert alert-warning alert-dismissible fade show" role="alert">
					<h4 class="alert-heading">
						<?php echo IconsHelper::get("warning") ?>
						<?php echo $ztext->get("ALERT_NO_TEACHER_TITLE", count($data->withoutTeacher)) ?>
					</h4>
					<ol class="list-group list-group-numbered">
						<?php foreach ( $data->withoutTeacher as $course ) : ?>
							<li class="list-group-item list-group-item-warning">
								<span class="badge badge-danger"><?php echo $course->uid ?></span>
								<span><?php echo $course->title ?></span>

							</li>
						<?php endforeach; ?>
					</ol>
					<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="<?php echo $ztext->get("CLOSE_ALERT") ?>"></button>
				</div>
			<?php endif; ?>
				
			<!-- Alert students pending -->
			<?php if ( count($data->pending) ) : ?>
				<div class="alert alert-warning alert-dismissible fade show" role="alert">
					<?php echo IconsHelper::get("students") ?>
					<?php echo $ztext->get("USERS_PENDING_ALERT", count($data->pending)) ?>

					<hr />
					<button class="btn btn-warning" type="button" 
							data-bs-toggle="offcanvas" 
							data-bs-target="#offcanvasPendingStudents" 
							aria-controls="offcanvasPendingStudents"
					>
						<?php echo IconsHelper::get("more") ?>
						<?php echo $ztext->get("SHOW_PENDING") ?>
					</button>
					<button type="button" 
							class="btn-close" 
							data-bs-dismiss="alert" 
							aria-label="<?php echo $ztext->get("CLOSE_ALERT") ?>"
					></button>
			</div>
				
			<?php endif; ?>

			<!-- Info no selection -->
			<p class="alert alert-info" role="alert" data-pw-selection-count="0">
				<?php echo $ztext->get("INFO_SELECT_A_COURSE") ?>	
			</p>

				
			<!-- Fast search in page list. -->
			<div class="fast-search">
				<input 
					type="search" 
					placeholder="<?php echo $ztext->get("FAST_SEARCH_PEOPLE") ?>" 
					class="form-control search-input" 
					data-pw-search="people"
					data-pw-people-count="0"
				/>
			</div>
		
			<!-- Users -->
			<?php foreach ( $userSections as $userSection ) : ?>

				<div class="card mb-3">
					
					<div class="card-body">
					
						<h5 class="card-title text-end mb-3">      			
							<?php echo $ztext->get("USERS_" . strtoupper($userSection)) ?>
							<?php echo IconsHelper::get($userSection, 0, "text-main-color"); ?>
						</h5>
						
						<!-- Alerts -->
						<?php $alertType = $userSection=="teacher" ? "danger" : "warning"; ?>
						<p class="alert alert-<?php echo $alertType ?>" data-pw-<?php echo $userSection ?>s-count="1">
							<?php echo $ztext->get("COURSES_NO_" . strtoupper($userSection)) ?>
						</p>
						
						<div class="row row-cols-lg-2 row-cols-1 g-2 justify-content-end" data-pw-<?php echo $userSection ?>s>
						
							
						</div>
					</div>
				</div>
			<?php endforeach; ?>  	

			<!-- Legend -->
			<div class="w-100 text-end">
					<a 
						class="btn btn-light mt-5" 
						data-bs-toggle="collapse" 
						href="#collapseLegend" 
						role="button" 
						aria-expanded="false" 
						aria-controls="collapseLegend"
					>
						<i class="fa-solid fa-circle-question"></i>
						<?php echo $ztext->get("QUESTION_HELP"); ?>
					</a>
			</div>
			<div class="collapse" id="collapseLegend">
							
				<table class="table table-striped table-dark table-hover">
				<?php
				$legends = [
					'add' => 'COURSES_ADD', 
					'delete' => 'COURSES_DELETE', 
					'clean' => 'COURSES_DO_EMPTY', 
					'add-student' => 'COURSES_ADD_STUDENT', 
					'add-teacher' => 'COURSES_ADD_TEACHER', 
					'unenrole' => 'COURSES_REMOVE_USERS', 
					'mailtoteachers' => 'COURSES_MAIL_TO_TEACHERS', 
					'mailtostudents' => 'COURSES_MAIL_TO_STUDENTS'
				];
				foreach ( $legends as $ic => $txt )
				{ ?>
					<tr>
						<td>
							<?php echo IconsHelper::get($ic) ?>
						</td>
						<td>
							<?php echo $ztext->get($txt) ?>
						</td>
					</tr>
				<?php
				} ?>
				</table>
			</div><!-- legend -->
				
		</div>

	</div><!-- row -->

	
	<!-- Pending offcanvas -->
	<div class="offcanvas offcanvas-bottom " 
		data-bs-scroll="true" 
		tabindex="-1" 
		id="offcanvasPendingStudents" 
		aria-labelledby="offcanvasPendingStudentsLabel"
	>
		<div class="offcanvas-header">
			<div>
				<h5 class="offcanvas-title" id="offcanvasPendingStudentsLabel">
					<?php echo $ztext->get("USERS_PENDING_ALERT", count($data->pending)) ?>
				</h5>
				<div class="btn-group mt-3" role="group">
					<button type="button" class="btn btn-primary" data-pw-select-pending>
						<?php echo $ztext->get('BTN_SELECT_PENDING') ?>
					</button>
					<button type="button" class="btn btn-success" data-pw-valid-pending>
						<?php echo $ztext->get('BTN_VALID_PENDING') ?>
					</button>
					<button type="button" class="btn btn-danger" data-pw-cancel-pending>
						<?php echo $ztext->get('BTN_CANCEL_PENDING') ?>
					</button>
				</div>
			</div>
			<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
		</div>
		<div class="offcanvas-body">
			<form action="<?php echo $factory->getUrl(Page::COURSES_VALID_PENDING, null, true) ?>" method="post">
				<!-- Students pending list -->
				<table class="table">
					<tbody>
						<?php 
						foreach ( $data->pending as $user ) : 
							$value = $user->userid . '.' . $user->role; 
							$ident = 'pu' . $user->userid . $user->role; 
						?>
							<tr data-pw-pending="<?php echo $value ?>">
								<td>
									<input type="checkbox" value="<?php echo $value ?>" name="pendingUser[]" id="<?php echo $ident ?>" />
								</td>
								<td>
									<label for="<?php echo $ident ?>">
										<?php echo $user->username; ?>
									</label>
								</td>
								<td>
									<?php echo $user->courseTitle; ?>
								</td>
								<td>
									
								</td>
								<td>
									
								</td>
							</tr>


						<?php endforeach; ?>
					</tbody>
				</table>
			</form>
		</div>
	</div>	

</div><!-- container -->



<!-- CSRF Token -->
<?php echo $factory->getTokenCSRF() ?>