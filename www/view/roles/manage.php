<?php 
use app\Helpers\IconsHelper;

defined('_PWE') or die("Limited acces");

global $factory, $ztext;

// Load some templates
\app\Helpers\TemplatesHelper::load("role.manage", new \app\Objects\Role());
\app\Helpers\TemplatesHelper::load("manage.user");

// Script file to load
$factory->addScriptFile("assets/js/users.list.js");

// Script to execute on page ready.
$idContent = "contentPart";
$idConfirm = "confirmRolesAction";

// Script pagination
$paginationScript = "null";
if ( isset($data->pagination) ) {
	$paginationScript = $data->pagination->getJSON();
}
// Order by
$orderBy = isset($data->orderBy) ? "orderBy: '" . $data->orderBy->id . "'," : "";

$script = "const PWRoles = new PWERoles"
    ."(
 			{
				content: {id: '" . $idContent . "'},
				confirm: {id: '" . $idConfirm . "'},
				toolbar: '" . $data->options->id . "',
				pagination: '" . $paginationScript . "',
				" . $orderBy . "
 				url: {
                    enroleto: '" . $factory->getUrl(\app\Enums\Page::ROLES_ENROLE_TO) . "',
                    enrole: '" . $factory->getUrl(\app\Enums\Page::ROLES_ENROLE) . "',
                    items: '" . $factory->getUrl(\app\Enums\Page::ROLES_MANAGE) . "',
					mailtorole: '" . $factory->getUrl(\app\Enums\Page::ROLES_MAIL_TO, null, true) . "',
					mailformroles: '" . $factory->getUrl(\app\Enums\Page::ROLES_MAIL_FORM_ROLES, null, true) . "',
					unenrole: '" . $factory->getUrl(\app\Enums\Page::ROLES_UNENROLE) . "',
                    users: '" . $factory->getUrl(\app\Enums\Page::ROLES_ROLE_USERS) . "',
                    clean: '" . $factory->getUrl(\app\Enums\Page::ROLES_ROLE_DO_EMPTY) . "',
                    delete: '" . $factory->getUrl(\app\Enums\Page::ROLES_ROLE_DELETE) . "'
                },
				templates: {
					item: 'role.manage',
					user: 'manage.user'
				}
 			}
 		);";
    
// Add script
$factory->addScriptDeclaration($script);
?>

<div id="<?php echo $idContent ?>" class="container mb-5">

	<!-- Rules form -->
	<?php if ( isset($data->rules) ) { 
		echo $data->rules; 
	} ?>
	
	<!-- page title -->
	<h1 class="page-title my-5 text-center">
		<?php echo IconsHelper::get("roles"); ?>
		<?php echo $this->txt("PAGE_TITLE")?>
	</h1>


	<div class="row">
		
		<div class="col col-sm-12 col-md-4 "  >
				
			<!-- Alert active filters --><?php 
			if ( isset($data->filtersAlert) ) {
				echo $data->filtersAlert; 
			} ?>

			<!-- Roles list -->
			<div data-pw-items ><?php
				foreach ( $data->items as $item ) {
					echo \app\Helpers\TemplatesHelper::get("role.manage", $item);
				} ?>
			</div>
			
			<!-- Pagination -->
			<?php
				if ( isset($data->pagination) ) {
					echo $data->pagination->render("PAGINATION_ARIA");
				}
			?>
			
			<div class="row cols-lg-2 cols-sm-1 mt-4">
				<!-- Export -->
				<?php if ( isset($data->export) ) { ?>
					<div class="col box-export">
						<?php echo $data->export; ?>
					</div>
				<?php } ?>

				<!-- Filters -->
				<?php if ( isset($data->filters) ) : ?>
					<div class="col" data-pw-filters >
						<?php echo $data->filters ?>
					</div>
				<?php endif; ?>
			</div>

		</div>
		
		<!-- Users related to the selected list -->
		<div class="col col-sm-12 col-md-8" data-pw-users-part>
							
					
			<!-- Toolbar options -->
			<?php echo $data->options->render() ?>

			<!-- Actions confirmation -->
			<?php echo \app\Helpers\HtmlHelper::confirmBox($idConfirm); ?>

		
			<!-- Users list -->
			<div class="card">
				<div class="card-body">
					<!-- Alert no users -->
					<p class="alert alert-secondary" data-pw-users-count="0">
						<?php echo IconsHelper::get("info") ?>
						<?php echo $ztext->get("ROLES_NO_USER") ?>
					</p>
					<div class="row row-cols-lg-2 row-cols-1 g-2 justify-content-end" data-pw-users></div>		
				</div>		
			</div>		
			
			<a 
				class="btn btn-light mt-5" 
				data-bs-toggle="collapse" 
				href="#pageHelp" 
				role="button" 
				aria-expanded="false" 
				aria-controls="pageHelp"
			>
				<?php echo IconsHelper::get("help", 0, 'text-main-color') ?>
				<?php echo $ztext->get("QUESTION_HELP"); ?>
			</a>

			<!-- Help -->
			<div class="row collapse" id="pageHelp">

				<div class="col-12" >
					<table class="table table-striped table-hover table-dark status-legend text-center my-5">
					<?php 
						// Table icons
						foreach ( ["create", "delete", "clean", "mail", "enrole", "unenrole", "edit", "notenrolable"] as $ic )
						{ ?>
							<tr>
								<td class="text-center"><?php echo IconsHelper::get($ic, 0, 'fs-5'); ?></td>
								<td class="text-start fw-bold">
									<?php echo $ztext->get("LEGEND_TB_" . $ic); ?><br />
								</td>
								<td class="text-end">
									<?php echo $ztext->get("LEGEND_TB_" . $ic . "_DESC"); ?>
								</td>
							</tr><?php
						} ?>
					</table>
				</div>
			</div>
		</div>
	</div>

	

	

	<!-- CSRF Token -->
	<?php echo $factory->getTokenCSRF() ?>
	
</div>


