<?php 
use app\Helpers\IconsHelper;
defined('_PWE') or die("Limited acces");

global $factory, $ztext;

// Page properties.
$IDForm = "accountForm";

// Script to execute on page ready.
$script = "const PWAccount = new UserAccount"
        ."(
 			{
 				form: '" . $IDForm . "'
 			}
 		);";
    
// Add script
$factory->addScriptDeclaration($script);
?>

<div class="container" >

	<!-- Page title -->
	<h1 class="page-title my-5 text-center">
		<?php echo IconsHelper::get("user-area", 0, "text-main-color"); ?>
		<?php echo $this->txt("PAGE_TITLE")?>
	</h1>

	<div class="row">
		<div class="col-md-4">

			<div id="acc-user" class="list-group mb-5">
				<a class="list-group-item list-group-item-action" href="#part-formations"><?php echo $ztext->get("LINK_FORMATIONS") ?></a>
				<a class="list-group-item list-group-item-action" href="#part-student"><?php echo $ztext->get("LINK_STUDENT") ?></a>
				<a class="list-group-item list-group-item-action" href="#part-teacher"><?php echo $ztext->get("LINK_TEACHER") ?></a>
				<a class="list-group-item list-group-item-action" href="#part-account"><?php echo $ztext->get("LINK_ACCOUNT") ?></a>
			</div>
				
			<!-- Export profile -->
			<?php echo $data->export ?>
		</div>
		<div class="col-md-8">
			<div class="w-100" >
				
				<!-- Formations -->
				<div class="card" id="part-formations">
					<div class="card-header">
						<h4>
							<?php echo IconsHelper::get("formations", 0, "text-main-color") ?>
							<?php echo $ztext->get("TITLE_FORMATIONS") ?></h4>
					</div>
					<div class="card-body">
						<?php if ( !count($data->formations) ) : ?>
							<p class="alert alert-secondary"><?php echo $ztext->get("NOT_FOLLOWING_FORMATIONS") ?></p>
						<?php else :  ?>
							<ul class="list-group">
								<?php foreach ( $data->formations as $item ) : 
									$title = $item->pending ? 'title="' . $ztext->getTitle("FORMATION_PENDING") . '"' : '';	
								?>
									<li class="list-group-item item-pending-<?php echo $item->pending ?>" <?php echo $title ?>>
										<a href="<?php echo $item->link ?>" title="<?php echo $item->link_title ?>">
											<?php echo $item->title ?>
										</a>

										<?php echo IconsHelper::get("pending", 0, "pending-" . $item->pending) ?>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
				<hr />

			<!-- Is student -->
			<div class="card" id="part-student">
				<div class="card-header">
					<h4>
						<?php echo IconsHelper::get("students", 0, "text-main-color") ?>
						<?php echo $ztext->get("TITLE_STUDENT") ?>
					</h4>
				</div>
				<div class="card-body">
					<?php if ( !count($data->student) ) : ?>
							<p class="alert alert-secondary"><?php echo $ztext->get("NOT_STUDENT") ?></p>
					<?php else :  ?>
						<ul class="list-group">
							<?php foreach ( $data->student as $item ) : 
								$title = $item->pending ? 'title="' . $ztext->getTitle("FORMATION_PENDING") . '"' : '';	
							?>
								<li class="list-group-item item-pending-<?php echo $item->pending ?>" <?php echo $title ?> >
									<a href="<?php echo $item->link ?>" title="<?php echo $item->link_title ?>">
										<?php echo $item->title ?>
									</a>
									<?php echo IconsHelper::get("pending", 0, "pending-" . $item->pending) ?>
								</li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
			<hr />   
			
			<!-- Is teacher -->
			<div class="card" id="part-teacher">
				<div class="card-header">
					<h4>
						<?php echo IconsHelper::get("teachers", 0, "text-main-color") ?>
						<?php echo $ztext->get("TITLE_TEACHER") ?>
					</h4>
				</div>
				<div class="card-body">
					<?php if ( !count($data->teacher) ) : ?>
							<p class="alert alert-secondary"><?php echo $ztext->get("NOT_TEACHER") ?></p>
					<?php else :  ?>
						<ul class="list-group">
							<?php foreach ( $data->teacher as $item ) : ?>
								<li class="list-group-item">
									<a href="<?php echo $item->link ?>" title="<?php echo $item->link_title ?>">
										<?php echo $item->title ?>
									</a>
								</li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>

			<hr />

			<!-- Form - Edit account -->
			<div class="card" id="part-account">
				<div class="card-header">
					<h4><?php echo $ztext->get("LINK_ACCOUNT") ?></h4>
				</div>
				<div class="card-body">

					<!-- Msg password secure -->
					<p class="alert alert-warning"><?php echo $this->txt("PAGE_DESC"); ?>

					<form
						id="<?php echo $IDForm ?>" 
						name="<?php echo $IDForm ?>" 
						method="post"
						action="<?php echo $factory->getUrl(\app\Enums\Page::USER_ACCOUNT) ?>"
						enctype="multipart/form-data"
					><?php 
					
						foreach ( $data->fields as $fieldsetName => $fieldset )
						{  
							// Social data
							if ( $fieldsetName=="social" ) : 
								$style = ""; 
								if ( isset($data->user->attribs->social->avatar) && !empty($data->user->attribs->social->avatar) ) {
									$style = "background-image: url('" . $data->user->attribs->social->avatar . "'); ";
								} ?>
									<div class="social-box">
										<div class="avatar-box ratio ratio-1x1 shadow" style="<?php echo $style ?>">
											
										</div>
										<div class="social-fields">
											<?php 
											foreach ( $fieldset as $field )
											{ ?>
												<div class="pw-form-field">
													<?php echo $field; ?>
												</div><?php
											}
											?>
										</div>    	   		
									</div><?php
									
								// Common data.
								else : ?>
									<div class="pw-form-fieldset fieldset-<?php echo $fieldsetName ?>"><?php 
								
										foreach ( $fieldset as $field ) 
										{ ?>
											<div class="pw-form-field">
												<?php echo $field; ?>
											</div><?php
										} ?>
									
									</div><?php 
							endif;
						} ?>
						
						<!-- Submit form -->
						<div class="w-100 text-center">
							<input 
								type="submit" 
								value="<?php echo $this->txt("SUBMIT_ACCOUNT")?>"
								class="btn btn-success submit-form my-4 mx-auto"
							/>
						</div>
						
						<!-- CSRF Token -->
						<?php echo $factory->getTokenCSRF() ?>
					</form>
				</div>
			</div>

			</div>
		</div>
	</div>
</div><!-- Container -->






































