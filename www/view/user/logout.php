<?php
use app\Helpers\IconsHelper;
defined('_PWE') or die("Limited acces");

global $factory;

// Page properties.
$IDForm = "logoutForm";

// Script to execute on page ready.
$script = "const PWLogin = new UserLogout"
            ."(
     			{
     				form: '" . $IDForm . "'
     			}
     		);";
    
// Add script
$factory->addScriptDeclaration($script);
?>
<div class="container h-container d-flex flex-column justify-content-center">
	<div class="row justify-content-center">
		<div class="col-lg-6 col-md-8 col-sm-12">
			<form
				id="<?php echo $IDForm ?>" 
				name="<?php echo $IDForm ?>" 
				method="post"
				action="<?php echo $factory->getUrl(\app\Enums\Page::USER_LOGOUT) ?>"
			>
				<p class="alert alert-warning" role="alert">
					<?php echo IconsHelper::get("logout", 0, "me-1 text-danger") ?>
					<?php echo $this->txt("LOGOUT_ASK"); ?>
				</p>

				<div class="w-100 text-center">
					<div class="btn-group">
						<button class="btn btn-success" type="submit">
							<?php echo $this->txt("P_YES"); ?>
						</button>
						<a class="btn btn-danger" href="index.php" >
							<?php echo $this->txt("P_NO"); ?>
						</a>
					</div>
				</div>

				<!-- CSRF Token -->
				<?php echo $factory->getTokenCSRF() ?>
			</form>
		</div>
	</div>
</div>