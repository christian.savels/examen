<?php 
use app\Helpers\IconsHelper;
defined('_PWE') or die("Limited acces");

use \app\Enums\Page as Pg;

global $factory;

// Load menu language file.
$factory->getTxt()->loadFiles(["menu"]);

// Manager ?
$manageLinks = \app\Helpers\MenuHelper::getUserManageLinks("dropdown-menu", );
?>

<nav class="navbar sticky-top navbar-expand-lg bg-body-tertiary" >
  <div class="container-fluid">
    <a class="navbar-brand" href="#">
    	<img src="assets/img/LOGO_EAFC.png" class="logo" />
	</a>
	<button class="navbar-toggler" type="button" 
			data-bs-toggle="collapse" 
			data-bs-target="#navbarMenu"
			aria-controls="navbarMenu" 
			aria-expanded="false" 
			aria-label="Toggle navigation"
	 >
      <span class="navbar-toggler-icon"></span>
    </button>
	<div class="collapse navbar-collapse" id="navbarMenu">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="index.php">Home</a>
        </li>
		
		<li class="nav-item">
			<a class="nav-link" href="<?php echo $factory->getUrl(Pg::FORMATIONS) ?>" >
				<?php echo $ztext->get("MENU_FORMATIONS"); ?>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="<?php echo $factory->getUrl(Pg::COURSES) ?>" >
				<?php echo $ztext->get("MENU_COURSES"); ?>
			</a>
		</li>
      </ul>

	  <!-- Right side -->
	  <ul class="navbar-nav ms-auto mb-2 me-5 mb-lg-0 d-flex align-items-end">

		<!-- Language -->
		<li class="nav-item dropdown nav-item-lang ms-auto">
			<?php echo $factory->getTxt()->getLangChooser(); ?>
		</li>

		<?php 		
		// Manage links
		if ( !empty($manageLinks) ) 
		{ ?>
			<li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" role="button" 
				data-bs-toggle="dropdown" aria-expanded="false">
				<?php echo $ztext->get("MENU_MANAGE"); ?>
            </a>
            <ul class="dropdown-menu"><?php
				foreach ( $manageLinks as $t => $link ) 
				{ ?>
					<li>
						<a class="dropdown-item" href="<?php echo $link ?>">
							<?php echo $t; ?>
						</a>
					</li><?php
				} ?>
			</ul>
		</li><?php
		} ?>

		<!-- Around user -->
		<li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" role="button" 
				data-bs-toggle="dropdown" aria-expanded="false">
				<?php echo $ztext->get("MENU_ME"); ?>
            </a>
            <ul class="dropdown-menu">

				<!-- Member links -->
				<?php if ( !$factory->getUser()->isGuest() ) : ?>
					<li>
						<a class="dropdown-item" href="<?php echo $factory->getUrl(Pg::USER_ACCOUNT) ?>">
							<?php echo $ztext->get("MENU_ACCOUNT"); ?>
						</a>
					</li>
					<li><hr class="dropdown-divider"></li>
				<?php endif; ?>
				
				<!-- Login/Logout -->
				<?php
					$pg = $factory->getUser()->isGuest() ? Pg::USER_LOGIN : Pg::USER_LOGOUT;
					$txt = $factory->getUser()->isGuest() ? "MENU_LOGIN" : "MENU_LOGOUT";
				?>
				<li>
					<a class="dropdown-item" href="<?php echo $factory->getUrl($pg) ?>" >
						<?php echo $ztext->get($txt); ?>
					</a>
				</li>

				<!-- Signup link -->
				<?php if ( $factory->getUser()->isGuest() ) : ?>
					<li>
						<a class="dropdown-item" href="<?php echo $factory->getUrl(Pg::USER_SIGNUP) ?>">
							<?php echo $ztext->get("MENU_SIGNUP"); ?>
						</a>
					</li>					
				<?php endif; ?>
				

            </ul>
          </li>
		  <?php
		  // Admin link (only for root users)
				if ( $factory->getUser()->isRoot() ) 
				{ ?>
					<li class="nav-item">
						<a class="nav-link"
							href="<?php echo $factory->getUrl(Pg::ADMIN) ?>" 
							title="<?php echo $ztext->getTitle("MENU_ADMIN"); ?>"
						>
							<?php echo IconsHelper::get("params", 0, 'text-main-color') ?>
						</a>
					</li><?php
				} ?>
      </ul>
     
    </div>
  </div>
</nav>