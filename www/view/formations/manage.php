<?php 
use app\Enums\Page;
use app\Helpers\IconsHelper;

defined('_PWE') or die("Limited acces");

global $factory, $ztext;

// Load some templates
\app\Helpers\TemplatesHelper::load('manage.user');
\app\Helpers\TemplatesHelper::load('formation.manage');

// Script to execute on page ready.
$idContent = "contentPart";
$idConfirm = "confirmFormationsAction";
$idToolBar = "tbFormationsOpt";
$orderBy = isset($data->orderBy) ? "orderBy: '" . $data->orderBy->id . "'," : "";
$paginationScript = "null";
if ( isset($data->pagination) ) {
	$paginationScript = $data->pagination->getJSON(
		$factory->getUrl(Page::FORMATIONS_MANAGE)
	);
}

$script = "const PWFormations = new PWEForm"
  			."(
				{
					content: {id: '" . $idContent . "'},
					confirm: {id: '" . $idConfirm . "'},
					toolbar: '" . $data->options->id . "',
					" . $orderBy . "
					pagination: '" . $paginationScript . "',
					url: {
						items: '" . $factory->getUrl(Page::FORMATIONS_MANAGE, null, true) . "',
						courses: '" . $factory->getUrl(Page::FORMATION_COURSES) . "',
						enrole: '" . $factory->getUrl(Page::FORMATIONS_ENROLE) . "',
						unenrole: '" . $factory->getUrl(Page::FORMATIONS_UNENROLE) . "',
						enroleto: '" . $factory->getUrl(Page::FORMATIONS_ENROLE_TO) . "',
						mailformstudents: '" . $factory->getUrl(Page::FORMATIONS_MAIL_FORM_STUDENTS, null, true) . "',
						mailformteachers: '" . $factory->getUrl(Page::FORMATIONS_MAIL_FORM_TEACHERS, null, true) . "',
						validatePending: '" . $factory->getUrl(Page::FORMATIONS_VALIDATE_PENDING, null, true) . "',
						cancelPending: '" . $factory->getUrl(Page::FORMATIONS_CANCEL_PENDING, null, true) . "',

						enrolecourse: '" . $factory->getUrl(Page::FORMATIONS_ENROLE_COURSE) . "',
						unenrolecourse: '" . $factory->getUrl(Page::FORMATIONS_UNENROLE_COURSE) . "',
						enrolecourseto: '" . $factory->getUrl(Page::FORMATIONS_ENROLE_COURSE_TO) . "',
						
						delete: '" . $factory->getUrl(Page::FORMATIONS_DELETE) . "',
						clean: '" . $factory->getUrl(Page::FORMATIONS_CLEAN) . "'
					},
					templates: {
						item: 'formation.manage',
						user: 'manage.user'
					}
 				}
 			);";
    
    // Add script
$factory->addScriptDeclaration($script);

?>

<div class="container mb-5">

	<!-- Rules form -->
	<?php if ( isset($data->rules) ) : ?>
		<div class="page-actions">
			<?php echo $data->rules;?>
		</div>
	<?php endif ?>

	<!-- Page title -->
	<h1 class="page-title my-3">
		<?php echo IconsHelper::get("formations"); ?>
		<?php echo $this->txt("PAGE_TITLE")?>
	</h1>

	<!-- Toolbar -->
	<?php echo $data->options->render() ?>

	<!-- Actions confirmation -->
	<?php echo \app\Helpers\HtmlHelper::confirmBox($idConfirm); ?>

	<div class="row mt-5">

		<!-- Formations -->
		<div class="col col-sm-12 col-md-4 col-md-4" data-pw-formations >
		
			<!-- Alert active filters --><?php 
			if ( isset($data->filtersAlert) ) {
				echo $data->filtersAlert; 
			} ?>
			
			<!-- Alert no result -->
			<p class="alert alert-secondary" role="alert" data-pw-items-count="<?php echo count($data->items) ?>">
				<?php echo IconsHelper::get("nothing") ?>
				<?php echo $ztext->get("NO_RESULT") ?>
			</p>
			
			<!-- Formations list -->
			<div class="list-group " data-pw-items>
				<?php foreach ($data->items as $item) {
					echo \app\Helpers\TemplatesHelper::get("formation.manage", $item);
				}; ?>
			</div>

			<!-- Pagination (formations) -->
			<?php
				if ( isset($data->pagination) ) {
					echo $data->pagination->render("PAGINATION_ARIA");
				}
			?>
			
			<div class="row cols-lg-2 cols-sm-1 mt-4">
				<!-- Export -->
				<?php if ( isset($data->export) ) { ?>
					<div class="col box-export">
						<?php echo $data->export; ?>
					</div>
				<?php } ?>

				<!-- Filters -->
				<?php if ( isset($data->filters) ) : ?>
					<div class="col" data-pw-filters >
						<?php echo $data->filters ?>
					</div>
				<?php endif; ?>
			</div>

		</div>

		<!-- People & Courses related to the selected formation -->
		<div class="col col-sm-12 col-md-8 col-lg-8" >

			<!-- Alert students pending -->
			<?php if ( count($data->pending) ) : ?>
				<div class="alert alert-warning alert-dismissible fade show" role="alert">
					<?php echo IconsHelper::get("students") ?>
					<?php echo $ztext->get("USERS_PENDING_ALERT", count($data->pending)) ?>

					<hr />
					<button class="btn btn-warning" type="button" 
							data-bs-toggle="offcanvas" 
							data-bs-target="#offcanvasPendingStudents" 
							aria-controls="offcanvasPendingStudents"
					>
						<?php echo IconsHelper::get("more") ?>
						<?php echo $ztext->get("SHOW_PENDING") ?>
					</button>
					<button type="button" 
							class="btn-close" 
							data-bs-dismiss="alert" 
							aria-label="<?php echo $ztext->get("CLOSE_ALERT") ?>"
					></button>
			</div>
				
			<?php endif; ?>

			<!-- Alerts -->
			<p class="alert alert-danger" data-pw-courses-count="1">
				<?php echo IconsHelper::get("courses") ?>
				<?php echo $ztext->get("NO_COURSE_ASSOCIATION"); ?>
			</p>
			<p class="alert alert-danger" data-pw-users-pending-count="0">
				<?php echo IconsHelper::get("students") ?>
				<?php echo $ztext->get("INFO_USERS_PENDING") ?>
			</p>
			<p class="alert alert-warning" data-pw-teachers-count="1">
			<?php echo IconsHelper::get("teachers") ?>
				<?php echo $ztext->get("INFO_NO_TEACHER") ?>
			</p>
			<p class="alert alert-warning" data-pw-students-count="1">
			<?php echo IconsHelper::get("students") ?>
				<?php echo $ztext->get("INFO_NO_STUDENT") ?>
			</p>

			<!-- Data accordion -->
			<div class="accordion accordion-flush" id="accFormation">

				<!-- Teachers -->
				<div class="accordion-item">
					<h5 class="accordion-header" id="accPartTeachers">
						<button class="accordion-button collapsed " 
							type="button" 
							data-bs-toggle="collapse" 
							data-bs-target="#collapse1" 
							aria-controls="collapse1"
							aria-expanded="false" 
						>
							<?php echo IconsHelper::get("teachers", 0, 'text-main-color') ?>
							<?php echo $ztext->get("FORMATION_TEACHERS") ?>
							(<span data-pw-tab-teachers-count="0" >0</span>)
						</button>
					</h5>
					<div 
						id="collapse1" 
						class="accordion-collapse collapse" 
						aria-labelledby="accPartTeachers" 
						data-bs-parent="#accFormation"
					>
						<div class="accordion-body">
							
							<!-- People list -->
							<div class="row row-cols-lg-2 row-cols-1 g-2 justify-content-end" data-pw-teachers >
								

							</div>
						</div>
					</div>
				</div>

				<!-- Students -->
				<div class="accordion-item">
					<h5 class="accordion-header" id="accPartStudents">
						<button class="accordion-button collapsed" 
						type="button" 
						data-bs-toggle="collapse" 
						data-bs-target="#collapse2" 
						aria-controls="collapse2"
						aria-expanded="false" 
					>
							<?php echo IconsHelper::get("students", 0, ' text-main-color') ?>
							<?php echo $ztext->get("FORMATION_STUDENTS") ?>
							(<span data-pw-tab-students-count="0" >0</span>)
						</button>
					</h5>
					<div 
						id="collapse2" 
						class="accordion-collapse collapse" 
						aria-labelledby="accPartStudents" 
						data-bs-parent="#accFormation"
					>
						<div class="accordion-body">							
							<!-- People list. -->
							<div class="row row-cols-lg-2 row-cols-1 g-2 justify-content-end" data-pw-students ></div>
						</div>
					</div>
				</div>

			</div><!-- accordion -->	
			
			
			<!-- Courses related to the selected formation. -->
			<div class="card mt-3">
				<div class="card-header">
				<?php echo IconsHelper::get("courses", 0, "mx-2 text-main-color") ?>
						<?php echo $ztext->get("FORMATION_COURSES") ?>
						(<span data-pw-tab-courses-count="0" >0</span>)
				</div>
				<div class="card-body">
					<table class="table table-striped table-hover"  >
						<thead>
							<tr>
								<th><?php echo $ztext->get("TH_TITLE") ?></th>
								<th><?php echo $ztext->get("TH_UID") ?></th>
								<th class="text-end"><?php echo $ztext->get("TH_ID") ?></th>
							</tr>
						</thead>
						
						<tbody data-pw-courses>
							<!-- A course -->
							<tr data-pw-course="0" data-pw-selected="0" >
								<td data-pw-title></td>
								<td >
									<span class="badge bg-main-color" data-pw-uid></span>
								</td>
								<td class="text-end">
									#<span data-pw-id></span>
								</td>
							</tr>
						</tbody>
					</table><!-- data-pw-courses -->
				</div>
			</div>

			<!-- Legend -->
			<div class="w-100 text-end">
				<a 
					class="btn btn-light mt-5" 
					data-bs-toggle="collapse" 
					href="#collapseLegend" 
					role="button" 
					aria-expanded="false" 
					aria-controls="collapseLegend"
				>
					<i class="fa-solid fa-circle-question"></i>
					<?php echo $ztext->get("QUESTION_HELP"); ?>
				</a>

				<div class="collapse" id="collapseLegend">
					<ul class="list-group list-group-horizontal d-flex flex-wrap status-legend text-center">
						<?php  
						$ics = [ 
							"published", "unpublished", "create", "delete", "cleancourses", "cleanstudents", 
							"enrolecourse", "unenrolecourse", "mailtoteachers", 
							"mailtostudents", "add-student", "unenrolestudent"
						];

						foreach ( $ics as $ic )
						{ ?>
							<li class="list-group-item flex-fill">
								<div class="legend-icon-group">
									<?php echo IconsHelper::get($ic); ?>
								</div>
								<?php echo $ztext->get("LEGEND_TB_" . $ic); ?>
							</li><?php
						} ?>
					</ul>
				</div><!-- collapse legend -->
			</div><!-- legend main -->
			
		</div><!-- col -->
	</div>

	<!-- Pending offcanvas -->
	<div class="offcanvas offcanvas-bottom " 
		data-bs-scroll="true" 
		tabindex="-1" 
		id="offcanvasPendingStudents" 
		aria-labelledby="offcanvasPendingStudentsLabel"
	>
		<div class="offcanvas-header">
			<div>
				<h5 class="offcanvas-title" id="offcanvasPendingStudentsLabel">
					<?php echo $ztext->get("USERS_PENDING_ALERT", count($data->pending)) ?>
				</h5>
				<div class="btn-group mt-3" role="group">
					<button type="button" class="btn btn-primary" data-pw-select-pending>
						<?php echo $ztext->get('BTN_SELECT_PENDING') ?>
					</button>
					<button type="button" class="btn btn-success" data-pw-valid-pending>
						<?php echo $ztext->get('BTN_VALID_PENDING') ?>
					</button>
					<button type="button" class="btn btn-danger" data-pw-cancel-pending>
						<?php echo $ztext->get('BTN_CANCEL_PENDING') ?>
					</button>
				</div>
			</div>
			<button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
		</div>
		<div class="offcanvas-body">
			<form action="<?php echo $factory->getUrl(Page::FORMATIONS_VALIDATE_PENDING, null, true) ?>" method="post">
				<!-- Students pending list -->
				<table class="table">
					<tbody>
						<?php 
						foreach ( $data->pending as $user ) : 
							$value = $user->userid . '.' . $user->fid; 
							$ident = 'pu' . $user->userid . $user->fid; 
						?>
							<tr data-pw-pending="<?php echo $value ?>">
								<td>
									<input type="checkbox" value="<?php echo $value ?>" name="pendingUser[]" id="<?php echo $ident ?>" />
								</td>
								<td>
									<label for="<?php echo $ident ?>">
										<?php echo $user->username; ?>
									</label>
								</td>
								<td>
									<?php echo $user->uid; ?>
								</td>
								<td>
									
								</td>
								<td>
									
								</td>
							</tr>


						<?php endforeach; ?>
					</tbody>
				</table>
			</form>
		</div>
	</div>

	<!-- CSRF Token -->
	<?php echo $factory->getTokenCSRF() ?>

</div><!-- container -->
