<?php 
// Public list of formations.

use app\Helpers\Factory;
use app\Helpers\IconsHelper;

defined('_PWE') or die("Limited acces");

global $factory, $ztext;

// Script to execute on page ready.
$idConfirm = "confirmFormationAction";
$paginationScript = "null";
if ( isset($data->pagination) ) {
	$paginationScript = $data->pagination->getJSON(
		$factory->getUrl(\app\Enums\Page::FORMATIONS)
	);
}

// Script to execute on page ready.
$orderBy = isset($data->orderBy) ? "orderBy: '" . $data->orderBy->id . "'," : "";
$script = "const PWFO = new PWEFormations"
  		 ."(
 			 {
				" . $orderBy . "
				url: { 
					items: '" . $factory->getUrl(\app\Enums\Page::FORMATIONS, null, true) . "',
				},
				pagination: '" . $paginationScript . "',
				templates: {
					item: 'formation.list'
				}
 			 }
 		 );";
  
// Add script
$factory->addScriptDeclaration($script);
?>

<div class="container mb-5">

	<!-- Page title -->
	<h1 class="my-5 page-title text-center">
		<?php echo IconsHelper::get("formations"); ?>
		<?php echo $this->txt("PAGE_TITLE")?>
	</h1>

	<div class="row justify-content-center">

		<!-- Formations -->
		<div class="col-12 col-lg-9" data-pw-formations >

			<!-- Alert active filters --><?php 
			if ( isset($data->filtersAlert) ) {
				echo $data->filtersAlert; 
			} ?>

			<!-- Count alert -->
			<p class="alert alert-secondary" data-pw-items-count="<?php echo $data->itemsCount ?>">
				<?php echo IconsHelper::get("nothing", 0, "me-2") ?>
				<?php echo $ztext->get("NO_FORMATIONS") ?>
			</p>
			
			<!-- Items -->
			<div class="card m-0 p-0">
				<div class="card-body">
					<div class="row g-3" data-pw-items >
						<?php foreach ($data->items as $item) {
							echo \app\Helpers\TemplatesHelper::get("formation.list", $item);
						}; ?>
					</div>
				</div>
			</div>
		</div><!-- col -->
	</div><!-- row -->

	<div class="row my-5 p-0 justify-content-center">

			<!-- Export -->
			<?php if ( isset($data->export) ) { ?>
				<div class="col-auto d-flex flex-column justify-content-center text-right box-export">
					<?php echo $data->export; ?>
				</div>
			<?php } ?>

			<!-- Filters / OrderBy -->
			<?php if ( isset($data->filters) ) : ?>
				<div class="col-auto d-flex flex-column justify-content-center text-right" data-pw-filters >
					<?php echo $data->filters ?>
				</div>
			<?php endif; ?>
			
			<!-- Pagination (courses) -->
			<?php if ( isset($data->pagination) ) : ?>
				<div class="col-auto d-flex flex-column justify-content-center align-items-end p-0 pe-4  m-0">
					<?php
						echo $data->pagination->render
						(
							"PAGINATION_ARIA", 
							$factory->getUrl(\app\Enums\Page::FORMATIONS)
					); ?>
				</div>
			<?php endif; ?>

		</div>

	

</div><!-- Container -->


	<!-- CSRF Token -->
	<?php echo $factory->getTokenCSRF() ?>