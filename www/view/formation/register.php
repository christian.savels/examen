<?php 
use app\Helpers\Factory;

defined('_PWE') or die("Limited acces");

global $factory, $ztext;

$idForm = "regForm";

// Add onReady script.
$script = "const PWReg = new PWERegisterTo('" . $idForm . "');";
$factory->addScriptDeclaration($script);
?>

<form
	 id="<?php echo $idForm ?>" 
	 name="<?php echo $idForm ?>" 
	 method="post"
	 action="<?php echo $factory->getUrl(\app\Enums\Page::FORMATIONS_REGISTER, $data->formation->id) ?>"
>
	<input type="hidden" name="formation" value="<?php echo $data->formation->id ?>" />

	<div class="container">

		<h1 class="page-title mt-5"><?php echo $ztext->get("PAGE_TITLE") ?></h1>        
		<h2 class="first-letter"><?php echo $data->formation->title ?></h2>        

		<!-- Courses -->
		<div class="row row-cols-sm-1 row-cols-lg-2 my-5">

			<!-- Not registered yet -->
			<div class="col">
				<div class="card h-100 shadow">
					<div class="card-header">
						<h4 class="m-0 first-letter"><?php echo $ztext->get("TITLE_NOTYET") ?></h4>
					</div>
					<div class="card-body">
						<!-- Alert no courses for this user -->
						<?php if ( !count($data->courses->notyet) ) : ?>
							<p class="alert alert-danger"><?php echo $ztext->get("NO_COURSES_FOR_YOU"); ?></p>
						<?php endif; ?> 
						<!-- List -->
						<?php echo \app\Helpers\HtmlHelper::makeList(
							$data->courses->notyet, "list-group ul-notyet", "list-group-item list-group-item-success"
						); ?>
					</div>
				</div>
			</div>
						
			<!-- Already registered -->
			<div class="col">
				<div class="card h-100 shadow-sm">
					<div class="card-header">
						<h4 class="m-0 first-letter"><?php echo $ztext->get("TITLE_ALREADY") ?></h4>
					</div>
					<div class="card-body">
						<!-- Alert no courses the user is already following -->
						<?php if ( !count($data->courses->registered) ) : ?>
							<p class="alert alert-secondary"><?php echo $ztext->get("NO_COURSES_ALREADY"); ?></p>
						<?php endif; ?>
						<!-- List -->
						<?php echo \app\Helpers\HtmlHelper::makeList(
							$data->courses->registered, "list-group ul-registered", "list-group-item list-group-item-dark"
						); ?>
					</div>
				</div>	
			</div>	

			<div class="col pt-3">
				<p class="alert alert-warning" role="alert">
					<?php echo \app\Helpers\IconsHelper::get("info", 0, 'me-2') ?>
					<?php echo $ztext->get('ALERT_REGISTER') ?>
				</p>
			</div>
			<div class="col pt-3 text-center">

				<?php if ( count($data->courses->notyet) ) : ?>
					<input 
						type="submit" 
						value="<?php echo $ztext->get("BTN_VALID") ?>" 
						class="btn btn-success btn-lg" 
					/>		
				<?php endif; ?>
				
				<a 
					title="<?php echo $ztext->get("BTN_CANCEL_TITLE") ?>" 
					class="btn bg-main-color text-light btn-lg" 
					href="<?php echo $data->formation->link ?>"
				><?php echo $ztext->get("BTN_CANCEL") ?></a>

			</div>
		</div>
	</div>


	<!-- CSRF Token -->
	<?php echo $factory->getTokenCSRF() ?>
</form>