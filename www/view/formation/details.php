<?php 
// Public list of formations.

use app\Helpers\Factory;
use app\Helpers\IconsHelper;

defined('_PWE') or die("Limited acces");

global $factory, $ztext;

// Script to execute on page ready.
$idContent = "coursesContentBox";
$orderBy = isset($data->orderBy) ? "orderBy: '" . $data->orderBy->id . "'," : "";
$paginationScript = "null";
if ( isset($data->pagination) ) {
	$paginationScript = $data->pagination->getJSON(
		$factory->getUrl(\app\Enums\Page::FORMATION_COURSES)
	);
}

$script = "const PWFormation = new PWFormDetails"
  			."(
				{
					" . $orderBy . "
					pagination: " . $paginationScript . ",
					content: {id: '" . $idContent . "'},
					url: {
						items: '" . $factory->getUrl(\app\Enums\Page::FORMATION_COURSES, $data->formation->id) . "',
					},
					templates: {
						item: 'formation.course'
					}
 				}
 			);";
    
// Add script
$factory->addScriptDeclaration($script);

 ?>

<!-- header -->
<?php 
$style = ".item-page-top { background-image: url('" . $data->formation->image . "'); }";
Factory::getInstance()->addStyleDeclaration($style);
?>

<!-- Big top part -->
<div class="w-100">
	<div class="item-page-top bg-cover shadow">
		<div class="container h-100 my-auto">
						
			<div class="row row-cols-sm-1 row-cols-md-2">
				<div class="col">
					<div class="d-flex flex-column justify-content-center  h-100 p-5">
						<div>
							<h2 class="page-title"><?php echo $this->txt("FORMATION_TITLE") ?></h2>
							<h3 class="formation-title"><?php echo $data->formation->title ?></h3>
							<span class="badge bg-main-color formation-code">
								<?php echo $data->formation->uid ?>
							</span>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="inner-middle">
						<!-- Short text description -->
						<p class="formation-desc alert alert-dark">
							<?php echo $data->formation->description; ?>
						</p>	
					</div>
				</div>
			</div>
		</div>
	</div><!-- end big top part -->
</div>

<div class="container" id="<?php echo $idContent ?>">

	<div class="row justify-content-center  g-0 g-md-4 ">
		<div class="col-lg-3 col-md-4 col-sm-12">

			<!-- Formation degree -->
			<div class="card mt-5 mb-2" >
				<div class="card-header">
					<?php echo IconsHelper::get("degrees", 0, 'me-2 text-main-color') ?>
					<?php echo $ztext->get("DEGREE_TITLE") ?>
				</div>
				<div class="card-body desc-degree">
					<?php echo $ztext->get("DEGREE_DESC", $data->formation->degree_title) ?>
				</div>
			</div>
			

			<!-- Register to formation -->
			<?php 
				$bTxt = $data->formation->isStudent ? 'TITLE_REGISTERED' : 'TITLE_REGISTER';
				$bTitle = $data->formation->isStudent ? 'REGISTERED_ON_CLICK' : 'REGISTER_ON_CLICK';
				$href = $data->formation->isStudent ? '#' : $data->formation->urlRegister;
				$bg = $data->formation->isStudent ? 'bg-main-color' : 'bg-success';
				$ic = $data->formation->isStudent ? 'yes' : 'register';
				$icStyle = $data->formation->isStudent ? 'text-success' : '';
			?>
			<div class="card" data-pw-register>
				<img src="../assets/img/bg-register.jpg" 
					class="card-img-top" 
					alt="<?php echo $ztext->getTitle($bTxt) ?>"
				/>
				<div class="card-body p-0 ">
					<a 
						class="d-block text-center p-3 <?php echo $bg ?>"
						href="<?php echo $href ?>" 
						title="<?php echo $bTitle; ?>" 
					>
						<?php echo IconsHelper::get($ic, 0, 'me-1 ' . $icStyle) ?>
						<?php echo $ztext->get($bTxt) ?>				
					</a>
				</div>
			</div>

		</div><!-- col -->


		<!-- Courses part -->
		<div class="col-lg-9 col-md-8 col-sm-12 pt-5">
			
			<div class="card">
				<div class="card-header">
					<h3 >
						<?php echo $ztext->get("TITLE_COURSES") ?>
					</h3>  
				</div>
				<div class="card-body">

					<!-- Alert no filtered courses -->
					<p class="alert alert-secondary" role="alert" data-pw-items-count="<?php echo $data->countCourses ?>">
						<?php echo IconsHelper::get("filters", 0, 'me-1 text-warning') ?>
						<?php echo $ztext->get("NO_FILTERED_RESULT") ?>
					</p>
					
					<div class="row row-cols-1 row-cols-sm-1 row-cols-lg-3 g-3" data-pw-items>
					
					<?php foreach ($data->courses as $item) {
						echo \app\Helpers\TemplatesHelper::get("formation.course", $item);
					} ?>
					</div>


				</div><!-- card-body -->
			</div><!-- card -->

			<div class="row mt-2 p-0">

				<!-- Filters / OrderBy -->
				<?php if ( isset($data->filters) ) : ?>
					<div class="col-lg-3 col-sm-12 d-flex flex-column justify-content-center" data-pw-filters >
						<?php echo $data->filters ?>
					</div>
				<?php endif; ?>
				
				<!-- Pagination (courses) -->
				<?php if ( isset($data->pagination) ) : ?>
					<div class="col-lg-9 col-sm-12 d-flex flex-column justify-content-center align-items-end p-0 m-0">
						<?php
							echo $data->pagination->render
							(
								"PAGINATION_ARIA", 
								$factory->getUrl(
									\app\Enums\Page::FORMATIONS_FORMATION, 
									$data->formation->id
								)
						); ?>
					</div>
				<?php endif; ?>

			</div>

			
			<!-- Full html description -->
			<?php if ( !empty($data->formation->content) ) : ?>
				<div class="card mt-3 mb-5">
					<div class="card-header">
						<?php echo $ztext->get("CONTENT_TITLE"); ?>
					</div>
					<div class="card-body">
						<?php echo $data->formation->content; ?>
					</div>
				</div>
			<?php endif; ?>


		</div><!-- col -->
	</div><!-- row -->
</div><!-- container -->


	



<!-- CSRF Token -->
<?php echo $factory->getTokenCSRF() ?>
