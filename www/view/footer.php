<?php defined('_PWE') or die("Limited acces");

use app\Helpers\IconsHelper; 

global $factory, $ztext, $manageLinks;

$isRoot = $factory->getUser()->isRoot();
$helper = new \app\Helpers\FooterHelper(); 
$social = $helper->getSocialIcons();
$counts = $helper->getCounts();
$sitemap = $helper->getSitemap();
?>
<footer>
	<!-- Counts -->
	<div class="w-100 m-0 p-0 bg-cover footer-counts-bg">
		
		<div class="container py-5">
			
			<div class="row counts-part">
				<div class="col-12 text-center pt-5">
					<h4 class="footer-counts-title"><?php echo $ztext->get("FOOTER_COUNTS_TITLE") ?></h4>
					<p><?php echo $ztext->get("FOOTER_COUNTS_DESC") ?></p>
				</div>

				<?php foreach ( $counts as $co ) : ?>
					<div class="col-md-3 col-sm-12 p-5">
						<div class="ratio ratio-1x1 footer-counts-item">
							<div class="d-flex flex-column justify-content-center text-center">
								<div class="footer-count-total"><?php echo $co->total ?></div>
								<div class="footer-count-title"><?php echo $co->title ?></div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="overlay-dark"></div>
	</div>

	<div class="w-100 m-0 p-0 bg-cover footer-contact-bg">
		<div class="container footer-container p-md-5">
			
			<!-- Address -->
			<div class="row g-5 row-address m-md-5">		
				<div class="col-md-4 col-sm-12 text-center col-address">
					<div class="h-100 p-3">
						<?php echo IconsHelper::get("address", 0, "footer-icon") ?>
						<h5 class="footer-title"><?php echo $ztext->get("FOOTER_ADDRESS_TITLE") ?></h5>
						<address>
							<?php echo $factory->cfg("address.street") ?><br />
							<?php echo $factory->cfg("address.city") ?>&nbsp;
							<?php echo $factory->cfg("address.zip") ?>
						</address>
					</div>
				</div>
				
				<div class="col-md-4 col-sm-12 text-center col-address">
					<div class="h-100 p-3">
						<?php echo IconsHelper::get("mail", 0, "footer-icon") ?>
						<h5 class="footer-title"><?php echo $ztext->get("FOOTER_MAIL_TITLE") ?></h5>
						<address>
							<a href="mailto: <?php echo $factory->cfg("contact.mail") ?>">
								<?php echo $factory->cfg("contact.mail") ?>
							</a>
						</address>
					</div>
				</div>

				<div class="col-md-4 col-sm-12 text-center col-address">
					<div class="h-100 p-3">
						<?php echo IconsHelper::get("phone", 0, "footer-icon") ?>
						<h5 class="footer-title"><?php echo $ztext->get("FOOTER_PHONE_TITLE") ?></h5>
						<address>
							<?php echo $factory->cfg("contact.phone") ?>
						</address>
					</div>
				</div>
			</div>

		</div><!-- container -->
		<div class="overlay-main-color"></div>
	</div><!-- footer-contact-bg -->

	<!-- Links + sitename -->
	<div class="row-footer w-100 bg-cover">
		<div class="overlay-dark h-100 w-100"></div>
		<div class="container pt-5">
			<div class="row">
				
				<div class="col-md-3 col-sm-12 footer-website">
					<h4 class="website-name mb-4"><?php echo $ztext->get("FOOTER_PROJECT_TITLE") ?></h4>
					<p><?php echo $ztext->get("FOOTER_PROJECT_DESC") ?></p>
					<p>
						<span class="footer-teacher"><?php echo $ztext->get("FOOTER_TEACHER") ?></span> : 
						<br />
						<span class="fw-bold">VAN ASS</span> Jean-François
					</p>
				</div>
				<div class="col-md-3 col-sm-12">

				</div>

				<!-- Sitemap -->
				<div class="col-md-3 col-sm-12">
					<h5 class="footer-title "><?php echo $ztext->get('FOOTER_SITEMAP') ?></h5>
					<ul>
						<li>
							<i class="fa-solid fa-chevron-right"></i>
							<a href="index.php" 
								title="<?php echo $txt ?>"
							><?php echo $ztext->get("MENU_HOME") ?></a>
						</li>
						<?php foreach ( $sitemap as $txt => $link ) : ?>
							<li>
								<i class="fa-solid fa-chevron-right"></i>
								<a href="<?php echo $link ?>" 
									title="<?php echo $txt ?>"
								><?php echo $txt ?></a>
							</li>
						<?php endforeach; ?>
						</ul>
				</div>

				<div class="col-md-3 col-sm-12">
					<?php if ( $isRoot || !empty($manageLinks) ) : ?>
						<h5 class="footer-title "><?php echo $ztext->get('FOOTER_MANAGE_LINKS') ?></h5>
						<ul>
							<!-- Global manage -->
							<?php if ( $isRoot ) : ?>
								<li>
									<i class="fa-solid fa-chevron-right"></i>
									<a href="<?php echo $factory->getUrl(\app\Enums\Page::ADMIN) ?>" >
										<?php echo $ztext->get("MENU_ADMIN") ?>
									</a>
								</li>
							<?php endif; ?>
							<?php foreach ( $manageLinks as $txt => $link ) : ?>
								<li>
									<i class="fa-solid fa-chevron-right"></i>
									<a href="<?php echo $link ?>">
										<?php echo $txt ?>
									</a>
								</li>
							<?php endforeach; ?>
							
							</ul>
					<?php endif; ?>
				</div>
			</div><!-- row -->
			
			<!-- Socials icons (disabled) -->
			<div class="row ">
				<div class="col-12 text-center ul-social ">
					<h5 class="footer-title mb-3"><?php echo $ztext->get('FOOTER_SOCIAL') ?></h5>
					<ul class="list-group list-group-horizontal text-center d-flex justify-content-center bg-transparent">
						<?php foreach ( $social as $so ) : ?>
							<li class="list-group-item bg-transparent">
								<a 
									href="<?php echo $so->link ?>" 
									title="<?php echo $so->title ?>"
								><?php echo $so->icon ?>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div><!-- row -->

			<!-- Copyright -->
			<div class="row m-0">
				<div class="col-12 p-0">
					<hr />
					<div class="footer-copyright mt-4 p-0">
						<?php echo $ztext->get("FOOTER_COPYRIGHT") ?>
					</div>
				</div>
			</div><!-- row -->

		</div><!-- container -->
	</div><!-- row-footer -->
</footer>