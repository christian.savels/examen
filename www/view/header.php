<?php
defined('_PWE') or die("Limited acces");

global $factory;

if ( $factory->getFormat()=="json" ) {
    header('Content-Type: application/json; charset=utf-8');
}
elseif ( $factory->getFormat()=="html" )
{  
    $tokenName = \app\Helpers\SessionHelper::TOKEN_CSRF; 
    $filesJS = $factory->getScriptFiles();
    $filesCSS = $factory->getStyleFiles(); ?>
    <head>
		<title><?php echo $factory->cfg("site.name") ?></title>
		<?php 
        if ( array_key_exists($tokenName, $_SESSION) ) : ?>
    		<meta name="<?php echo $tokenName ?>" content="<?php echo $_SESSION[$tokenName] ?>">
			<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php endif; ?>

    	<script src="assets/js/main.js"></script>
    	<script src="assets/js/fetch.js"></script>
    	<script src="assets/js/notify.js"></script>
    	<script src="assets/js/confirm.js"></script>
    	<script src="assets/js/items.js"></script>
    	
    	<?php foreach ( array_keys($filesJS) as $script ) :  ?>
    			<script src="<?php echo $script ?>"></script>
    	<?php endforeach; ?>
    	
    	<?php if ( $factory->cfg("bootstrap") ) : ?>
    		<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    	<?php endif; ?>
		
    	<link rel="stylesheet" href="assets/css/main.css" />
    	<link rel="stylesheet" href="assets/css/modal.css" />
    	<link rel="stylesheet" href="assets/css/notify.css" />
    	
    	<!-- Fontawesome -->
    	<link href="assets/fontawesome/css/fontawesome.css" rel="stylesheet">
  		<link href="assets/fontawesome/css/all.css" rel="stylesheet">
    	
    	<?php foreach ( array_keys($filesCSS) as $css ) :  ?>
    			<link rel="stylesheet" href="<?php echo $css ?>" />
    	<?php endforeach; ?>

		<!-- Style declarations -->
		<?php
			$sc = $factory->getStylesDeclaration();
			if ( !empty($sc) ) : ?>
				<style><?php echo implode(" ", array_keys($sc)); ?></style>
			<?php endif; ?>
    </head><?php 
} ?>