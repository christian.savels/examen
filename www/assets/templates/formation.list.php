<?php 
defined('_PWE') or die("Limited acces");

global $factory, $ztext; 

$factory->addStyleDeclaration(
    '[data-pw-item="' . $item->id . '"] > .card-formation > .card-body {'
    . "background-image: url('" . $item->image . "');"
    . "}"
);
?>
<div
    class="col-lg-4 col-sm-6 col-md-12" 
    data-pw-item="<?php echo $item->id ?>" 
    title="<?php echo $ztext->getTitle("ITEM_SELECT_IT") ?>" 
>
    <div class="card w-100 h-100 card-formation">
        
        <div class="card-header">
            <h6 class="mb-1"  >   
                <a 
                    href="<?php echo $item->link ?>" 
                    title="<?php echo $ztext->getTitle("FORMATION_SHOW") ?>"
                    data-pw-link
                >
                    <span class="badge float-end text-uppercase bg-main-color text-light" data-pw-uid>
                    <?php echo $item->uid ?>
                    </span>
                    <span data-pw-title><?php echo $item->title ?></span>
                </a>
            </h6>
        </div>
        <div class="card-body bg-cover" data-pw-image>
            <div class="bg-overlay"></div>
            <p class="mb-1" data-pw-description ><?php echo $item->description ?></p>
        </div>
        <div class="card-footer p-0">
            <a 
                href="<?php echo $item->link ?>" 
                class="btn bg-main-color w-100 m-0"
                title="<?php \app\Helpers\Factory::getInstance()->getTxt()->getTitle("FORMATION_SHOW") ?>"
                data-pw-link
            >
                <?php echo \app\Helpers\IconsHelper::get("more"); ?>
            </a>
        </div>
    </div>
</div>