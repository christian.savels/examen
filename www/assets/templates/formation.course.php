<?php 
defined('_PWE') or die("Limited acces");

global $factory, $ztext; 

$cstyle = '[data-pw-item="' . $item->id . '"] > .card-course > .card-body {';
$cstyle .= "background-image: url('" . $item->image . "');";
$cstyle .= "}";
$factory->addStyleDeclaration($cstyle);
?>
<div
    class="col" 
    data-pw-item="<?php echo $item->id ?>" 
    title="<?php echo $ztext->getTitle("ITEM_SELECT_IT") ?>" 
>
    <div class="card w-100 h-100 card-course"
        data-pw-is-student="<?php echo (int)$item->isStudent; ?>"
        data-pw-is-teacher="<?php echo (int)$item->isTeacher; ?>"
    >
        <div class="card-header">
            <h6 class="mb-1 text-white">
                <a 
                    href="<?php echo $factory->getUrl(\app\Enums\Page::COURSE, $item->id) ?>" 
                    title="<?php echo $ztext->getTitle("COURSE_SHOW") ?>"
                    data-pw-link
                >
                    <span class="badge bg-main-color float-end text-uppercase" data-pw-uid>
                        <?php echo $item->uid ?>
                    </span>
                    <span data-pw-title><?php echo $item->title ?></span>
                </a>
            </h6>        
        </div>
        <div class="card-body bg-cover">
            <!-- <p class="mb-1" data-pw-description ><?php echo $item->description ?></p> -->
            <a 
                href="<?php echo $factory->getUrl(\app\Enums\Page::COURSE, $item->id) ?>" 
                class=""
                title="<?php echo $ztext->getTitle("COURSE_SHOW") ?>"
                data-pw-link-edit
            >
                <i class="fa-regular fa-pen-to-square fa-2x icon-edit"></i>
            </a>            
        </div>
        <div class="card-footer text-end">
            <small><?php echo $ztext->get("BEGINS_ON", $item->start_on); ?></small>
        </div>
    </div>
</div>
