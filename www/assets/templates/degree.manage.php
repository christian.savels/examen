<?php 
defined('_PWE') or die("Limited acces");

global $factory, $ztext; 
?>
<div
    class="list-group-item list-group-item-action" 
    data-pw-item="<?php echo $item->id ?>" 
    title="<?php echo $ztext->getTitle("DEGREES_SELECT_IT") ?>" 
>
    <div class="d-flex flex-row w-100 justify-content-between">
        <div class="item-info">
            <h6 class="mb-1" data-pw-title><?php echo $item->title ?></h6>        
            <p class="mb-1" data-pw-description><?php echo $item->description ?></p>
            <small>
                <?php echo $ztext->get("DEGREE_INSIDE_TITLE") ?>
                <span class="ms-2 badge bg-secondary" data-pw-count><?php echo $item->count_inside ?></span>
            </small>
        </div>
        <div class="d-flex flex-column justify-content-around item-icons">
            <div class="badge bg-main-color badge-id">#<span data-pw-id><?php echo $item->id ?></span></div>
            <a 
                href="<?php echo $item->link ?>" 
                class=""
                title="<?php echo $ztext->getTitle("DEGREES_EDIT") ?>"
                data-pw-link
            >
                <i class="fa-regular fa-pen-to-square fa-2x icon-edit"></i>
            </a>
            
        </div>
    </div>
</div><!-- item -->