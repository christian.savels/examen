<?php 
defined('_PWE') or die("Limited acces");

global $factory, $ztext; 
?>
<div
    class="list-group-item list-group-item-action" 
    data-pw-item="<?php echo $item->id ?>" 
    title="<?php echo $ztext->getTitle("ITEM_SELECT_IT") ?>" 
>
    <div class="d-flex flex-row w-100 justify-content-end">
        <div class="flex-grow-1 item-info">

            <h6 class="my-2 item-title">																	
                <span data-pw-title data-pw-searchable><?php echo $item->title ?></span>
            </h6>

            <div >
                <span class="badge bg-secondary" 
                    title="<?php echo $ztext->get("TITLE_ID") ?>"
                    >#<span data-pw-id data-pw-searchable><?php echo $item->id ?></span>
                </span>		
                <span class="badge bg-main-color text-uppercase" 
                    title="<?php echo $ztext->get("TITLE_UID") ?>"
                    data-pw-uid 
                    data-pw-searchable
                >
                    <?php echo $item->uid ?>
                </span>

                <span class="badge bg-secondary" 
                    data-pw-teacher="<?php echo trim($item->teacher) ?>"
                    title="<?php echo $ztext->getTitle("TAG_TEACHER") ?>"
                    >
                    <?php echo \app\Helpers\IconsHelper::get("teachers") ?>
                    <span data-pw-teacherName >
                        <?php echo $item->teacher ?>
                    </span>
                </span>
                
            </div>
                                                
        </div>
        <div class="d-flex flex-column justify-content-between ms-4 item-icons" 
            data-pw-published-eval="<?php echo $item->published ?>"
        >
            <a 
                href="<?php echo $item->link ?>" 
                class=""
                title="<?php echo $ztext->getTitle("COURSE_EDIT") ?>"
                data-pw-link
            >
                <?php echo \app\Helpers\IconsHelper::get("edit", 0, "icon-edit") ?>
            </a>						
            
            <?php echo \app\Helpers\IconsHelper::get("unpublished", false, "text-danger published-0 m-0") ?>
            <?php echo \app\Helpers\IconsHelper::get("published", false, "text-success published-1 ") ?>
        
        </div>
    </div>
</div>