<?php 
defined('_PWE') or die("Limited acces");

global $factory, $ztext; 
?>
<div
    class="list-group-item list-group-item-action" 
    data-pw-item="<?php echo $item->id ?>" 
    title="<?php echo $ztext->getTitle("ITEM_SELECT_IT") ?>" 
>
    <div class="d-flex flex-row w-100 justify-content-end">
        <div class="flex-grow-1 item-info">

            <h6 class="my-2 item-title">
            
                <span 
                    data-pw-title
                    title="<?php echo $ztext->get("TITLE_FORMATION") ?>"
                >
                    <?php echo $item->title ?>							
                </span>
            </h6>

            <span class="badge bg-main-color text-uppercase me-2" 
                title="<?php echo $ztext->get("TITLE_UID") ?>"
                data-pw-uid>
                <?php echo $item->uid ?>
            </span>
                
            <span 
                data-pw-degree_title 
                class="" 
                title="<?php echo $ztext->get("TITLE_DEGREE") ?>"
            >
                <?php echo $item->degree_title ?>
            </span>
            
        </div>
        <div class="d-flex flex-column justify-content-around text-end ms-4 item-icons" data-pw-published-eval="<?php echo $item->published ?>">

            <h6 class="fst-italic m-0" 
                title="<?php echo $ztext->get("TITLE_ID") ?>"
                >#<span data-pw-id ><?php echo $item->id ?></span>
            </h6>								
            
            <?php echo \app\Helpers\IconsHelper::get("unpublished", false, "text-danger published-0 m-0") ?>
            <?php echo \app\Helpers\IconsHelper::get("published", false, "text-success published-1") ?>
        
            <a 
                href="<?php echo $factory->getUrl(\app\Enums\Page::FORMATIONS_EDIT, $item->id) ?>" 
                class=""
                title="<?php echo $ztext->getTitle("FORMATION_EDIT") ?>"
                data-pw-link-edit
            >
                <i class="fa-regular fa-pen-to-square icon-edit"></i>
            </a>
        </div>
    </div>
</div>