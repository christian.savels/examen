<?php 
defined('_PWE') or die("Limited acces");

global $factory, $ztext; 
?>
<!-- An item -->
<div 
    class="card w-100" 
    data-pw-item="<?php echo $item->id ?>" 
    title="<?php echo $ztext->getTitle("ROLES_SELECT_IT") ?>" 
>
    <div class="card-body">
        <a 
            href="<?php echo $item->link ?>" 
            class="float-end"
            data-pw-link
        >
            <i class="fa-regular fa-pen-to-square fa-2x icon-edit"></i>
        </a>
        <h6 class="card-title" >
            <i class="<?php echo \app\Helpers\IconsHelper::get("notenrolable", true) ?>"
                 data-pw-enrolable="<?php echo $item->enrolable ? 1 : 0 ?>">
            </i>
            <span data-pw-title><?php echo $item->title ?></span>
        </h6>
        <p class="card-text fw-lighter" data-pw-description>
            <small><?php echo $item->description ?></small>
        </p>
    </div>
</div>