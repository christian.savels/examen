<?php 
defined('_PWE') or die("Limited acces");

global $factory, $ztext;  
?>
<!-- An user -->
<tr 
    data-pw-item="<?php echo $item->id ?>" 
    data-pw-item-published="<?php echo $item->published ?>" 
    data-pw-item-selected="0" 
>
    <td class="text-center">
        #<span data-pw-id><?php echo $item->id ?></span>
    </td>
    <td 
        class="text-center" 
    >
        <div class="btn-group" role="group" aria-label="<?php echo $ztext->getTitle("USER_ROW_OPTIONS") ?>">
            <button 
                type="button" 
                class="btn text-danger"
                data-pw-select="<?php echo $item->id ?>"
                title="<?php echo $ztext->getTitle("TITLE_SELECT") ?>"
            ><?php echo \app\Helpers\IconsHelper::get("select"); ?></button>
            <button 
                type="button" 
                class="btn"
                data-pw-edit="<?php echo $item->id ?>"
                title="<?php echo $ztext->getTitle("TITLE_EDIT") ?>"
            ><?php echo \app\Helpers\IconsHelper::get("edit"); ?></button>
            <button 
                type="button" 
                class="btn"
                data-pw-more="<?php echo $item->id ?>"
                title="<?php echo $ztext->getTitle("TITLE_MORE") ?>"
            ><?php echo \app\Helpers\IconsHelper::get("more", 0, "text-main-color"); ?></button>
        </div>
    </td>
    <td data-pw-user-edit="<?php echo $item->id ?>">
        <!-- Lastname - Firstname -->
        <span data-pw-lastname><?php echo $item->lastname ?></span>
        <span data-pw-firstname><?php echo $item->firstname ?></span>
        
        <!-- Email -->
        <br />
        <small class="badge bg-secondary" data-pw-email><?php echo $item->email ?></small>
    </td>
    <td>
        <small>
            <span data-pw-zip><?php echo $item->zip ?></span>
            <span data-pw-city><?php echo $item->city ?></span>
            <span data-pw-country>(<?php echo $item->country ?>)</span>
        </small>
    </td>
    <td class="text-center">
        <span data-pw-isTeacher><?php echo $item->isTeacher ?></span>
    </td>	
    <td class="text-center">
        <span data-pw-isStudent><?php echo $item->isStudent ?></span>
    </td>	
    <td class="text-center">
        <span data-pw-access="<?php echo $item->id ?>"><?php echo $item->access ?></span>
    </td>
    <td class="text-center" data-pw-published-icon>
        <?php echo $item->published_icon ?>
    </td>
</tr>