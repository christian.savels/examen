<?php 
defined('_PWE') or die("Limited acces");

global $factory, $ztext; 

// Init image as background.
$item->idImg = "itemImg" . $item->id;
$factory->addStyleDeclaration("#" . $item->idImg . " {
    background-image: url('" . $item->image . "');
}");
?>

<div class="col-lg-4 col-sm-6 col-md-12" data-pw-item="<?php echo $item->id ?>">
    
    <div 
        class="card h-100"
        data-pw-is-student="<?php echo $item->isStudent ?>"
         data-pw-is-teacher="<?php echo $item->isTeacher ?>"
    >							
        <div class="card-header">
            <span class="badge bg-main-color mx-2 my-auto" data-pw-uid><?php echo $item->uid ?></span>
            <span >
                #<span data-pw-id><?php echo $item->id ?>
            </span>
        </div>

        <!-- Illustration -->
        <div 
            id="<?php echo $item->idImg ?>"
            class="w-100 ratio ratio-16x9 bg-cover"
            data-pw-image
        ></div>

        <!-- Course info -->
        <div class="card-body">
            <h6 class="card-title fw-bold" data-pw-title>
                <?php echo $item->title ?>
            </h6>
            <hr />
            <p class="card-text" data-pw-description>
                <?php echo $item->description ?>
            </p>
        </div>
        <div class="card-footer bg-transparent p-0">
            <a 
                href="<?php echo $item->link ?>" 
                class="btn bg-main-color btn-md w-100"
                title="<?php echo $ztext->getTitle("LINK_TITLE", $item->title) ?>"
                data-pw-link
            >
                <?php echo \app\Helpers\IconsHelper::get("more") ?>
            </a>
        </div>
    </div>
</div><!-- Item -->