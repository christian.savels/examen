<?php 
defined('_PWE') or die("Limited acces");

global $ztext; 
?>
<div 
    class="col"
    data-pw-user="0" 
    data-pw-selected="0"
    data-pw-is-pending="0"
    title="<?php echo $ztext->getTitle("COURSES_SELECT_USER") ?>" 
>
    <div 
        class="card h-100" 							
    >
        <div class="card-body p-2 bg-dark bg-gradient to-highlight">

            <!-- Name -->
            <h6 class="">
                <span data-pw-lastname data-pw-searchable ></span>
                <span data-pw-firstname data-pw-searchable ></span>
                    
                <span 
                    class="float-end fw-normal" 
                    title="<?php echo $ztext->getTitle("TITLE_USER_ID") ?>" 
                >#<i data-pw-id></i></span>
            </h6>											
            
            <!-- Infos list -->
            <div class="user-infos">
            
                <ul class="fs-6">
                    <li>
                        <span class="fa-solid fa-phone me-3 icon"></span>
                        <span data-pw-phone data-pw-searchable ></span>
                    </li>
                    <li>
                        <span class="fa-regular fa-address-card me-3 icon"></span>
                        <span data-pw-street data-pw-searchable ></span>
                        <span data-pw-number></span><br />
                    </li>
                    <li>
                        <span class="fa-solid fa-street-view me-3 icon"></span>
                        <span data-pw-zip data-pw-searchable ></span>
                        <span data-pw-city data-pw-searchable ></span>
                    </li>
                </ul>
                
                <!-- Avatar -->
                <div class="user-avatar">
                    <div 
                        class="ratio ratio-1x1 bg-cover circle my-auto shadow" 
                        data-pw-social-avatar 
                    ></div>
                </div>
            </div>
        </div>
    </div><!-- card -->
</div><!-- col -->