class PWConfirm
{
	constructor(params) 
	{
		this.params = params;
		this.$confirm = null;
		this.callback = null;
		this.action = null;

		this.#initConfirm();
	}
			
		
	/**
	 *	Initialize.
	 */
		#initConfirm() 
		{
			if ( this.params!=null )
			{
				this.$confirm = document.getElementById(this.params.id);

				this.$confirm.querySelector("[data-pw-cancel-it]")
								.addEventListener("click", (e)=>{
									e.preventDefault();

									console.log("CLICKED:)")
									this.$confirm.setAttribute("data-pw-confirm", 0);
								});

				this.$confirm.querySelector("[data-pw-confirm-it]")
								.addEventListener("click", (e)=> {
									e.preventDefault();
									console.log("====>TEST OK CLICKED");
									this[this.callback](this.action);
									this.$confirm.setAttribute("data-pw-confirm", 0);
								});
			}
		}
			
		
	/**
	 *	Hide the confirmation alert.
	 */
		hideConfirm(hidden=true) {
	    	this.$confirm.setAttribute("data-pw-confirm", hidden ? 0 : 1);
		}
		
	
	/**
	 *	Show confirmation message.
	 */		
		askConfirmAndDo(options)
		{			
			this.callback = options.callback;
			this.action = options.action;
			this.$confirm.setAttribute("data-pw-confirm", 1);
			
			// Apply custom confirmation message.
			if ( options.msg!=null ) {
				this.$confirm.querySelector("[data-confirm-content]").innerHTML = options.msg;
			}
				    
		    window.scrollTo(0, this.$confirm.offsetTop-64);
		}
		
}
 