class PWUsersList extends PWItems
{
    constructor(from, options)
    {
        super(options);

        this.from = from;
        this.options = options;
        this.$tmplItem = null;

        this.#init();
    }


    /**
     * Init...
     */
        #init() {
            this.$tmplItem = this.$content.querySelector("[data-pw-item]");
        }


    /**
     * Place items received via the pagination system.
     * 
     * @param data 
     */
        placeItems(data)
        {
            console.log(this.constructor.name + "::placeItems()");

            const $parent = this.$content.querySelector("[data-pw-items]");
            var $tmpl, item;

            if ( $parent!=null )
            {
                $parent.innerHTML = ""; 
                
                for ( let i in data )
                {
                    item = data[i];
                    $tmpl = this.$tmplItem.cloneNode(true);

                    for ( let k in item )
                    {
                        if ( typeof item[k]=="object" )
                        {                       
                            for ( let k1 in item[k] ) 
                            {
                                $tmpl.querySelectorAll("[data-pw-" + k + "-" + k1 + "]")
                                    .forEach( $e=> {
                                        $e.innerHTML = item[k][k1];
                                    });
                            }
                        }
                        else 
                        {
                            if ( k=="id" ) 
                            {
                                $tmpl.querySelector("[data-pw-select]")
                                        .setAttribute("data-pw-select", item[k]); 
                                $tmpl.setAttribute("data-pw-item", item[k]);
                            } 
                            
                            $tmpl.querySelectorAll("[data-pw-" + k + "]")
                                .forEach( $e=> {
                                    $e.innerHTML = item[k];
                                });   
                                
                        }
                    }

                    $parent.appendChild($tmpl);
                }

  	 	    // User selection
  	 	    $parent.querySelectorAll("tbody tr")
                .forEach( $tr=>
                {
                    console.log("$row");
                    $tr.querySelector("[data-pw-select]")
                        .addEventListener("click", (e)=>{
                            console.log("USER SELECTED : " + $tr.getAttribute("data-pw-item"));
                            $tr.classList.toggle("table-success");
                            $tr.classList.toggle("selected-user");
                        });
                });
            }
        }
}