class PWFetch
{
	constructor(url, tokenName, format="json", method="post")
	{
		this.url = url;
		this.tokenName = tokenName;
		this.format = format;
		this.method = method;		
	}


	/**
	 * 
	 * @param {*} data 
	 * @param {*} fname 
	 * 
	 * @source Inspiré de https://code-boxx.com/download-file-javascript-fetch/
	 * 
	 * @returns 
	 */
		async download(data, fname)
		{
			const $token = document.body.querySelector('[name="' + this.tokenName + '"');
			const formData = new FormData();
				formData.append(this.tokenName, $token.value);

			if ( data!=null ) {
				this.#fusionFormData(formData, data);
			}

			fetch(
				this.url, 
				{ 
					method: this.method,
					body: formData
				}
			)
			.then( r=> {
				if ( r.status!=200 ) {
					console.warn(this.url + " is invalid url." )
				} else {
					return r.blob();
				}
			})
			.then(blob=>{
				console.log("TEST DATA BLOB : " + blob);

				this.forceFownload(blob, fname);
			});
		}


	forceFownload(blob, fname)
	{
		const url = window.URL.createObjectURL(blob),

		a = document.createElement("a");
		a.href = url;
		a.download = fname;
		a.click();
	}

	
	/**
	 * Get text response.
	 */
		async getTxt(data=new FormData()) 
		{
			const $token = document.body.querySelector('[name="' + this.tokenName + '"');
			const formData = new FormData();
				  formData.append("format", this.format);
				  formData.append(this.tokenName, $token.value);

			if ( data!=null ) {
				this.#fusionFormData(formData, data);
			}
			
			try 
			{
	        	let res = await fetch(
					this.url, 
					{ 
						method: this.method,
						body: formData
					}
				);
				const data = await res.text();
				
				console.log("data received : ", data);
				
				// Notify messages.
				PWE.renderMessages(data.messages);
				
				// No answer, refresh page to force login if session is expired.
				if ( data==null || data.length==0 ) {
					window.location.reload();
				}
				// Redirect after delay if needed.
				else if ( data.redirect!=null ) 
				{
					let t = data.redirectDelay!=null ? data.redirectDelay : 10;
					setTimeout( ()=>{
						window.location = data.redirect;
					}, t );
				} 
				
				return data;
		    } 
		    catch (error) {
		        console.log(error);
		    }
		}

	
	/**
	 * Get JSON response.
	 */
		async getJSON(data=new FormData()) 
		{
			
			const $token = document.body.querySelector('[name="' + this.tokenName + '"');
			const formData = new FormData();
				  formData.append("format", this.format);
				  formData.append(this.tokenName, $token.value);

				  console.log("$token " + this.tokenName+ " = " +$token.value)


			if ( data!=null ) {
				this.#fusionFormData(formData, data);
			}
			
			try 
			{
	        	let res = await fetch(
					this.url, 
					{ 
						method: this.method,
						body: formData
					}
				);
				const data = await res.json();
				
				console.log("data received : ", data);
				
				// Notify messages.
				PWE.renderMessages(data.messages);
				
				// Redirect after delay if needed.
				if ( data.redirect!=null ) 
				{
					let t = data.redirectDelay!=null ? data.redirectDelay : 10;
					setTimeout( ()=>{
						window.location = data.redirect;
					}, t );
				} 
				
				return data;
		    } 
		    catch (error) {
		        console.log(error);
		    }
		}
			
		
	/**
	 * Fusion of two FormData.
	 * @param fd1 Main FormData.
	 * @param fd2 FormData to fusion into fd1.
	 **/
		#fusionFormData(fd1, fd2)
		{
			for ( let p of fd2.entries() ) {
				fd1.append(p[0], p[1]);
			}
		}
}