class PWItems extends PWConfirm
{
    constructor(options)
    {
        super(options.confirm);
        
        this.$content = null;
        this.options = options;
        this.working = false;
        this.$tmplItem = null;
        this.$tmplPagination = null;
        this.$filtersAlert = null;
        this.orderBySelected = null;
        this.selectedItems = [];

        this.#initThis();
    }


    /**
     * Init oject.
     */
        #initThis() 
        {
            this.#initContentPart();

            console.log("options ", this.options);
            console.log("$content ", this.$content);

            if ( this.options.url.items!=null )
            {
                this.#initItems();
                this.#initPagination();
                this.#initOrderBy();
                this.#observeFilters();
                this.#observeExport();
            }
            else {
                console.warn(this.constructor.name + " this.options.url.items is not set");

            }
        }


    /**
     * Init content part (part with items).
     * Used to permit multi pagination systems.
     * (like users and roles pagination on the same page for example...).
     */
        #initContentPart() 
        {
            if ( this.options.content!=null && this.options.content.id!=null ) {
                this.$content = document.getElementById(this.options.content.id);
            }
            
            if ( this.$content==null ) {
                this.$content = document.body;
            }

            this.$filtersAlert = this.$content.querySelectorAll("[data-pw-alert-filters]");
        }


    /**
     * Init items.
     */
        #initItems() 
        {
            // Get from template 
            if ( this.options.templates!=null && this.options.templates.item!=null ) {
                this.$tmplItem = PWE.getTemplate(this.options.templates.item);
            }

            if ( this.$tmplItem==null )
            {
                this.$content.querySelector("[data-pw-items] [data-pw-item]");
                if ( this.$tmplItem!=null ) {
                    this.$tmplItem = this.$tmplItem.cloneNode(true);
                }
            } else {
                this.$tmplItem = this.$tmplItem.querySelector("[data-pw-item]");
            }
        }


    /**
     * Init order by section.
     */
        #initOrderBy()
        {
            if ( this.options.orderBy!=null ) {
                this.#observeOrderBy();
            }
        }


    /**
     * Init pagination sections.
     */
        #initPagination()
        {
            // Pagination
            if ( this.options.pagination!=null )
            {
                let $p = this.$content.querySelector("[data-pw-pagination]");

                if ( $p!=null )
                {
                    console.log("this.$content", this.$content);
                    console.log("this.$content.querySelector([data-pw-pagination])", this.$content.querySelector("[data-pw-pagination]"));
                    this.$tmplPagination = $p.querySelector("[data-pw-page]").cloneNode(true);
                    this.$tmplPagination.classList.remove("active");

                    try {
                        this.options.pagination = JSON.parse(this.options.pagination);
                    } catch (e) {}

                    this.#observePagination();
                }
            }
        }


    /**
     * Observe export links.
     */
        #observeExport()
        {
            this.$content.querySelectorAll("[data-pw-export-list]")
                .forEach( $e=>{
                    $e.addEventListener("click", e=>{
                        e.preventDefault();
                        this.exportList($e.getAttribute("href"), $e.getAttribute("data-pw-export-list"));
                    });
                });
        }


    /**
     * Export filtered (or not) items list.
     * 
     * @param {*} url 
     * @param {*} format 
     */
        async exportList(url, format) 
        {
            const fd = new FormData(); 
                  fd.append("pagination", JSON.stringify(this.options.pagination) );
                  fd.append("orderBy", this.orderBySelected );
                  fd.append("filters", JSON.stringify(this.getFiltersValues()) );
    
            const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
            await f.download(fd, 'export.' + format);
        }


    /**
     * Observe search filters.
     */
        #observeFilters()
        {
            this.$content.querySelectorAll("[data-pw-filters] [data-pw-filter]")
                .forEach( $e=>
                {
                    $e.addEventListener('input', (e)=>
                    {
                        this.options.pagination.page = 0;                        
                        this.#loadPage(1);
                        this.#filtersAlertVisibility();
                    });
                });
        }


    /**
     * Show/hide alert filters are active.
     */
        #filtersAlertVisibility()
        {
            var v=0;
            const filters = this.$content.querySelectorAll("[data-pw-filters] [data-pw-filter]");
 
            filters.forEach( $e=>{
                if ( $e.value!='' ) {
                    v = 1;
                }
            });

            this.$filtersAlert.forEach( $e=>{
                $e.setAttribute('data-pw-alert-filters', v);
            });
        }
        

    /**
     * Observe order by form.
     */
        #observeOrderBy()
        {
            const $ob = document.getElementById(this.options.orderBy);

            if ( $ob!=null )
            {
                $ob.addEventListener("change", async (e)=>
                {
                    this.orderBySelected = $ob.querySelector("select").value;
                    this.options.pagination.page = 0;
                    this.#loadPage(1); 
                });
            }
        }
        

    /**
     * Observe pagination elements.
     */
        #observePagination() 
        {
            this.$content.querySelectorAll("[data-pw-pagination]")
                .forEach( $e=>
                {
                    // Link to specific page.
                    $e.querySelectorAll("[data-pw-page]")
                        .forEach($p=>
                        {
                            $p.addEventListener("click", (e)=>
                            {
                                e.preventDefault();
                                if ( !$p.classList.contains("active") ) {
                                    this.#loadPage($p.dataset.pwPage);
                                }
                            });
                        });
                        
                    // Link to next page.
                    $e.querySelectorAll("[data-pw-next]")
                        .forEach($p=>
                        {
                            $p.addEventListener("click", async (e)=>
                            {
                                e.preventDefault();                                
                                var p = this.options.pagination.page;   
                                p++;         
                                await this.#loadPage(p);
                            });
                        });

                    // Link to previous page.
                    $e.querySelectorAll("[data-pw-prev]")
                        .forEach($p=>
                        {
                            $p.addEventListener("click", async (e)=>
                            {
                                e.preventDefault();              
                                var p = this.options.pagination.page;  
                                p--;          
                                await this.#loadPage(p);
                            });
                        });
                });
        }

    
    /**
     * Herited method.
     * Place received data.
     */
        placeItems(data) {
            console.warn("Herited method placeItems(data) is not implemented in " + this.constructor.name );
        }

    
    /**
     * Herited method.
     * Clean selection.
     */
        cleanSelection() {
            console.warn("Herited method cleanSelection() is not implemented in " + this.constructor.name );
        }


    /**
     * Reload pagination with received params.
     * 
     * @param pagination 
     */
        #setPagination(pagination)
        {
            var $tmpl, $parent, $prev, $next, $tmp;
            const currentPage = this.options.pagination.page;

            if ( pagination.pages==0 ) {
                pagination.pages = 1;
            }

            this.options.pagination = pagination;
            this.options.pagination.page = currentPage;

            this.$content.querySelectorAll("[data-pw-pagination]")
                .forEach( $e=>
                {
                    $e.setAttribute("data-pw-pagination", this.options.pagination.pages);

                    $tmp = document.createElement("div");
                    $prev = $e.querySelector("[data-pw-prev]").parentNode.cloneNode(true);
                    $next = $e.querySelector("[data-pw-next]").parentNode.cloneNode(true);

                    $parent = $e.querySelector("[data-pw-page]").parentNode;
                    $parent.innerHTML = "";
                    $parent.appendChild($prev);
                    
                    for ( let i=1; i<=pagination.pages; i++ )
                    {
                        $tmpl = this.$tmplPagination.cloneNode(true);
                        $tmpl.querySelector("a").innerHTML = i;
                        $tmpl.querySelector("a").setAttribute("title", pagination.text.replace("%s", i));
                        $tmpl.setAttribute("data-pw-page", i);
                        
                        if( i==currentPage ) {
                            $tmpl.classList.add("active");
                        }

                        $parent.appendChild($tmpl);
                    }

                    $parent.appendChild($next);
                });      

            this.#observePagination();
        }


    /**
     * Force to reload page with filters.
     */
        reloadPage() 
        {
            const cpage = this.options.pagination.page;
            this.options.pagination.page = cpage-1;
            this.#loadPage(cpage);
        }


    /**
     * Load specific page.
     */
        async #loadPage(page) 
        {            
            if ( !this.working 
                && page 
                && page!=this.options.pagination.page 
                && page<=this.options.pagination.pages )
            {
                this.#setCurrentPage(page);
                this.cleanSelection();
                const r = await this.getPageItems();
                if ( r!=null && r.done ) 
                {
                    this.placeItems(r.data);
                    if ( r.pagination!=null ) {
                        this.#setPagination(r.pagination);
                    }

                    // Go to over placed items.
                    this.goOverItems();
                }
            } else {
                console.warn( page + " is invalid. Max : " + this.options.pagination.pages );
            }
        }


    /**
     * Scroll over items element.
     */
        goOverItems() {
            this.$content.querySelector("[data-pw-items] [data-pw-item]").scrollIntoView();
        }


    /**
     * Apply current page on paginations sys.
     * @param int page 
     */
        #setCurrentPage(page) 
        {
            if ( page<=0 ) {
                page = 1;
            }

            this.options.pagination.page = page;
            this.$content.querySelectorAll("[data-pw-pagination]")
                .forEach($p=>{
                    $p.querySelectorAll("[data-pw-page]")
                        .forEach( $e=>
                        {
                            if ( $e.dataset.pwPage==page) {
                                $e.classList.add("active");
                            } 
                            else {                                
                                $e.classList.remove("active");
                            }
                        });
                });
        }


    /**
     * Get items from server.
     */
        async getPageItems(url)
        {
            // Warn if bad implementation.
            if ( url==null && this.options.url.items==null ) {
                console.warn( this.constructor.name + 
                    " does not implement \"options.url.items\", needed to load items from server." );
            } 
            else if ( !this.working ) 
            {
                // Init url.
                if ( url==null ) {
                    url = this.options.url.items;
                }

                if ( url!=null )
                {
                    // Get data.
                    this.working = true;

                    const fd = new FormData(); 
                        fd.append("pagination", JSON.stringify(this.options.pagination) );
                        fd.append("orderBy", this.orderBySelected );
                        fd.append("filters", JSON.stringify(this.getFiltersValues()) );
                    const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
                    const r = await f.getJSON(fd);
                
                    if ( r!=null && r.done ) {
                        this.working = false;
                        return r;
                    }
				}
			}			
        }


    /**
     * Get filters values.
     * @returns 
     */
        getFiltersValues()
        {
            const values = {};

            this.$content.querySelectorAll("[data-pw-filter")
                .forEach( $i=>{
                    values[$i.name] = $i.value;
                });

            return values;
        }

            
    /**
     * Assign count to attribute.
     * 
     * @param {*} count 
     * @param {*} context 
     */
        assignCount(count, context) 
        {
            this.$content.querySelectorAll("[data-pw-" + context + "-count]")
                .forEach($e=>{
                    $e.setAttribute("data-pw-" + context + "-count", count);
                });
        }
        
		
	/**
	 * Apply a value on element.
	 * 
	 * @param $on 
	 * @param attribute 
	 * @param val 
	 */
		applyValue($on, attribute, val)
		{
			$on.querySelectorAll(attribute)
				.forEach($e=>{
					$e.innerHTML = val;
			});
		}
        

	/**
	 * Add/remove entry int array.
	 * 
	 * @param {*} getWhat 
	 * @param {*} getFrom 
	 * @param {*} removeIt True to remove, false to add. 
	 */
		addOrRemoveFrom(getWhat, getFrom, removeIt=false)
		{
			const i = getFrom.indexOf(getWhat);
			
			if (removeIt && i>-1) {
				getFrom.splice(i, 1);
			} else if ( !removeIt ) {
				getFrom.push(getWhat);
			}			
		}

			
	/**
	 * Remove entry from array.
	 * 
	 * @param {*} getWhat 
	 * @param {*} getFrom 
	 */
		removeFrom(getWhat, getFrom) {
			this.addOrRemoveFrom(getWhat, getFrom, true);
		}


	/**
	 * Add entry from array.
	 * 
	 * @param {*} removeWhat 
	 * @param {*} removeFrom 
	 */
		addIn(getWhat, getFrom) {
			this.addOrRemoveFrom(getWhat, getFrom, false);
		}

}