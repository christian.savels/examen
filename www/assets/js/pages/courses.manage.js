class PWECourses extends PWItems
{
	constructor(options) 
	{
		super(options);
		
		this.action;
		this.selectedItem = 0;
		this.$toolbar;
		this.$teachers;
		this.$students;
		this.$tmplUser;
		
		this.#init();
	}
	
	
	/**
	 *	Initialize.
	 */
		#init()
		{
			this.#initElements();
			this.#observe();
		}
	
		
	/**
	 *	Init elements.
	 */
		#initElements()
		{
			this.$toolbar = document.getElementById(this.options.toolbar);
			this.$teachers = document.body.querySelector("[data-pw-teachers]");
			this.$students = document.body.querySelector("[data-pw-students]");
			this.$tmplUser = PWE.getTemplate(this.options.templates.user).querySelector("[data-pw-user]");
			
			this.#optionsVisibility();
		}

		
	/**
	 *	Change options visiblility.
	 */
		#optionsVisibility() 
		{
			const disp = !this.selectedItem ? 'none' : 'block'; 
			this.$content.querySelectorAll('[data-pw-course-action]').forEach($e=>{
				$e.style.display = disp;
			});
		}
	
		
	/**
	 *	Get user template.
	 */
		#getTmplUser() {
			return this.$tmplUser.cloneNode(true);
		}
	
	
	/**
	 *	Observe pending elements.
	 */
		#observePending()
		{
			this.$content.querySelector("[data-pw-select-pending]")
				.addEventListener("click", e=>{
					this.#selectPending();
				});
			this.$content.querySelector("[data-pw-valid-pending]")
				.addEventListener("click", async e=>{
					const r = await this.#validPending(true);
					this.#removePending(r);
				});
			this.$content.querySelector("[data-pw-cancel-pending]")
				.addEventListener("click", async e=>{
					const r = await this.#validPending(false);
					this.#removePending(r);
				});
		}


	/**
	 * Remove pending rows.
	 * 
	 * @param {*} data 
	 */
		#removePending(data)
		{
			if ( data!=null )
			{
				var node;

				for ( let i in data )
				{
					node = this.$content
						.querySelector('tr[data-pw-pending="' + data[i] + '"');
					node.parentNode.removeChild(node);
				}
			}
		}

	
	/**
	 *	Select all pending students.
	 */
		async #validPending(valid=true) 
		{		
			if ( !this.working )
            {
				this.working = true;

				const form = document.getElementById("offcanvasPendingStudents").querySelector("form");
				const fd = new FormData(form);
					  fd.append("valid", valid);
                const f = new PWFetch(form.action, PWE.options.tokenName, 'json', 'POST');
                const r = await f.getJSON(fd);

                this.working = false;

                return r.data;
            }         
		}
	
		
	/**
	 *	Select all pending students.
	 */
		#selectPending() 
		{
			document.getElementById("offcanvasPendingStudents")
				.querySelector("form")
				.querySelectorAll("input")
				.forEach($e=>{
					$e.checked = true;
				});
		}

	
	/**
	 *	Observe some elements.
	 */
		#observeItems()
		{
			// Click on a course, load related users.
			this.$content.querySelectorAll("[data-pw-item]")
				.forEach( $e=>
				{
					$e.addEventListener("click", (e)=> 
					{
						const id = $e.getAttribute("data-pw-item");
						const old = this.selectedItem;

						if ( id!=old ) 
						{
							this.selectedItem = id;
							this.#switchselectedItem();
							this.#loadRelatedUsers();
							this.hideConfirm(true);
							this.assignCount(1, "selection");
							
							if ( !old ) {
								this.#optionsVisibility();
							}
						}
					});
				});	
		}

	
	/**
	 *	Observe some elements.
	 */
		#observe()
		{
			this.#observeItems();
			this.#observePending();
			
			// Click on course action
			this.$toolbar.querySelectorAll("[data-pw-course-action]")
				.forEach($e=>
				{
					const action = $e.dataset.pwAction;
									
					$e.addEventListener('click', async (e)=>
					{
						e.preventDefault();	

						var data;
						
						if ( !this.working )
						{
							if ( action=='add-student' || action=='add-teacher' ) 
							{
								data = await this.#getUsersTable(action);
								this.#showUsers(data.html, action); 
							}
							else if ( action=="mailformstudents" || action=="mailformteachers") 
							{
								data = await this.doCourseAction(action);
								if ( data.done && data.html!=null ) {
									this.#showMailInModal(data.html);
								}
							} 
							else
							{
								if ( $e.dataset.pwConfirm!=null && !this.working ) 
								{
									this.askConfirmAndDo({
										action: action, 
										msg: $e.dataset.pwConfirm, 
										callback: 'doCourseAction'
									});
								} else {
									this.doCourseAction(action);
								}
							}
						}
					});
				});
				
				// Search in items on the page.
				document.body.querySelectorAll("[data-pw-search]")
					.forEach($e=>{
						$e.addEventListener("input", (e)=>
						{
							switch($e.getAttribute("data-pw-search")) 
							{
								case 'people' : 
									this.#searchPeople($e.value.trim());
								break;
								case 'courses' : 
								 	this.#searchCourses($e.value.trim());
								break;
							}
						});
					});
												
				
					
		}


		
		
        #showMailInModal($html) 
        {
            const $h = document.createElement("div");
                  $h.classList.add("mail-to-form");
                  $h.innerHTML = $html;

            this.#observeMailFormSubmit($h);

            PWE.showModal(null, $h);
        }

		

        #observeMailFormSubmit($parent) 
        {
            $parent.querySelector('form').addEventListener("submit", async (e)=>
            {
                e.preventDefault();

				// Disable form fields/buttons.
				e.currentTarget.querySelectorAll("[pw-form-input],button")
					.forEach($e=>{
						$e.disabled = true;
					});

                const r = await this.#sendMail(
                    e.currentTarget.action,
                    $parent.querySelector('[pw-form-input][name="target"]').value,
                    $parent.querySelector('[pw-form-input][name="subject"]').value,
                    $parent.querySelector('[pw-form-input][name="content"]').value
                );
                
                if( r.done ) {
                    PWE.closeModal();
                }
            });                 
               
        }


	/**
	 * Send mail form.
	 * @param {*} url 
	 * @param {*} target 
	 * @param {*} subject 
	 * @param {*} body 
	 * @returns 
	 */
        async #sendMail(url, target, subject, body) 
        {
            if ( !this.working )
            {
                this.working = true;

                const fd = new FormData();
                      fd.append("course", this.selectedItem);
                      fd.append("target", target);
                      fd.append("subject", subject);
                      fd.append("content", body);
                const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
                const r = await f.getJSON(fd);

                this.working = false;

                return r;
            }            
        }

	
		
		
	/**
	 *	Search term in searchable elements.
	 */
		#searchCourses(term) {
			this.#searchInTree(this.$content.querySelectorAll("[data-pw-item]") , term);
		}
		
		
	/**
	 *	Search term in specific elements tree.
	 */		
		#searchInTree($tree, term)
		{
			$tree.forEach($e=>
			{
				var found = false;
				if ( term.length===0 ) {
					found = true;
				} 
				else
				{							
					$e.querySelectorAll("[data-pw-searchable]")
						.forEach($s=>
						{
							if ( !found ) 
							{										
								if ( $s.innerText.toLowerCase().search(term)>-1 ) {
									found = true;
								} else {
								}
							}
						});
				}	
				
				if (found) {
					$e.classList.remove("item-no-match");
				} else {
					$e.classList.add("item-no-match");
				}
			});
		}
		
		
	/**
	 *	Search term in searchable elements.
	 */
		#searchPeople(term)
		{
			const targets = [this.$teachers, this.$students];
			var $parent;
			
			for ( let t in targets )
			{
				$parent = targets[t];
				this.#searchInTree($parent.querySelectorAll("[data-pw-user]"), term);
			}
		}
		
		
		#showUsers(data, action)
		{
			const $usersTable = document.createElement("div");
		 	 	  $usersTable.classList.add("pw-selectable-users");
		  	 	  $usersTable.innerHTML = data; 
	  	 	const $btnFinish = document.createElement("button");
  	 	  		  $btnFinish.classList.add("btn", "btn-lg", "btn-outline-success", "text-end");
  	 	  		  $btnFinish.innerHTML = '<i class="fa-solid fa-list-ul fa-2x"></i>';
  	 	  		  
  	 	   // User selection
  	 	   $usersTable.querySelectorAll("tbody tr")
  	 	   		.forEach( $tr=>{
					$tr.querySelector("[data-pw-select]")
						.addEventListener("click", (e)=>{
							$tr.classList.toggle("table-success");
							$tr.classList.toggle("selected-user");
						});
				});
				
		   	// Finish selection
		    $btnFinish.addEventListener("click", async (e)=>
		    {
				if ( !this.working )
				{
					$btnFinish.disabled = true;
					
					const users = [];
					$usersTable.querySelectorAll("tr.selected-user")
						.forEach($tr=>{
							users.push(
								$tr.querySelector("[data-pw-userid]")
									.getAttribute("data-pw-userid")
							);
						});
						
					
					const r = await this.#addToRole(users, action);
					
					this.#placeUsers(r.data);
						
					PWE.closeModal();
				}
			});
		  	 
		   PWE.showModal(null, $usersTable, [$btnFinish]);
		}
		
		
	/**
	 *	Add selected users to group.
	 */
		async #addToRole(users, action) 
		{
			this.working = true;
			
			const url = this.options.url.enroleto;
			const fd = new FormData();
			  	  fd.append("course", this.selectedItem);
			  	  fd.append("action", action);
			  	  fd.append("users", JSON.stringify(users));
			const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
			const r = await f.getJSON(fd);
			
			if ( r.done ) {
				this.working = false;
			}
			
			return r;
		}
		
		
	/**
	 *	Get table of users.
	 */
		async #getUsersTable(context) 
		{
			this.working = true;
			
			const url = this.options.url.enrole;			
			const fd = new FormData();
			  	  fd.append("course", this.selectedItem);
			  	  fd.append("context", context);
			const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
			const r = await f.getJSON(fd);
			
			if ( r.done ) {
				this.working = false;
			}
			
			return r;
		}
		
		
	/**
	 * Get all selected users.
	  */
		#getSelectedUsers()
		{
			const data = [];			
			document.body.querySelectorAll('[data-pw-user][data-pw-selected="1"]')
				.forEach($e=>{
					data.push($e.getAttribute("data-pw-user"));
				});
						
			return data;
		}
		
	/**
	 * Do an user action.
	  
		async #doUsersAction(action)
		{			
			if ( !this.working && this.options.url[action]!=null )
			{
				this.working = true;
				
				const url = this.options.url[action];
				const fd = new FormData();
				  	  fd.append("role", this.selectedItem);
				  	  fd.append("users", JSON.stringify(this.#getSelectedUsers()) );
				const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
				const json = await f.getJSON(fd);
				
				if ( json.done ) 
				{
					this.#doAfter(action, this.selectedItem, json.data.data);
					this.working = false;
				}
			}			
		}
		*/
		
	/**
	 * Do an action related to the selected course.
	  */
		async doCourseAction(action)
		{									
			if ( this.options.url[action]!=null )
			{
				this.working = true;
				
				const url = this.options.url[action];
				const fd = new FormData();
				  	  fd.append("course", this.selectedItem);
				  	  fd.append("users", this.#getSelectedUsers());
				const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
				const r = await f.getJSON(fd);
				
				if ( r.done ) 
				{
					this.#doAfter(action, this.selectedItem, r.data);
					this.working = false;
				}

				return r;
			}			
		}
		
		
	/**
	 * Do something after action.
	  */
		#doAfter(action, uid, data)
		{ 
			var $e;
			
			switch(action)
			{
				case 'delete' : 
					$e = document.body.querySelector('[data-pw-courses] [data-pw-item="' + uid + '"]');
					if ( $e!=null ) {
						$e.parentNode.removeChild($e);
					}
					break;
				case 'clean' : 
				case 'enrole' : 
					this.#placeUsers(data);
					break;
				case 'unenrole' : 
					const c = document.body.querySelectorAll("[data-pw-user]").length;				
					this.assignCount(c-this.#removeSelectedUsers());
					break;
				default : break;
			}
		}
		

	/**
	 * Remove nodes related to the selelected users.
	 * @returns int Number of removed nodes.
	 */
		#removeSelectedUsers()
		{
			var c = 0;				
			document.body.querySelectorAll('[data-pw-user][data-pw-selected="1"]')
				.forEach($e=>{
					$e.parentNode.removeChild($e);
					c++;
				});

			return c;
		}
		
	/**
	 * Apply selected style on selected role.
	 */
		#switchselectedItem() 
		{
			this.$content.querySelectorAll("[data-pw-item]")
			  .forEach( $r=>{
				if ( $r.getAttribute("data-pw-item")==this.selectedItem ) {
					$r.classList.add("selected-course");
				} else {
					$r.classList.remove("selected-course");
				}
			});
		}
		
		
	/**
	 * Load users related to the selected role.
	 */
		async #loadRelatedUsers() 
		{
			const fd = new FormData();
				  fd.append("course", this.selectedItem);
			const f = new PWFetch(this.options.url.users, PWE.options.tokenName, 'json', 'POST');
			const json = await f.getJSON(fd);
				
			this.#placeUsers(json.data);
		}
		
				
	/**
	 * Place users realted to selected course.
	 */
		#placeUsers(data)
		{
			var count = 0;
			let $tmpl, avatar, $box, c;
			
			this.$teachers.innerHTML = "";
			this.$students.innerHTML = "";
			
			// Place users
			for ( let section in data)
			{
				$box = this['$'+section];	
				
				// Apply count on elements.
				document.body.querySelectorAll('[data-pw-' + section + '-count]')
					.forEach($e=>{
						$e.setAttribute('data-pw-' + section + '-count', data[section].length);
				});
				
				// Assign count
				c = data[section].length;
				count += c;
				this.assignCount(c, section);

				// Make users list.
				for ( let i in data[section] ) 
				{															
					$tmpl = this.#getTmplUser();
					 $tmpl.setAttribute("data-pw-user", data[section][i].id);
					 $tmpl.addEventListener("click", (e)=>
					 {
						const s = e.currentTarget.getAttribute("data-pw-selected");
						e.currentTarget.setAttribute("data-pw-selected", s==1 ? 0 : 1);	
						
						if ( s==1 ) {
							e.currentTarget.classList.remove("shadow");
						} else {
							e.currentTarget.classList.add("shadow");
						}
					 });
					
					for ( let ii in data[section][i] ) 
					{
						if ( ii=="address" || ii=="contact" ) 
						{
							for ( let s in data[section][i][ii] ) {
								this.#applyValue($tmpl, "[data-pw-" + s + "]", data[section][i][ii][s]);
							}
						}
						else {
							this.#applyValue($tmpl, "[data-pw-" + ii + "]", data[section][i][ii]);
						}
					}
					
					// Avatar
					$tmpl.querySelector("[data-pw-social-avatar]")
						.style.backgroundImage = "url('" + data[section][i].social.avatar +  "')"
					
					$box.appendChild($tmpl);
				}
			}
			
			this.assignCount(count, 'people');
		}
		
		
		#applyValue($on, attribute, val)
		{
			$on.querySelectorAll(attribute)
				.forEach($e=>{
					$e.innerHTML = val;
			});
		}


	/**
	 * Clean the selection (to no selection).
	 */
		cleanSelection() 
		{
			this.selectedItem = 0;
			this.selectedItems = [];
			
			this.assignCount(0, "selection");
			this.assignCount(0, "people");
			this.assignCount(1, "students");
			this.assignCount(1, "teachers");

			this.$content.querySelectorAll("[data-pw-teachers],[data-pw-students]")
				.forEach( $e=>{
					$e.innerHTML = "";
				});
		}


		
    /**
     * Place items received via the pagination system.
     * @param data 
     */
		placeItems(data)
		{
			const $parent = document.querySelector("[data-pw-courses] [data-pw-items]");
			var $tmpl = this.$tmplItem.cloneNode(true);
			var item;

			if ( $parent!=null )
			{
				$parent.innerHTML = "";
				
				for ( let i in data )
				{
					item = data[i];
					$tmpl = this.$tmplItem.cloneNode(true);
					 $tmpl.setAttribute("data-pw-item", item.id);
					 $tmpl.querySelectorAll("[data-pw-published-eval]")
					 	.forEach($e=>{
							$e.setAttribute("data-pw-published-eval", item.published);
						});

					for ( let k in item )
					{
						$tmpl.querySelectorAll("[data-pw-" + k + "]")
							.forEach( $e=> 
							{
								if ( k=="link" ) {
									$e.setAttribute("href", item[k]);
								} 
								else if ( k=="teacher" ) 
								{
									$e.setAttribute("data-pw-teacher", item[k]);
									$e.querySelector("[data-pw-teacherName]").innerHTML = item[k];
								}
								else  {
									$e.innerHTML = item[k];
								}
							});
						
					}

					$parent.appendChild($tmpl);
				}
			}

			this.#observeItems();
		}
		
	
}