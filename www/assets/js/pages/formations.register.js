class PWERegisterTo
{
    constructor(idForm)
    {
        this.$form = document.getElementById(idForm);
        this.#observe();
    }


    /**
     * Observe submit.
     */
        #observe()
        {
            this.$form.addEventListener("submit", e=>
            {
                e.preventDefault();

                this.$form.querySelectorAll('input[type="submit"]')
                    .forEach( $b=>{
                        $b.disabled = true;
                    });

                this.#sendRequest();
            });
        }


    /**
     * Send request to the server.
     */
        async #sendRequest() 
        {
            const url = this.$form.action;
            const fd = new FormData();
                fd.append("formation", this.$form.querySelector('input[name="formation"]').value);
            const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
            const r = await f.getJSON(fd);
        }
}