class PWFormDetails extends PWItems
{
    constructor(options)
    {
        super(options);
    }


    placeItems(data) 
    {
        const pref = "data-pw-";
        const $itemsBox = this.$content.querySelector("[data-pw-items]");
        var item, v, $tmpl;

        $itemsBox.innerHTML = "";

        for ( let i in data.courses ) 
        {
            item = data.courses[i];
            $tmpl = this.$tmplItem.cloneNode(true);
         
            for ( let k in item )
            {
                v = item[k];

                if ( k=="id" ) {
                    $tmpl.setAttribute(pref + "item", v);
                } 
               
                $tmpl.querySelectorAll("[" + pref + k + "]")
                        .forEach( $e=>
                        {
                            if ( k=="isStudent" || k=="isTeacher" ) {
                                $e.setAttribute(pref + k, v);
                            } 
                            else if ( k=="link" ) {
                                $e.setAttribute("href", v);
                            } 
                            else if ( k=="image" ) {
                                $e.style.backgroundImage = "url('" + v + "')";
                            } 
                            else {
                                $e.innerHTML = v;
                            }
                        });
                

                $itemsBox.appendChild($tmpl);
            }
        }

        this.assignCount(data.courses.length, 'items');
    }
}