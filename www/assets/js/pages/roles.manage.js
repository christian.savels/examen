class PWERoles extends PWItems
{
	constructor(options) 
	{
		super(options);

		this.options = options;
		this.selectedItem = 0;
		this.$users;
		this.$tmplUser;
		this.$toolbar;
		this.working = false;
		
		this.#init();
	}
	
	
	/**
	 *	Initialize.
	 */
		#init()
		{
			this.#initElements();
			this.#observe();
		}
	
		
	/**
	 *	Init elements.
	 */
		#initElements()
		{
			this.$toolbar = document.getElementById(this.options.toolbar);
			this.$users = this.$content.querySelector("[data-pw-users]");
			this.$tmplUser = PWE.getTemplate(this.options.templates.user).querySelector("[data-pw-user]");

			this.#optionsVisibility();
		}
	
		
	/**
	 *	Change options visiblility.
	 */
		#optionsVisibility() 
		{
			const r = this.selectedItem!=0 ? 0 : 1;
			const u = (r==1||this.#getSelectedUsers().length==0) ? 1 : 0;
			
			this.$toolbar.querySelectorAll("[data-pw-role-action]")
				.forEach($e=>{
					$e.setAttribute("data-disabled", r);
				});
			
			this.$toolbar.querySelectorAll("[data-pw-user-action]")
				.forEach($e=>{
					$e.setAttribute("data-disabled", u);
				});
		}
	
		
	/**
	 *	Get user template.
	 */
		#getTmplUser() {
			return this.$tmplUser.cloneNode(true);
		}
	
	
	/**
	 *	Observe some elements.
	 */
		#observe()
		{
			this.#observeItems();
			this.#observeActions();			
		}
		
	
	/**
	 *	Observe do action on...
	 */
		#observeActions()
		{		
			this.$toolbar.querySelectorAll("[data-pw-action]")
				.forEach($e=>
				{
					const action = $e.dataset.pwAction;
									
					$e.addEventListener('click', async (e)=>
					{
						e.preventDefault();	
						var data;
						var disabled = $e.getAttribute("data-disabled");
						
						if ( !this.working && disabled!=1 )
						{
							if ( action=='enrole' ) 
							{
								data = await this.getUsersTable();
								if ( data.done ) {
									this.showUsers(data);
								}
							}							
							else if ( action=="mailformroles") 
							{
								data = await this.doItemAction(action);
								if ( data.done && data.html!=null ) {
									this.#showMailInModal(data.html, data.title);
								}
							} 
							else
							{
								if ( $e.dataset.pwConfirm!=null && !this.working ) 
								{
									this.askConfirmAndDo({
										action: action, 
										msg: $e.dataset.pwConfirm, 
										callback: 'doItemAction'
									});
								} else {
									this.doItemAction(action);  
								}
							}
						}
					});
				});	
		}

		
	/**
	 * Show mail form in a modal.
	 * @param {*} $html 
	 */
		#showMailInModal($html, title) 
		{
			const $h = document.createElement("div");
				$h.classList.add("mail-to-form");
				$h.innerHTML = $html;

			this.#observeMailFormSubmit($h);

			PWE.showModal(title, $h);
		}

		
	/**
	 * Observe mailto form.
	 * 
	 * @param {*} $parent 
	 */
        #observeMailFormSubmit($parent) 
        {
			// Submit form
            $parent.querySelector('form').addEventListener("submit", async (e)=>
            {
                e.preventDefault();

				// Disable form fields/buttons.
				e.currentTarget.querySelectorAll("[pw-form-input],button")
					.forEach( $e=>{
						$e.disabled = true;
					});

                const r = await this.#sendMail(
                    e.currentTarget.action,
                    $parent.querySelector('[pw-form-input][name="target"]').value,
                    $parent.querySelector('[pw-form-input][name="subject"]').value,
                    $parent.querySelector('[pw-form-input][name="content"]').value
                );
                
                if( r.done ) {
                    PWE.closeModal();
                }
            }); 
			
			// Cancel mail
			$parent.querySelector('[data-pw-cancel]').addEventListener("click", async (e)=>
            {
                e.preventDefault();
				PWE.closeModal();
            });                 
        }


	/**
	 * Send mail form.
	 * 
	 * @param {*} url 
	 * @param {*} target 
	 * @param {*} subject 
	 * @param {*} body 
	 * @returns 
	 */
        async #sendMail(url, target, subject, body) 
        {
            if ( !this.working )
            {
                this.working = true;

                const fd = new FormData();
                      fd.append("role", this.selectedItem);
                      fd.append("target", target);
                      fd.append("subject", subject);
                      fd.append("content", body);
                const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
                const r = await f.getJSON(fd);

                this.working = false;

                return r;
            }            
        }

		

	/**
	 * Observe items events.
	 */
		#observeItems()
		{
			// Click on a role, load related users.
			this.$content.querySelectorAll("[data-pw-item]")
				.forEach( $e=>
				{
					$e.addEventListener("click", (e)=> 
					{
						const id = $e.getAttribute("data-pw-item");
						const old = this.selectedItem;
						
						if ( id!=old ) 
						{
							this.selectedItem = id;
							this.#switchSelectedRole(); 
							this.#loadRelatedUsers();
							this.hideConfirm(true);
							
							if ( !old ) {
								this.#optionsVisibility();
							}
						}
					})
				});	
		}


		
	/**
	 * Do an action related to the selected item.
	  */
		async doItemAction(action)
		{										
			if ( this.options.url[action]!=null )
			{
				this.working = true;
				
				const url = this.options.url[action];
				const fd = new FormData();
						fd.append("role", this.selectedItem);
						fd.append("users", JSON.stringify(this.#getSelectedUsers()));
				const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
				const r = await f.getJSON(fd);
				
				if ( r.done ) {
					this.#doAfter(action, this.selectedItem, r.data);
				}

				this.working = false;

				return r;
			}			
		}
	
		
	/**
	 * Show users list into modal.
	 * 
	 * @param {*} data 
	 */
		showUsers(data)
		{
			let pwul, $usersTable, $btnCancel, $btnFinish;

			// Init modal after opening.
			if ( data.html!=null )
			{
				$usersTable = document.createElement("div");
				 $usersTable.classList.add("pw-selectable-users");
				 $usersTable.innerHTML = data.html; 

				$btnCancel = document.createElement("button");
				 $btnCancel.classList.add("btn", "btn-lg", "btn-outline-danger", "text-end");
				 $btnCancel.innerHTML = '<i class="fa-solid fa-x fa-2x"></i>';
			
				$btnFinish = document.createElement("button");
				 $btnFinish.classList.add("btn", "btn-lg", "btn-outline-success", "text-end");
				 $btnFinish.innerHTML = '<i class="fa-solid fa-list-ul fa-2x"></i>';
						
				// Cancel selection
				$btnCancel.addEventListener("click", async (e)=>
				{
					PWE.closeModal();
					pwul = null;
				});

				// Finish selection
				$btnFinish.addEventListener("click", async (e)=>
				{
					if ( !this.working )
					{
						$btnFinish.disabled = true;
						this.working = true;
						
						const users = [];
						$usersTable.querySelectorAll("tr.selected-user")
							.forEach($tr=>{
								users.push(
									$tr.querySelector("[data-pw-userid]")
										.getAttribute("data-pw-userid")
								);
							});
						
						const r = await this.#addToRole(users);
						this.#placeUsers(r.data);
							
						PWE.closeModal();
						pwul = null;
					}
				});

				// Pagination - Filters - OrderBy			
				let o = this.options;
					o.url.items = this.options.url.enrole + '/' + this.selectedItem;
					o.pagination = data.pagination;
					o.content.id = PWE.getModalID();

				// Show modal
				PWE.showModal(null, $usersTable, [$btnFinish, $btnCancel]);		
						
				//pwul = new PWUsersList(o, $usersTable); // Laisser en dernier.
			}
			// Init after pagination.
			else {
				console.log("::: Content was already prepared.");
			}
  	 	  	
  	 	    // User selection
  	 	    $usersTable.querySelectorAll("tbody tr")
  	 	   		.forEach( $tr=>{
					$tr.querySelector("[data-pw-select]")
						.addEventListener("click", (e)=>{
							$tr.classList.toggle("table-success");
							$tr.classList.toggle("selected-user");
						});
				});
			


		  	 
		   
		}



		
		
	/**
	 *	Add selected users to group.
	 */
		async #addToRole(users) 
		{
			const url = this.options.url.enroleto;
			const fd = new FormData();
			  	  fd.append("role", this.selectedItem);
			  	  fd.append("users", JSON.stringify(users));
			const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
			const r = await f.getJSON(fd);
			
			this.working = false;
			
			return r;
		}
		
		
	/**
	 *	Get table of users.
	 */
		async getUsersTable() 
		{
			const url = this.options.url.enrole;
			const fd = new FormData();
			  	  fd.append("role", this.selectedItem);
			const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
			const r = await f.getJSON(fd);
			
			this.working = false;
			
			return r;
		}
		
		
	/**
	 * Get array of selected users (id).
	 * @returns []
	 */
		#getSelectedUsers()
		{
			const data = [];
			
			this.$users.querySelectorAll('[data-pw-user][data-pw-selected="1"]')
				.forEach($e=>{
					data.push($e.getAttribute("data-pw-user"));
				});
							
			return data;
		}
		
	/**
	 * Do an user action.
	 
		async #doUsersAction(action)
		{			
			if ( !this.working && this.options.url[action]!=null )
			{
				this.working = true;
				
				const url = this.options.url[action];
				const fd = new FormData();
				  	  fd.append("role", this.selectedItem);
				  	  fd.append("users", JSON.stringify(this.#getSelectedUsers()) );
				const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
				const json = await f.getJSON(fd);
				
				if ( json.done ) {
					this.#doAfter(action, this.selectedItem, json.data);
				}

				this.working = false;
			}			
		} */
		
		/*
		async #doRoleAction(action)
		{
			if ( !this.working && this.options.url[action]!=null )
			{
				this.working = true;
				
				const url = this.options.url[action];
				const fd = new FormData();
				  	  fd.append("role", this.selectedItem);
				const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
				const json = await f.getJSON(fd);
				
				if ( json.done ) {
					this.#doAfter(action, this.selectedItem);
				}

				this.working = false;
			}			
		}*/
		
		
	/**
	 * Do something after action.
	  */
		#doAfter(action, uid, data)
		{
			var $e;
			
			switch(action)
			{
				case 'delete' : 
					$e = this.$content.querySelectorAll('[data-pw-items] [data-pw-item="' + uid + '"]')
					if ( $e!=null ) {
						$e.parentNode.removeChild($e);
					}
					break;
				case 'clean' : 
					this.$users.innerHTML = "";
					this.#assignCountUsers(0);
					break;
				case 'enrole' : 
					this.#placeUsers(json.data);
					break;
				case 'unenrole' : 
					var c = this.$content.querySelectorAll("[data-pw-user]").length;				
					this.$users.querySelectorAll('[data-pw-user][data-pw-selected="1"]')
						.forEach($e=>{
							$e.parentNode.removeChild($e);
							c = c-1;
						});
					this.#assignCountUsers(c);
					break;
				default : break;
			}
		}
		
		
	/**
	 * Apply selected style on selected role.
	 */
		#switchSelectedRole() 
		{
			this.$content.querySelectorAll("[data-pw-items] [data-pw-item").forEach( $r=>{
				if ( $r.getAttribute("data-pw-item")==this.selectedItem ) {
					$r.classList.add("selected-role");
				} else {
					$r.classList.remove("selected-role");
				}
			});
		}
		
		
	/**
	 * Load users related to the selected role.
	 */
		async #loadRelatedUsers() 
		{
			const fd = new FormData();
				  fd.append("role", this.selectedItem);
			const f = new PWFetch(this.options.url.users, PWE.options.tokenName, 'json', 'POST');
			const json = await f.getJSON(fd);
				
			this.#placeUsers(json.data); 
		}
		
		
	/**
	 * Assign count
	 * 
	 * @param {*} count 
	 */
		#assignCountUsers(count) 
		{
			this.$content.querySelectorAll("[data-pw-users-count]")
				.forEach($e=>{
					$e.setAttribute("data-pw-users-count", count);
				});
		}
		
		
	/**
	 * Place users related to the selected role.
	 */
		#placeUsers(data)
		{
			this.$users.innerHTML = "";			
			const count = data==null ? 0 : data.length;
			let $tmpl, avatar;
			
			// Assign count
			this.#assignCountUsers(count);
			
			// Place users
			for ( let i in data)
			{
				$tmpl = this.#getTmplUser();
				 $tmpl.setAttribute("data-pw-user", data[i].id);
				 $tmpl.addEventListener("click", (e)=>
				 {
					const s = e.currentTarget.getAttribute("data-pw-selected");
					e.currentTarget.setAttribute("data-pw-selected", s==1 ? 0 : 1);	
					this.#optionsVisibility();
				});
				
				for ( let ii in data[i] ) 
				{
					if ( ii=="address" || ii=="contact" ) 
					{
						for ( let s in data[i][ii] ) {
							this.#applyValue($tmpl, "[data-pw-" + s + "]", data[i][ii][s]);
						}
					}
					else {
						this.#applyValue($tmpl, "[data-pw-" + ii + "]", data[i][ii]);
					}
				}
				
				// Avatar
				$tmpl.querySelector("[data-pw-social-avatar]")
					.style.backgroundImage = "url('" + data[i].social.avatar +  "')"
				
				this.$users.appendChild($tmpl);
			}

			// Go to users 
			this.$content.querySelector("[data-pw-users-part]").scrollIntoView();
		}
		
		
		#applyValue($on, attribute, val)
		{
			$on.querySelectorAll(attribute)
				.forEach($e=>{
					$e.innerHTML = val;
			});
		}
		
	
	/**
	 * Place items received from pagination.
	 * 
	 * @param {*} data 
	 */
		placeItems(data)
		{			
			const $parent = this.$content.querySelector("[data-pw-items]");
			let $tmpl, item;

			// Clear old selection & related users.
			this.$content.querySelector("[data-pw-users]").innerHTML = "";
			this.selectedItem = 0;
			this.#optionsVisibility();

			if ( $parent!=null )
			{
				$parent.innerHTML = "";

				for ( var i in data )
				{
					item = data[i];
					$tmpl = this.$tmplItem.cloneNode(true);
					
					for ( var k in data[i] )
					{
						if ( k=="link")
						{
							$tmpl.querySelectorAll("[data-pw-link]")
								.forEach($e=>{
									$e.setAttribute("href", data[i][k]);
								});
						}
						else if ( k=="id") {
							$tmpl.setAttribute("data-pw-item", data[i][k]);
						}
						else if ( k=="enrolable") 
						{
							$tmpl.querySelectorAll("[data-pw-enrolable]")
								.forEach($e=>{
									$e.setAttribute("data-pw-enrolable", data[i][k]);
								});
						}
						else 
						{
							$tmpl.querySelectorAll("[data-pw-" + k + "]")
								.forEach($e=>{
									$e.innerHTML = data[i][k];
								});
						}
					}

					$parent.appendChild($tmpl);
				}

				this.#observeItems();
			}			
		}
}