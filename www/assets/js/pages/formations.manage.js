class PWEForm extends PWItems
{
	constructor(options) 
	{
		super(options);
		
		this.action;
		this.selectedItem = 0;
		this.selectedCourses = [];
		this.selectedStudents = [];
		this.$toolbar;
		this.$courses;
		this.$tmplCourse;
		this.$tmplUser;
		
		this.#init();
	}
	
	
	/**
	 *	Initialize.
	 */
		#init()
		{
			this.#initElements();
			this.#observe();
		}
	
		
	/**
	 *	Init elements.
	 */
		#initElements()
		{
			this.$toolbar = document.getElementById(this.options.toolbar);
			this.$courses = document.body.querySelector("[data-pw-courses]"); 
			this.$tmplCourse = document.body.querySelector("[data-pw-course]").cloneNode(true);
			this.$tmplUser = PWE.getTemplate(this.options.templates.user).querySelector("[data-pw-user]");

			this.#optionsVisibility();
		}


		
	/**
	 *	Observe pending elements.
	 */
		#observePending()
		{
			this.$content.querySelector("[data-pw-select-pending]")
				.addEventListener("click", e=>{
					this.#selectPending();
				});
			this.$content.querySelector("[data-pw-valid-pending]")
				.addEventListener("click", async e=>{
					const r = await this.#validPending(true);
					this.#removePending(r);
				});
			this.$content.querySelector("[data-pw-cancel-pending]")
				.addEventListener("click", async e=>{
					const r = await this.#validPending(false);
					this.#removePending(r);
				});
		}


	/**
	 * Remove pending rows.
	 * 
	 * @param {*} data 
	 */
		#removePending(data)
		{
			if ( data!=null )
			{
				var node;

				for ( let i in data )
				{
					node = this.$content
						.querySelector('tr[data-pw-pending="' + data[i] + '"');
					node.parentNode.removeChild(node);
				}
			}
		}

		
	/**
	 *	Select all pending students.
	*/
		async #validPending(valid=true) 
		{		
			if ( !this.working )
			{
				this.working = true;

				const form = document.getElementById("offcanvasPendingStudents").querySelector("form");
				const fd = new FormData(form);
					  fd.append("valid", valid);
				const f = new PWFetch(form.action, PWE.options.tokenName, 'json', 'POST');
				const r = await f.getJSON(fd);

				this.working = false;

				return r.data;
			}         
		}
		
	
	/**
	 *	Select all pending students.
	*/
		#selectPending() 
		{
			document.getElementById("offcanvasPendingStudents")
				.querySelector("form")
				.querySelectorAll("input")
				.forEach($e=>{
					$e.checked = true;
				});
		}

		
	/**
	 *	Change options visiblility.
	 */
		#optionsVisibility() 
		{
			var disp, count;

			// Hide all
			if ( !this.selectedItem )
			{
				document.body.querySelectorAll('[data-pw-f-action],[data-pw-user-action],[data-pw-course-action],[data-pw-pending-action]')
					.forEach($e=>{
						//$e.style.visibility = disp;
						$e.setAttribute("data-disabled", 1);
					});
			}
			else
			{
				// Formation action
				document.body.querySelectorAll('[data-pw-f-action]')
					.forEach($e=>{
						//$e.style.visibility = 'visible';
						$e.setAttribute("data-disabled", 0);
					});

				// Courses actions
				disp = !this.selectedCourses.length ? 1 : 0; 
				document.body.querySelectorAll('[data-pw-course-action]')
					.forEach($e=>{
						//$e.style.visibility = disp;
						$e.setAttribute("data-disabled", disp);
					});

				// Students actions
				disp = !this.selectedStudents.length ? 1 : 0; 
				document.body.querySelectorAll('[data-pw-user-action]')
					.forEach($e=>{
						//$e.style.visibility = disp; 
						$e.setAttribute("data-disabled", disp);
					});

				// Students pending
				disp = document.body.querySelectorAll('[data-pw-is-pending="1"]').length ? 0 : 1;
				document.body.querySelectorAll('[data-pw-pending-action]')
						.forEach($e=>{
							$e.setAttribute("data-disabled", disp);
						});
			}

			if ( !this.selectedItem ) {
				this.hideConfirm(true);
			}
		}
	
		
	/**
	 *	Get course template.
	 */
		#getTmplCourse() {
			return this.$tmplCourse.cloneNode(true);
		}
	
	
	/**
	 *	Observe some elements.
	 */
		#observeItems()
		{
			// Click on a formation, load related courses.
			document.body.querySelectorAll("main [data-pw-item]")
				.forEach( $e=>
				{
					$e.addEventListener("click", (e)=> 
					{
						const id = $e.getAttribute("data-pw-item");
						const old = this.selectedItem;

						if ( id!=old ) 
						{
							this.selectedItem = id;
							this.#switchselectedItem(); 
							this.#loadRelatedData();
							this.hideConfirm(true);
							this.#optionsVisibility();
						}
					});
				});
		}

	
	/**
	 *	Observe some elements.
	 */
		#observe()
		{
			this.#observeItems();
			this.#observePending();
			
			// Click on formation action
			this.$toolbar.querySelectorAll("[data-pw-action]")
				.forEach($e=>
				{
					const action = $e.dataset.pwAction;
									
					$e.addEventListener('click', async (e)=>
					{
						e.preventDefault();	
						var data;
						var disabled = $e.getAttribute("data-disabled");
						
						if ( !this.working && disabled!=1 )
						{
							if ( action=='enrole' ) 
							{
								data = await this.#getUsersTable();
								this.#showUsers(data.html); 
							}
							else if ( action=='enrolecourse' ) 
							{
								data = await this.#getCoursesTable();
								this.#showCourses(data.html, action); 
							}
							else if ( action=="mailformstudents" || action=="mailformteachers") 
							{
								data = await this.doItemAction(action);
								if ( data.done && data.html!=null ) {
									this.#showMailInModal(data.html);
								}
							} 
							else
							{
								if ( $e.dataset.pwConfirm!=null ) 
								{
									this.askConfirmAndDo({
										action: action, 
										msg: $e.dataset.pwConfirm, 
										callback: 'doItemAction'
									});
								} else {
									this.doItemAction(action); 
								}
							}
						}
					});
				});	
		}


	/**
	 * Show mail form in a modal.
	 * @param {*} $html 
	 */
        #showMailInModal($html) 
        {
            const $h = document.createElement("div");
                  $h.classList.add("mail-to-form");
                  $h.innerHTML = $html;

            this.#observeMailFormSubmit($h);

            PWE.showModal(null, $h);
        }

		

	/**
	 * Observe form to submit email to send.
	 * 
	 * @param {*} $parent 
	 */
        #observeMailFormSubmit($parent) 
        {
            $parent.querySelector('form').addEventListener("submit", async (e)=>
            {
                e.preventDefault();

				// Disable form fields/buttons.
				e.currentTarget.querySelectorAll("[pw-form-input],button")
					.forEach($e=>{
						$e.disabled = true;
					});

                const r = await this.#sendMail(
                    e.currentTarget.action,
                    $parent.querySelector('[pw-form-input][name="target"]').value,
                    $parent.querySelector('[pw-form-input][name="subject"]').value,
                    $parent.querySelector('[pw-form-input][name="content"]').value
                );
                
                if( r.done ) {
                    PWE.closeModal();
                }
            });                 
        }


	/**
	 * Send mail form.
	 * 
	 * @param {*} url 
	 * @param {*} target 
	 * @param {*} subject 
	 * @param {*} body 
	 * @returns 
	 */
        async #sendMail(url, target, subject, body) 
        {
            if ( !this.working )
            {
                this.working = true;

                const fd = new FormData();
                      fd.append("formation", this.selectedItem);
                      fd.append("target", target);
                      fd.append("subject", subject);
                      fd.append("content", body);
                const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
                const r = await f.getJSON(fd);

                this.working = false;

                return r;
            }            
        }

	
	/**
	 * Show the users table to select them.
	 * 
	 * @param {*} data 
	 */
		#showUsers(data)
		{
			const $usersTable = document.createElement("div");
		 	 	  $usersTable.classList.add("pw-selectable-users");
		  	 	  $usersTable.innerHTML = data; 
	  	 	const $btnFinish = document.createElement("button");
  	 	  		  $btnFinish.classList.add("btn", "btn-lg", "btn-outline-success", "text-end");
  	 	  		  $btnFinish.innerHTML = '<i class="fa-solid fa-list-ul fa-2x"></i>';
  	 	  		  
  	 	   // User selection
  	 	   $usersTable.querySelectorAll("tbody tr")
  	 	   		.forEach( $tr=>{
					$tr.querySelector("[data-pw-select]")
						.addEventListener("click", (e)=>{
							$tr.classList.toggle("table-success");
							$tr.classList.toggle("selected-user");
						});
				});
				
		   	// Finish selection
		    $btnFinish.addEventListener("click", async (e)=>
		    {
				if ( !this.working )
				{
					$btnFinish.disabled = true;
					this.working = true;
					
					const users = [];
					$usersTable.querySelectorAll("tr.selected-user")
						.forEach($tr=>{
							users.push(
								$tr.querySelector("[data-pw-userid]")
									.getAttribute("data-pw-userid")
							);
						});
					
					const r = await this.#addToFormation(users);
					this.#placeUsers(r.data.users);
					this.#placeCourses(r.data.courses);
					this.#assignCountCourses(r.data.pending, "users-pending", true);
						
					PWE.closeModal();
				}
			});
		  	 
		   PWE.showModal(null, $usersTable, [$btnFinish]);
		}


			
	/**
	 *	Register selected users to the formation.
	 */
		async #addToFormation(users) 
		{
			const url = this.options.url.enroleto;
			const fd = new FormData();
					fd.append("formation", this.selectedItem);
					fd.append("users", JSON.stringify(users));
			const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
			const r = await f.getJSON(fd);
			
			if ( r.done ) {
				this.working = false;
			}
			
			return r;
		}
		
		
	/**
	 *	Get table of users.
	 */
		async #getUsersTable() 
		{
			const url = this.options.url.enrole;
			const fd = new FormData();
				fd.append("formation", this.selectedItem);
			const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
			const r = await f.getJSON(fd);
			
			if ( r.done ) {
				this.working = false;
			}
			
			return r;
		}

		
	/**
	 * Show courses to add to the selected formation.
	 * 
	 * @param {*} data 
	 * @param {*} action 
	 */
		#showCourses(data, action)
		{
			const $coursesTable = document.createElement("div");
		 	 	  $coursesTable.classList.add("pw-selectable-courses");
		  	 	  $coursesTable.innerHTML = data; 
	  	 	const $btnFinish = document.createElement("button");
  	 	  		  $btnFinish.classList.add("btn", "btn-lg", "btn-outline-success", "text-end");
  	 	  		  $btnFinish.innerHTML = '<i class="fa-solid fa-list-ul fa-2x"></i>';
  	 	  		  
  	 	   // Course selection
  	 	   $coursesTable.querySelectorAll("tbody tr")
  	 	   		.forEach( $tr=>{
					$tr.querySelector("[data-pw-select]")
						.addEventListener("click", (e)=>{
							$tr.classList.toggle("table-success");
							$tr.classList.toggle("selected-course");
						});
				});
				
		   	// Finish selection
		    $btnFinish.addEventListener("click", async (e)=>
		    {
				if ( !this.working )
				{
					$btnFinish.disabled = true;
					
					const courses = [];
					$coursesTable.querySelectorAll("tr.selected-course")
						.forEach($tr=>{
							courses.push(
								$tr.querySelector("[data-pw-courseid]")
									.getAttribute("data-pw-courseid")
							);
						});
						
					
					const r = await this.#addToItem(courses, action);
					
					this.#placeUsers(r.data.users);
					this.#placeCourses(r.data.courses);
					this.#assignCountCourses(r.data.pending, "users-pending");
						
					PWE.closeModal();
				}
			});
		  	 
		   PWE.showModal(null, $coursesTable, [$btnFinish]);
		}
		
		
	/**
	 *	Add selected courses to formation.
	 */
		async #addToItem(courses, action) 
		{
			this.working = true;
			
			const url = this.options.url.enrolecourseto;
			const fd = new FormData();
			  	  fd.append("formation", this.selectedItem);
			  	  fd.append("courses", JSON.stringify(courses));
			const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
			const r = await f.getJSON(fd);
			
			if ( r.done ) {
				this.working = false;
			}
			
			return r;
		}
		
		
	/**
	 *	Get table of courses.
	 */
		async #getCoursesTable(context) 
		{
			this.working = true;
			
			const url = this.options.url.enrolecourse;			
			const fd = new FormData();
			  	  fd.append("formation", this.selectedItem);
			const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
			const r = await f.getJSON(fd);
			
			if ( r.done ) {
				this.working = false;
			}
			
			return r;
		}
		
		
	/**
	 * Do an action related to the selected item.
	  */
		async doItemAction(action)
		{									
			if ( this.options.url[action]!=null )
			{
				this.working = true;
				
				const url = this.options.url[action];
				const fd = new FormData();
				  	  fd.append("formation", this.selectedItem);
				  	  fd.append("courses", JSON.stringify(this.selectedCourses));
				  	  fd.append("users", JSON.stringify(this.selectedStudents));
				const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
				const r = await f.getJSON(fd);
				
				if ( r.done ) {
					this.#doAfter(action, this.selectedItem, r.data);
				}

				this.working = false;

				return r;
			}			
		}
		
		
	/**
	 * Do something after action.
	  */
		#doAfter(action, uid, data)
		{ 
			var $e;
			 
			switch(action)
			{
				case 'delete' : 
					$e = document.body.querySelector('[data-pw-formations] [data-pw-item="' + uid + '"]');
					if ( $e!=null ) {
						$e.parentNode.removeChild($e);
					}
					break;
				case 'validatePending' : 
				case 'cancelPending' : 
					this.#loadRelatedData();
					break;
				case 'cleanstudents' : 
				case 'cleancourses' : 
				case 'enrole' : 
				case 'unenrole' : 
				case 'unenrolecourse' : 
					if ( data!=null ) {
						this.#placeCourses(data.courses); 
						this.#placeUsers(data.users);
					}

					//const c = document.body.querySelectorAll("[data-pw-course]").length;				
					//this.#assignCountCourses(c-this.#removeSelectedCourses());
					break;		
			//	case 'unenrole' : 
					//const u = document.body.querySelectorAll("[data-pw-user]").length;				
					//this.#assignCount(c-this.#removeSelectedCourses());
					//break;
				default : break;
			}
		}
		

	/**
	 * Swith selection.
	 */
		#switchselectedItem() 
		{
			document.body.querySelectorAll("[data-pw-formations] [data-pw-item]")
				.forEach( $r=>{
					if ( $r.getAttribute("data-pw-item")==this.selectedItem ) {
						$r.classList.add("selected-formation");
					} else {
						$r.classList.remove("selected-formation");
					}
				});

			this.selectedCourses.length = 0;
			this.selectedStudents.length = 0;

			document.body.querySelectorAll("[data-pw-teachers],[data-pw-students]")
				.forEach($e=>{
					$e.innerHTML = ""
				});  
		}
		
		
	/**
	 * Load courses related to the selected item.
	 */
		async #loadRelatedData() 
		{
			const fd = new FormData();
				  fd.append("formation", this.selectedItem);
			const f = new PWFetch(this.options.url.courses, PWE.options.tokenName, 'json', 'POST');
			const json = await f.getJSON(fd);
				
			this.#placeUsers(json.data.users);
			this.#placeCourses(json.data.courses);
			this.#assignCountCourses(json.data.pending, "users-pending");
		}
		
		
	/**
	 * Assign count of courses.
	 * 
	 * @param {*} count 
	 * @param {*} context 
	 */
		#assignCountCourses(count, context, inner=false) 
		{
			const ident = "[data-pw-" + context + "-count]";
			// Assign count
			document.body.querySelectorAll(ident)
				.forEach(
					$e=> {
						$e.setAttribute("data-pw-" + context + "-count", count);
						if (inner) {
							$e.innerHTML = count;
						}
					}
				);
		}
		
		
	/**
	 * Place courses related to the selected formation.
	 */
		#placeCourses(data)
		{
			let $tmpl;
			
			this.$courses.innerHTML = "";

			// Make courses list.
			for ( let i in data ) 
			{															
				$tmpl = this.#getTmplCourse();
				 $tmpl.setAttribute("data-pw-course", data[i].id);
				 $tmpl.setAttribute("data-pw-selected", 0);
				
				// Observe it.
				this.#observeCourse($tmpl);
				
				for ( let ii in data[i] ) {
					this.applyValue($tmpl, "[data-pw-" + ii + "]", data[i][ii]);
				}
				
				this.$courses.appendChild($tmpl);
			}
			
			// Apply count on elements.
			this.#assignCountCourses(data.length, 'courses');
			this.#assignCountCourses(data.length, "tab-courses", true); 
		}


	/**
	 * Observe click on course node.
	 * 
	 * @param {*} $o 
	 */
		#observeCourse($o)
		{ 
			$o.addEventListener("click", (e)=>
			{
				const id = e.currentTarget.getAttribute("data-pw-course");
				const s = e.currentTarget.getAttribute("data-pw-selected");

				e.currentTarget.setAttribute("data-pw-selected", s==1 ? 0 : 1);	
			
				// Record selection.
				if ( s==1 ) {
					this.removeFrom(id, this.selectedCourses);
				} else {
					this.addIn(id, this.selectedCourses);
				}

				// Check visibility of options related to the selected course(s).
				this.#optionsVisibility();
			});
		}


	/**
	 * Clean selection.
	 */
		cleanSelection()
		{
			this.$courses.innerHTML = "";  
			this.selectedItem = 0;

			this.#assignCountCourses(1, 'courses'); 			// Showed if value=0;
			this.#assignCountCourses(1, 'students'); 			// Showed if value=0;
			this.#assignCountCourses(1, 'teachers'); 			// Showed if value=0;
			this.#assignCountCourses(0, 'tab-teachers', true);	// Showed if value!=0;
			this.#assignCountCourses(0, 'tab-students', true);	// Showed if value!=0;
			this.#assignCountCourses(0, 'tab-courses', true);	// Showed if value!=0;

			this.#switchselectedItem();
			this.#optionsVisibility();
		}

		
    /**
     * Place items received via the pagination system.
     * @param data 
     */
		placeItems(data)
		{
			const $parent = document.querySelector("[data-pw-formations] [data-pw-items]");
			var $tmpl = this.$tmplItem.cloneNode(true);
			var item;

			// Clean params related to previous selection.
			this.cleanSelection();
						
			// Place items.
			if ( $parent!=null )
			{
				$parent.innerHTML = "";
				
				for ( let i in data )
				{
					item = data[i];
					$tmpl = this.$tmplItem.cloneNode(true);
					 $tmpl.setAttribute("data-pw-item", item.id);
					 $tmpl.querySelectorAll("[data-pw-published-eval]")
					 	.forEach($e=>{
							$e.setAttribute("data-pw-published-eval", item.published);
						});

					for ( let k in item )
					{
						$tmpl.querySelectorAll("[data-pw-" + k)
							.forEach( $e=> {
								$e.innerHTML = item[k];
							});
					}

					// Edit url
					$tmpl.querySelectorAll("a[data-pw-link-edit]")
						.forEach( $e=>{
							$e.setAttribute("href", item.editLink)
						});
					$parent.appendChild($tmpl);
				}
			}

			this.#observeItems();
		}

		
	/**
	 * Place users realated to selected course.
	 */
		#placeUsers(data)
		{
			const sections = ['students', 'teachers'];
			var $tmpl, $parent, section;
			
			for ( let index in sections )
			{
				section = sections[index];
				$parent = document.body.querySelector("[data-pw-" + section + "]");
				 $parent.innerHTML = "";
				
				// Apply count on elements.
				document.body.querySelectorAll('[data-pw-tab-' + section + '-count]')
					.forEach($e=>{
						$e.innerHTML = data[section].length;
						$e.setAttribute('data-pw-tab-' + section + '-count', data[section].length);
				});
				this.#assignCountCourses(data[section].length, section);
				
				// Make users list.
				for ( let i in data[section] ) 
				{															
					$tmpl = this.#getTmplUser();
					$tmpl.setAttribute("data-pw-user", data[section][i].id);
					$tmpl.setAttribute("data-pw-is-pending", data[section][i].pending);
					
					// Observe
					this.#observeUser($tmpl);
					
					for ( let ii in data[section][i] ) 
					{
						if ( ii=="address" || ii=="contact" ) 
						{
							for ( let s in data[section][i][ii] ) {
								this.applyValue($tmpl, "[data-pw-" + s + "]", data[section][i][ii][s]);
							}
						}
						else {
							this.applyValue($tmpl, "[data-pw-" + ii + "]", data[section][i][ii]);
						}
					}
					
					// Avatar
					$tmpl.querySelector("[data-pw-social-avatar]")
						.style.backgroundImage = "url('" + data[section][i].social.avatar +  "')"
					
					$parent.appendChild($tmpl);
				}			
			}			
		}


		#observeUser($o)
		{
			$o.addEventListener("click", (e)=>
			{
				const id = e.currentTarget.getAttribute("data-pw-user");
				const s = e.currentTarget.getAttribute("data-pw-selected");
				e.currentTarget.setAttribute("data-pw-selected", s==1 ? 0 : 1);	
				
				if ( s==1 ) {
					e.currentTarget.classList.remove("shadow");
					this.removeFrom(id, this.selectedStudents);
				} else {
					e.currentTarget.classList.add("shadow");
					this.addIn(id, this.selectedStudents);
				}

				this.#optionsVisibility();
			});
		}

		
	/**
	 *	Get user template.
	 */
	 #getTmplUser() {
		return this.$tmplUser.cloneNode(true);
	}
		

}