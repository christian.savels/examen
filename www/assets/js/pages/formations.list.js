class PWEFormations extends PWItems
{
    constructor(options)
    {
        super(options)
        this.#init();
    }

    #init()
    {

    }


    /**
     * Place items received via the pagination system.
     * @param data 
     */
        placeItems(data)
        {
            const $parent = document.querySelector("[data-pw-items]");
            var $tmpl, item;
            
            document.body.querySelectorAll("[data-pw-courses-count]")
                .forEach($e=>{
                    $e.setAttribute("data-pw-courses-count", data.length);
                });

            if ( $parent!=null )
            {
                $parent.innerHTML = "";
                
                for ( let i in data )
                {
                    item = data[i];
                    $tmpl = this.$tmplItem.cloneNode(true);

                    for ( let k in item )
                    {
                        $tmpl.setAttribute("data-pw-item", item.id);

                        if ( k=="link") 
                        {
                            $tmpl.querySelectorAll("[data-pw-" + k)
                            .forEach( $e=> {
                                $e.setAttribute("href", item[k]);
                            });
                        } 
                        else if ( k=="image") 
                        {
                            $tmpl.querySelectorAll("[data-pw-" + k)
                            .forEach( $e=> {
                                $e.style.backgroundImage = "url('" + item[k] + "')";
                            });
                        }
                        else 
                        {
                            $tmpl.querySelectorAll("[data-pw-" + k)
                                .forEach( $e=> {
                                    $e.innerHTML = item[k];
                                });
                        }
                    }

                    $parent.appendChild($tmpl);
                }
            }

            this.assignCount(data.length, 'items');
        }
}