class UserLogout
{
	constructor(options)
	{
		this.options = options;

		this.#init();
	}
	
	/**
	 *	Initialize.
	 */
		#init() {
			this.#observe();
		}

	
	/**
	 *	Observe some elements.
	 */
		#observe()
		{			
			// Submit form.
			document.getElementById(this.options.form)
				.addEventListener("submit", e=>
				{
					e.preventDefault();
					this.#submitForm(e.currentTarget.action);
				}
			);	
		}
		
		
	/**
	 *	Submit form.
	 *  WIthout control. Should be checked before.
	 */
		async #submitForm(action)
		{
			const f = new PWFetch(action, PWE.options.tokenName, 'json', 'POST');
			const json = await f.getJSON();
			
			return json.done;
		}
		
}