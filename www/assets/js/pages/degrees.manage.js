class PWEDegrees extends PWItems
{
	constructor(options) 
	{
		super(options);

		this.options = options;
		
		this.selectedItem = 0;
		this.$confirm;
		this.$items;
		this.$toolbar;
		this.working = false;
		
		this.#init();		
	}
	
	
	/**
	 *	Initialize.
	 */
		#init()
		{
			this.#initElements();
			this.#observe();
		}
	
		
	/**
	 *	Init elements.
	 */
		#initElements()
		{
			this.$toolbar = document.getElementById(this.options.toolbar);
			this.$items = this.$content.querySelector("[data-pw-items]");
			this.$tmplUser = this.$items.querySelector("[data-pw-item]").cloneNode(true);

			this.#optionsVisibility();
		}
		
		
		#hideConfirm(hidden=true)
		{
	    	this.$confirm.setAttribute("data-pw-confirm", hidden ? 0 : 1);
		}
		
		
	/**
	 *	Change options visiblility.
	 */
		#optionsVisibility() 
		{
			const r = this.selectedItem ? 0 : 1;

			this.$toolbar.querySelectorAll("[data-pw-item-action]")
				.forEach($e=>{
					$e.setAttribute("data-disabled", r);
				});
		}
	
	
	/**
	 *	Observe some elements.
	 */
		#observeItems()
		{
			// Click on a degree, load related users.
			this.$content.querySelectorAll("[data-pw-items] [data-pw-item]")
				.forEach( $e=>
				{
					$e.addEventListener("click", (e)=> 
					{
						const id = $e.getAttribute("data-pw-item");
						const old = this.selectedItem;
						if ( id!=old ) 
						{
							this.selectedItem = id;
							this.#switchSelected();
							this.#hideConfirm(true);
							
							if ( !old ) {
								this.#optionsVisibility();
							}
						}
					})
				});					
		}
	
	/**
	 *	Observe some elements.
	 */
		#observe()
		{
			this.#observeItems();


				this.$toolbar.querySelectorAll("[data-pw-action]")
				.forEach($e=>
				{
					const action = $e.dataset.pwAction;
									
					$e.addEventListener('click', async (e)=>
					{
						e.preventDefault();	
						var disabled = $e.getAttribute("data-disabled");
						
						if ( !this.working && disabled!=1 )
						{
							if ( $e.dataset.pwConfirm!=null && !this.working ) 
							{
								this.askConfirmAndDo({
									action: action, 
									msg: $e.dataset.pwConfirm, 
									callback: 'doItemAction'
								});
							} else {
								this.doItemAction(action);  
							}
						}
					});
				});	
		}

		
		

		
			
		
		
		
		async doItemAction(action)
		{
			if ( !this.working && this.options.url[action]!=null )
			{
				this.working = true;
				
				const url = this.options.url[action];
				const fd = new FormData();
				  	  fd.append("degree", this.selectedItem);
				const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
				const json = await f.getJSON(fd);
				
				if ( json.done ) 
				{
					this.#doAfter(action, this.selectedItem);
				}

				this.working = false;
			}			
		}
		
		
	/**
	 * Do something after action.
	  */
		#doAfter(action, uid)
		{
			var $e;
			
			switch(action)
			{
				case 'delete' : 
					$e = this.$items.querySelector('[data-pw-item="' + uid + '"]');
					if ( $e!=null ) {
						$e.parentNode.removeChild($e);
					}
					break;
				default : break;
			}
		}
		
		
	/**
	 * Apply selected style on selected degree.
	 */
		#switchSelected() 
		{
			this.$items.querySelectorAll("[data-pw-item]")
			  .forEach( $r=>{
				  if ( $r.getAttribute("data-pw-item")==this.selectedItem ) {
					  $r.classList.add("selected-degree");
				  } else {
					  $r.classList.remove("selected-degree");
				  }
			  });
		}
		
	
		
		placeItems(data) 
		{
			const $parent = this.$content.querySelector("[data-pw-items]");
			let $tmpl, item;

			// Clear old selection & related users.
			this.$content.querySelector("[data-pw-degrees]").innerHTML = "";
			this.selectedItem = 0;
			this.#optionsVisibility();

			if ( $parent!=null )
			{
				$parent.innerHTML = "";

				for ( var i in data )
				{
					item = data[i];
					$tmpl = this.$tmplItem.cloneNode(true);
					
					for ( var k in data[i] )
					{
						if ( k=="link")
						{
							$tmpl.querySelectorAll("[data-pw-link]")
								.forEach($e=>{
									$e.setAttribute("href", data[i][k]);
								});
						}
						else if ( k=="id") 
						{
							$tmpl.setAttribute("data-pw-item", data[i][k]);
							$tmpl.querySelectorAll("[data-pw-" + k + "]")
								.forEach($e=>{
									$e.innerHTML = data[i][k];
								});
						}	
						else if ( k=="count_inside") {
							$tmpl.querySelectorAll("[data-pw-count]")
								.forEach($e=>{
									$e.setAttribute("data-pw-count", data[i][k]);
									$e.innerHTML = data[i][k];
								});
						}
						else 
						{
							$tmpl.querySelectorAll("[data-pw-" + k + "]")
								.forEach($e=>{
									$e.innerHTML = data[i][k];
								});
						}
					}

					$parent.appendChild($tmpl);
				}

				this.assignCount(data.length, 'items');

				this.#observeItems();
			}			
		}
}