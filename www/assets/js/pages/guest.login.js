class UserLogin
{
	constructor( options )
	{
		this.options = options;
		this.#init();
	}
	
	
	/**
	 *	Initialize.
	 */
		#init() {
			this.#observe();
		}
	
	
	
	/**
	 *	Observe some elements.
	 */
		#observe()
		{
			const $form = document.getElementById(this.options.form);
			const buttons = $form.querySelectorAll(".submit-form");
			
			$form.addEventListener("submit", e=>
			{
				e.preventDefault();
				
				this.#enableButtons(buttons, false); // Avoid double clic.
				this.#submitForm($form, buttons);
			});
		}
		
		
	/**
	 *	Submit form.
	 */
		async #submitForm($form, buttons)
		{
			const fd = new FormData($form);				 
			const f = new PWFetch($form.action, PWE.options.tokenName);
			const json = await f.getJSON(fd);
				
			// Enable buttons after delay if no redirect.				
			setTimeout( ()=>{
				this.#enableButtons(buttons, true);
			}, this.options.delay);
			
			return json.done;
		}
		
		
	/**
	 *	Enable/Disable form buttons.
	 */
		#enableButtons(buttons, enable=true)
		{
			buttons.forEach( $b=> {
				$b.disabled = !enable;	
			});
		}
}