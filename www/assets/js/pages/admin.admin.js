class PWEAdmin
{
	constructor(options) 
	{
		this.options = options;
		this.working = false;

		this.#init();
	}
	
	
	/**
	 *	Initialize.
	 */
		#init() {
			this.#observe();
		}

	
	/**
	 *	Observe elements.
	 */
		#observe()
		{
			this.#observeGroups();
			this.#observeFormsSiteParams(); 
			this.#observeFormsRules();
			this.#observeMailTo();
		}

	
	/**
	 *	Observe mail to...
	 */
		#observeMailTo()
		{
			document.body.querySelectorAll("[data-pw-mailto]")
				.forEach( $e=>
				{
					$e.addEventListener("click", async e=> 
					{
						e.preventDefault();

						const r = await this.#getMailForm(e.currentTarget.getAttribute("data-pw-mailto"));
						if ( r.done ) {
							this.#showMailInModal(r.html);
						}
					});
				});

		}

	
	/**
	 *	Observe rules forms.
	 */
		#observeGroups()
		{
			document.body.querySelectorAll("[data-rules-form]")
				.forEach($f=>{
					const groups = $f.querySelectorAll('[data-pw-group]');
					const rules = $f.querySelectorAll('[data-pw-group-rules]');
					
					groups.forEach( $e=>{
						$e.addEventListener("click", (e)=>{
							e.preventDefault();
							this.#showRules(groups, $e.getAttribute("data-pw-group"), rules);
						});
					});
				});
		}

		
	/**
	 *	Switch panel rules visibility.
	 */
		#showRules(groups, gid, rules)
		{
			groups.forEach( $g=>
			{
				if ( $g.getAttribute("data-pw-group")==gid ) {
					$g.classList.add("group-active");
				} else {
					$g.classList.remove("group-active");
				}
			});
			
			rules.forEach( $e=>
			{
				if ( $e.getAttribute("data-pw-group-rules")==gid ) {
					$e.classList.add("rules-active");
				} else {
					$e.classList.remove("rules-active");
				}
			});
		}

	
	/**
	 *	Observe site params form.
	 */
		#observeFormsSiteParams()
		{
			const $f = document.getElementById("siteParamsForm");
			if ( $f!=null )
			{
				$f.addEventListener("submit", async (e)=>{
					e.preventDefault();
					const r = await this.#saveSiteParams($f);
				});
			}
		}
	
	/**
	 *	Observe rules forms.
	 */
		#observeFormsRules()
		{
			document.body.querySelectorAll("[data-rules-form]")
				.forEach( $e=>{
					const buttons = $e.querySelectorAll('input[type="submit"]');
						
					$e.addEventListener("submit", async (e)=>{
						e.preventDefault();

						buttons.forEach($s=>{
							$s.disabled = 1
						});

						const r = await this.#saveRules($e);
						if ( r.done==true ) {
							buttons.forEach($s=>{
								$s.disabled = 0
							});
						}
					});
				});
		}

	
	/**
	 *	Save site params form.
	 */
		async #saveSiteParams($form)
		{
			const f = new PWFetch($form.action, PWE.options.tokenName, 'json', 'POST');
			const json = await f.getJSON(this.getSiteParamsValues($form));

			return json;
		}
	
	/**
	 *	Save rules form.
	 */
		async #saveRules($form)
		{
			const f = new PWFetch($form.action, PWE.options.tokenName, 'json', 'POST');
			const json = await f.getJSON(this.getRulesValues($form));

			return json;
		}

	
	/**
	 * Compile site params form values.
	 */
		getSiteParamsValues($form) 
		{
			const values = new FormData();
			
			$form.querySelectorAll('input[type="text"]')
				.forEach
				( 
					$s=>
					{
						const name = $s.getAttribute("name");
						const val = $s.value;
						
						values.append(name, val);
					}
				);
			
			return values;
		}
	
	/**
	 * Compile rules form values.
	 */
		getRulesValues($form) 
		{
			const values = new FormData();
			
			$form.querySelectorAll('select[name^="rules-"]')
				.forEach
				( 
					$s=>
					{
						const name = $s.getAttribute("name");
						const val = $s.value;
						
						values.append(name, val);
					}
				);
			
			return values;
		}

	
	/**
	 *	Save site params form.
	 */
		async #getMailForm(mailTo)
		{
			const fd = new FormData();
				fd.append('mailto', mailTo);
			const f = new PWFetch(this.options.url.mailform, PWE.options.tokenName, 'json', 'POST');
			const json = await f.getJSON(fd);

			return json;
		}
		

	/**
	 * Show mail form in a modal.
	 * 
	 * @param {*} $html 
	 */
		#showMailInModal($html) 
		{
			const $h = document.createElement("div");
				  $h.classList.add("mail-to-form");
				  $h.innerHTML = $html;

			this.#observeMailFormSubmit($h);

			PWE.showModal(null, $h);
		}


	/**
	 * Observe form to submit email to send.
	 * 
	 * @param {*} $parent 
	 */
        #observeMailFormSubmit($parent) 
        {
            $parent.querySelector('form').addEventListener("submit", async (e)=>
            {
                e.preventDefault();

				// Disable form fields/buttons.
				e.currentTarget.querySelectorAll("[pw-form-input],button")
					.forEach($e=>{
						$e.disabled = true;
					});

                const r = await this.#sendMail(
                    e.currentTarget.action,
                    $parent.querySelector('[pw-form-input][name="target"]').value,
                    $parent.querySelector('[pw-form-input][name="subject"]').value,
                    $parent.querySelector('[pw-form-input][name="content"]').value
                );
                
                if( r.done ) {
                    PWE.closeModal();
                }
            });                 
        }


	/**
	 * Send mail form.
	 * 
	 * @param {*} url 
	 * @param {*} target 
	 * @param {*} subject 
	 * @param {*} body 
	 * @returns 
	 */
        async #sendMail(url, target, subject, body) 
        {
            if ( !this.working )
            {
                this.working = true;

                const fd = new FormData();
                      fd.append("mailto", target);
                      fd.append("subject", subject);
                      fd.append("content", body);
                const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
                const r = await f.getJSON(fd);

                this.working = false;

                return r;
            }            
        }
}