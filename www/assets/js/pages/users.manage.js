class PWUsers extends PWItems
{
    constructor(options)
    {
        super(options);

        this.$toolbar;

        this.#init();
    }


    /**
     * Init class.
     */
        #init() 
        {
            this.$toolbar = document.getElementById(this.options.toolbar);

            this.#optionsVisibility();
            this.#observe();
        }


    /**
     * Observe elements.
     */
        #observe()
        {
            this.#observeEditUser(document.body);
            this.#observeItemClicked();
            this.#observeCreate();
            this.#observeMore(document.body);
            this.#observeAccess(document.body);
            this.#observeToolbar();
        }


    /**
     * Observe change access.
     */
        #observeAccess($parent)
        {
            $parent.querySelectorAll("[data-pw-access]")
                .forEach($e=>{
                    $e.querySelectorAll("select")
                        .forEach( $s=>{
                            $s.addEventListener("change", async e=>{
                                e.preventDefault();
                                const done = this.#changeAccess($e.getAttribute("data-pw-access"), $s.value);
                            });
                        });
                        
                });
        }


    /**
     * Change the selected user access type.
     * @param {*} userid 
     * @param {*} newAccess 
     */
        async #changeAccess(userid, newAccess)
        {
            const url = this.options.url.access;
            const fd = new FormData();
                  fd.append("user", userid);
                  fd.append("access", newAccess);
            const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
            const r = await f.getJSON(fd);

            return r;
        }


    /**
     * Observe Create an user button.
     */
        #observeCreate()
        {
            document.querySelectorAll("[data-pw-create-action]")
                .forEach($e=>{
                    $e.addEventListener("click", async e=>
                    {
                        e.preventDefault();
                        const r = await this.#getEditForm(0, this.options.url.create);

                        if ( r.done && r.html!=null ) {
                            this.#placeEditUserForm(r.html); 
                        }
                    });
                });
        }


    /**
     * Observe the toolbar
     */
        #observeToolbar()
        {
			// Click on item action
			this.$toolbar.querySelectorAll("[data-pw-action]")
				.forEach($e=>
				{
					const action = $e.dataset.pwAction;
                    var r; 

					$e.addEventListener('click', async (e)=>
					{
						e.preventDefault();	

                        var disabled = $e.getAttribute("data-disabled");
						
						if ( !this.working && disabled!=1 )
						{
                            if ( $e.dataset.pwConfirm!=null  ) 
                            {
                                this.askConfirmAndDo({
                                    action: action, 
                                    msg: $e.dataset.pwConfirm, 
                                    callback: 'doItemAction'
                                });
                            } 
                            else 
                            {
                                if ( action=="mailform" ) 
                                {
                                    r = await this.doItemAction(action, false);
                                    if ( r.done && r.html!=null ) {
                                        this.#showMailInModal(r.html);
                                    }
                                } else {
                                    this.doItemAction(action); 
                                }
                            }
						}
					});
				});	
        }
        

        #showMailInModal($html) 
        {
            const $h = document.createElement("div");
                  $h.classList.add("mail-to-form");
                  $h.innerHTML = $html;

            this.#observeMailFormSubmit($h);

            PWE.showModal(null, $h);
        }

    /**
     * Observe when item has been clicked.
     */
        #observeItemClicked($parent)
        {
            if ( $parent!=null )
            {
                $parent.querySelector("[data-pw-select")
                    .addEventListener("click", e=>
                    {
                        const item = $parent.getAttribute("data-pw-item");
                        const sel = $parent.getAttribute("data-pw-item-selected")==1 ? 0 : 1;
                        
                        $parent.setAttribute("data-pw-item-selected", sel);
                    
                        if ( sel==1 ) {
                            this.addIn(item, this.selectedItems);
                        } else {
                            this.removeFrom(item, this.selectedItems);
                        }

                        this.#optionsVisibility();
                    });
            }
            else 
            {
                document.querySelectorAll("[data-pw-item]")
                    .forEach( $e=>{
                        this.#observeItemClicked($e);
                    });
            }
        }


        #observeMailFormSubmit($parent) 
        {
            $parent.querySelector('form').addEventListener("submit", async (e)=>
            {
                e.preventDefault();

                const r = await this.#sendMail(
                    e.currentTarget.action,
                    $parent.querySelector('[pw-form-input][name="subject"]').value,
                    $parent.querySelector('[pw-form-input][name="content"]').value
                );
                
                if( r.done ) {
                    PWE.closeModal();
                }
            });                 
               
        }


        async #sendMail(url, subject, body) 
        {
            if ( !this.working )
            {
                this.working = true;

                const fd = new FormData();
                      fd.append("users", JSON.stringify(this.selectedItems));
                      fd.append("subject", subject);
                      fd.append("content", body);
                const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
                const r = await f.getJSON(fd);

                this.working = false;

                return r;
            }            
        }

		
	/**
	 * Do an action related to the selected item.
	  */
		async doItemAction(action, reload=true)
		{									
			if ( !this.working && this.options.url[action]!=null )
			{
				this.working = true;
				
				const url = this.options.url[action];
				const fd = new FormData();
				  	  fd.append("users", JSON.stringify(this.selectedItems));
				const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
				const r = await f.getJSON(fd);
				
				if ( r.done) 
				{
					this.working = false;
                    if (reload) {
                        this.reloadPage();
                    }
				}

                return r;
			}			
		}


	/**
	 *	Change options visiblility.
	 */
		#optionsVisibility() 
		{
            const dis = this.selectedItems.length ? 0 : 1;

            document.body.querySelectorAll('[data-pw-item-action]')
                .forEach($e=>{
                    $e.setAttribute("data-disabled", dis);
                });

			if (dis) {
				this.hideConfirm(true);
			}
		}
	


    /**
     * Observe link to edit an user.
     */
        #observeEditUser($parent)
        {
            $parent.querySelectorAll("[data-pw-edit]")
                .forEach( $e=>{
                    $e.addEventListener("click", async e=>
                    {
                        e.preventDefault();
                        const r = await this.#getEditForm($e.dataset.pwEdit, this.options.url.edit);

                        if ( r.done && r.html!=null ) {
                            this.#placeEditUserForm(r.html, r.data);
                        }
                    });
                });
        }

    /**
     * Observe click on "more" link.
     */
        #observeMore($parent)
        {
            $parent.querySelectorAll("[data-pw-more]")
                .forEach( $e=>{
                    $e.addEventListener("click", async e=>
                    {
                        e.preventDefault();
                        const r = await this.#getMore($e.dataset.pwMore);

                        if ( r.done && r.html!=null ) {
                            this.#placeMoreData(r.html);
                        }
                    });
                });
        }


    /**
     * More information about user.
     * 
     * @param {*} html 
     */
        #placeMoreData(html)
        {
            const $h = document.createElement("div");
                  $h.innerHTML = html;

            PWE.showModal(null, $h);
        }


    /**
     * @override
     */
        placeItems(data)
        {
            const $parent = document.body.querySelector("[data-pw-items]");
                  $parent.innerHTML = "";
            var $tmpl, item;

            // Reset old items selection
            this.selectedItems = [];
            this.#optionsVisibility();

            for ( let i in data )
            {
                item = data[i];
                $tmpl = this.$tmplItem.cloneNode(true);
                 $tmpl.setAttribute("data-pw-item", item.id);
                 $tmpl.setAttribute("data-pw-item-published", item.published);
                 $tmpl.querySelector("[data-pw-edit]").setAttribute("data-pw-edit", item.id);
                 $tmpl.querySelector("[data-pw-more]").setAttribute("data-pw-more", item.id);
                 $tmpl.querySelector("[data-pw-user-edit]").setAttribute("data-pw-user-edit", item.id);
                 $tmpl.querySelector("[data-pw-access]").setAttribute("data-pw-access", item.id);
                 $tmpl.querySelector("[data-pw-published-icon]").innerHTML = item.published_icon;

                // Set listeners.
                this.#observeItemClicked($tmpl);
                this.#observeEditUser($tmpl);
                this.#observeMore($tmpl);
                this.#observeAccess($tmpl);

                for ( let k in item )
                {
                    $tmpl.querySelectorAll("[data-pw-" + k + "]")
                        .forEach($e=>{
                            $e.innerHTML = item[k];
                        });
                }

                $parent.appendChild($tmpl);
            }
        }


    /**
     * Show form to edit user in the modal.
     * @param {*} html 
     * @param {*} data 
     */
        async #placeEditUserForm(html)
        {
            const $h = document.createElement("div");
                  $h.innerHTML = html;
            const $f = $h.querySelector("form");
            const $b = document.createElement("button");
                 $b.innerHTML = '<i class="fa-solid fa-check"></i>'
                 $b.classList.add("btn", "btn-lg", "btn-success");
                 $b.addEventListener("click", async e=>
                 {
                        e.preventDefault();
                        const r = await this.#saveUserEditForm($f);
                        if ( r.done ) 
                        {
                            this.reloadPage();
                            PWE.closeModal();
                        }
                 });
              

            PWE.showModal(null, $h, [$b]);
        }


    
    /**
     * Get form values.
     * @param {*} $form 
     * @returns 
     */
        #getFormValues($form)
        {
            const r = {};
            $form.querySelectorAll("[pw-form-input]")
                .forEach($input=>{
                    r[$input.name] = $input.value;
                });

            return r;
        }


    /**
     * Save edited user.
     * 
     * @param {*} $form 
     * @returns 
     */
        async #saveUserEditForm($form) 
        {
            const values = this.#getFormValues($form);
            const url = $form.action;
            const fd = new FormData();

            // Apply form values.
            for ( let k in values ) {
                fd.append(k, values[k]);
            }

            const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
            const r = await f.getJSON(fd);

            return r;
        }


    /**
     * Get form to edit user from server.
     * @param {*} userID 
     * @returns 
     */
        async #getEditForm(userID, url) 
        {
            const fd = new FormData();
                fd.append("user", userID);
            const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
            const r = await f.getJSON(fd);
                    
            return r;
        }


    /**
     * Get form to edit user from server.
     * @param {*} userID 
     * @returns 
     */
        async #getMore(userID) 
        {
            const fd = new FormData();
                  fd.append("user", userID);
            const f = new PWFetch(this.options.url.more, PWE.options.tokenName, 'json', 'POST');
            const r = await f.getJSON(fd);
                    
            return r;
        }

}