class UserSignup
{
	constructor(options)
	{
		this.options = options;
		this.$form;
		this.$username;
		this.$email;
		this.$firstname;
		this.$lastname;
		this.$lang;
		this.$pwd;			
		this.$pwd2;		

		this.#init();
	}
	
	/**
	 *	Initialize.
	 */
		#init()
		{
			this.#initElements();
			this.#observe();
		}
	
	
	/**
	 *	Initialize.
	 */
		#initElements()
		{
			this.$form = document.getElementById(this.options.form);
			this.$username = this.$form.querySelector("#" + this.options.username);
			this.$email = this.$form.querySelector("#" + this.options.email);
			this.$firstname = this.$form.querySelector("#" + this.options.firstname);
			this.$lastname = this.$form.querySelector("#" + this.options.lastname);
			this.$lang = this.$form.querySelector("#" + this.options.lang);
			this.$pwd = this.$form.querySelector("#" + this.options.pwd);			
			this.$pwd2 = this.$form.querySelector("#" + this.options.pwd2);			
		}
	
	
	/**
	 *	Observe some elements.
	 */
		#observe()
		{
			const buttons = this.$form.querySelectorAll(".submit-form");
			
			// Check form on value change
			for ( let i in this)
			{
				if ( i!="$form" && i!="options" )
				{
					this[i].addEventListener("input", (e)=> {
						this.#enableButtons(buttons, this.#isValidForm(false));
					});
				}
			}
			
			// Submit form buttons.
			buttons.forEach( $e=>
			{
				$e.addEventListener("click", e=>
				{
					e.preventDefault();

					this.#enableButtons(buttons, false);
					this.#submitForm(buttons);
				});
			});
			
			// Submit form buttons.
			this.$form.addEventListener("submit", e=>
			{
				e.preventDefault();
				
				this.#enableButtons(buttons, false);				
				if ( this.#isValidForm(true) ) {
					this.#submitForm();
				}
			});
		
		}
		
		
	/**
	 *	Submit form.
	 *  WIthout control. Should be checked before.
	 */
		async #submitForm(buttons)
		{
			const fd = new FormData();
				  fd.append("username", this.$username.value);
				  fd.append("firstname", this.$firstname.value);
				  fd.append("lastname", this.$lastname.value);
				  fd.append("email", this.$email.value);
				  fd.append("lang", this.$lang.value);
				  fd.append("pwd", this.$pwd.value);
				  fd.append("pwd2", this.$pwd2.value);
			const f = new PWFetch(this.$form.action, PWE.options.tokenName, 'json', 'POST');
			const json = await f.getJSON(fd);
				
			
			// Enable buttons after delay if no redirect.				
			setTimeout( ()=>{
				this.#enableButtons(buttons, true);
			}, this.options.delay);
			
			return json.done;
		}
		
		
	/**
	 *	Enable/Disable form buttons.
	 */
		#enableButtons(buttons, enable=true)
		{
			buttons.forEach( $b=> {
				$b.disabled = !enable;	
			});
		}
				
		
	/**
	 * Basic control of form values.
	 * @return bool
	  */
		#isValidForm()
		{
			var valid = true;
			
			for ( let i in this)
			{
				if ( this[i].required )
				{
					if ( !this[i].value.length ) 
					{
						valid = false;
						this[i].classList.add("invalid");
					} 
					else {
						this[i].classList.remove("invalid");
					}
				}
			}
			
			if ( this.$pwd.value!=this.$pwd2.value) {
				valid = false;
			}
			
			return valid;
		}
}