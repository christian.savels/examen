class UserAccount
{
	constructor(options)
	{
		this.options = options;
		this.$form;
		this.$secure;
		this.formEnabled = false;

		this.#init();
	}
	
	
	/**
	 *	Initialize.
	 */
		#init()
		{
			this.#initElements();
			this.#observe();
		}
	
	
	/**
	 *	Initialize.
	 */
		#initElements()
		{
			this.$form = document.getElementById(this.options.form);
			this.$secure = document.getElementById("pwd_secure");

			this.#disabledForm(true);
		}
		
		
	/**
	 *	Disable form fields.
	 */
		#disabledForm(disabled) 
		{
			this.$form.querySelectorAll('[name]')
				.forEach( $e=>
				{
					if ( $e.getAttribute("name")!="pwd_secure" ) { 
						$e.disabled = disabled;
					}
				});
		}
	
	
	/**
	 *	Observe some elements.
	 */
		#observe()
		{
			const $form = document.getElementById(this.options.form);
			
			// Submit form
			this.$form.addEventListener("submit", async (e)=> 
			{
				e.preventDefault();

				if ( this.$secure.value.length && !this.formEnabled ) {
					await this.#sendForm();
				}
			});

			// Lang changed.
			$form.querySelectorAll("[data-pw-lang]")
				.forEach( $e=>
				{
					$e.addEventListener("change", (e)=>
					{
						PWExam.setCookie("lang", $e.value);
						$form.submit();
					});
				});
			
			// Secure value change.
			this.$secure.addEventListener("input", (e)=> 
			{
				if ( this.$secure.value.length==0 && this.formEnabled )
				{
					this.$form.querySelector(".fieldset-secure").classList.remove("field-secured");
					this.#disabledForm(true);
				} 
				else if ( this.$secure.value.length && !this.formEnabled ) 
				{
					this.$form.querySelector(".fieldset-secure").classList.add("field-secured");
					this.#disabledForm(false);
				}
			});

			this.#observeExport();
		}

		
    /**
     * Observe export links.
     */
		#observeExport()
		{
			document.body.querySelectorAll("[data-pw-export-list]")
				.forEach( $e=>{
					$e.addEventListener("click", e=>{
						e.preventDefault();
						this.exportList($e.getAttribute("href"), $e.getAttribute("data-pw-export-list"));
					});
				});
		}


	/**
	 * Export filtered (or not) items list.
	 * 
	 * @param {*} url 
	 * @param {*} format 
	 */
		async exportList(url, format) 
		{
			const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
			await f.download(new FormData(), 'export.' + format);
		}


	/**
	 * Send form to server.
	 * 
	 * @param {*} url 
	 */
        async #sendForm() 
        {
            const values = this.#getFormValues();
			const fd = new FormData();
                for ( let k in values ) {
                    fd.append(k, values[k]);
                }
			  	  
			const f = new PWFetch(this.$form.action, PWE.options.tokenName, 'json', 'POST');
			const r = await f.getJSON(fd);

			return r;
        }


	/**
	 * Get form values.
	 * 
	 * @returns 
	 */
        #getFormValues()
        {
            const r = {};
            this.$form.querySelectorAll("[pw-form-input]")
                    .forEach($input=>
					{
						if ( $input.type=="file" ) {
							r[$input.name] = $input.files[0];
						} 
						else 
						{
							if ( $input.type!="checkbox"
								  || ($input.type=="checkbox" && $input.checked==true)
							) {
								r[$input.name] = $input.value;
							} 
						}
                    });

            return r;
        }
		
}