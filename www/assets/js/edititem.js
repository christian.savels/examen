class PWEditItem
{
    constructor(options)
    {
        this.options = options;
        
        this.#init();
    }

    
    /**
     * Init...
     */
        #init() {
            this.#observe();
        }


    /**
     * Observe form.
     */
        #observe()
        {
            const $f = document.getElementById(this.options.form);
            const buttons = $f.querySelectorAll('input[type="submit"]');
            if ( $f!=null)
            {
                $f.addEventListener("submit", (e)=>{
                    e.preventDefault();
                    this.#enableButtons(buttons, false);
                    this.#sendForm(e.currentTarget, $f.action, buttons);
                });
            }
        }


        async #sendForm(form, url, buttons) 
        {
            const values = this.#getFormValues();
			const fd = new FormData(form);
            /*
			const fd = new FormData();
                for ( let k in values ) {
                    fd.append(k, values[k]);
                }*/
			  	  
			const f = new PWFetch(url, PWE.options.tokenName, 'json', 'POST');
			const r = await f.getJSON(fd);

            // Enable buttons after delay if no redirect.				
			setTimeout( ()=>{
				this.#enableButtons(buttons, true);
			}, this.options.delay);
        }


        #getFormValues()
        {
            const r = {};
            document.getElementById(this.options.form)
                .querySelectorAll("[pw-form-input]")
                    .forEach($input=>{
                        r[$input.name] = $input.value;
                    });


            return r;
        }

        
	/**
	 *	Enable/Disable form buttons.
	 */
		#enableButtons(buttons, enable=true)
		{
			buttons.forEach( $b=> {
				$b.disabled = !enable;	
			});
		}

}