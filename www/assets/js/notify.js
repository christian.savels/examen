class PWNotify
{
	constructor(options) 
	{		
		this.$toastsNode = null;
		this.$tmplToast = null;
		this.options = options;
	}
	
	
	/**
	 *	Display messages.
	 *	Possible keys : errors, infos, warnings, success.
	 */
	render(data) 
	{		
		if ( data!=null )
		{
			if ( data.errors!=null ) {
				this.#renderErrors(data.errors);
			}
			if ( data.infos!=null ) {
        		this.#renderInfos(data.infos);
        	}
        	if ( data.warnings!=null ) {
        		this.#renderWarnings(data.warnings);
        	}
        	if ( data.success!=null ) {
        		this.#renderSuccess(data.success);
			}
		}
	}
	
	
	/**
	 *	Append toast to main container.
	 */
		#appendToast($toast) 
		{
			const c = document.querySelectorAll("[data-pw-toasts] [data-pw-toast]").length+1;			
			const $p = document.querySelector("[data-pw-toasts]");
				  $p.setAttribute("data-pw-toasts", c);
				  $p.appendChild($toast);
		}
	
	
	
	renderToasts(type, messages) 
	{
		var title;
		
		for ( let t in messages )
		{
			try 
			{
				title = Number.parseInt(t); // Numeric index = no title.
				title = "";
			} catch(e) {
				title = t; // String index = with title.
			}
			
			this.#renderToast(type, title, messages[t]);
		}			
		
	}
	
	
	#getTmplToast() 
	{		
		if ( this.$tmplToast==null )
		{
			const $toastsNode = document.body.querySelector("[data-pw-toasts]");
			if ( $toastsNode!=null )
			{
				const $toast = $toastsNode.querySelector("[data-pw-toast]");
				this.$tmplToast = $toast.cloneNode(true);
				document.body.appendChild($toastsNode);	
				
				this.#removeToast($toast);			
			}
		}
		
		return this.$tmplToast.cloneNode(true);
	}
	
	
	#renderToast(type, title, message)
	{
		const ts = title==null || title.length==0 ? 0 : 1;
		const ms = message==null || message.length==0 ? 0 : 1;
		const $toast = this.#getTmplToast(); 		
		
		
		if ( $toast!=null )
		{
			$toast.setAttribute("data-pw-toast", type);
		  	$toast.querySelectorAll('[data-pw-title]').forEach( $e=>
		  	{
				$e.setAttribute("data-pw-title", ts);
				$e.innerHTML = title==null ? "" : title ;
			});
	  		$toast.querySelectorAll('[data-pw-message]').forEach( $e=>
		  	{
				$e.setAttribute("data-pw-message", ms);
				$e.innerHTML = message;
			});
	  		$toast.querySelectorAll('[data-pw-toast-close]').forEach( $e=>
		  	{
				$e.addEventListener("click", (e)=>{
					e.preventDefault();
					this.#removeToast($toast);
				});
			});
			
			this.#appendToast($toast); 
			
			// Remove toast after the delay.
			setTimeout( ()=> {				
				this.#removeToast($toast);
			}, this.options.delay);			
		}			
	}
	
	
	/**
	 * Remove toast from parent.
	 */
		#removeToast($toast) 
		{
			const c = document.querySelectorAll("[data-pw-toasts] [data-pw-toast]").length-1;			
			const $p = $toast.parentNode;

			if ( $p!=null ) {
				$p.removeChild($toast);
			}

			// Set new count.
			document.querySelector("[data-pw-toasts]").setAttribute("data-pw-toasts", c);
		}
		
	
	#renderInfos(messages) {
		this.renderToasts('info', messages);
	}
		

	#renderErrors(messages) {
		this.renderToasts('alert', messages);
	}
	
	
	#renderWarnings(messages) {
		this.renderToasts('warning', messages);	
	}
	
	
	#renderSuccess(messages){
		this.renderToasts('success', messages);
	}
}