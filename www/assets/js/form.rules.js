class PWFormRules
{
	constructor(options) 
	{
		this.options = options;
		this.$form = null;
		
		this.#init();
	}
	
	

		
	/**
	 *	Init rules.
	 */
		#init()
		{
			this.$form = document.getElementById(this.options.formID);
			this.#observe();			
		}
		
		
	/**
	 *	Observe elements. 
	 */
		#observe()
		{
			const groups = this.$form.querySelectorAll('[data-pw-group]');
			const rules = this.$form.querySelectorAll('[data-pw-group-rules]');
			
			groups.forEach( $e=>{
				$e.addEventListener("click", (e)=>{
					e.preventDefault();
					this.#showRules(groups, $e.getAttribute("data-pw-group"), rules);
				});
			});
			
			this.$form.addEventListener("submit", (e)=>{
				e.preventDefault();
				this.#saveRules();
			});
		}
				
		
	/**
	 *	Switch panel rules visibility.
	 */
		#showRules(groups, gid, rules)
		{
			groups.forEach( $g=>
			{
				if ( $g.getAttribute("data-pw-group")==gid ) {
					$g.classList.add("group-active");
				} else {
					$g.classList.remove("group-active");
				}
			});
			
			rules.forEach( $e=>
			{
				if ( $e.getAttribute("data-pw-group-rules")==gid ) {
					$e.classList.add("rules-active");
				} else {
					$e.classList.remove("rules-active");
				}
			});
		}
		
		
	/**
	 *	Save rules form.
	 */
		async #saveRules()
		{
			const f = new PWFetch(this.$form.action, PWE.options.tokenName, 'json', 'POST');
			const json = await f.getJSON(this.getRulesValues());
		}
	
	
	/**
	 * Compile rules form values.
	 */
		getRulesValues() 
		{
			const values = new FormData();
			
			this.$form.querySelectorAll('select[name^="rules-"]')
				.forEach
				( 
					$s=>
					{
						const name = $s.getAttribute("name");
						const val = $s.value;
						
						values.append(name, val);
					}
				);
			
			return values;
		}
}