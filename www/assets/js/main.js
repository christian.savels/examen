class PWExam
{
	constructor(options)
	{
		this.options = options;
		this.notify = null;
		this.$modal = null;	
		
		this.#init();
	}
	
	
	/**
	 *	Init class.
	 */
		#init() 
		{
			if ( this.options.notify!=null && this.options.notify.messages!=null ) {
				this.renderMessages(this.options.notify.messages);
			}

			this.#initLang();
		}
		
	
	/**
	 *	Observe language chooser if active.
	 */
		#initLang() 
		{
			if ( this.options.language.multi ) 
			{
				document.body.querySelectorAll(".pw-menu-ul-lang [data-pw-lang]")
					.forEach( $e=>{
						console.log("TEST TEST TEST", this.options.language);

						$e.addEventListener("click", e=> 
						{ 
							console.log("lang clicked : " + $e.dataset.pwLang + " & current = " + this.options.language.current);
							
							if ( $e.dataset.pwLang!=this.options.language.current ) 
							{
								console.log("==> change lang");
								this.setCookie("changeLang", $e.dataset.pwLang, this.options.cookies.validity);
								window.location.reload();
							}
						});
					});
			}
		}


	/**
	 * Get template content.
	 * 
	 * @return node 
 	 */
		getTemplate(identTmpl)
		{
			const $tmpl = document.getElementById(identTmpl);
			return $tmpl.content.cloneNode(true);
		}	
		
		
	/**
	 * Close the modal.
 	 */
		closeModal() {
			this.$modal.classList.remove("pw-modal-active");
			this.$modal.querySelector(".pw-modal-content").innerHTML = "";
		}		
		
		
	/**
	 *	Show modal with content.
	 */
		showModal(title, $content, buttons) 
		{
			if ( this.$modal==null ) 
			{
				this.$modal = document.getElementById(this.options.elements.modalID);
				if ( this.$modal!=null ) 
				{
					this.$modal.querySelectorAll(".pw-modal-close")
						.forEach( $e=>{
							$e.addEventListener("click", (e)=>{
								e.preventDefault();
								this.$modal.classList.remove("pw-modal-active");
							});
						});
				}
			}
			
			if ( this.$modal!=null ) 
			{
				const $modContent = this.$modal.querySelector(".pw-modal-content");
				
				this.$modal.classList.add("pw-modal-active");
				
				if ( title!=null ) {
					this.$modal.querySelector(".pw-modal-title").innerHTML = title;
				}
				
				if ( $content!=null )
				{
					$modContent.innerHTML = "";
					$modContent.appendChild($content);
				}
				
				// Buttons
				var bcount = 0;
				const $bNode = this.$modal.querySelector("[data-pw-buttons-count]");
					  $bNode.innerHTML = "";
					  
				if ( buttons!=null )
				{
								
					for ( let i in buttons ) {
						$bNode.appendChild(buttons[i]);
					}
				} 
				
				// Set count attribute to hide/show section.
				this.$modal.querySelector("[data-pw-buttons-count]")
						.setAttribute("data-pw-buttons-count", bcount);
			}
		}
		
	
	/**
	 * 	Render messages
	 *  using notify system.
	 */	
		renderMessages(data)
		{
			if ( this.notify==null ) {
				this.notify = new PWNotify(this.options.notify);
			}
			
			this.notify.render(data);
		}
		
		
	/**
	 * Get the modal id.
	 * 
	 * @returns string
	 */
		getModalID() {
			return this.options.elements.modalID;
		}


	/**
	 * @source https://www.w3schools.com/js/js_cookies.asp
	 * 
	 * @param {*} cname 
	 * @param {*} cvalue 
	 * @param {*} exdays 
	 */
		setCookie(cname, cvalue, exdays=0) 
		{
			const d = new Date();

			if ( exdays==0 ) {
				exdays = this.options.cookies.validity;
			}

			d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
			let expires = "expires="+d.toUTCString();
			document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		}

		  
	/**
	 * @source https://www.w3schools.com/js/js_cookies.asp
	 * 
	 * @param {*} cname 
	 * 
	 * @returns 
	 */
		getCookie(cname) 
		{
			let name = cname + "=";
			let ca = document.cookie.split(';');
			for(let i = 0; i < ca.length; i++) {
			  let c = ca[i];
			  while (c.charAt(0) == ' ') {
				c = c.substring(1);
			  }
			  if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			  }
			}
			
			return "";
		}
}